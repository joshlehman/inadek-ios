//
//  AppDelegate.h
//  Inadek
//
//  Created by Josh Lehman on 11/23/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCTransitionViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

/* BrightUI Transition View Controller, for managing views */
@property (nonatomic, retain) BCTransitionViewController *transitionVC;

@end

