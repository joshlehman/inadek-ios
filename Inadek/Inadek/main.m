//
//  main.m
//  Inadek
//
//  Created by Josh Lehman on 11/23/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <PixateFreestyle/PixateFreestyle.h>

int main(int argc, char * argv[]) {
    @autoreleasepool {
        [PixateFreestyle initializePixateFreestyle];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
