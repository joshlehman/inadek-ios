//
//  DeckSelectViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "CategoryTableViewCell.h"
#import "CreateCardViewController.h"

#import "Card.h"

@interface DeckSelectViewController : BCViewController <UITableViewDataSource, UITableViewDelegate> {
    UITableView *decksTV;
    NSArray *decks;
    NSMutableArray *selectedDecks;
}


@property (nonatomic, retain) Card *card;
@property (nonatomic, retain) BCViewController *priorViewController;

- (void)setCurrentCard:(Card *)thisCard;

@end
