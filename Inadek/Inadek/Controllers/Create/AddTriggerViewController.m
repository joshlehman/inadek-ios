//
//  AddTriggerViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "AddTriggerViewController.h"
#import "DeckTrigger+Core.h"
#import "CardTrigger+Core.h"

#import "UIAlertView+BCAlertView.h"

@interface AddTriggerViewController () {
    BOOL isEditing;
    id thisTrigger;
}
@end

@implementation AddTriggerViewController


- (id)initWithTrigger:(id)aTrigger{
    
    self = [self init];
    if (self) {
        
        if ([aTrigger isKindOfClass:[DeckTrigger class]]) {
            DeckTrigger *this = aTrigger;
            trigger = @{@"name":this.name,@"type":[NSString stringWithFormat:@"trigger_%@", this.trigger_type],@"value":this.value, @"placeholder":[Common triggerPlaceholderText:this.trigger_type],@"verb":this.action_verb};
        } else if ([aTrigger isKindOfClass:[CardTrigger class]]) {
            CardTrigger *this = aTrigger;
            trigger = @{@"name":this.name,@"type":[NSString stringWithFormat:@"trigger_%@", this.trigger_type],@"value":this.value, @"placeholder":[Common triggerPlaceholderText:this.trigger_type],@"verb":this.action_verb};
        }
        
        
        thisTrigger = aTrigger;
        isEditing = YES;

    }
    
    return self;
    
}

- (id)initWithTriggerType:(NSString *)triggerType
{
    self = [self init];
    if (self) {
        
        NSArray *triggers = [[NSUserDefaults standardUserDefaults] objectForKey:@"triggers"];
        for (NSDictionary *t in triggers) {
            NSString *trig = [triggerType stringByReplacingOccurrencesOfString:@"trigger_" withString:@""];
            if ([[t objectForKey:@"trigger_type"] isEqualToString:trig]) {
                trigger = @{@"name":[t objectForKey:@"name"],@"type":triggerType,@"placeholder":[Common triggerPlaceholderText:triggerType],@"verb":[t objectForKey:@"verb"]};
                break;
            }
        }
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    if (trigger && [trigger objectForKey:@"type"]) {
        self.navigationItem.rightBarButtonItem = [(BCNavController *)self.navigationController getNavBarItem:kBCNavButtonStyleText withTitle:@"SAVE" andPosition:kBCNavPositionRight target:self action:@selector(addAction:)];
        
        [self.view setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
        
        UILabel *triggerType = UILabel.new;
        [triggerType addStyleClass:@"lbl-trigger-type"];
        [self.view addSubview:triggerType];
        [triggerType makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@13);
            make.left.equalTo(self.view).with.offset(20);
            make.right.equalTo(self.view).with.offset(-20);
            make.height.equalTo(@30);
        }];
        
        [triggerType setText:[trigger objectForKey:@"name"]];
        
        UILabel *triggerDesc = UILabel.new;
        [triggerDesc addStyleClass:@"lbl-trigger-description"];
        [self.view addSubview:triggerDesc];
        [triggerDesc makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(triggerType.bottom).with.offset(8);
            make.left.equalTo(self.view).with.offset(20);
            make.right.equalTo(self.view).with.offset(-20);
            make.height.equalTo(@34);
        }];
        
        [triggerDesc setNumberOfLines:0];
        [triggerDesc setText:[Common triggerSummaryText:[trigger objectForKey:@"type"]]];
        
        NSString *value = @"";
        if ([trigger objectForKey:@"value"]) {
            value = [trigger objectForKey:@"value"];
        }
        
        NSArray *fieldsArray = [[NSArray alloc] initWithObjects:
                                    @{@"style":[NSNumber numberWithInt:kFieldStyleText],
                                      @"placeholder":[trigger objectForKey:@"placeholder"],
                                      @"name":[trigger objectForKey:@"type"],
                                      @"value":value,
                                      @"keyboard_type":@([Common triggerKeyboardType:[trigger objectForKey:@"type"]]),
                                      @"return_key":@"next"},
                                
                                nil];
        
        triggerFS = [[BCFieldSetView alloc] initWithTitle:nil dataFields:fieldsArray andStyle:@"white"];
        
        [self.view addSubview:triggerFS];
        [triggerFS makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(triggerDesc.bottom).with.offset(10);
            make.width.equalTo(self.view);
            make.height.equalTo([triggerFS fieldHeight]);
        }];
        
        [triggerFS makeIndexFirstResponder:0];
        
        fieldsArray = [[NSArray alloc] initWithObjects:
                       @{@"style":[NSNumber numberWithInt:kFieldStyleTextCapped],
                         @"placeholder":[trigger objectForKey:@"verb"],
                         @"name":@"verb",
                         @"keyboard_type":@(UIKeyboardTypeDefault),
                         @"return_key":@"done"},
                       
                       nil];
        
        triggerVerb = [[BCFieldSetView alloc] initWithTitle:@"ACTION BUTTON TEXT" dataFields:fieldsArray andStyle:@"white"];
        
        [self.view addSubview:triggerVerb];
        [triggerVerb makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(triggerFS.bottom).with.offset(10);
            make.width.equalTo(self.view);
            make.height.equalTo([triggerVerb fieldHeight]);
        }];
        
        if (isEditing) {
            BCButton *removeTrigger = BCButton.new;
            [removeTrigger setStyleClass:@"lbl-new-button-sm"];
            [removeTrigger setTitle:@"Remove Action" forState:UIControlStateNormal];
            [removeTrigger setBackgroundColor:[UIColor colorWithWhite:0.8 alpha:0.2]];
            [self.view addSubview:removeTrigger];
            [removeTrigger makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 20, 0, 20));
                make.height.equalTo(@40);
                make.top.equalTo(triggerVerb.bottom).with.offset(10);
                make.centerX.equalTo(self.view);
            }];
            
            [removeTrigger addTarget:self action:@selector(removeAction:) forControlEvents:UIControlEventTouchUpInside];
            
        }
    }
    
}

- (void)setCurrentDeck:(Deck *)thisDeck
{
    if (!thisDeck) {
        return;
    }
    
    _deck = thisDeck;
    
}

- (void)setCurrentCard:(Card *)thisCard
{
    if (!thisCard) {
        return;
    }
    
    _card = thisCard;
    
}

- (void)addAction:(id)sender
{
    NSString *verb = @"";
    verb = [triggerVerb valueForFieldNamed:@"verb"];

    for (NSDictionary *field in [triggerFS fields]) {
        if ([field objectForKey:@"name"]) {
            NSString *value = [triggerFS valueForFieldNamed:[field objectForKey:@"name"]];
            NSString *type = [[field objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"trigger_" withString:@""];
            
            NSMutableDictionary *triggerDefaults = [[Common triggerDetailsForType:type] mutableCopy];
            if (verb && ![verb isEqualToString:@""]) {
                [triggerDefaults setObject:verb forKey:@"verb"];
            }
            
            if (_deck) {
                [_deck createTriggerWithDetails:triggerDefaults andValue:value];
            } else if (_card) {
                [_card createTriggerWithDetails:triggerDefaults andValue:value];
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)removeAction:(id)sender
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:@"Are you sure you want to remove this action?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [av showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            if (thisTrigger) {
                if ([thisTrigger isKindOfClass:[DeckTrigger class]]) {
                    [(DeckTrigger *)thisTrigger deleteFromDeck:_deck];
                    [self.navigationController popViewControllerAnimated:YES];
                } else if ([thisTrigger isKindOfClass:[CardTrigger class]]) {
                    [(CardTrigger *)thisTrigger deleteFromCard:_card];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
    }];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        if (_card) {
            [(CreateCardViewController *)self.priorViewController refreshViewWithCard:_card];
        } else if (_deck) {
            [(CreateDeckViewController *)self.priorViewController refreshViewWithDeck:_deck];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
