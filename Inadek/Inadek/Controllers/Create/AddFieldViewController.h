//
//  AddTriggerViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "UIBCNavButton.h"
#import "BCNavController.h"
#import "BCFieldSetView.h"
#import "Common.h"

#import "CreateCardViewController.h"
#import "DeckTrigger+Core.h"
#import "CardField+Core.h"
#import "Card+Core.h"

@interface AddFieldViewController : BCViewController <BCFieldSetDelegate> {
    NSDictionary *field;
    BCFieldSetView *triggerFS;
    BCFieldSetView *triggerVerb;
    
    NSNumber *isFrontBool;
}

@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) Card *card;
@property (nonatomic, retain) BCViewController *priorViewController;

- (void)setCurrentDeck:(Deck *)thisDeck;
- (void)setCurrentCard:(Card *)thisCard;
- (id)initWithFieldCID:(NSString *)fieldCID isFront:(BOOL)front andCard:(Card *)card;
- (id)initWithFieldCID:(NSString *)fieldCID isFront:(BOOL)front;
- (id)initWithField:(CardField *)aField;

@end
