//
//  CreateCardViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "Deck+Core.h"
#import "Card+Core.h"
#import "CardTrigger+Core.h"
#import "Common.h"
#import "MBProgressHUD.h"

#import "BCCardTypeButton.h"
#import "BCCardEditImage.h"
#import "BCFieldSetView.h"
#import "BCFooterActionButton.h"
#import "CategorySelectViewController.h"
#import "BCGroupedActionSheetView.h"
#import "AddTriggerViewController.h"

@interface CreateCardViewController : BCViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, BCCardEditImageDelegate, BCFooterActionButtonDelegate, BCFieldSetDelegate, BCGroupedActionSheetDelegate> {
    UIImagePickerController *imagePicker;
    
    BCFieldSetView *cardGeneralFS;
    BCFieldSetView *cardFieldsFS;
    BCFieldSetView *cardBackFieldsFS;
    BCFieldSetView *cardTriggersFS;
    BCFieldSetView *cardDeleteFS;
    BCCardEditImage *cardImage;
    
    MASConstraint *deckImageTopCS;
    MASConstraint *scrollHolderHeightCS;
    
    BOOL has_image;
    int current_total_height;
    
    UIView *scrollHolder;
    
    BCFooterActionButton *footerButton;
    BOOL isEditing;
    
    UIImage *temporary_image;
    
}

@property (nonatomic, retain) Card *card;
@property (nonatomic, retain) Deck *into_deck;
@property (nonatomic, retain) BCViewController *priorViewController;
@property (nonatomic, retain) NSString *page;
@property (nonatomic, retain) UIScrollView *typesSV;

- (id)initWithCard:(Card *)thisCard andPage:(NSString *)page;
- (id)initWithPage:(NSString *)page;
- (void)startCardType:(NSString *)cardCid;

- (void)refreshViewWithCard:(Card *)cardObject;

- (void)setEditingMode:(BOOL)mode;

@end
