//
//  CategorySelectViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CategorySelectViewController.h"

@interface CategorySelectViewController ()

@end

@implementation CategorySelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!categories) {
        categories = [[DeckManager sharedManager] getAllCategories];
    }
    
    categoryTV = [[UITableView alloc] initWithFrame:CGRectZero];
    categoryTV.delegate = self;
    categoryTV.dataSource = self;
    
    [self.view addSubview:categoryTV];
    [categoryTV makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    if (self.deck) {
        [categoryTV reloadData];
    }
    
    // Do any additional setup after loading the view.
}

- (void)setCurrentDeck:(Deck *)thisDeck
{
    if (!thisDeck) {
        return;
    }
    
    if (!categories) {
        categories = [[DeckManager sharedManager] getAllCategories];
    }
    
    _deck = thisDeck;
    selectedCategories = NSMutableArray.new;
    NSArray *thisDeckCategories = [[thisDeck categories] allObjects];
    for (DeckCategory *cat in categories) {
        for (DeckCategory *thisCat in thisDeckCategories) {
            if ([thisCat.cid isEqualToString:cat.cid]) {
                [selectedCategories addObject:cat];
                break;
            }
        }
    }
    
    [categoryTV reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [categories count];    //count number of row from counting array hear cataGorry is An Array
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[CategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
    if ([categories count] > indexPath.row) {
        DeckCategory *cat = [categories objectAtIndex:indexPath.row];
        if (cat) {
            [cell.label setText:cat.name];
            if ([selectedCategories containsObject:cat]) {
                [cell setChecked:YES];
            }
            [cell setCategory:cat];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryTableViewCell *cell = (CategoryTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if ([cell isChecked]) {
        // Cell was selected
        [_deck addCategoriesObject:cell.category];
        
    } else {
        // Cell was deselected
        [_deck removeCategoriesObject:cell.category];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        [(CreateDeckViewController *)self.priorViewController refreshViewWithDeck:_deck];
    }
}

@end
