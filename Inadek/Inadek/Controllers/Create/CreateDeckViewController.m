//
//  CreateDeckViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CreateDeckViewController.h"
#import "BCTransitionViewController.h"

#import "UIAlertView+BCAlertView.h"

@interface CreateDeckViewController ()

@end

@implementation CreateDeckViewController

- (id)initWithDeck:(Deck *)thisDeck andPage:(NSString *)page
{
    self = [self initWithPage:page];
    if (self) {
        if (thisDeck) {
            self.deck = thisDeck;
        } else {
            self.deck = [Deck myNew];
            self.deck.cid = @"!NEW!";
            self.deck.created_at = nil;
        }
    }
    
    return self;
}

- (id)initWithPage:(NSString *)page
{
    self = [super init];
    if (self) {
        
        if (page) {
            self.page = page;
        } else {
            self.page = @"page1";
        }
        
    }
    
    return self;

}

- (void)cleanUpUnsavedObject:(BOOL)forceDelete
{
    if (self.deck && [self.deck.cid isEqualToString:@"!NEW!"]) {
        [self.deck delete];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    if (!self.page) {
        self.page = @"page1";
    }
    
    BOOL is_editing = YES;
    if (!self.deck) {
        self.deck = [Deck myNew];
        is_editing = NO;
    }
    
    if ([self.page isEqualToString:@"page1"]) {
        [self loadPageOneView];
    } else if ([self.page isEqualToString:@"page2"]) {
        [self loadPageTwoView];
    }
    
    if (is_editing) {
        [self refreshViewWithDeck:self.deck];
    }

}



- (void)loadPageOneView
{
    
    UIView *container = UIView.new;
    [container setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:container];
    
    [container makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    _typesSV = UIScrollView.new;
    [_typesSV setBackgroundColor:[UIColor clearColor]];
    [container addSubview:_typesSV];
    
    [_typesSV makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(container);
    }];
    
    scrollHolder = UIView.new;
    [_typesSV addSubview:scrollHolder];
    
    [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_typesSV);
        make.width.equalTo(_typesSV);
    }];
    
    
    NSArray *generalDeckTypes = [[DeckManager sharedManager] localDeckTypes:@{@"segment":@"general"}];
    BCDeckTypeButton *priorObject = nil;
    
    int side_padding = 30;
    NSNumber *side_padding_obj = [NSNumber numberWithInt:side_padding];
    
    int counter = 0;
    for (NSDictionary *dt in generalDeckTypes) {
        if ([dt objectForKey:@"cid"]) {
            
            counter += 1;
            BCDeckTypeButton *btn = [[BCDeckTypeButton alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            [btn drawButtonWithType:dt];
            btn.parentViewController = self;
            
            [scrollHolder addSubview:btn];
            NSNumber *cellWidth = [[NSNumber alloc] initWithInt:((self.view.bounds.size.width / 2) - side_padding - 8)];
            [btn makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(cellWidth);
                make.height.equalTo(@130);
                if (!priorObject) {
                    make.top.equalTo(@30);
                }
                if (counter % 2) {
                    // ODD
                    if (priorObject) {
                        make.top.equalTo(priorObject.bottom).with.offset(16);
                    }
                    make.left.equalTo(side_padding_obj);
                } else {
                    // EVEN
                    if (priorObject) {
                        make.top.equalTo(priorObject.top);
                    }
                    NSNumber *spo_second = [NSNumber numberWithInt:([side_padding_obj integerValue] + [cellWidth integerValue] + 16)];
                    make.left.equalTo(spo_second);
                }
            }];
            
            priorObject = btn;
        } else {
            
        }
    }
    
    if (priorObject) {
        [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(priorObject.bottom).with.offset(100);
        }];
    }
}

- (void)loadPageTwoView
{
    
    UIView *container = UIView.new;
    [container setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:container];
    
    [container makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    _typesSV = UIScrollView.new;
    [_typesSV setBackgroundColor:[UIColor clearColor]];
    [container addSubview:_typesSV];
    
    [_typesSV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(container);
        make.left.equalTo(container);
        make.right.equalTo(container);
        make.bottom.equalTo(container.bottom).with.offset(-80);
    }];
    
    scrollHolder = UIView.new;
    [_typesSV addSubview:scrollHolder];
    [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_typesSV);
        make.width.equalTo(_typesSV);
    }];
    
    int total_height = 0;
    
    deckImage = [[BCDeckEditImage alloc] initWithFrame:CGRectZero];
    [deckImage setDelegate:self];
    [deckImage setBackgroundColor:[UIColor clearColor]];
    
    
    [scrollHolder addSubview:deckImage];
    [deckImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(scrollHolder.width).with.offset(-20);
        deckImageTopCS = make.height.equalTo(scrollHolder.width).with.offset(-160);
        make.centerX.equalTo(scrollHolder.centerX);
        make.top.equalTo(scrollHolder).with.offset(10);
    }];
    
    if ([_deck imageObj]) {
        // Existing image, show it here
        [deckImage showImage:[_deck imageObj]];
        deckImageTopCS.offset = -15;
        total_height += 350;
        has_image = YES;
        
        [deckImage addImageEditButton];
    } else if (_deck.image_url) {
        [deckImage loadImage:_deck.image_url];
        deckImageTopCS.offset = -15;
        total_height += 320;
        has_image = YES;
        
        [deckImage addImageEditButton];
    } else {
        [deckImage buildButtons];
        total_height += 200;
    }
    
    
    
    NSArray *fieldsArray = [[NSArray alloc] initWithObjects:
                            @{@"style":[NSNumber numberWithInt:kFieldStyleText],@"placeholder":@"Deck Name",@"name":@"deck_name",@"value":[_deck nameString]},
                            nil];
    
    BCFieldSetView *deckTitleFS = [[BCFieldSetView alloc] initWithTitle:nil dataFields:fieldsArray andStyle:@"white"];
    deckTitleFS.delegate = self;
    
    [scrollHolder addSubview:deckTitleFS];
    [deckTitleFS makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(deckImage.bottom).with.offset(20);
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([deckTitleFS fieldHeight]);
    }];
    
    total_height += [[deckTitleFS fieldHeight] integerValue] + 20;
    
    fieldsArray = [[NSArray alloc] initWithObjects:
                            @{@"style":[NSNumber numberWithInt:kFieldStyleSelect],
                              @"title":@"Deck Type",
                              @"name":@"deck_type",
                              @"value":[Common currentDeckTypeAsString:_deck],
                              @"action_callback":@"doChangeDeckType:"},
                   
                            @{@"style":[NSNumber numberWithInt:kFieldStyleMultiselect],
                              @"title":@"Category",
                              @"name":@"deck_categories",
                              @"placeholder":@"None Set",
                              @"value":[Common currentDeckCategoriesAsString:_deck],
                              @"action_callback":@"doChangeCategories:"},
                   
                            @{@"style":[NSNumber numberWithInt:kFieldStyleSwitch],
                              @"title":@"Private Deck",
                              @"name":@"is_private",
                              @"action_callback":@"doChangePrivacy:"},
                            nil];
    
    deckGeneralFS = [[BCFieldSetView alloc] initWithTitle:@"GENERAL" dataFields:fieldsArray andStyle:@"white"];
    deckGeneralFS.delegate = self;
    
    [scrollHolder addSubview:deckGeneralFS];
    [deckGeneralFS makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(deckTitleFS.bottom);
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([deckGeneralFS fieldHeight]);
    }];
    
    total_height += [[deckGeneralFS fieldHeight] integerValue] + 20;
    
    
    if ([[Common triggersForDeckTypeId:_deck.type_id] count] > 0) {
        
        NSMutableArray *mFieldsArray = NSMutableArray.new;
        
        for (NSDictionary *trigger in [Common triggersForDeckTypeId:_deck.type_id]) {
            NSString *trigger_name = [NSString stringWithFormat:@"trigger_%@", [trigger objectForKey:@"trigger_type"]];
            
            NSMutableDictionary *t = NSMutableDictionary.new;
            NSNumber *style = [NSNumber numberWithInt:kFieldStyleTextSelect];
            
            if ([[trigger objectForKey:@"trigger_type"] isEqualToString:@"phone"]) {
                style = [NSNumber numberWithInt:kFieldStyleTextSelectPhone];
            } else if ([[trigger objectForKey:@"trigger_type"] isEqualToString:@"email"]) {
                style = [NSNumber numberWithInt:kFieldStyleTextSelectEmail];
            }
            
            [t setObject:style forKey:@"style"];
            [t setObject:[trigger objectForKey:@"name"] forKey:@"title"];
            [t setObject:trigger_name forKey:@"name"];
            [t setObject:@"Off" forKey:@"placeholder"];
            [t setObject:@"" forKey:@"value"];
            [mFieldsArray addObject:t];
        }
        
        fieldsArray = [self getDeckTriggerFieldsArray];
        
        deckTriggersFS = [[BCFieldSetView alloc] initWithTitle:@"ACTIONS" dataFields:fieldsArray andStyle:@"white"];
        deckTriggersFS.delegate = self;
        
        [scrollHolder addSubview:deckTriggersFS];
        [deckTriggersFS makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(deckGeneralFS.bottom);
            make.width.equalTo(scrollHolder.width);
            make.height.equalTo([deckTriggersFS fieldHeight]);
        }];
        
        total_height += [[deckTriggersFS fieldHeight] integerValue] + 20;
    }
    
    NSArray *deleteFieldsArray = [[NSArray alloc] initWithObjects:
                    @{@"style":[NSNumber numberWithInt:kFieldStyleDeleteButton],
                     @"title":@"Delete Deck",
                     @"action_callback":@"doDelete:"},
                   nil];
    
    deckDeleteFS = [[BCFieldSetView alloc] initWithTitle:@"" dataFields:deleteFieldsArray andStyle:@"white"];
    deckDeleteFS.delegate = self;
    
    [scrollHolder addSubview:deckDeleteFS];

    [deckDeleteFS makeConstraints:^(MASConstraintMaker *make) {
        if (deckTriggersFS) {
            make.top.equalTo(deckTriggersFS.bottom).with.offset(40);
        } else {
            make.top.equalTo(deckGeneralFS.bottom).with.offset(40);
        }
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([deckDeleteFS fieldHeight]);
    }];
    
    total_height += [[deckDeleteFS fieldHeight] integerValue] + 80;
    
    
    current_total_height = total_height + 50;
    
    [Common log:@"Total Height" withObject:@(total_height)];
    
    [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
        scrollHolderHeightCS = make.height.equalTo(@0).with.offset(total_height);
    }];
    
    
    footerButton = [[BCFooterActionButton alloc] initWithFrame:CGRectZero];
    footerButton.delegate = self;
    [self.view addSubview:footerButton];
    [footerButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@80);
        make.width.equalTo(scrollHolder);
        make.bottom.equalTo(self.view);
        make.centerX.equalTo(self.view);
    }];
    
    if (isEditing) {
        [footerButton setTitle:@"SAVE CHANGES"];
    }
    
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    
    imagePicker.mediaTypes = [NSArray arrayWithObjects: (NSString *) kUTTypeImage, (NSString *) kUTTypeMovie, nil];
    imagePicker.allowsEditing = YES;
}

- (NSArray *)getDeckTriggerFieldsArray
{
    
    NSArray *fieldsArray = NSArray.new;
    
    if (!_deck) {
        return fieldsArray;
    }
    
    if ([[_deck.triggers allObjects] count] > 0) {
        NSMutableArray *arr = NSMutableArray.new;
        for (DeckTrigger *trig in [_deck.triggers allObjects]) {
            [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleSelect],
                             @"title":trig.name,
                             @"name":[NSString stringWithFormat:@"trigger_%@", trig.trigger_type],
                             @"value":trig.value,
                             @"action_callback":@"doChangeTrigger:",
                             @"action_object":trig}];
        }
        
        [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                         @"title":@"Add Deck Action",
                         @"action_callback":@"doAddAction:"}];
        
        fieldsArray = [[NSArray alloc] initWithArray:arr];
        
    } else {
        fieldsArray = [[NSArray alloc] initWithObjects:
                       @{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                         @"title":@"Add Deck Action",
                         @"action_callback":@"doAddAction:"},
                       nil];
    }
    
    return fieldsArray;
    
}

- (void)startDeckType:(NSString *)deckCid
{
    NSDictionary *deckTypeDetails = [[DeckManager sharedManager] deckTypeDetailsByCid:deckCid];
    
    if (!deckTypeDetails) {
        //TODO: Fix this to show an error
        return;
    }
    
    _deck.type_id = [deckTypeDetails objectForKey:@"id"];
    
    if (self.priorViewController && [self.priorViewController isKindOfClass:[CreateDeckViewController class]]) {
        
        [(CreateDeckViewController *)self.priorViewController refreshViewWithDeck:_deck];
        [[self navigationController] popViewControllerAnimated:YES];
        
    } else {
        
        CreateDeckViewController *startDeck = [[CreateDeckViewController alloc] initWithDeck:_deck andPage:@"page2"];
        startDeck.priorViewController = self;
        
        if (deckTypeDetails && [deckTypeDetails objectForKey:@"name"]) {
            startDeck.title = [NSString stringWithFormat:@"%@ %@", [deckTypeDetails objectForKey:@"name"], @"Deck"];
        } else {
            startDeck.title = @"Deck";
        }
        
        [self.navigationController pushViewController:startDeck animated:YES];
        
    }

}

- (void)footerActionButton:(BCFooterActionButton *)button didContinue:(BOOL)didContinue
{
    [self.view endEditing:YES];
    [self saveDeck];
}

- (void)setEditingMode:(BOOL)mode
{
    if (mode) {
        isEditing = YES;
    } else {
        isEditing = NO;
    }
}


- (void)deleteDeck
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Deleting Deck";
    
    BOOL didDelete = [[DeckManager sharedManager] deleteDeck:_deck callback:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.navigationController dismissViewControllerAnimated:YES completion:^{

        }];
        
        [self.baseViewController postUpdateNotification:kRefreshDecks withObject:nil];
    } failureCallback:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}


- (void)saveDeck
{
    
    
    
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Saving Your Deck";
    
    if ([_deck.cid isEqualToString:@"!NEW!"]) {
        [_deck setCid:@"!PENDING!"];
        [_deck save];
    }
    
    // Dump the junk decks that are being created while I cry...
    NSMutableArray *decksToDelete = NSMutableArray.new;
    for (Deck *d in [Deck all]) {
        if (d.cid == nil || [d.cid isEqualToString:@""]) {
            [decksToDelete addObject:d];
        }
    }
    
    for (Deck *d in decksToDelete) {
        [d delete];
    }
    
    if (temporary_image) {
        _deck.base_color = [Common getHexForAverageColor:temporary_image];
        [_deck setImageObj:temporary_image];
    }

    
    BOOL didSave = [[DeckManager sharedManager] saveDeck:_deck callback:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            NSDictionary *deckParams = NSDictionary.new;
            if (_deck) {
                deckParams = @{@"deck":_deck};
            }
            [self.baseViewController transitionTo:@"deck" withParams:deckParams];
        }];
        
        [self.baseViewController postUpdateNotification:kRefreshDecks withObject:nil];
    } failureCallback:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)deckEditImage:(BCDeckEditImage *)image selectImage:(NSString *)mode
{
    if (mode && [mode isEqualToString:@"show_picker"]) {
        [self.view endEditing:YES];
        [self doSelectPhotoType:nil];
    } else {
        [self.view endEditing:YES];
        [self openPhotoAccess:mode];
    }

}

- (void)openPhotoAccess:(NSString *)mode
{
    
    if ([mode isEqualToString:@"cameraroll"]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *)kUTTypeImage, nil];
    } else if ([mode isEqualToString:@"camera"]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *)kUTTypeImage, nil];
        [imagePicker takePicture];
    }
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fieldSetView:(BCFieldSetView *)view didChangeField:(NSString *)field withValue:(id)value
{
    if ([field isEqualToString:@"deck_name"]) {
        _deck.name = (NSString *)value;
    }
}

- (void)fieldSetView:(BCFieldSetView *)view didCallAction:(NSString *)selector withInfo:(NSDictionary *)userInfo
{
    [self.view endEditing:YES];
    
    if (selector == nil) {
        return;
    }
    
    SEL s = NSSelectorFromString(selector);
    if ([self respondsToSelector:s]) {
        [self performSelectorOnMainThread:s withObject:userInfo waitUntilDone:NO];
    } else {
        
    }
}

- (void)fieldSetView:(BCFieldSetView *)view didBeginEditing:(id)textField
{
    if (_typesSV && textField) {
        CGPoint svos = _typesSV.contentOffset;
        
        CGPoint pt;
        CGRect rc = [textField bounds];
        
        rc = [textField convertRect:rc toView:_typesSV];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= 60;
        
        [_typesSV setContentOffset:pt animated:YES];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        CGRect cropRect = [info[UIImagePickerControllerCropRect] CGRectValue];
        UIImage *scaledImage = [Common scaleAndRotateImage:[Common cropImage:image withRect:cropRect]];
        
        if (scaledImage) {
            
            temporary_image = scaledImage;
            [deckImage showImage:temporary_image];
            deckImageTopCS.offset = -15;
            if (has_image) {
                scrollHolderHeightCS.offset = current_total_height;
            } else {
                has_image = YES;
                current_total_height = current_total_height + 160;
                scrollHolderHeightCS.offset = current_total_height;
            }
        }
        
    } else {
        // Video not yet supported
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void)doChangeDeckType:(id)object
{
    CreateDeckViewController *deckTypeVC = [[CreateDeckViewController alloc] initWithDeck:_deck andPage:@"page1"];
    deckTypeVC.priorViewController = self;
    [self.navigationController pushViewController:deckTypeVC animated:YES];
}

- (void)doChangeTrigger:(id)object
{
    [self editTrigger:[(NSDictionary *)object objectForKey:@"object"]];
}

- (void)doDelete:(id)sender
{
    UIAlertView *sure = [[UIAlertView alloc] initWithTitle:@"Delete Deck?" message:@"Are you sure you want to delete this deck?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [sure showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [self deleteDeck];
        }
    }];
    
}

- (void)doAddAction:(id)sender
{
    NSMutableArray *mFieldsArray = NSMutableArray.new;
    
    for (NSDictionary *trigger in [Common triggersForDeckTypeId:_deck.type_id]) {
        NSString *trigger_name = [NSString stringWithFormat:@"trigger_%@", [trigger objectForKey:@"trigger_type"]];
        
        NSMutableDictionary *t = NSMutableDictionary.new;
        
        [t setObject:[trigger objectForKey:@"name"] forKey:@"name"];
        [t setObject:trigger_name forKey:@"id"];
        [mFieldsArray addObject:t];
    }
    
    BCGroupedActionSheetView *actionSheet = [[BCGroupedActionSheetView alloc] initWithOptions:mFieldsArray withTitle:@"Select a Deck Action" andFrame:CGRectZero];
    actionSheet.delegate = self;
    actionSheet.sheetCid = @"deck_triggers";
    
    [actionSheet showInView:self.view];
}

- (void)doSelectPhotoType:(id)sender
{
    NSMutableArray *mFieldsArray = NSMutableArray.new;
    
    [mFieldsArray addObject:@{@"name":@"Take a Photo",@"id":@"camera"}];
    [mFieldsArray addObject:@{@"name":@"Select from Camera Roll",@"id":@"cameraroll"}];
    
    BCGroupedActionSheetView *actionSheet = [[BCGroupedActionSheetView alloc] initWithOptions:mFieldsArray withTitle:@"Change Photo" andFrame:CGRectZero];
    actionSheet.delegate = self;
    actionSheet.sheetCid = @"change_photo";
    
    [actionSheet showInView:self.view];
}

- (void)groupedActionSheet:(BCGroupedActionSheetView *)actionSheet selectedItemWithCid:(NSString *)cid andGroup:(NSString *)group
{
    if (group && [group isEqualToString:@"deck_triggers"]) {
        [self addTrigger:cid];
    } else if (group && [group isEqualToString:@"change_photo"]) {
        [self openPhotoAccess:cid];
    } else {
        
    }
}

- (void)groupedActionSheetDidCancel:(BCGroupedActionSheetView *)actionSheet
{
    
}

- (void)doChangeCategories:(id)object
{
  
    CategorySelectViewController *categorySelectVC = [[CategorySelectViewController alloc] init];
    [categorySelectVC setTitle:@"Select Categories"];
    [categorySelectVC setPriorViewController:self];
    [categorySelectVC setCurrentDeck:_deck];
    //[categorySelectVC setSelectedCategories:[_deck.categories allObjects]];
    [self.navigationController pushViewController:categorySelectVC animated:YES];

}

- (void)editTrigger:(DeckTrigger *)trigger
{
    if (trigger) {
        AddTriggerViewController *addTriggerVC = [[AddTriggerViewController alloc] initWithTrigger:trigger];
        [addTriggerVC setTitle:@"Edit Action"];
        [addTriggerVC setPriorViewController:self];
        [addTriggerVC setCurrentDeck:_deck];
        
        [self.navigationController pushViewController:addTriggerVC animated:YES];
    }
}

- (void)addTrigger:(NSString *)triggerType
{
    AddTriggerViewController *addTriggerVC = [[AddTriggerViewController alloc] initWithTriggerType:triggerType];
    [addTriggerVC setTitle:@"Add Action"];
    [addTriggerVC setPriorViewController:self];
    [addTriggerVC setCurrentDeck:_deck];
    
    [self.navigationController pushViewController:addTriggerVC animated:YES];
}

- (void)refreshViewWithDeck:(Deck *)deckObject
{
    _deck = deckObject;
    [deckGeneralFS updateValueForFieldNamed:@"deck_categories" withValue:[Common currentDeckCategoriesAsString:deckObject]];
    [deckGeneralFS updateValueForFieldNamed:@"deck_type" withValue:[Common currentDeckTypeAsString:deckObject]];
    if (deckTriggersFS) {
        // Need to rebuild the field data, then pass it into the deckTriggersFS view to re-draw
        
        current_total_height = current_total_height - deckTriggersFS.bounds.size.height;
        [deckTriggersFS removeFromSuperview];
        
        NSArray *triggerFields = [self getDeckTriggerFieldsArray];
        [deckTriggersFS rebuildFields:triggerFields];
        
        deckTriggersFS.delegate = self;
        
        [scrollHolder addSubview:deckTriggersFS];
        [deckTriggersFS updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(deckGeneralFS.bottom);
            make.width.equalTo(scrollHolder.width);
            make.height.equalTo([deckTriggersFS fieldHeight]);
        }];
        
        current_total_height += [[deckTriggersFS fieldHeight] integerValue];
        
        if (deckDeleteFS) {
            [deckDeleteFS updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(deckTriggersFS.bottom).with.offset(40);
            }];
        }
        
        current_total_height += [[deckDeleteFS fieldHeight] integerValue] + 80;
        
        //current_total_height += 50;
        
        [scrollHolder updateConstraints:^(MASConstraintMaker *make) {
            scrollHolderHeightCS = make.height.equalTo(@0).with.offset(current_total_height);
        }];
        
    }
}

- (IBAction)popCurrentViewController:(id)sender
{
    [self cleanUpUnsavedObject:YES];
    if (self.navigationController) {
        [sender setSelected:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
// I don't think we need any of this now that we're handling this situation via the cleanUnsaved method call near the top
//    if (self.isMovingFromParentViewController) {
//        if (self.priorViewController) {
//            if ([self.priorViewController isKindOfClass:[CreateDeckViewController class]]) {
//                [(CreateDeckViewController *)self.priorViewController setDeck:_deck];
//            }
//        }
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
