//
//  DeckSelectViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckSelectViewController.h"

@interface DeckSelectViewController ()

@end

@implementation DeckSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!decks) {
        decks = [[DeckManager sharedManager] myLocalDecks];
    }
    
    decksTV = [[UITableView alloc] initWithFrame:CGRectZero];
    decksTV.delegate = self;
    decksTV.dataSource = self;
    
    [self.view addSubview:decksTV];
    [decksTV makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    if (self.card) {
        [decksTV reloadData];
    }
    
    // Do any additional setup after loading the view.
}

- (void)setCurrentCard:(Card *)thisCard
{
    if (!thisCard) {
        return;
    }
    
    if (!decks) {
        decks = [[DeckManager sharedManager] myLocalDecks];
    }
    
    _card = thisCard;
    selectedDecks = NSMutableArray.new;
    NSArray *thisCardDecks = [[thisCard decks] allObjects];
    for (Deck *d in thisCardDecks) {
        [selectedDecks addObject:d];
    }
    
    [decksTV reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [decks count];    //count number of row from counting array hear cataGorry is An Array
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[CategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
    if ([decks count] > indexPath.row) {
        Deck *deck = [decks objectAtIndex:indexPath.row];
        if (deck) {
            [Common log:deck.name];
            [cell.label setText:deck.name];
            if ([selectedDecks containsObject:deck]) {
                [cell setChecked:YES];
            }
            [cell setDeck:deck];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryTableViewCell *cell = (CategoryTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if ([cell isChecked]) {
        // Cell was selected
        [_card addDecksObject:cell.deck];
        
    } else {
        // Cell was deselected
        [_card removeDecksObject:cell.deck];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        [(CreateCardViewController *)self.priorViewController refreshViewWithCard:_card];
    }
}

@end
