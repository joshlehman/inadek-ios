//
//  CategorySelectViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "CategoryTableViewCell.h"
#import "DeckManager.h"
#import "CreateDeckViewController.h"

#import "Deck.h"

@interface CategorySelectViewController : BCViewController <UITableViewDataSource, UITableViewDelegate> {
    UITableView *categoryTV;
    NSArray *categories;
    NSMutableArray *selectedCategories;
}


@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) BCViewController *priorViewController;

- (void)setSelectedCategories:(NSArray *)categories;
- (void)setCurrentDeck:(Deck *)thisDeck;

@end
