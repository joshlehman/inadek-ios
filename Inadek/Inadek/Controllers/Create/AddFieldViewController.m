//
//  AddTriggerViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "AddFieldViewController.h"
#import "UIAlertView+BCAlertView.h"

@interface AddFieldViewController () {
    BOOL isEditing;
    CardField *thisField;
}

@end

@implementation AddFieldViewController


- (id)initWithField:(CardField *)aField {
    
    self = [self init];
    if (self) {
        
        isFrontBool = aField.on_front;
        
        NSString *placeholder = @"";
        if (aField.placeholder) {
            placeholder = aField.placeholder;
        }
        
        NSNumber *has_title = [NSNumber numberWithBool:NO];
        if (aField.has_title) {
            has_title = aField.has_title;
        }
        field = @{@"has_title":has_title, @"field_value":aField.field_value,@"field_title":aField.title,@"title":aField.title,@"cid":aField.field_cid,@"placeholder":placeholder,@"existing_field":aField,@"value_type":aField.value_type};
        
        isEditing = YES;
        thisField = aField;
        
    }
    
    return self;
    
}

- (id)initWithFieldCID:(NSString *)fieldCID isFront:(BOOL)front andCard:(Card *)card
{
    self = [self init];
    if (self) {
        
        _card = card;
        
        isFrontBool = [NSNumber numberWithBool:front];
        
        // If the cid has the ctf! string, we know this is a custom field for this type of card
        if ([fieldCID rangeOfString:@"ctf!"].location != NSNotFound) {
            field = [self getCustomCardFieldDataByCID:[fieldCID stringByReplacingOccurrencesOfString:@"field_" withString:@""] withCardFields:[Common fieldsForCardTypeId:_card.type_id]];
        } else {
            field = [self getFieldDataByCID:[fieldCID stringByReplacingOccurrencesOfString:@"field_" withString:@""]];
        }
        
    }
    
    return self;
}

- (id)initWithFieldCID:(NSString *)fieldCID isFront:(BOOL)front
{
    self = [self init];
    if (self) {
        
        isFrontBool = [NSNumber numberWithBool:front];
        
        field = [self getFieldDataByCID:[fieldCID stringByReplacingOccurrencesOfString:@"field_" withString:@""]];
        
    }
    
    return self;
}

- (NSDictionary *)getCustomCardFieldDataByCID:(NSString *)cid withCardFields:(NSArray *)cardFields
{
    NSDictionary *returnField = NSDictionary.new;
    
    NSArray *fields = cardFields;
    for (NSDictionary *t in fields) {
        NSString *aField = cid;
        if ([[t objectForKey:@"cid"] isEqualToString:aField]) {
            returnField = @{@"has_title":@1,@"field_title":[t objectForKey:@"name"],@"title":[t objectForKey:@"name"],@"cid":aField,@"placeholder":[t objectForKey:@"default_placeholder"],@"field_value":@"",@"value_type":[t objectForKey:@"value_type"]};
            
            // Single image
            if ([[t objectForKey:@"value_type"] isEqualToString:@"image"]) {
                
            }
            
            // Image gallery
            if ([[t objectForKey:@"value_type"] isEqualToString:@"images"]) {
                
            }
            
            break;
        }
    }
    
    return returnField;
}

- (NSDictionary *)getFieldDataByCID:(NSString *)cid
{
    NSDictionary *returnField = NSDictionary.new;
    
    NSArray *fields = [[NSUserDefaults standardUserDefaults] objectForKey:@"fields"];
    for (NSDictionary *t in fields) {
        NSString *aField = cid;
        if ([[t objectForKey:@"cid"] isEqualToString:aField]) {
            returnField = @{@"has_title":[t objectForKey:@"has_title"],@"field_title":[t objectForKey:@"name"],@"title":@"",@"cid":aField,@"placeholder":[t objectForKey:@"default_placeholder"],@"field_value":@"",@"value_type":[t objectForKey:@"value_type"]};
            
            // Single image
            if ([[t objectForKey:@"value_type"] isEqualToString:@"image"]) {
                
            }
            
            // Image gallery
            if ([[t objectForKey:@"value_type"] isEqualToString:@"images"]) {
                
            }
            
            break;
        }
    }
    
    return returnField;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    if (field && [field objectForKey:@"cid"]) {
        
        if ([field objectForKey:@"value_type"] && [[field objectForKey:@"value_type"] isEqualToString:@"images"]) {
            self.navigationItem.rightBarButtonItem = [(BCNavController *)self.navigationController getNavBarItem:kBCNavButtonStyleText withTitle:@"DONE" andPosition:kBCNavPositionRight target:self action:@selector(addField:)];
        } else {
            self.navigationItem.rightBarButtonItem = [(BCNavController *)self.navigationController getNavBarItem:kBCNavButtonStyleText withTitle:@"SAVE" andPosition:kBCNavPositionRight target:self action:@selector(addField:)];
        }
        
        
        [self.view setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
        
        UILabel *triggerType = UILabel.new;
        [triggerType addStyleClass:@"lbl-trigger-type"];
        [self.view addSubview:triggerType];
        [triggerType makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@13);
            make.left.equalTo(self.view).with.offset(20);
            make.right.equalTo(self.view).with.offset(-20);
            make.height.equalTo(@30);
        }];
        
        if ([field objectForKey:@"field_title"] && ![[field objectForKey:@"field_title"] isEqualToString:@""]) {
            [triggerType setText:[field objectForKey:@"field_title"]];
        } else {
            [triggerType setText:[CardField getDefaultTitleForCid:[field objectForKey:@"cid"]]];
        }
        
        NSString *value = @"";
        NSString *title = @"";
        if ([field objectForKey:@"field_value"]) {
            value = [field objectForKey:@"field_value"];
            title = [field objectForKey:@"title"];
        }
        
        NSArray *fieldsArray = NSArray.new;
        
        if ([field objectForKey:@"value_type"] && [[field objectForKey:@"value_type"] isEqualToString:@"images"]) {
            
        } else {
            
            if ([[field objectForKey:@"has_title"] integerValue] == 1) {
                fieldsArray = [[NSArray alloc] initWithObjects:
                               @{@"style":[NSNumber numberWithInt:kFieldStyleTextCapped],
                                 @"placeholder":[Common fieldStyleTitlePlaceholder:[field objectForKey:@"value_type"]],
                                 @"name":[NSString stringWithFormat:@"%@%@", [field objectForKey:@"title"], @"_title"],
                                 @"value":title,
                                 @"keyboard_type":@(UIKeyboardTypeDefault),
                                 @"return_key":@"next"},
                               @{@"style":[Common fieldStyle:[field objectForKey:@"value_type"]],
                                 @"placeholder":[field objectForKey:@"placeholder"],
                                 @"name":[field objectForKey:@"title"],
                                 @"value":value,
                                 @"keyboard_type":@([Common fieldKeyboardType:[field objectForKey:@"value_type"]]),
                                 @"return_key":@"next"},
                               
                               nil];
            } else {
                fieldsArray = [[NSArray alloc] initWithObjects:
                               @{@"style":[NSNumber numberWithInt:kFieldStyleTextCapped],
                                 @"placeholder":[field objectForKey:@"placeholder"],
                                 @"name":[field objectForKey:@"title"],
                                 @"value":value,
                                 @"keyboard_type":@([Common fieldKeyboardType:[field objectForKey:@"value_type"]]),
                                 @"return_key":@"next"},
                               
                               nil];
            }
            
        }
        
        triggerFS = [[BCFieldSetView alloc] initWithTitle:nil dataFields:fieldsArray andStyle:@"white"];
        
        [self.view addSubview:triggerFS];
        [triggerFS makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(triggerType.bottom).with.offset(10);
            make.width.equalTo(self.view);
            make.height.equalTo([triggerFS fieldHeight]);
        }];
        
        [triggerFS makeIndexFirstResponder:1];
        
        if (isEditing) {
            BCButton *removeField = BCButton.new;
            [removeField setStyleClass:@"lbl-new-button-sm"];
            [removeField setTitle:@"Remove Field" forState:UIControlStateNormal];
            [removeField setBackgroundColor:[UIColor colorWithWhite:0.8 alpha:0.2]];
            [self.view addSubview:removeField];
            [removeField makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 20, 0, 20));
                make.height.equalTo(@40);
                make.top.equalTo(triggerFS.bottom).with.offset(10);
                make.centerX.equalTo(self.view);
            }];
            
            [removeField addTarget:self action:@selector(removeField:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
    }
    
}

- (void)removeField:(id)sender
{
    if (!thisField) {
        return;
    }

    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:@"Are you sure you want to remove this field?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [av showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            if (thisField) {
                [_card removeCard_fieldsObject:thisField];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }];
}

- (void)setCurrentCard:(Card *)thisCard
{
    if (!thisCard) {
        return;
    }
    
    _card = thisCard;
    
}

- (void)addField:(id)sender
{
    // Need to create this....
    if (field) {
        NSMutableDictionary *aField = [[NSMutableDictionary alloc] initWithDictionary:field];
        
        NSString *value = [triggerFS valueForFieldNamed:[field objectForKey:@"title"]];
//        NSString *field_display_title = @"";
//        
//        if ([triggerFS valueForFieldNamed:[NSString stringWithFormat:@"%@%@", [field objectForKey:@"title"], @"_title"]]) {
//            NSString *title = [triggerFS valueForFieldNamed:[NSString stringWithFormat:@"%@%@", [field objectForKey:@"title"], @"_title"]];
//            if (title && ![title isEqualToString:@""]) {
//                field_display_title = title;
//            }
//        }
        
        if ([triggerFS valueForFieldNamed:[NSString stringWithFormat:@"%@%@", [field objectForKey:@"title"], @"_title"]]) {
            if ([aField objectForKey:@"has_title"] && [[aField objectForKey:@"has_title"] integerValue] == 1) {
                NSString *optional_title = [triggerFS valueForFieldNamed:[NSString stringWithFormat:@"%@%@", [field objectForKey:@"title"], @"_title"]];
                if (![optional_title isEqualToString:@""]) {
                    [aField setObject:optional_title forKey:@"field_title"];
                } else {
                    [aField setObject:@"" forKey:@"field_title"];
                }
            }
        }
        
        [aField setObject:value forKey:@"field_value"];
        
        // Will add a new field, OR update existing field if the existing_field param exists
        [_card addFieldWithDetails:aField isFront:[isFrontBool boolValue]];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)removeAction:(id)sender
{
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        [(CreateCardViewController *)self.priorViewController refreshViewWithCard:_card];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
