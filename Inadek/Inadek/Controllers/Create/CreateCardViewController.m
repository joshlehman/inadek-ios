//
//  CreateCardViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CreateCardViewController.h"
#import "AddFieldViewController.h"
#import "AddGalleryViewController.h"
#import "DeckSelectViewController.h"
#import "FieldReorderViewController.h"
#import "DeckViewController.h"

#import "UIAlertView+BCAlertView.h"

@interface CreateCardViewController ()

@end

@implementation CreateCardViewController

- (id)initWithCard:(Card *)thisCard andPage:(NSString *)page
{
    self = [self initWithPage:page];
    if (self) {
        if (thisCard) {
            self.card = thisCard;
        } else {
            self.card = [Card myNew];
        }
    }
    
    return self;
}

- (id)initWithPage:(NSString *)page
{
    self = [super init];
    if (self) {
        
        if (page) {
            self.page = page;
        } else {
            self.page = @"page1";
        }
        
    }
    
    return self;
    
}

- (void)cleanUpUnsavedObject:(BOOL)forceDelete
{
    if (self.card) {
        if (self.card && [self.card.cid isEqualToString:@"!NEW!"]) {
            [self.card delete];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    if (!_card) {
        _card = [Card myNew];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    if (!self.page) {
        self.page = @"page1";
    }
    
    BOOL is_editing = YES;
    if (!self.card) {
        self.card = [Card myNew];
        is_editing = NO;
    }
    
    if ([self.page isEqualToString:@"page1"]) {
        [self loadPageOneView];
    } else if ([self.page isEqualToString:@"page2"]) {
        [self loadPageTwoView];
    }
    
    if (is_editing) {
        [self refreshViewWithCard:self.card];
    }
    
}



- (void)loadPageOneView
{
    
    UIView *container = UIView.new;
    [container setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:container];
    
    [container makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    _typesSV = UIScrollView.new;
    [_typesSV setBackgroundColor:[UIColor clearColor]];
    [container addSubview:_typesSV];
    
    [_typesSV makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(container);
    }];
    
    scrollHolder = UIView.new;
    [_typesSV addSubview:scrollHolder];
    
    [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_typesSV);
        make.width.equalTo(_typesSV);
    }];
    
    
    NSArray *generalCardTypes = [[DeckManager sharedManager] localCardTypes:@{@"segment":@[@"general",@"pro_sports",@"entertainment"]}];
    BCDeckTypeButton *priorObject = nil;
    
    int side_padding = 30;
    NSNumber *side_padding_obj = [NSNumber numberWithInt:side_padding];
    
    int counter = 0;
    for (NSDictionary *dt in generalCardTypes) {
        if ([dt objectForKey:@"cid"]) {
            
            counter += 1;
            BCCardTypeButton *btn = [[BCCardTypeButton alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            [btn drawButtonWithType:dt];
            btn.parentViewController = self;
            
            [scrollHolder addSubview:btn];
            NSNumber *cellWidth = [[NSNumber alloc] initWithInt:((self.view.bounds.size.width / 2) - side_padding - 8)];
            [btn makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(cellWidth);
                make.height.equalTo(@130);
                if (!priorObject) {
                    make.top.equalTo(@30);
                }
                if (counter % 2) {
                    // ODD
                    if (priorObject) {
                        make.top.equalTo(priorObject.bottom).with.offset(16);
                    }
                    make.left.equalTo(side_padding_obj);
                } else {
                    // EVEN
                    if (priorObject) {
                        make.top.equalTo(priorObject.top);
                    }
                    NSNumber *spo_second = [NSNumber numberWithInt:([side_padding_obj integerValue] + [cellWidth integerValue] + 16)];
                    make.left.equalTo(spo_second);
                }
            }];
            
            priorObject = btn;
        } else {
            
        }
    }
    
    if (priorObject) {
        [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(priorObject.bottom).with.offset(100);
        }];
    }
}

- (void)loadPageTwoView
{
    if (_into_deck && _card) {
        [_card addDecksObject:_into_deck];
    }
    
    UIView *container = UIView.new;
    [container setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:container];
    
    [container makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    _typesSV = UIScrollView.new;
    [_typesSV setBackgroundColor:[UIColor clearColor]];
    [container addSubview:_typesSV];
    
    [_typesSV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(container);
        make.left.equalTo(container);
        make.right.equalTo(container);
        make.bottom.equalTo(container.bottom).with.offset(-80);
    }];
    
    scrollHolder = UIView.new;
    [_typesSV addSubview:scrollHolder];
    [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_typesSV);
        make.width.equalTo(_typesSV);
    }];
    
    int total_height = 0;
    
    cardImage = [[BCCardEditImage alloc] initWithFrame:CGRectZero];
    [cardImage setDelegate:self];
    [cardImage setBackgroundColor:[UIColor clearColor]];
    
    if ([_card hasCoverPhoto]) {
        [scrollHolder addSubview:cardImage];
        [cardImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(scrollHolder.width).with.offset(-20);
            deckImageTopCS = make.height.equalTo(scrollHolder.width).with.offset(-160);
            make.centerX.equalTo(scrollHolder.centerX);
            make.top.equalTo(scrollHolder).with.offset(10);
        }];
        
        if ([_card imageObj]) {
            // Existing image, show it here
            [cardImage showImage:[_card imageObj]];
            deckImageTopCS.offset = -15;
            total_height += 320;
            has_image = YES;
            
            [cardImage addImageEditButton];
        } else if (_card.image_url && ![_card.image_url isEqualToString:@""]) {
            [cardImage loadImage:_card.image_url];
            deckImageTopCS.offset = -15;
            total_height += 320;
            has_image = YES;
            
            [cardImage addImageEditButton];
        } else {
            [cardImage buildButtons];
            total_height += 180;
        }
    } else {
        [scrollHolder addSubview:cardImage];
        [cardImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(scrollHolder.width).with.offset(-20);
            make.height.equalTo(@0);
            make.centerX.equalTo(scrollHolder.centerX);
            make.top.equalTo(scrollHolder).with.offset(10);
        }];
    }
    
    
    
    NSString *fieldValue = @"";
    if (_card && _card.name && ![_card.name isEqualToString:@""]) {
        fieldValue = _card.name;
    }
    
    NSArray *fieldsArray = [[NSArray alloc] initWithObjects:
                            @{@"style":[NSNumber numberWithInt:kFieldStyleText],@"placeholder":[Common defaultPlaceholderForCardType:_card withDefault:@"Card Name"],@"name":@"card_name",@"value":fieldValue},
                            nil];
    
    BCFieldSetView *cardTitleFS = [[BCFieldSetView alloc] initWithTitle:nil dataFields:fieldsArray andStyle:@"white"];
    cardTitleFS.delegate = self;
    
    [scrollHolder addSubview:cardTitleFS];
    [cardTitleFS makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cardImage.bottom).with.offset(20);
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([cardTitleFS fieldHeight]);
    }];
    
    total_height += [[cardTitleFS fieldHeight] integerValue] + 20;
    
    fieldsArray = [[NSArray alloc] initWithObjects:
                   @{@"style":[NSNumber numberWithInt:kFieldStyleSelect],
                     @"title":@"Card Type",
                     @"name":@"card_type",
                     @"value":[Common currentCardTypeAsString:_card],
                     @"action_callback":@"doChangeCardType:"},
                   @{@"style":[NSNumber numberWithInt:kFieldStyleMultiselect],
                     @"title":@"Decks",
                     @"name":@"card_decks",
                     @"placeholder":@"None",
                     @"value":[Common currentCardDecksAsString:_card],
                     @"action_callback":@"doChangeDecks:"},
                   @{@"style":[NSNumber numberWithInt:kFieldStyleSwitch],
                     @"title":@"Private Card",
                     @"name":@"is_private",
                     @"value":[_card isPrivateOrDefault],
                     @"action_callback":@"doChangePrivacy:"},
                   nil];
    
    cardGeneralFS = [[BCFieldSetView alloc] initWithTitle:@"GENERAL" dataFields:fieldsArray andStyle:@"white"];
    cardGeneralFS.delegate = self;
    
    [scrollHolder addSubview:cardGeneralFS];
    [cardGeneralFS makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cardTitleFS.bottom);
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([cardGeneralFS fieldHeight]);
    }];
    
    total_height += [[cardGeneralFS fieldHeight] integerValue] + 20;
    
    fieldsArray = [[NSArray alloc] initWithObjects:
                   @{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                     @"title":@"Add Front Content",
                     @"action_callback":@"doAddFrontField:"},
                   
                   nil];
    
    cardFieldsFS = [[BCFieldSetView alloc] initWithTitle:@"FRONT CONTENT" dataFields:fieldsArray andStyle:@"white"];
    cardFieldsFS.delegate = self;
    
    [scrollHolder addSubview:cardFieldsFS];
    [cardFieldsFS makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cardGeneralFS.bottom);
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([cardFieldsFS fieldHeight]);
    }];
    
    
    total_height += [[cardFieldsFS fieldHeight] integerValue] + 20;
    
    fieldsArray = [[NSArray alloc] initWithObjects:
                   @{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                     @"title":@"Add Back Content",
                     @"action_callback":@"doAddBackField:"},
                   
                   nil];
    
    cardBackFieldsFS = [[BCFieldSetView alloc] initWithTitle:@"BACK CONTENT" dataFields:fieldsArray andStyle:@"white"];
    cardBackFieldsFS.delegate = self;
    
    [scrollHolder addSubview:cardBackFieldsFS];
    [cardBackFieldsFS makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cardFieldsFS.bottom);
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([cardBackFieldsFS fieldHeight]);
    }];
    
    
    total_height += [[cardBackFieldsFS fieldHeight] integerValue] + 20;
    
    
    if ([[Common triggersForCardTypeId:_card.type_id] count] > 0) {
        
        NSMutableArray *mFieldsArray = NSMutableArray.new;
        
        for (NSDictionary *trigger in [Common triggersForCardTypeId:_card.type_id]) {
            NSString *trigger_name = [NSString stringWithFormat:@"trigger_%@", [trigger objectForKey:@"trigger_type"]];
            
            NSMutableDictionary *t = NSMutableDictionary.new;
            NSNumber *style = [NSNumber numberWithInt:kFieldStyleTextSelect];
            
            if ([[trigger objectForKey:@"trigger_type"] isEqualToString:@"phone"]) {
                style = [NSNumber numberWithInt:kFieldStyleTextSelectPhone];
            } else if ([[trigger objectForKey:@"trigger_type"] isEqualToString:@"email"]) {
                style = [NSNumber numberWithInt:kFieldStyleTextSelectEmail];
            }
            
            [t setObject:style forKey:@"style"];
            [t setObject:[trigger objectForKey:@"name"] forKey:@"title"];
            [t setObject:trigger_name forKey:@"name"];
            [t setObject:@"Off" forKey:@"placeholder"];
            [t setObject:@"" forKey:@"value"];
            [mFieldsArray addObject:t];
        }
        
        fieldsArray = [self getCardTriggerFieldsArray];
        
        cardTriggersFS = [[BCFieldSetView alloc] initWithTitle:@"ACTIONS" dataFields:fieldsArray andStyle:@"white"];
        cardTriggersFS.delegate = self;
        
        [scrollHolder addSubview:cardTriggersFS];
        [cardTriggersFS makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(cardBackFieldsFS.bottom);
            make.width.equalTo(scrollHolder.width);
            make.height.equalTo([cardTriggersFS fieldHeight]);
        }];
        
        total_height += [[cardTriggersFS fieldHeight] integerValue] + 20;
    }

    NSArray *deleteFieldsArray = [[NSArray alloc] initWithObjects:
//                                  @{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
//                                    @"title":@"Save As Template",
//                                    @"action_callback":@"doSaveAs:"},
                                  @{@"style":[NSNumber numberWithInt:kFieldStyleDeleteButton],
                                    @"title":@"Delete Card",
                                    @"action_callback":@"doDelete:"},
                                  nil];
    
    cardDeleteFS = [[BCFieldSetView alloc] initWithTitle:@"" dataFields:deleteFieldsArray andStyle:@"white"];
    cardDeleteFS.delegate = self;
    
    [scrollHolder addSubview:cardDeleteFS];
    [cardDeleteFS makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([cardDeleteFS fieldHeight]);
    }];
    
    total_height += [[cardDeleteFS fieldHeight] integerValue] + 20;
    
    current_total_height = total_height;
    
    [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
        scrollHolderHeightCS = make.height.equalTo(@0).with.offset(total_height);
    }];
    
    
    footerButton = [[BCFooterActionButton alloc] initWithFrame:CGRectZero];
    footerButton.delegate = self;
    [self.view addSubview:footerButton];
    [footerButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@80);
        make.width.equalTo(scrollHolder);
        make.bottom.equalTo(self.view);
        make.centerX.equalTo(self.view);
    }];
    
    if (isEditing) {
        [footerButton setTitle:@"SAVE CHANGES"];
    }
    
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    
    imagePicker.mediaTypes = [NSArray arrayWithObjects: (NSString *) kUTTypeImage, (NSString *) kUTTypeMovie, nil];
    imagePicker.allowsEditing = YES;
}

- (NSArray *)getCardFieldsArray
{
    // Get all the existing card fields in order...
    
    NSArray *fieldsArray = NSArray.new;
    
    if (!_card) {
        return fieldsArray;
    }
    
    if ([[_card cardFrontFields] count] > 0) {
        NSMutableArray *arr = NSMutableArray.new;
        for (CardField *field in [_card cardFrontFields]) {
            if ([field fieldIsPhotoGallery]) {
                [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStylePhotos],
                                 @"value":[field fieldValueAsText],
                                 @"title":@"Photo Gallery",
                                 @"name":field.title,
                                 @"has_title":[Common sanitizedNumber:field.has_title],
                                 @"action_callback":@"doChangeField:",
                                 @"action_object":field}];
            } else {
                [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleSelectText],
                                 @"value":[field fieldValueAsText],
                                 @"name":field.title,
                                 @"has_title":[Common sanitizedNumber:field.has_title],
                                 @"action_callback":@"doChangeField:",
                                 @"action_object":field}];
            }
        }
        
        
        [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                         @"title":@"Add Front Content",
                         @"action_callback":@"doAddFrontField:"}];
        
        if ([[_card cardFrontFields] count] > 1) {
            [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleReorderButton],
                         @"title":@"Reorder",
                         @"action_callback":@"doReorderFrontFields:"}];
        }
        
        fieldsArray = [[NSArray alloc] initWithArray:arr];
        
    } else {

        
        NSMutableArray *arr = NSMutableArray.new;
        
        [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                         @"title":@"Add Front Content",
                         @"action_callback":@"doAddFrontField:"}];
        
        fieldsArray = [[NSArray alloc] initWithArray:arr];
    }
    
    return fieldsArray;
    
}

- (NSArray *)getCardBackFieldsArray
{
    // Get all the existing card fields in order...
    
    NSArray *fieldsArray = NSArray.new;
    
    if (!_card) {
        return fieldsArray;
    }
    
    if ([[_card cardBackFields] count] > 0) {
        NSMutableArray *arr = NSMutableArray.new;
        for (CardField *field in [_card cardBackFields]) {
            if ([field fieldIsPhotoGallery]) {
                [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStylePhotos],
                                 @"value":[field fieldValueAsText],
                                 @"title":@"Photo Gallery",
                                 @"name":field.title,
                                 @"has_title":[Common sanitizedNumber:field.has_title],
                                 @"action_callback":@"doChangeField:",
                                 @"action_object":field}];
            } else {
                [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleSelectText],
                                 @"value":[field fieldValueAsText],
                                 @"name":field.title,
                                 @"has_title":[Common sanitizedNumber:field.has_title],
                                 @"action_callback":@"doChangeField:",
                                 @"action_object":field}];
            }
        }
        
        
        
        [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                         @"title":@"Add Back Content",
                         @"action_callback":@"doAddBackField:"}];
        
        if ([[_card cardBackFields] count] > 1) {
            [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleReorderButton],
                             @"title":@"Reorder",
                             @"action_callback":@"doReorderBackFields:"}];
        }
        
        fieldsArray = [[NSArray alloc] initWithArray:arr];
        
    } else {
        fieldsArray = [[NSArray alloc] initWithObjects:
                       @{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                         @"title":@"Add Back Content",
                         @"action_callback":@"doAddBackField:"},
                       nil];
    }
    
    return fieldsArray;
    
}

- (NSArray *)getCardTriggerFieldsArray
{
    
    NSArray *fieldsArray = NSArray.new;
    
    if (!_card) {
        return fieldsArray;
    }
    
    if ([[_card.triggers allObjects] count] > 0) {
        NSMutableArray *arr = NSMutableArray.new;
        for (CardTrigger *trig in [_card.triggers allObjects]) {
            [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleSelect],
                             @"title":trig.name,
                             @"name":[NSString stringWithFormat:@"trigger_%@", trig.trigger_type],
                             @"value":trig.value,
                             @"action_callback":@"doChangeTrigger:",
                             @"action_object":trig}];
        }
        
        [arr addObject:@{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                         @"title":@"Add Card Action",
                         @"action_callback":@"doAddAction:"}];
        
        fieldsArray = [[NSArray alloc] initWithArray:arr];
        
    } else {
        fieldsArray = [[NSArray alloc] initWithObjects:
                       @{@"style":[NSNumber numberWithInt:kFieldStyleAddButton],
                         @"title":@"Add Card Action",
                         @"action_callback":@"doAddAction:"},
                       nil];
    }
    
    return fieldsArray;
    
}

- (void)startCardType:(NSString *)cardCid
{
    NSDictionary *cardTypeDetails = [[DeckManager sharedManager] cardTypeDetailsByCid:cardCid];
    
    if (!cardTypeDetails) {
        //TODO: Fix this to show an error
        return;
    }
    
    _card.type_id = [cardTypeDetails objectForKey:@"id"];
    
    if (self.priorViewController && [self.priorViewController isKindOfClass:[CreateCardViewController class]]) {
        
        [(CreateCardViewController *)self.priorViewController refreshViewWithCard:_card];
        [[self navigationController] popViewControllerAnimated:YES];
        
    } else {
        
        CreateCardViewController *startCard = [[CreateCardViewController alloc] initWithCard:_card andPage:@"page2"];
        startCard.priorViewController = self;
        startCard.delegateViewController = self.delegateViewController;
        
        if (_into_deck && _card) {
            startCard.into_deck = _into_deck;
        }
        
        if (cardTypeDetails && [cardTypeDetails objectForKey:@"name"]) {
            startCard.title = [NSString stringWithFormat:@"%@ %@", [cardTypeDetails objectForKey:@"name"], @"Card"];
        } else {
            startCard.title = @"Card";
        }
        
        [self.navigationController pushViewController:startCard animated:YES];
        
    }
    
}

- (void)footerActionButton:(BCFooterActionButton *)button didContinue:(BOOL)didContinue
{
    [self.view endEditing:YES];
    [self saveCard];
}

- (void)setEditingMode:(BOOL)mode
{
    if (mode) {
        isEditing = YES;
    } else {
        isEditing = NO;
    }
}

- (void)deleteCard
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Deleting Card";
    
    BOOL didDelete = [[DeckManager sharedManager] deleteCard:_card callback:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (self.delegateViewController && [self.delegateViewController respondsToSelector:@selector(reloadCardsAfterEdit:)]) {
            [(DeckViewController *)self.delegateViewController reloadCardsAfterEdit:nil];
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } failureCallback:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)saveCard
{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Updating Your Card";
    
    if ([_card.cid isEqualToString:@"!NEW!"]) {
        [_card setCid:@"!PENDING!"];
        [_card save];
        hud.labelText = @"Creating Your Card";
    }

    // Dump the junk cards that are being created while I cry...
    NSMutableArray *cardsToDelete = NSMutableArray.new;
    for (Card *d in [Card all]) {
        if (d.cid == nil || [d.cid isEqualToString:@""]) {
            [cardsToDelete addObject:d];
        }
    }
    
    for (Card *d in cardsToDelete) {
        [d delete];
    }
    
    if (temporary_image) {
        _card.base_color = [Common getHexForAverageColor:temporary_image];
        [_card setImageObj:temporary_image];
    }
    
    
    BOOL didSave = [[DeckManager sharedManager] saveCard:_card callback:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (self.delegateViewController && [self.delegateViewController respondsToSelector:@selector(reloadCardsAfterEdit:)]) {
            [(DeckViewController *)self.delegateViewController reloadCardsAfterEdit:nil];
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } failureCallback:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    if (!didSave) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Unable to Save" message:@"Please ensure your card has either a photo or a title, at mininum, and you have added it to at least one deck in order to save." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [av show];
        
    }
}

- (void)doSelectPhotoType:(id)sender
{
    NSMutableArray *mFieldsArray = NSMutableArray.new;
    
    [mFieldsArray addObject:@{@"name":@"Take a Photo",@"id":@"camera"}];
    [mFieldsArray addObject:@{@"name":@"Select from Camera Roll",@"id":@"cameraroll"}];
    
    BCGroupedActionSheetView *actionSheet = [[BCGroupedActionSheetView alloc] initWithOptions:mFieldsArray withTitle:@"Change Photo" andFrame:CGRectZero];
    actionSheet.delegate = self;
    actionSheet.sheetCid = @"change_photo";
    
    [actionSheet showInView:self.view];
}

- (void)cardEditImage:(BCCardEditImage *)image selectImage:(NSString *)mode
{
    if (mode && [mode isEqualToString:@"show_picker"]) {
        [self.view endEditing:YES];
        [self doSelectPhotoType:nil];
    } else {
        [self.view endEditing:YES];
        [self openPhotoAccess:mode];
    }
}

- (void)openPhotoAccess:(NSString *)mode
{
    
    if ([mode isEqualToString:@"cameraroll"]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *)kUTTypeImage, nil];
    } else if ([mode isEqualToString:@"camera"]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *)kUTTypeImage, nil];
        [imagePicker takePicture];
    }
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fieldSetView:(BCFieldSetView *)view didChangeField:(NSString *)field withValue:(id)value
{
    if ([field isEqualToString:@"card_name"]) {
        _card.name = (NSString *)value;
    }
}

- (void)fieldSetView:(BCFieldSetView *)view didCallAction:(NSString *)selector withInfo:(NSDictionary *)userInfo
{
    [self.view endEditing:YES];
    if (selector == nil) {
        return;
    }
    
    SEL s = NSSelectorFromString(selector);
    if ([self respondsToSelector:s]) {
        [self performSelectorOnMainThread:s withObject:userInfo waitUntilDone:NO];
    } else {
        
    }
}

- (void)fieldSetView:(BCFieldSetView *)view didBeginEditing:(id)textField
{
    if (_typesSV && textField) {
        CGPoint svos = _typesSV.contentOffset;
        
        CGPoint pt;
        CGRect rc = [textField bounds];
        
        rc = [textField convertRect:rc toView:_typesSV];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= 60;
        
        [_typesSV setContentOffset:pt animated:YES];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        CGRect cropRect = [info[UIImagePickerControllerCropRect] CGRectValue];
        UIImage *scaledImage = [Common scaleAndRotateImage:[Common cropImage:image withRect:cropRect]];
        
        if (scaledImage) {
            
            temporary_image = scaledImage;
            [cardImage showImage:temporary_image];
            deckImageTopCS.offset = -15;
            if (has_image) {
                scrollHolderHeightCS.offset = current_total_height;
            } else {
                has_image = YES;
                current_total_height = current_total_height + 160;
                scrollHolderHeightCS.offset = current_total_height;
            }
        }
        
    } else {
        // Video not yet supported
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void)doChangeCardType:(id)object
{
    CreateCardViewController *cardTypeVC = [[CreateCardViewController alloc] initWithCard:_card andPage:@"page1"];
    cardTypeVC.priorViewController = self;
    cardTypeVC.delegateViewController = self.delegateViewController;
    [cardTypeVC setTitle:@"Change Card Type"];
    [self.navigationController pushViewController:cardTypeVC animated:YES];
}

- (void)doChangeTrigger:(id)object
{
    [self editTrigger:[(NSDictionary *)object objectForKey:@"object"]];
}

- (void)doDelete:(id)sender
{
    UIAlertView *sure = [[UIAlertView alloc] initWithTitle:@"Delete Card?" message:@"Are you sure you want to delete this card?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [sure showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [self deleteCard];
        }
    }];
    
}

- (void)doAddAction:(id)sender
{
    NSMutableArray *mFieldsArray = NSMutableArray.new;
    
    for (NSDictionary *trigger in [Common triggersForCardTypeId:_card.type_id]) {
        NSString *trigger_name = [NSString stringWithFormat:@"trigger_%@", [trigger objectForKey:@"trigger_type"]];
        
        NSMutableDictionary *t = NSMutableDictionary.new;
        
        [t setObject:[trigger objectForKey:@"name"] forKey:@"name"];
        [t setObject:trigger_name forKey:@"id"];
        [mFieldsArray addObject:t];
    }
    
    BCGroupedActionSheetView *actionSheet = [[BCGroupedActionSheetView alloc] initWithOptions:mFieldsArray withTitle:@"Select a Deck Action" andFrame:CGRectZero];
    actionSheet.delegate = self;
    actionSheet.sheetCid = @"card_triggers";
    
    [actionSheet showInView:self.view];
}

- (void)doChangeDecks:(id)object
{
    
    DeckSelectViewController *deckSelectVC = [[DeckSelectViewController alloc] init];
    [deckSelectVC setTitle:@"Select Decks"];
    [deckSelectVC setPriorViewController:self];
    [deckSelectVC setCurrentCard:_card];
    [self.navigationController pushViewController:deckSelectVC animated:YES];
    
}

- (void)doReorderFrontFields:(id)sender
{
    FieldReorderViewController *reorderVC = [[FieldReorderViewController alloc] init];
    [reorderVC setFields:[[_card cardFrontFields] mutableCopy]];
    reorderVC.priorViewController = self;
    reorderVC.card = _card;
    [reorderVC setTitle:@"Re-order Front Fields"];
    
    [self.navigationController pushViewController:reorderVC animated:YES];
}

- (void)doReorderBackFields:(id)sender
{
    FieldReorderViewController *reorderVC = [[FieldReorderViewController alloc] init];
    [reorderVC setFields:[[_card cardBackFields] mutableCopy]];
    reorderVC.priorViewController = self;
    reorderVC.card = _card;
    [reorderVC setTitle:@"Re-order Back Fields"];
    
    [self.navigationController pushViewController:reorderVC animated:YES];
}

- (void)doAddFrontField:(id)sender
{
    NSMutableArray *mFieldsArray = NSMutableArray.new;
    
    for (NSDictionary *field in [Common fieldsForCardTypeId:_card.type_id]) {
        NSString *field_name = [NSString stringWithFormat:@"field_%@", [field objectForKey:@"cid"]];
        
        NSMutableDictionary *t = NSMutableDictionary.new;
        
        [t setObject:[field objectForKey:@"name"] forKey:@"name"];
        [t setObject:field_name forKey:@"id"];
        [mFieldsArray addObject:t];
    }
    
    BCGroupedActionSheetView *actionSheet = [[BCGroupedActionSheetView alloc] initWithOptions:mFieldsArray withTitle:@"Select a Field Type" andFrame:CGRectZero];
    actionSheet.delegate = self;
    actionSheet.sheetCid = @"card_front_fields";
    
    [actionSheet showInView:self.view];
}

- (void)doAddBackField:(id)sender
{
    NSMutableArray *mFieldsArray = NSMutableArray.new;
    
    for (NSDictionary *field in [Common fieldsForCardTypeId:_card.type_id]) {
        NSString *field_name = [NSString stringWithFormat:@"field_%@", [field objectForKey:@"cid"]];
        
        NSMutableDictionary *t = NSMutableDictionary.new;
        
        [t setObject:[field objectForKey:@"name"] forKey:@"name"];
        [t setObject:field_name forKey:@"id"];
        [mFieldsArray addObject:t];
    }
    
    BCGroupedActionSheetView *actionSheet = [[BCGroupedActionSheetView alloc] initWithOptions:mFieldsArray withTitle:@"Select a Field Type" andFrame:CGRectZero];
    actionSheet.delegate = self;
    actionSheet.sheetCid = @"card_back_fields";
    
    [actionSheet showInView:self.view];
}

- (void)groupedActionSheet:(BCGroupedActionSheetView *)actionSheet selectedItemWithCid:(NSString *)cid andGroup:(NSString *)group
{
    if (group && [group isEqualToString:@"card_triggers"]) {
        [self addTrigger:cid];
    } else if (group && [group isEqualToString:@"card_front_fields"]) {
        [self addFrontField:cid];
    } else if (group && [group isEqualToString:@"card_back_fields"]) {
        [self addBackField:cid];
    } else if (group && [group isEqualToString:@"change_photo"]) {
        [self openPhotoAccess:cid];
    } else {
        
    }
}

- (void)groupedActionSheetDidCancel:(BCGroupedActionSheetView *)actionSheet
{
    
}

- (void)editTrigger:(DeckTrigger *)trigger
{
    if (trigger) {
        AddTriggerViewController *addTriggerVC = [[AddTriggerViewController alloc] initWithTrigger:trigger];
        [addTriggerVC setTitle:@"Edit Action"];
        [addTriggerVC setPriorViewController:self];
        [addTriggerVC setCurrentCard:_card];
        
        [self.navigationController pushViewController:addTriggerVC animated:YES];
    }
}

- (void)addTrigger:(NSString *)triggerType
{
    AddTriggerViewController *addTriggerVC = [[AddTriggerViewController alloc] initWithTriggerType:triggerType];
    [addTriggerVC setTitle:@"Add Action"];
    [addTriggerVC setPriorViewController:self];
    [addTriggerVC setCurrentCard:_card];
    
    [self.navigationController pushViewController:addTriggerVC animated:YES];
}

- (void)editField:(DeckTrigger *)trigger
{
    if (trigger) {
        AddTriggerViewController *addTriggerVC = [[AddTriggerViewController alloc] initWithTrigger:trigger];
        [addTriggerVC setTitle:@"Edit Action"];
        [addTriggerVC setPriorViewController:self];
        [addTriggerVC setCurrentCard:_card];
        
        [self.navigationController pushViewController:addTriggerVC animated:YES];
    }
}

- (void)doChangeField:(NSDictionary *)cardField
{
    [Common log:@"Field" withObject:cardField];
    
    CardField *cf = [cardField objectForKey:@"object"];
    
    if (cf && [Common isFieldTypePhotos:cf.field_cid]) {
        NSLog(@"You want the photos, don't you!");
        AddGalleryViewController *addVC = [[AddGalleryViewController alloc] initWithField:cf];
        [addVC setCard:_card];
        [addVC setPriorViewController:self];

        [self.navigationController pushViewController:addVC animated:YES];
    } else {
        if (cardField && [cardField objectForKey:@"object"]) {
            AddFieldViewController *editFieldVC = [[AddFieldViewController alloc] initWithField:[cardField objectForKey:@"object"]];
            [editFieldVC setTitle:@"Edit"];
            [editFieldVC setPriorViewController:self];
            [editFieldVC setCurrentCard:_card];
            
            [self.navigationController pushViewController:editFieldVC animated:YES];
        }
    }
}


- (void)addFrontField:(NSString *)fieldType
{
    if ([Common isFieldTypePhotos:fieldType]) {
        NSLog(@"You want the photos, don't you!");
        AddGalleryViewController *addVC = [[AddGalleryViewController alloc] initWithFieldCID:fieldType isFront:YES];
        [addVC setCard:_card];
        [addVC setPriorViewController:self];
        
        [self.navigationController pushViewController:addVC animated:YES];
        
    } else {
        AddFieldViewController *addFieldVC = [[AddFieldViewController alloc] initWithFieldCID:fieldType isFront:YES andCard:_card];
        [addFieldVC setTitle:@"Add Front Content"];
        [addFieldVC setPriorViewController:self];
        [addFieldVC setCurrentCard:_card];
        
        [self.navigationController pushViewController:addFieldVC animated:YES];
    }

}

- (void)addBackField:(NSString *)fieldType
{
    if ([Common isFieldTypePhotos:fieldType]) {
        NSLog(@"You want the photos, don't you!");
        AddGalleryViewController *addVC = [[AddGalleryViewController alloc] initWithFieldCID:fieldType isFront:NO];
        [addVC setCard:_card];
        [addVC setPriorViewController:self];
        
        [self.navigationController pushViewController:addVC animated:YES];
        
    } else {
        
        if (_card.hasBackWebContent) {
            
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Web Content" message:@"The back of your card currently contains full web content which will take up the entire back of the card. If you'd like to add or use other fields, please first remove the Web Content field." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            
            [av showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                
            }];
            
        } else {
            AddFieldViewController *addFieldVC = [[AddFieldViewController alloc] initWithFieldCID:fieldType isFront:NO andCard:_card];
            [addFieldVC setTitle:@"Add Back Content"];
            [addFieldVC setPriorViewController:self];
            [addFieldVC setCurrentCard:_card];
        
            [self.navigationController pushViewController:addFieldVC animated:YES];
        }
    }
}

- (void)refreshViewWithCard:(Card *)cardObject
{
    _card = cardObject;
    [cardGeneralFS updateValueForFieldNamed:@"card_decks" withValue:[Common currentCardDecksAsString:cardObject]];
    [cardGeneralFS updateValueForFieldNamed:@"card_types" withValue:[Common currentCardTypeAsString:cardObject]];
    
    if (cardFieldsFS) {
        
        current_total_height = current_total_height - cardFieldsFS.bounds.size.height;
        
        [cardFieldsFS removeFromSuperview];
        
        NSArray *valueFields = [self getCardFieldsArray];
        [cardFieldsFS rebuildFields:valueFields];
        
        cardFieldsFS.delegate = self;
        
        [scrollHolder addSubview:cardFieldsFS];
        [cardFieldsFS updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(cardGeneralFS.bottom);
            make.width.equalTo(scrollHolder.width);
            make.height.equalTo([cardFieldsFS fieldHeight]);
        }];
    
        
        current_total_height += [[cardFieldsFS fieldHeight] integerValue];
        
        
    }
    
    if (cardBackFieldsFS) {
        
        current_total_height = current_total_height - cardBackFieldsFS.bounds.size.height;
        
        [cardBackFieldsFS removeFromSuperview];
        
        NSArray *valueFields = [self getCardBackFieldsArray];
        [cardBackFieldsFS rebuildFields:valueFields];
        
        cardBackFieldsFS.delegate = self;
        
        [scrollHolder addSubview:cardBackFieldsFS];
        [cardBackFieldsFS updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(cardFieldsFS.bottom);
            make.width.equalTo(scrollHolder.width);
            make.height.equalTo([cardBackFieldsFS fieldHeight]);
        }];
        
        NSLog(@"Why? %@", [cardGeneralFS fieldHeight]);
        NSLog(@"Me? %d", current_total_height);
        
        current_total_height += [[cardBackFieldsFS fieldHeight] integerValue];
        
    }
    
    [scrollHolder updateConstraints:^(MASConstraintMaker *make) {
        scrollHolderHeightCS = make.height.equalTo(@0).with.offset(current_total_height);
    }];
    
    
    if (cardTriggersFS) {
        // Need to rebuild the field data, then pass it into the deckTriggersFS view to re-draw
        
        current_total_height = current_total_height - cardTriggersFS.bounds.size.height;
        [cardTriggersFS removeFromSuperview];
        
        NSArray *triggerFields = [self getCardTriggerFieldsArray];
        [cardTriggersFS rebuildFields:triggerFields];
        
        cardTriggersFS.delegate = self;
        
        [scrollHolder addSubview:cardTriggersFS];
        [cardTriggersFS updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(cardBackFieldsFS.bottom);
            make.width.equalTo(scrollHolder.width);
            make.height.equalTo([cardTriggersFS fieldHeight]);
        }];
        
        current_total_height += [[cardTriggersFS fieldHeight] integerValue];
        
        if (cardDeleteFS) {
            [cardDeleteFS updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(cardTriggersFS.bottom).with.offset(40);
            }];
            
            current_total_height += [[cardDeleteFS fieldHeight] integerValue];
            
        }
        
        
        [scrollHolder updateConstraints:^(MASConstraintMaker *make) {
            scrollHolderHeightCS = make.height.equalTo(@0).with.offset(current_total_height);
        }];
        
    } else {
        
        if (cardDeleteFS) {
            
            
            if (cardBackFieldsFS) {
                [cardDeleteFS updateConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(cardBackFieldsFS.bottom).with.offset(40);
                }];
                
                current_total_height += [[cardDeleteFS fieldHeight] integerValue];
                
            } else if (cardFieldsFS) {
                [cardDeleteFS updateConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(cardFieldsFS.bottom).with.offset(40);
                }];
                
                current_total_height += [[cardDeleteFS fieldHeight] integerValue];
                
            }
            
            [scrollHolder updateConstraints:^(MASConstraintMaker *make) {
                scrollHolderHeightCS = make.height.equalTo(@0).with.offset(current_total_height);
            }];
            
        }
        
    }
}

- (IBAction)popCurrentViewController:(id)sender
{
    if (self.navigationController) {
        [sender setSelected:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        if (self.priorViewController) {
            if ([self.priorViewController isKindOfClass:[CreateCardViewController class]]) {
                //[(CreateDeckViewController *)self.priorViewController setDeck:nil];
            } else {
                [_card delete];
            }
        }
        
    }
}


@end
