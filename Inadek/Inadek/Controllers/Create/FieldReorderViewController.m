//
//  FieldReorderViewController.m
//  Inadek
//
//  Created by Josh Lehman on 6/13/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "UIBCNavButton.h"
#import "BCNavController.h"
#import "FieldReorderViewController.h"
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import "CategoryTableViewCell.h"
#import "CreateCardViewController.h"

#import "CardField+Core.h"

@interface FieldReorderViewController ()

@end

@implementation FieldReorderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.rightBarButtonItem = [(BCNavController *)self.navigationController getNavBarItem:kBCNavButtonStyleText withTitle:@"SAVE" andPosition:kBCNavPositionRight target:self action:@selector(updateOrder:)];
    
    
    reorderTV = UITableView.new;
    reorderTV.delegate = self;
    reorderTV.dataSource = self;
    [reorderTV setEditing:YES];
    [self.view addSubview:reorderTV];
    [reorderTV makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    if (reorderTV) {
        [reorderTV reloadData];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        if (_card) {
            [(CreateCardViewController *)self.priorViewController refreshViewWithCard:_card];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_fields count];    //count number of row from counting array hear cataGorry is An Array
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    CardField *field = [_fields objectAtIndex:sourceIndexPath.row];
    [_fields removeObjectAtIndex:sourceIndexPath.row];
    [_fields insertObject:field atIndex:destinationIndexPath.row];
    
    DLog(@"Fields");
    DLog(@"%@",_fields);
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[CategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:MyIdentifier];
    }
    
    if ([_fields count] > indexPath.row) {
        CardField *thisField = [_fields objectAtIndex:indexPath.row];
        
        if (thisField.title && [thisField.title isKindOfClass:[NSString class]] && ![thisField.title isEqualToString:@""]) {
            [cell.label setText:[thisField title]];
        } else {
            if (thisField.field_value && [thisField.field_value isKindOfClass:[NSString class]]) {
                [cell.label setText:[thisField fieldShortValueAsText]];
            }
        }
        
        
//        DeckCategory *cat = [categories objectAtIndex:indexPath.row];
//        if (cat) {
//            [cell.label setText:cat.name];
//            if ([selectedCategories containsObject:cat]) {
//                [cell setChecked:YES];
//            }
//            [cell setCategory:cat];
//        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryTableViewCell *cell = (CategoryTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if ([cell isChecked]) {
        // Cell was selected
        //[_deck addCategoriesObject:cell.category];
        
    } else {
        // Cell was deselected
        //[_deck removeCategoriesObject:cell.category];
    }
}

- (void)updateOrder:(id)sender
{
    if (_card) {
        int index = 1;
        for (CardField *c in _fields) {
            for (CardField *f in _card.card_fields) {
                if (c == f) {
                    f.position = [NSNumber numberWithInteger:index];
                    [f save];
                }
            }
            
            index = index + 1;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
