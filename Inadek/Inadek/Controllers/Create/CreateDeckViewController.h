//
//  CreateDeckViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "Deck+Core.h"
#import "DeckTrigger+Core.h"
#import "Common.h"
#import "MBProgressHUD.h"

#import "BCDeckTypeButton.h"
#import "BCDeckEditImage.h"
#import "BCFieldSetView.h"
#import "BCFooterActionButton.h"
#import "CategorySelectViewController.h"
#import "BCGroupedActionSheetView.h"
#import "AddTriggerViewController.h"

@interface CreateDeckViewController : BCViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, BCDeckEditImageDelegate, BCFooterActionButtonDelegate, BCFieldSetDelegate, BCGroupedActionSheetDelegate> {
    UIImagePickerController *imagePicker;
    
    BCFieldSetView *deckGeneralFS;
    BCFieldSetView *deckTriggersFS;
    BCFieldSetView *deckDeleteFS;
    BCDeckEditImage *deckImage;
    
    MASConstraint *deckImageTopCS;
    MASConstraint *scrollHolderHeightCS;
    
    BOOL has_image;
    int current_total_height;
    
    UIView *scrollHolder;
    
    BCFooterActionButton *footerButton;
    BOOL isEditing;
    
    UIImage *temporary_image;
}

@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) BCViewController *priorViewController;
@property (nonatomic, retain) NSString *page;
@property (nonatomic, retain) UIScrollView *typesSV;

- (id)initWithDeck:(Deck *)thisDeck andPage:(NSString *)page;
- (id)initWithPage:(NSString *)page;
- (void)startDeckType:(NSString *)deckCid;

- (void)setEditingMode:(BOOL)mode;

- (void)refreshViewWithDeck:(Deck *)deckObject;
- (void)cleanUpUnsavedObject:(BOOL)forceDelete;

@end
