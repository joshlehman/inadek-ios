//
//  AddGalleryViewController.m
//  Inadek
//
//  Created by Josh Lehman on 11/16/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "AddGalleryViewController.h"
#import "ImageCVCell.h"
#import "BCNavController.h"
#import "BCGroupedActionSheetView.h"
#import "UzysAssetsPickerController.h"
#import "CreateCardViewController.h"

@interface AddGalleryViewController () {
    CardField *thisField;
}

@end

@implementation AddGalleryViewController

- (id)initWithFieldCID:(NSString *)fieldCID isFront:(BOOL)front
{
    self = [self init];
    if (self) {
        
        isFrontBool = [NSNumber numberWithBool:front];
        NSArray *fields = [[NSUserDefaults standardUserDefaults] objectForKey:@"fields"];
        for (NSDictionary *t in fields) {
            NSString *aField = [fieldCID stringByReplacingOccurrencesOfString:@"field_" withString:@""];
            if ([[t objectForKey:@"cid"] isEqualToString:aField]) {
                
                // Single image
                if ([[t objectForKey:@"value_type"] isEqualToString:@"image"]) {
                    
                }
                
                // Image gallery
                if ([[t objectForKey:@"value_type"] isEqualToString:@"images"]) {
                    field = @{@"has_title":[t objectForKey:@"has_title"],@"field_title":[t objectForKey:@"name"],@"title":@"",@"cid":aField,@"placeholder":@"",@"field_value":@"",@"value_type":[t objectForKey:@"value_type"]};
                }
                
                break;
            }
        }
    }
    
    return self;
}

- (id)initWithField:(CardField *)aField {
    
    self = [self init];
    if (self) {
        
        isFrontBool = aField.on_front;
        
        NSString *placeholder = @"";
        if (aField.placeholder) {
            placeholder = aField.placeholder;
        }
        
        NSNumber *has_title = [NSNumber numberWithBool:NO];
        if (aField.has_title) {
            has_title = aField.has_title;
        }
        field = @{@"has_title":has_title, @"field_value":aField.field_value,@"field_title":aField.title,@"title":aField.title,@"cid":aField.field_cid,@"placeholder":placeholder,@"existing_field":aField,@"value_type":aField.value_type};
        
        isEditing = YES;
        thisField = aField;
        
    }
    
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"Photo Gallery"];
    
    self.navigationItem.rightBarButtonItem = [(BCNavController *)self.navigationController getNavBarItem:kBCNavButtonStyleText withTitle:@"DONE" andPosition:kBCNavPositionRight target:self action:@selector(addField:)];
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    
    _images = NSMutableArray.new;
    
    if (self.card) {
        
    }
    
    UILabel *triggerType = UILabel.new;
    [triggerType addStyleClass:@"lbl-trigger-type"];
    [self.view addSubview:triggerType];
    [triggerType makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@13);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.equalTo(@30);
    }];
    
    if ([field objectForKey:@"field_title"] && ![[field objectForKey:@"field_title"] isEqualToString:@""]) {
        [triggerType setText:[field objectForKey:@"field_title"]];
    } else {
        [triggerType setText:[CardField getDefaultTitleForCid:[field objectForKey:@"cid"]]];
    }
    
    NSString *value = @"";
    NSString *title = @"";
    if ([field objectForKey:@"field_value"]) {
        value = [field objectForKey:@"field_value"];
        title = [field objectForKey:@"title"];
    }
    
    NSArray *fieldsArray = NSArray.new;
    
    CGSize cvCellSize = CGSizeMake(112, 112);
    
    if (IS_IPHONE_6P) {
        cvCellSize = CGSizeMake(112, 112);
    }
    
    else if (IS_IPHONE_6) {
        cvCellSize = CGSizeMake(108, 108);
    }
    
    else {
        cvCellSize = CGSizeMake(90, 90);
    }
    
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.itemSize = cvCellSize;
    [flowLayout setSectionInset:UIEdgeInsetsMake(0, 18, 0, 18)];
    
    self.imagesCV = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.imagesCV.backgroundColor = [UIColor colorWithHexString:@"F0EEEA"];
    
    // Register the collection cell
    [self.imagesCV registerClass:[ImageCVCell class] forCellWithReuseIdentifier:@"imageCell"];
    
    [self.imagesCV setDataSource:self];
    [self.imagesCV setDelegate:self];
    [self.view addSubview:self.imagesCV];
    [self.imagesCV makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).with.offset(-80);
        make.top.equalTo(triggerType.bottom).with.offset(10);
    }];
    
    footerButton = [[BCFooterActionButton alloc] initWithFrame:CGRectZero];
    footerButton.delegate = self;
    [self.view addSubview:footerButton];
    [footerButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@80);
        make.width.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.centerX.equalTo(self.view);
    }];
    
    [footerButton setTitle:@"+ ADD PHOTOS"];

    UzysAppearanceConfig *appearanceConfig = [[UzysAppearanceConfig alloc] init];
    appearanceConfig.finishSelectionButtonColor = [UIColor colorWithHexString:@"F15400"];
    [UzysAssetsPickerController setUpAppearanceConfig:appearanceConfig];
    
    imagePicker = [[UzysAssetsPickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.maximumNumberOfSelectionPhoto = 10;
    imagePicker.maximumNumberOfSelectionVideo = 0;
    
    UILongPressGestureRecognizer *longGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(moveCardInDeck:)];
    [_imagesCV addGestureRecognizer:longGR];
    
    if (thisField) {
        _images = [thisField allCardImages];
        [self reloadImageCV:NO];
    }

    
    // Do any additional setup after loading the view.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        [(CreateCardViewController *)self.priorViewController refreshViewWithCard:_card];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didRemoveImage:(BCButton *)sender
{
    if (sender.userInfo && [sender.userInfo objectForKey:@"cardimage"]) {
        CardImage *toDelete = [sender.userInfo objectForKey:@"cardimage"];
        [_images removeObject:toDelete];
        [toDelete delete];
    }
    NSLog(@"Remove Image");
    [self reloadImageCV:NO];
}

- (void)reloadImageCV:(BOOL)reloadTemps
{
    if (!reloadTemps) {
        [_imagesCV reloadData];
        return;
    }
    int currentPosition = 1;
    NSMutableArray *cv_to_delete = NSMutableArray.new;
    for (CardImage *cv in _images) {
        if (!cv.cardfield) {
            [cv_to_delete addObject:cv];
        } else {
            cv.position = [NSNumber numberWithInteger:currentPosition];
            [cv save];
            currentPosition = currentPosition + 1;
        }
    }
    
    for (CardImage *cv_delete in cv_to_delete) {
        [_images removeObject:cv_delete];
        [cv_delete delete];
    }
    
    for (UIImage *i in _temporary_images) {
        CardImage *newCI = [CardImage myNew];
        newCI.image = i;
        newCI.position = [NSNumber numberWithInteger:currentPosition];
        currentPosition = currentPosition + 1;
        [_images addObject:newCI];
        
    }
    
    
    [_imagesCV reloadData];
}

- (void)moveCardInDeck:(UIGestureRecognizer *)gesture
{
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            NSIndexPath *indexPathOfCentralCell = [_imagesCV indexPathForItemAtPoint:[gesture locationInView:_imagesCV]];
            if (indexPathOfCentralCell) {
                NSLog(@"STARTED movement");
                [_imagesCV beginInteractiveMovementForItemAtIndexPath:indexPathOfCentralCell];
                break;
            } else {
                break;
            }
            
        }
        case UIGestureRecognizerStateChanged:
        {
            NSLog(@"CHANGED movement");
            [_imagesCV updateInteractiveMovementTargetPosition:[gesture locationInView:gesture.view]];
            break;
            
        }
        case UIGestureRecognizerStateEnded:
        {
            NSLog(@"ENDED movement");
            [_imagesCV endInteractiveMovement];
            [self reloadImageCV:NO];
            break;
        }
            
        default:
            break;
    }
}

- (void)addField:(id)sender
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Adding Photos";
    
    if (field) {
        NSMutableDictionary *aField = [[NSMutableDictionary alloc] initWithDictionary:field];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [aField setObject:_images forKey:@"card_images"];
            [_card addFieldWithDetails:aField isFront:[isFrontBool boolValue]];
            
            // update UI on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        });
        
        
//        [aField setObject:_images forKey:@"card_images"];
//        
//        // Will add a new field, OR update existing field if the existing_field param exists
//        [_card addFieldWithDetails:aField isFront:[isFrontBool boolValue]];
//        
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        
//        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)footerActionButton:(BCFooterActionButton *)button didContinue:(BOOL)didContinue
{
    //[self doSelectPhotoType:nil];
    [self openPhotoAccess:nil];
}

- (void)groupedActionSheet:(BCGroupedActionSheetView *)actionSheet selectedItemWithCid:(NSString *)cid andGroup:(NSString *)group
{
    if (group && [group isEqualToString:@"change_photo"]) {
        [self openPhotoAccess:cid];
    } else {
        
    }
}

- (void)groupedActionSheetDidCancel:(BCGroupedActionSheetView *)actionSheet
{
    
}

- (void)openPhotoAccess:(NSString *)mode
{
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)doSelectPhotoType:(id)sender
{
    NSMutableArray *mFieldsArray = NSMutableArray.new;
    
    [mFieldsArray addObject:@{@"name":@"Take a Photo",@"id":@"camera"}];
    [mFieldsArray addObject:@{@"name":@"Select from Camera Roll",@"id":@"cameraroll"}];
    
    BCGroupedActionSheetView *actionSheet = [[BCGroupedActionSheetView alloc] initWithOptions:mFieldsArray withTitle:@"Change Photo" andFrame:CGRectZero];
    actionSheet.delegate = self;
    actionSheet.sheetCid = @"change_photo";
    
    [actionSheet showInView:self.view];
}

- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    if (_temporary_images) {
        _temporary_images = nil;
    }
    _temporary_images = NSMutableArray.new;
    
    __weak typeof(self) weakSelf = self;
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {
        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = obj;
            
            UIImage *img = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                               scale:representation.defaultRepresentation.scale
                                         orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            [_temporary_images addObject:img];
            NSLog(@"An image...");
        }];
        
        [self reloadImageCV:YES];
    }
}

//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//{
//    if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:(NSString *)kUTTypeImage]) {
//        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
//        CGRect cropRect = [info[UIImagePickerControllerCropRect] CGRectValue];
//        UIImage *scaledImage = [Common scaleAndRotateImage:[Common cropImage:image withRect:cropRect]];
//        
//        if (scaledImage) {
//            
//            temporary_image = scaledImage;
//            [deckImage showImage:temporary_image];
//            deckImageTopCS.offset = -15;
//            if (has_image) {
//                scrollHolderHeightCS.offset = current_total_height;
//            } else {
//                has_image = YES;
//                current_total_height = current_total_height + 160;
//                scrollHolderHeightCS.offset = current_total_height;
//            }
//        }
//        
//    } else {
//        // Video not yet supported
//    }
//    
//    [self dismissModalViewControllerAnimated:YES];
//}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 6;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 6;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSLog(@"Count: %lu", (unsigned long)_images.count);
    return _images.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Setup cell identifier
    static NSString *cellIdentifier = @"imageCell";
    
    /* Uncomment this block to use subclass-based cells */
    ImageCVCell *cell = (ImageCVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.delegate = self;
    
    CardImage *image = [_images objectAtIndex:indexPath.row];
    [cell buildWithImage:image];
    
    [cell.layer setCornerRadius:4];
    
    NSLog(@"....... created cell");
    
    // Return the cell
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    
    if (_images) {
        id thisImage = [_images objectAtIndex:sourceIndexPath.row];
        [_images removeObjectAtIndex:sourceIndexPath.row];
        [_images insertObject:thisImage atIndex:destinationIndexPath.row];
        
        // NEED TO UPDATE SORT ON THE SERVER NOW
    }
    
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    id currentCell = [collectionView cellForItemAtIndexPath:indexPath];
    if ([currentCell isKindOfClass:[ImageCVCell class]]) {
        
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
