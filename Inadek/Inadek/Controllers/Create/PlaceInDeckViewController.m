//
//  PlaceInDeckViewController.m
//  Inadek
//
//  Created by Josh Lehman on 11/3/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "PlaceInDeckViewController.h"
#import "BCNavController.h"
#import "MBProgressHUD.h"

#import "Deck+Core.h"
#import "Card+Core.h"
#import "CategoryTableViewCell.h"

@interface PlaceInDeckViewController () {
    UITableView *decksTV;
    NSMutableArray *myDecks;
    NSMutableArray *decksToAdd;
}

@end

@implementation PlaceInDeckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    
    self.navigationItem.rightBarButtonItem = [(BCNavController *)self.navigationController getNavBarItem:kBCNavButtonStyleText withTitle:@"ADD" andPosition:kBCNavPositionRight target:self action:@selector(addToDeck:)];
    
    NSMutableArray *decks = NSMutableArray.new;
    myDecks = [[DeckManager sharedManager] myLocalDecks];
    decksToAdd = NSMutableArray.new;
    
    decksTV = UITableView.new;
    decksTV.delegate = self;
    decksTV.dataSource = self;
    [self.view addSubview:decksTV];
    [decksTV makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(130, 0, 0, 0));
    }];
    
    UIView *topShadow = UIView.new;
    [self.view addSubview:topShadow];
    [topShadow makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(decksTV.top).with.offset(-4);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@1);
    }];
    [topShadow setStyleClass:@"box-shadow"];
    
    if (_deck) {
    
        UILabel *deck_name = UILabel.new;
        [deck_name setStyleClass:@"lbl-card-headervalue"];
        [self.view addSubview:deck_name];
        [deck_name makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@20);
            make.height.equalTo(@30);
            make.width.equalTo(self.view).with.insets(UIEdgeInsetsMake(20, 20, 20, 20));
            make.centerX.equalTo(self.view);
        }];
        
        [deck_name setNumberOfLines:0];
        [deck_name setText:_deck.name];
        
        UILabel *summary = UILabel.new;
        [summary setStyleClass:@"lbl-message"];
        [self.view addSubview:summary];
        [summary makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(deck_name.bottom);
            make.height.equalTo(@70);
            make.left.right.equalTo(self.view).with.insets(UIEdgeInsetsMake(20, 20, 20, 20));
            make.centerX.equalTo(self.view);
        }];
        
        [summary setNumberOfLines:0];
        [summary setText:@"You can place this deck into any of your own decks, as a deck 'inadek' by selecting one or more of your decks from the list below."];
        
    }
    
    // Do any additional setup after loading the view.
}

- (void) addToDeck:(id)sender
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Adding to Your Deck(s)";
    
    __block NSNumber *returnCount = @(decksToAdd.count);
    __block BOOL isDone = NO;
    
    for (Deck *d in decksToAdd) {
        // Add each of them
        [[DeckManager sharedManager] addDeckToDeck:_deck intoDeck:d callback:^(id responseObject) {
            //
            
            if (!isDone) {
                
                int newReturnCount = [returnCount integerValue] - 1;
                returnCount = @(newReturnCount);
                
                if (returnCount && [returnCount integerValue] == 0) {
                    isDone = YES;
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    DLog(@"We're done here, folks.");
                    [self dismissModalViewControllerAnimated:YES];
                }
            }
            
        } failureCallback:^(id responseObject) {
            
            if (!isDone) {
                
                int newReturnCount = [returnCount integerValue] - 1;
                returnCount = @(newReturnCount);
                
                if (returnCount && [returnCount integerValue] == 0) {
                    isDone = YES;
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    DLog(@"We're done here, folks.");
                    [self dismissModalViewControllerAnimated:YES];
                }
            }
            
        }];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [myDecks count];    //count number of row from counting array hear cataGorry is An Array
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[CategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:MyIdentifier];
    }
    
    if ([myDecks count] > indexPath.row) {
        Deck *thisDeck = [myDecks objectAtIndex:indexPath.row];
        [cell setDeck:thisDeck];
        [cell.label setText:thisDeck.name];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryTableViewCell *cell = (CategoryTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if ([cell isChecked]) {
        [decksToAdd addObject:cell.deck];
        // Cell was selected
        //[_deck addCategoriesObject:cell.category];
        
    } else {
        [decksToAdd removeObject:cell.deck];
        // Cell was deselected
        //[_deck removeCategoriesObject:cell.category];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
