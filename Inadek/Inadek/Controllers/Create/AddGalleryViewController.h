//
//  AddGalleryViewController.h
//  Inadek
//
//  Created by Josh Lehman on 11/16/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "Card+Core.h"
#import "CardField+Core.h"
#import "CardImage.h"
#import "BCFooterActionButton.h"
#import "BCGroupedActionSheetView.h"
#import "UzysAssetsPickerController.h"

@interface AddGalleryViewController : BCViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, BCFooterActionButtonDelegate, UzysAssetsPickerControllerDelegate, BCGroupedActionSheetDelegate> {
    BCFooterActionButton *footerButton;
    BOOL isEditing;
    UzysAssetsPickerController *imagePicker;
    
    NSDictionary *field;
    NSNumber *isFrontBool;
}

@property (nonatomic, retain) UICollectionView *imagesCV;
@property (nonatomic, retain) Card *card;
@property (nonatomic, retain) BCViewController *priorViewController;
@property (nonatomic, retain) NSMutableArray *images;
@property (nonatomic, retain) NSMutableArray *temporary_images;

- (id)initWithFieldCID:(NSString *)fieldCID isFront:(BOOL)front;
- (id)initWithField:(CardField *)field;

@end
