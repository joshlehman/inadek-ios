//
//  FieldReorderViewController.h
//  Inadek
//
//  Created by Josh Lehman on 6/13/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//
#define MAS_SHORTHAND

#import "BCViewController.h"

@class Card, Deck;

@interface FieldReorderViewController : BCViewController <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *reorderTV;
}

@property(nonatomic, retain) NSMutableArray *fields;
@property(nonatomic, retain) Card *card;
@property (nonatomic, retain) BCViewController *priorViewController;

@end
