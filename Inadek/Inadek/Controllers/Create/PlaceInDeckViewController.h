//
//  PlaceInDeckViewController.h
//  Inadek
//
//  Created by Josh Lehman on 11/3/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"

@class Deck, Card;

@interface PlaceInDeckViewController : BCViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) Card *card;

@end
