//
//  AddTriggerViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "UIBCNavButton.h"
#import "BCNavController.h"
#import "BCFieldSetView.h"
#import "Common.h"

#import "CreateDeckViewController.h"
#import "CreateCardViewController.h"
#import "DeckTrigger+Core.h"

@interface AddTriggerViewController : BCViewController <BCFieldSetDelegate> {
    NSDictionary *trigger;
    BCFieldSetView *triggerFS;
    BCFieldSetView *triggerVerb;
}

@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) Card *card;
@property (nonatomic, retain) BCViewController *priorViewController;

- (void)setCurrentDeck:(Deck *)thisDeck;
- (void)setCurrentCard:(Card *)thisCard;
- (id)initWithTriggerType:(NSString *)triggerType;
- (id)initWithTrigger:(id)aTrigger;

@end
