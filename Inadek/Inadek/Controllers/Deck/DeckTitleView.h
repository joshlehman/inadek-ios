//
//  DeckTitleView.h
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import "Deck+Core.h"
#import "Card+Core.h"
#import "NSDate+Formatting.h"

@class DeckViewController;

@interface DeckTitleView : UIView

@property (nonatomic, retain) MASConstraint *deckTitleMS;

- (id)initWithDeck:(Deck *)deck andParent:(DeckViewController *)parentVC;
- (id)initWithCard:(Card *)card andParent:(DeckViewController *)parentVC;


@end
