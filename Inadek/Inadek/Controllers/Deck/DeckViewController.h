//
//  DeckViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/7/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "CardActionButton.h"
#import "CardView.h"
#import "DeckView.h"
#import "DeckTitleView.h"
#import "Deck+Core.h"
#import "Card+Core.h"

#import "PromoDecksViewController.h"


#import <POP/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>

@interface DeckViewController : BCViewController <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate> {
    CardView *cardCenter;
    CardView *cardLeft;
    CardView *cardRight;
    
    DeckView *deckView;
    
    DeckTitleView *deckTitle;
    
    UIView *hvCenter;
    UIView *nextCardView;
    UIView *cardCollectionView;
    UIScrollView *cardSlider;
    UIView *holderView;
    
    NSMutableArray *cardStack; // A full stack of card data entities
    BOOL deckActionsShown;
    BOOL isTinyCardMoving;
    BOOL isCardTransitioning;
    CardView *movingCard;
    CardView *currentCard;
    CGPoint initialMovingCardLocation;
    
    UIVisualEffectView *vev;
    NSMutableArray *actionButtonAnimations;
    
    MASConstraint *nextCardleftEdgeMC;
}

@property (nonatomic, retain) NSDictionary *deckParams;
@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) Card *card; // For showing single cards
@property (nonatomic, retain) NSNumber *currentIndex;

@property (nonatomic, retain) NSMutableArray *deckStackHistory;
@property (nonatomic, retain) NSMutableDictionary *currentDeckHistory;

- (id)initWithDeckParams:(NSDictionary *)deckParams;
- (id)initWithCard:(NSDictionary *)cardParams;
- (void)dismissDeck:(id)sender;
- (void)goIntoDeck:(id)sender;
- (void)goIntoDeckAtIndex:(int)cardIndex;
- (void)goIntoSubdeck:(Deck *)deck;
- (void)returnFromSubdeck;
- (void)openShareActions:(id)sender;
- (void)showDeckActions:(id)sender;
- (void)showCardActions:(id)sender;
- (void)disabledCardTapped:(id)sender;
- (void)showCardCollection;
- (void)startMovingCard:(BOOL)shouldMove andCard:(CardView *)card;
- (void)transitionToCard:(Card *)card;

- (void)transitionToNextCard;
- (void)transitionToPrevCard;
- (void)transitionIsComplete;

- (void)showAllCards;

- (void)editCard:(Card *)thisCard;
- (void)editDeck:(id)sender;

- (void)updateHistoryLocation:(int)index;
- (void)updateHistoryLocation:(Deck *)deck atIndex:(int)index;
- (Deck *)goBackInHistoryLocation;

- (void)reloadCards;
- (void)reloadCardsAfterEdit:(id)sender;

@end
