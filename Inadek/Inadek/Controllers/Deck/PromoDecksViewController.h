//
//  PromoDecksViewController.h
//  Inadek
//
//  Created by Josh Lehman on 2/8/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "Deck+Core.h"
#import "Card+Core.h"
#import "BCFieldSetView.h"

#import <POP/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>

@interface PromoDecksViewController : BCViewController <BCFieldSetDelegate> {
    int current_total_height;
    UIView *scrollHolder;
    MASConstraint *scrollHolderHeightCS;
}

@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) BCViewController *priorViewController;
@property (nonatomic, retain) NSString *page;
@property (nonatomic, retain) UIScrollView *detailsSV;

@end
