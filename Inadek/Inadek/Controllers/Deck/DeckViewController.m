//
//  DeckViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/7/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckViewController.h"
#import "CardView.h"
#import "AppDelegate.h"

#import "CreateCardViewController.h"
#import "PlaceInDeckViewController.h"
#import "ReorderDeckViewController.h"

#import "Card+Core.h"

#import "UIAlertView+BCAlertView.h"

@import AddressBook;

#define kTopOffset      70
#define kSidePadding    10

@interface DeckViewController () {
    ReorderDeckViewController *reorderVC;
    CardActionButton *followBtn;
    BOOL backOutOfInaDeck;
    BOOL isSingleCard;
}

@end

@implementation DeckViewController

- (id)initWithDeckParams:(NSDictionary *)deckParams
{
    self = [self init];
    if (self) {
        
        backOutOfInaDeck = NO;
        isSingleCard = NO;
        
        if (deckParams) {
            if ([deckParams objectForKey:@"deck"]) {
                _deck = [deckParams objectForKey:@"deck"];
            }
        } else {
            
        }
    }
    
    return self;
}

- (id)initWithCard:(NSDictionary *)cardParams
{
    self = [self init];
    if (self) {
        
        backOutOfInaDeck = NO;
        isSingleCard = YES;
        
        if (cardParams) {
            if ([cardParams objectForKey:@"card"]) {
                _card = [cardParams objectForKey:@"card"];
            }
        } else {
            
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _deckStackHistory = NSMutableArray.new;
    // Do any additional setup after loading the view.
    
    [self makeBackgroundView:[UIColor colorWithWhite:0.1 alpha:1.0] animate:@"fadein" withCloseAction:NO];
    
    // This views will manage panning and swiping and moving the cards around with special transforms
    [self buildBaseHolderViews];
    
    [self loadTitle];
    
    // Load up the cards, placing the cover of the deck first -or- if a card ID was passed in jump directly to that card
    [self loadCards];

}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.transitionVC setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)loadTitle {
    if (!_deck && !_card) {
        return;
    }
    
    if (_card) {
        
        if (!deckTitle) {
            deckTitle = [[DeckTitleView alloc] initWithCard:_card andParent:self];
            [self.view addSubview:deckTitle];
            [deckTitle makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@0);
                make.width.equalTo(self.view);
                make.height.equalTo(@60);
                make.centerX.equalTo(self.view);
            }];
        }
        
        return;
    }
    
    if (!deckTitle) {
        deckTitle = [[DeckTitleView alloc] initWithDeck:_deck andParent:self];
        [self.view addSubview:deckTitle];
        [deckTitle makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@0);
            make.width.equalTo(self.view);
            make.height.equalTo(@60);
            make.centerX.equalTo(self.view);
        }];
    }
}

- (void)updateHistoryLocation:(int)index
{
    if (_currentDeckHistory) {
        [_currentDeckHistory setObject:@(index) forKey:@"card_index"];
        [[_deckStackHistory lastObject] setObject:@(index) forKey:@"card_index"];
    }
}

- (void)updateHistoryLocation:(Deck *)deck atIndex:(int)index
{
    _currentDeckHistory = [@{@"deck":deck,@"card_index":@(index)} mutableCopy];
    [_deckStackHistory addObject:_currentDeckHistory];
}

- (Deck *)goBackInHistoryLocation
{
    if (_deckStackHistory.count > 1) {
        [_deckStackHistory removeLastObject];
        _currentDeckHistory = _deckStackHistory.lastObject;
    }
    
    if (_currentDeckHistory && [_currentDeckHistory objectForKey:@"deck"]) {
        Deck *backToDeck = [_currentDeckHistory objectForKey:@"deck"];
        return backToDeck;
    } else {
        return nil;
    }
}

- (void)reloadCards {
    
    NSMutableArray *all_cards = NSMutableArray.new;
    
    if (_deck) {
        all_cards = [_deck orderedCards];
    } else if (_card) {
        all_cards = [@[_card] mutableCopy];
    }
    
    Card *thisCard = nil;
    if (currentCard && currentCard.card) {
        thisCard = currentCard.card;
    }
    
    cardStack = NSMutableArray.new;
    for (Card *c in all_cards) {
        CardView *cv = [[CardView alloc] initWithCard:self andCard:c];
        [cv buildInitialCardLayout];
        [cardStack addObject:cv];
        
        if (thisCard && [thisCard.remoteID integerValue] == [c.remoteID integerValue]) {
            [currentCard rebuildMyView:cv];
        }
    }
    
    
    
}

- (void)loadCards {
    
    NSMutableArray *all_cards = NSMutableArray.new;
    
    if (_deck) {
        all_cards = [_deck orderedCards];
        _currentDeckHistory = [@{@"deck":_deck,@"card_index":@(0)} mutableCopy];
        [_deckStackHistory addObject:_currentDeckHistory];
    } else if (_card) {
        all_cards = [@[_card] mutableCopy];
    }
    
    deckView = [[DeckView alloc] initWithOwner:self andView:hvCenter toSide:0 shouldSlideUp:NO];
    
    cardStack = NSMutableArray.new;
    for (Card *c in all_cards) {
        CardView *cv = [[CardView alloc] initWithCard:self andCard:c];
        [cv buildInitialCardLayout];
        [cardStack addObject:cv];
    }
    
    if (self.deckParams && [self.deckParams objectForKey:@"card_id"]) {
        // We have a specific card we're trying to land on
    } else {
        // No card specified, load the deck
        
        if (_deck) {
            _currentIndex = @(0);
            [deckView buildCardInDeck:_deck];

            // Now move it to where it needs to be immediately
            POPSpringAnimation *slideIn = [POPSpringAnimation new];
            slideIn.toValue = @(10);
            slideIn.springBounciness = 4;
            slideIn.springSpeed = 25;
            slideIn.property = [POPAnimatableProperty mas_offsetProperty];
            [deckView.leftEdgeMC pop_addAnimation:slideIn forKey:@"PopDeckSlideInAnimation"];
        } else if (_card) {
            
            UIView *whitebox = UIView.new;
            
            [self.view addSubview:whitebox];
            [whitebox makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@70); //.with.offset(kTopOffset);
                nextCardleftEdgeMC = make.left.equalTo(self.view).with.offset(self.view.bounds.size.width);
                make.width.equalTo(self.view).with.offset(-(10 * 2));
                make.height.equalTo(self.view).with.offset(0 - 70 - 10);
            }];
            
            currentCard = [cardStack objectAtIndex:0];
            [whitebox addSubview:currentCard];
            [currentCard makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(whitebox);
            }];
            
            // Now move it to where it needs to be immediately
            POPSpringAnimation *slideNewCardIn = [POPSpringAnimation new];
            slideNewCardIn.toValue = @(10);
            slideNewCardIn.springBounciness = 4;
            slideNewCardIn.springSpeed = 25;
            slideNewCardIn.property = [POPAnimatableProperty mas_offsetProperty];
            
            [nextCardleftEdgeMC pop_addAnimation:slideNewCardIn forKey:@"PopCardSlideInAnimation"];
            
        }
        
        
    }
}

- (void)buildBaseHolderViews
{
    CGRect basebounds = self.view.bounds;
    if (!hvCenter) {
        hvCenter = [[UIView alloc] initWithFrame:basebounds];
        [hvCenter setBackgroundColor:[UIColor clearColor]];
    }

    
    [self.view addSubview:hvCenter];
}

- (void)showAllCards
{
    
}

- (void)transitionToNextCard
{
    if (isCardTransitioning || isSingleCard) {
        return;
    }
    
    isCardTransitioning = YES;
    if ([_currentIndex integerValue] == [cardStack count]) {
        // We're on the last card, circle back to the first card
        _currentIndex = @1;

        
    } else {
        
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
        transform = CGAffineTransformScale(transform, 0.9, 0.9);
        
        _currentIndex = @([_currentIndex integerValue] + 1);
        
    }
    
    if (cardStack.count < 2) {
        isCardTransitioning = NO;
        return;
    }
    
    UIView *whitebox = UIView.new;
    
    if ([cardStack count] > ([_currentIndex integerValue] - 1)) {
        
        [self.view addSubview:whitebox];
        [whitebox makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@70); //.with.offset(kTopOffset);
            nextCardleftEdgeMC = make.left.equalTo(self.view).with.offset(self.view.bounds.size.width);
            make.width.equalTo(self.view).with.offset(-(10 * 2));
            make.height.equalTo(self.view).with.offset(0 - 70 - 10);
        }];
        
        movingCard = [cardStack objectAtIndex:([_currentIndex integerValue] - 1)];
        [whitebox addSubview:movingCard];
        [movingCard makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(whitebox);
        }];
        
    }
    
    
    //[deckView animateDeckBack];
    [currentCard animateCardAway:0];
    
    // Now move it to where it needs to be immediately
    POPSpringAnimation *slideNewCardIn = [POPSpringAnimation new];
    slideNewCardIn.toValue = @(10);
    slideNewCardIn.springBounciness = 4;
    slideNewCardIn.springSpeed = 25;
    slideNewCardIn.property = [POPAnimatableProperty mas_offsetProperty];
    slideNewCardIn.completionBlock = ^(POPAnimation *anim, BOOL finished){
        [nextCardView removeFromSuperview];
        nextCardView = whitebox;
    };
    
    [nextCardleftEdgeMC pop_addAnimation:slideNewCardIn forKey:@"PopCardSlideInAnimation"];
    
    [UIView animateWithDuration:4 animations:^{
        //deckView.transform = transform;
    } completion:^(BOOL finished) {
        currentCard = movingCard;
        [self updateHistoryLocation:_currentIndex];
    }];
    
}

- (void)transitionIsComplete
{
    isCardTransitioning = NO;
}

- (void)transitionToPrevCard
{
    if (isCardTransitioning || isSingleCard) {
        return;
    }
    
    isCardTransitioning = YES;
    
    if ([_currentIndex integerValue] == 1) {
        // Return to deck card
        [self returnToDeckCover];
        isCardTransitioning = NO;
        
    } else {
        // Go back to previous card
        
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
        transform = CGAffineTransformScale(transform, 0.9, 0.9);
        
        _currentIndex = @([_currentIndex integerValue] - 1);
        
        
        UIView *whitebox = UIView.new;
        
        if ([cardStack count] > ([_currentIndex integerValue] - 1)) {
            
            [self.view addSubview:whitebox];
            [whitebox makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@70); //.with.offset(kTopOffset);
                nextCardleftEdgeMC = make.left.equalTo(self.view).with.offset(0 - self.view.bounds.size.width);
                make.width.equalTo(self.view).with.offset(-(10 * 2));
                make.height.equalTo(self.view).with.offset(0 - 70 - 10);
            }];
            
            movingCard = [cardStack objectAtIndex:([_currentIndex integerValue] - 1)];
            [whitebox addSubview:movingCard];
            [movingCard makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(whitebox);
            }];
            
        }
        
        
        //[deckView animateDeckBack];
        [currentCard animateCardAway:1];
        
        // Now move it to where it needs to be immediately
        POPSpringAnimation *slideNewCardIn = [POPSpringAnimation new];
        slideNewCardIn.toValue = @(10);
        slideNewCardIn.springBounciness = 4;
        slideNewCardIn.springSpeed = 25;
        slideNewCardIn.property = [POPAnimatableProperty mas_offsetProperty];
        slideNewCardIn.completionBlock = ^(POPAnimation *anim, BOOL finished){
            [nextCardView removeFromSuperview];
            nextCardView = whitebox;
        };
        
        [nextCardleftEdgeMC pop_addAnimation:slideNewCardIn forKey:@"PopCardSlideInAnimation"];
        
        [UIView animateWithDuration:0.2 animations:^{
            //deckView.transform = transform;
        } completion:^(BOOL finished) {
            currentCard = movingCard;
            [self updateHistoryLocation:_currentIndex];
        }];

    }
}

- (void)goIntoSubdeck:(Deck *)deck
{
    [self updateHistoryLocation:deck atIndex:0];
    [self goIntoDeck:deck];
}

- (void)returnFromSubdeck
{
    Deck *newBackDeck = [self goBackInHistoryLocation];
    if (newBackDeck) {
        [self returnToDeck:newBackDeck];
    }
}

- (void)returnToDeck:(id)miscObject
{

    // We're diving into an Inadek!!! Down the rabbit hole we go...
    
    if ([miscObject isKindOfClass:[Deck class]]) {
        _deck = miscObject;
        [self reloadCards];
    }
    
    UIView *priorDeckView = nil;
    if (deckView) {
        priorDeckView = deckView;
    }
    
    deckView = [[DeckView alloc] initWithOwner:self andView:hvCenter toSide:1 shouldSlideUp:YES];
    
    if ([_deckStackHistory count] > 1) {
        backOutOfInaDeck = YES;
    }
    
    
    _currentIndex = @(0);
    [deckView buildCardInDeck:_deck];
    
    [currentCard animateCardBack];
    
    // Now move it to where it needs to be immediately
    POPSpringAnimation *slideIn = [POPSpringAnimation new];
    slideIn.toValue = @(70);
    slideIn.springBounciness = 4;
    slideIn.springSpeed = 25;
    slideIn.property = [POPAnimatableProperty mas_offsetProperty];
    slideIn.completionBlock = ^(POPAnimation *anim, BOOL finished){
        
        if (priorDeckView) {
            [priorDeckView removeFromSuperview];
        }
        
        [currentCard removeFromSuperview];
        currentCard = nil;
        [nextCardView removeFromSuperview];
        nextCardView = nil;
    };
    
    [deckView.leftEdgeMC pop_addAnimation:slideIn forKey:@"PopDeckSlideInAnimation"];
    
    [UIView animateWithDuration:0.2 animations:^{
        //deckView.transform = transform;
    } completion:^(BOOL finished) {

    }];

}

- (void)goIntoDeck:(id)miscObject
{
    if (deckView) {
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
        transform = CGAffineTransformScale(transform, 0.9, 0.9);
        
        _currentIndex = @(1);
        
        UIView *whitebox = UIView.new;
        
        if ([cardStack count] > ([_currentIndex integerValue] - 1)) {
            
            [self.view addSubview:whitebox];
            [whitebox makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@70); //.with.offset(kTopOffset);
                nextCardleftEdgeMC = make.left.equalTo(self.view).with.offset(self.view.bounds.size.width);
                make.width.equalTo(self.view).with.offset(-(10 * 2));
                make.height.equalTo(self.view).with.offset(0 - 70 - 10);
            }];
            
            currentCard = [cardStack objectAtIndex:([_currentIndex integerValue] - 1)];
            [whitebox addSubview:currentCard];
            [currentCard makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(whitebox);
            }];
            
        } else {
            return;
        }
        
        
        //[deckView animateDeckBack];
        [deckView animateDeckAway];
        
        // Now move it to where it needs to be immediately
        POPSpringAnimation *slideNewCardIn = [POPSpringAnimation new];
        slideNewCardIn.toValue = @(10);
        slideNewCardIn.springBounciness = 4;
        slideNewCardIn.springSpeed = 25;
        slideNewCardIn.property = [POPAnimatableProperty mas_offsetProperty];
        slideNewCardIn.completionBlock = ^(POPAnimation *anim, BOOL finished){
            [deckView removeFromSuperview];
            deckView = nil;
            [nextCardView removeFromSuperview];
            nextCardView = whitebox;
            [self updateHistoryLocation:[_currentIndex integerValue]];
        };
        
        [nextCardleftEdgeMC pop_addAnimation:slideNewCardIn forKey:@"PopCardSlideInAnimation"];

        [UIView animateWithDuration:0.2 animations:^{
            //deckView.transform = transform;
        } completion:^(BOOL finished) {
            
        }];
    } else {
        // We're diving into an Inadek!!! Down the rabbit hole we go...
        
        if ([miscObject isKindOfClass:[Deck class]]) {
            _deck = miscObject;
            [self reloadCards];
        }
        
        deckView = [[DeckView alloc] initWithOwner:self andView:hvCenter toSide:1 shouldSlideUp:YES];
        
        if ([_deckStackHistory count] > 1) {
            backOutOfInaDeck = YES;
        }
        
        
        _currentIndex = @(0);
        [deckView buildCardInDeck:_deck];
        
        [currentCard animateCardBack];
        
        // Now move it to where it needs to be immediately
        POPSpringAnimation *slideIn = [POPSpringAnimation new];
        slideIn.toValue = @(70);
        slideIn.springBounciness = 4;
        slideIn.springSpeed = 25;
        slideIn.property = [POPAnimatableProperty mas_offsetProperty];
        slideIn.completionBlock = ^(POPAnimation *anim, BOOL finished){
            [currentCard removeFromSuperview];
            currentCard = nil;
            [nextCardView removeFromSuperview];
            nextCardView = nil;
        };
        
        [deckView.leftEdgeMC pop_addAnimation:slideIn forKey:@"PopDeckSlideInAnimation"];
        
        [UIView animateWithDuration:0.2 animations:^{
            //deckView.transform = transform;
        } completion:^(BOOL finished) {
            
        }];
        
    }
}

- (void)goIntoDeckAtIndex:(int)cardIndex
{
    [self disabledCardTapped:nil];
    
    if (currentCard) {
        if (isCardTransitioning) {
            return;
        }
        
        if (_currentIndex && [_currentIndex integerValue] == cardIndex) {
            return;
        }
        
        isCardTransitioning = YES;

            
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
        transform = CGAffineTransformScale(transform, 0.9, 0.9);
        
        _currentIndex = @(cardIndex);
        
        
        UIView *whitebox = UIView.new;
        
        if ([cardStack count] > ([_currentIndex integerValue] - 1)) {
            
            [self.view addSubview:whitebox];
            [whitebox makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@70); //.with.offset(kTopOffset);
                nextCardleftEdgeMC = make.left.equalTo(self.view).with.offset(self.view.bounds.size.width);
                make.width.equalTo(self.view).with.offset(-(10 * 2));
                make.height.equalTo(self.view).with.offset(0 - 70 - 10);
            }];
            
            movingCard = [cardStack objectAtIndex:([_currentIndex integerValue] - 1)];
            [whitebox addSubview:movingCard];
            [movingCard makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(whitebox);
            }];
            
        }
        
        
        //[deckView animateDeckBack];
        [currentCard animateCardAway:0];
        
        // Now move it to where it needs to be immediately
        POPSpringAnimation *slideNewCardIn = [POPSpringAnimation new];
        slideNewCardIn.toValue = @(10);
        slideNewCardIn.springBounciness = 4;
        slideNewCardIn.springSpeed = 25;
        slideNewCardIn.property = [POPAnimatableProperty mas_offsetProperty];
        slideNewCardIn.completionBlock = ^(POPAnimation *anim, BOOL finished){
            [nextCardView removeFromSuperview];
            nextCardView = whitebox;
        };
        
        [nextCardleftEdgeMC pop_addAnimation:slideNewCardIn forKey:@"PopCardSlideInAnimation"];
        
        [UIView animateWithDuration:4 animations:^{
            //deckView.transform = transform;
        } completion:^(BOOL finished) {
            currentCard = movingCard;
            [self updateHistoryLocation:[_currentIndex integerValue]];
        }];

    }
    
    if (deckView) {
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
        transform = CGAffineTransformScale(transform, 0.9, 0.9);
        
        if (cardIndex < 1) {
            cardIndex = 1;
        }
        
        _currentIndex = @(cardIndex);
        
        UIView *whitebox = UIView.new;
        
        if ([cardStack count] > ([_currentIndex integerValue] - 1)) {
            
            [self.view addSubview:whitebox];
            [whitebox makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@70); //.with.offset(kTopOffset);
                nextCardleftEdgeMC = make.left.equalTo(self.view).with.offset(self.view.bounds.size.width);
                make.width.equalTo(self.view).with.offset(-(10 * 2));
                make.height.equalTo(self.view).with.offset(0 - 70 - 10);
            }];
            
            currentCard = [cardStack objectAtIndex:([_currentIndex integerValue] - 1)];
            [whitebox addSubview:currentCard];
            [currentCard makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(whitebox);
            }];
            
        } else {
            return;
        }
        
        
        //[deckView animateDeckBack];
        [deckView animateDeckAway];
        
        // Now move it to where it needs to be immediately
        POPSpringAnimation *slideNewCardIn = [POPSpringAnimation new];
        slideNewCardIn.toValue = @(10);
        slideNewCardIn.springBounciness = 4;
        slideNewCardIn.springSpeed = 25;
        slideNewCardIn.property = [POPAnimatableProperty mas_offsetProperty];
        slideNewCardIn.completionBlock = ^(POPAnimation *anim, BOOL finished){
            [deckView removeFromSuperview];
            deckView = nil;
            [nextCardView removeFromSuperview];
            nextCardView = whitebox;
        };
        
        [nextCardleftEdgeMC pop_addAnimation:slideNewCardIn forKey:@"PopCardSlideInAnimation"];
        
        [UIView animateWithDuration:0.2 animations:^{
            //deckView.transform = transform;
        } completion:^(BOOL finished) {
            [self updateHistoryLocation:[_currentIndex integerValue]];
        }];
    }
}

- (void)returnToDeckCover
{

    deckView = [[DeckView alloc] initWithOwner:self andView:hvCenter toSide:1 shouldSlideUp:NO];
    

    _currentIndex = @(0);
    [deckView buildCardInDeck:_deck];
    
    [currentCard animateCardAway:1];
    
    // Now move it to where it needs to be immediately
    POPSpringAnimation *slideIn = [POPSpringAnimation new];
    slideIn.toValue = @(10);
    slideIn.springBounciness = 4;
    slideIn.springSpeed = 25;
    slideIn.property = [POPAnimatableProperty mas_offsetProperty];
    slideIn.completionBlock = ^(POPAnimation *anim, BOOL finished){
        [currentCard removeFromSuperview];
        currentCard = nil;
        [nextCardView removeFromSuperview];
        nextCardView = nil;
    };
    
    [deckView.leftEdgeMC pop_addAnimation:slideIn forKey:@"PopDeckSlideInAnimation"];
    
    [UIView animateWithDuration:0.2 animations:^{
        //deckView.transform = transform;
    } completion:^(BOOL finished) {

    }];
    

    
}

- (void)showCardCollection
{
    if (deckActionsShown) {
        return;
    }
    
    if (currentCard) {
        
        NSMutableArray *cards = NSMutableArray.new;
        if ([[_deck orderedCards] count] > 0) {
            cards = [_deck orderedCards];
        }
        
        if ([cards count] == 0) {
            return;
        }
        
        deckActionsShown = YES;
        
        [currentCard disableInteraction];
        [currentCard animateDeckBackUp];
        
        UIBlurEffect *be = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        vev = [[UIVisualEffectView alloc] initWithEffect:be];
        [vev setTag:10];
        [vev setAlpha:0.0];
        [vev setFrame:self.view.frame];
        [self.view addSubview:vev];
        [vev makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.equalTo(@200);
        }];
        
        [UIView animateWithDuration:0.3 animations:^{
            [vev setAlpha:1.0];
        }];
        
        [self presentDeckCardCollection];

        
    }
    
    if (deckView && _deck) {
        
        NSMutableArray *cards = NSMutableArray.new;
        if ([[_deck orderedCards] count] > 0) {
            cards = [_deck orderedCards];
        }
        
        if ([cards count] == 0) {
            return;
        }
        
        deckActionsShown = YES;
        
        [deckView disableInteraction];
        [deckView animateDeckBackUp];
        
        UIBlurEffect *be = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        vev = [[UIVisualEffectView alloc] initWithEffect:be];
        [vev setTag:10];
        [vev setAlpha:0.0];
        [vev setFrame:self.view.frame];
        [self.view addSubview:vev];
        [vev makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.equalTo(@200);
        }];
        
        [UIView animateWithDuration:0.3 animations:^{
            [vev setAlpha:1.0];
        }];
        
        [self presentDeckCardCollection];
    }
}

- (void)showCardActions:(id)sender
{
    if (currentCard) {
        
        deckActionsShown = YES;
        
        [currentCard disableInteraction];
        [currentCard animateDeckBack];
        
        UIBlurEffect *be = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        vev = [[UIVisualEffectView alloc] initWithEffect:be];
        [vev setTag:10];
        [vev setAlpha:0.0];
        [vev setFrame:self.view.frame];
        [self.view addSubview:vev];
        [vev makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.equalTo(@130);
        }];
        
        [UIView animateWithDuration:0.3 animations:^{
            [vev setAlpha:1.0];
        }];
        
        
        [self presentAvailableActionsForCard];
        
    }
    
}

- (void)showDeckActions:(id)sender
{
    if (deckView && _deck) {
        
        deckActionsShown = YES;
        
        [deckView disableInteraction];
        [deckView animateDeckBack];
        
        UIBlurEffect *be = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        vev = [[UIVisualEffectView alloc] initWithEffect:be];
        [vev setTag:10];
        [vev setAlpha:0.0];
        [vev setFrame:self.view.frame];
        [self.view addSubview:vev];
        [vev makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.equalTo(@130);
        }];
        
        [UIView animateWithDuration:0.3 animations:^{
            [vev setAlpha:1.0];
        }];
    
        
        [self presentAvailableActionsForDeck];
    }
}

- (void)hideDeckActions:(id)sender {
    
    for (MASConstraint *anim in actionButtonAnimations) {
        POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
        popupAnimation.toValue = @(110);
        popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
        popupAnimation.springBounciness = 8;
        popupAnimation.springSpeed = 20;
        [anim pop_addAnimation:popupAnimation forKey:@"test"];
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        [vev setAlpha:0.0];
    } completion:^(BOOL finished) {
        [vev removeFromSuperview];
        vev = nil;
    }];
    
    if (deckView) {
        [deckView animateDeckForward];
    }
    
    if (currentCard) {
        [currentCard animateDeckForward];
    }
    
}

- (void)presentDeckCardCollection
{
    
    NSMutableArray *cards = NSMutableArray.new;
    if ([[_deck orderedCards] count] > 0) {
        cards = [_deck orderedCards];
    }
    
    __block MASConstraint *animUp = nil;
    cardCollectionView = UIView.new;
    [self.view addSubview:cardCollectionView];
    [cardCollectionView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@200);
        animUp = make.bottom.equalTo(self.view).with.offset(200);
    }];
    
    UILabel *title = UILabel.new;
    [title setText:@"Slide left and right to scan all cards in deck:"];
    [title setNumberOfLines:0];
    [title setStyleClass:@"lbl-deck-subtitle"];
    [cardCollectionView addSubview:title];
    [title makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(cardCollectionView);
        make.top.equalTo(cardCollectionView).with.offset(10);
        make.height.equalTo(@22);
    }];
    

    reorderVC = [[ReorderDeckViewController alloc] init];
    reorderVC.delegate = self;
    reorderVC.deck = _deck;
    
    if ([_deck isMine]) {
        [reorderVC setCanMoveCards:YES];
    } else {
        [reorderVC setCanMoveCards:NO];
    }

    [cardCollectionView addSubview:reorderVC.view];
    [reorderVC.view makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(cardCollectionView);
        make.top.equalTo(title.bottom);
    }];
    
    UILongPressGestureRecognizer *longGR = [[UILongPressGestureRecognizer alloc] initWithTarget:reorderVC action:@selector(moveCardInDeck:)];
    [reorderVC.deckCV addGestureRecognizer:longGR];
    
    
    [reorderVC loadCards:cards];
    
    
//    cardSlider = UIScrollView.new;
//    [cardSlider setScrollEnabled:YES];
//    [cardSlider setShowsHorizontalScrollIndicator:YES];
//    [cardCollectionView addSubview:cardSlider];
//    [cardSlider makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.bottom.equalTo(cardCollectionView);
//        make.top.equalTo(title.bottom);
//    }];
//    
//    UIPanGestureRecognizer *svPanGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movingCard:)];
//    [svPanGR setDelegate:self];
//    [cardSlider addGestureRecognizer:svPanGR];
//    
//    [self fillCardCollection:cardSlider];
    
    POPBasicAnimation *slideUp = [POPBasicAnimation new];
    slideUp.toValue = @0;
    slideUp.duration = 0.2;
    slideUp.property = [POPAnimatableProperty mas_offsetProperty];
    [animUp pop_addAnimation:slideUp forKey:@"slide_up_collection"];
}




- (void)hideCardCollection
{
    if (cardCollectionView) {
        
        [UIView animateWithDuration:0.2 animations:^{
            [cardCollectionView setAlpha:0.0];
        } completion:^(BOOL finished) {
            [cardCollectionView removeFromSuperview];
            cardCollectionView = nil;
        }];
        
    }
        
        
}

- (void)fillCardCollection:(UIScrollView *)sv
{
    NSMutableArray *cards = NSMutableArray.new;
    if ([[_deck orderedCards] count] > 0) {
        cards = [_deck orderedCards];
    }

    holderView = UIView.new;
    [holderView setUserInteractionEnabled:YES];
    [sv addSubview:holderView];
    [holderView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(sv);
        make.height.equalTo(sv);
    }];
    
    UIView *priorObject = nil;
    int total_width = 0;
    for (Card *card in cards) {
        CardView *t = [[CardView alloc] initWithMiniView:self andCard:card];
        [t respondToLongPress];
        [holderView addSubview:t];
        [t makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(holderView).with.offset(10);
            make.height.equalTo(@150);
            make.width.equalTo(@95);
            if (priorObject) {
                make.left.equalTo(priorObject.right).with.offset(10);
            } else {
                make.left.equalTo(holderView).with.offset(10);
            }
        }];
        priorObject = t;
        total_width += 105;
        
    }
    
    [holderView updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@0).with.offset(total_width + 10);
    }];
    
}

- (void)transitionToCard:(Card *)card
{
    [self disabledCardTapped:nil];
}

- (void)startMovingCard:(BOOL)shouldMove andCard:(CardView *)card;
{
    if (!cardSlider) {
        return;
    }
    
    if (shouldMove && !isTinyCardMoving) {
        isTinyCardMoving = YES;
        [cardSlider setScrollEnabled:NO];
        
        if (card) {
            [card setAlpha:0.4];
            movingCard = [[CardView alloc] initWithMiniView:self andCard:card.card];
            [movingCard isFloating:YES];
            [holderView addSubview:movingCard];
            CGRect currentCenter = CGRectMake(card.center.x - (card.frame.size.width / 2), card.center.y - (card.frame.size.height / 2) - 10, card.frame.size.width, card.frame.size.height);
            [movingCard setFrame:currentCenter];
            initialMovingCardLocation = movingCard.center;
            
        }
    }
}

- (void)movingCard:(UIPanGestureRecognizer *)gesture
{
    if (!isTinyCardMoving){
        return;
    }
    
    NSLog(@"Panning...");
    CGPoint translation = [gesture translationInView:self.view];
    
    NSLog(@"TRanslation: %@", NSStringFromCGPoint(translation));
    
    
    
    if (gesture.state == UIGestureRecognizerStateChanged)
    {
        int xDelta  = movingCard.center.x - [gesture locationInView:cardSlider].x;
        [movingCard setCenter:CGPointMake([gesture locationInView:cardSlider].x, initialMovingCardLocation.y)];
        
        [self scrollIfNeeded:[gesture locationInView:cardSlider.superview] withDelta:xDelta];
        
        return;
    }
    
}

-(void)scrollIfNeeded:(CGPoint)locationInScrollSuperview withDelta:(int)xDelta
{
    UIView * scrollSuperview = cardSlider.superview;
    CGRect bounds = scrollSuperview.bounds;
    CGPoint scrollOffset = cardSlider.contentOffset;
    int xOfs = 0;
    int speed = 10;
    
    if ((locationInScrollSuperview.x > bounds.size.width * 0.7) && (xDelta < 0))
    {
        xOfs = speed * locationInScrollSuperview.x/bounds.size.width;
    }
    
    if ((locationInScrollSuperview.x < bounds.size.width * 0.3) && (xDelta > 0))
    {
        xOfs = -speed * (1.0f - locationInScrollSuperview.x/bounds.size.width);
    }
    
    if (xOfs < 0)
    {
        if (scrollOffset.x == 0)    return;
        if (xOfs < -scrollOffset.x) xOfs = -scrollOffset.x;
    }
    scrollOffset.x += xOfs;
    CGRect rect = CGRectMake(scrollOffset.x, 0, cardSlider.bounds.size.width, cardSlider.bounds.size.height);
    [cardSlider scrollRectToVisible:rect animated:NO];
    CGPoint center = movingCard.center;
    center.x += xOfs;
    movingCard.center=center;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)presentAvailableActionsForCard
{

    NSMutableArray *actions = NSMutableArray.new;
    
    if (currentCard.card && [currentCard.card isMine]) {
        [actions addObject:@"edit_card"];
        [actions addObject:@"add_card"];
    }
    
    if ([currentCard.card isContactable]) {
        [actions addObject:@"add_contact_card"];
    }
    
    [actions addObject:@"share_card"];
    
    // IF owner, allow for deck edit
    // IF owner AND deck is group deck, allow add member to deck
    //NSArray *actions = [[NSArray alloc] initWithObjects:@"edit_deck", @"add_card", @"add_user", @"place_deck", nil];
    
    //NSArray *actions = [[NSArray alloc] initWithObjects:@"edit_deck", @"add_card", nil];
    
    NSMutableArray *actionButtons = NSMutableArray.new;
    actionButtonAnimations = NSMutableArray.new;
    
    float actionRadius = 40.0f;
    float buttonRadius = 20.0f;
    
    for (NSString *action in actions) {
        
        CardActionButton *baseBtn = CardActionButton.new;
        [baseBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [baseBtn setTitleColor:[UIColor colorWithWhite:0.4 alpha:1.0] forState:UIControlStateHighlighted];
        [baseBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, -50, 0)];
        [baseBtn setStyleClass:@"lbl-deck-action-button"];
        
        UIImageView *btn = UIImageView.new;
        [btn setBackgroundColor:[UIColor clearColor]];
        
        //[btn setClipsToBounds:YES]
        
        baseBtn.myImage = btn;
        [baseBtn addSubview:baseBtn.myImage];
        [baseBtn.myImage makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(buttonRadius * 2));
            make.height.equalTo(@(buttonRadius * 2));
            make.top.equalTo(baseBtn).with.offset(20);
            make.centerX.equalTo(baseBtn);
        }];
        
        if ([action isEqualToString:@"edit_card"]) {
            // Add image, add target / action
            [baseBtn setTitle:@"Edit Card" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-Edit"]];
            [baseBtn addTarget:self action:@selector(editThisCard:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"add_card"]) {
            if (_deck) {
                baseBtn.userInfo = @{@"deck":_deck};
            }
            [baseBtn setTitle:@"Add Card" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddCard"]];
            [baseBtn addTarget:self action:@selector(doNewCard:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"add_contact_card"]) {
            if (_deck) {
                baseBtn.userInfo = @{@"deck":_deck};
            }
            [baseBtn setTitle:@"Save Contact" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-CardAction-AddContact"]];
            [baseBtn addTarget:self action:@selector(doAddContact:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if ([action isEqualToString:@"share_card"]) {
            if (_deck) {
                baseBtn.userInfo = @{@"deck":_deck};
            }
            [baseBtn setTitle:@"Share Card" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-CardAction-Share"]];
            [baseBtn addTarget:self action:@selector(openShareActions:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if ([action isEqualToString:@"follow_deck"]) {
            
            followBtn = baseBtn;
            
            [followBtn setTitle:@"Follow" forState:UIControlStateNormal];
            if (_deck && [_deck is_deck_followed]) {
                [followBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-FollowOn"]];
                [followBtn setTitle:@"Unfollow" forState:UIControlStateNormal];
            } else {
                [followBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddCard"]];
            }
            [followBtn addTarget:self action:@selector(doFollow:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        if ([action isEqualToString:@"advert_deck"]) {
            [baseBtn setTitle:@"Add My Deck" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddMyDeck"]];
            [baseBtn addTarget:self action:@selector(requestPromoDeckAdd:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"view_promos"]) {
            [baseBtn setTitle:@"Promo Decks" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-Edit"]];
            [baseBtn placeCounter:0];
            //[baseBtn placeAlertCounter:0];
            [baseBtn addTarget:self action:@selector(goToPromoDecks:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"add_user"]) {
            [baseBtn setTitle:@"Add Member" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddCard"]];
        }
        if ([action isEqualToString:@"place_deck"]) {
            [baseBtn setTitle:@"Inadek This!" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-Place"]];
            [baseBtn addTarget:self action:@selector(doPlaceInDeck:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"delete_deck"]) {
            [baseBtn setTitle:@"Delete" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-Delete"]];
        }
        
        
        
        
        [actionButtons addObject:baseBtn];
    }
    
    if ([actionButtons count] > 0) {
        int spacer = self.view.bounds.size.width;
        spacer = spacer / [actionButtons count];
        int spacer_location = spacer / 2;
        NSLog(@"Spacer: %d", spacer_location);
        
        for (UIButton *button in actionButtons) {
            [self.view addSubview:button];
            [button makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(actionRadius * 2));
                make.height.equalTo(@((actionRadius * 2) + 25));
                MASConstraint *bottom = make.bottom.equalTo(self.view.bottom).with.offset((actionRadius * 2) + 25);
                make.left.equalTo(self.view).with.offset(spacer_location - actionRadius);
                
                [actionButtonAnimations addObject:bottom];
            }];
            spacer_location = spacer_location + spacer;
        }
    }
    
    for (MASConstraint *anim in actionButtonAnimations) {
        POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
        popupAnimation.toValue = @(-20);
        popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
        popupAnimation.springBounciness = 8;
        popupAnimation.springSpeed = 20;
        [anim pop_addAnimation:popupAnimation forKey:@"test"];
    }
}

- (void)presentAvailableActionsForDeck
{
    NSArray *actions = nil;
    if ([_deck isMine]) {
        actions = [[NSArray alloc] initWithObjects:@"edit_deck", @"add_card", @"place_deck", @"view_promos", nil];
    } else {
        actions = [[NSArray alloc] initWithObjects:@"follow_deck", @"advert_deck", @"place_deck", nil];
    }
    // IF owner, allow for deck edit
    // IF owner AND deck is group deck, allow add member to deck
    //NSArray *actions = [[NSArray alloc] initWithObjects:@"edit_deck", @"add_card", @"add_user", @"place_deck", nil];

    //NSArray *actions = [[NSArray alloc] initWithObjects:@"edit_deck", @"add_card", nil];
    
    NSMutableArray *actionButtons = NSMutableArray.new;
    actionButtonAnimations = NSMutableArray.new;
    
    float actionRadius = 40.0f;
    float buttonRadius = 20.0f;
    
    for (NSString *action in actions) {
        
        CardActionButton *baseBtn = CardActionButton.new;
        [baseBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [baseBtn setTitleColor:[UIColor colorWithWhite:0.4 alpha:1.0] forState:UIControlStateHighlighted];
        [baseBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 0, -50, 0)];
        [baseBtn setStyleClass:@"lbl-deck-action-button"];
        
        UIImageView *btn = UIImageView.new;
        [btn setBackgroundColor:[UIColor clearColor]];
        
        //[btn setClipsToBounds:YES]
        
        baseBtn.myImage = btn;
        [baseBtn addSubview:baseBtn.myImage];
        [baseBtn.myImage makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(buttonRadius * 2));
            make.height.equalTo(@(buttonRadius * 2));
            make.top.equalTo(baseBtn).with.offset(20);
            make.centerX.equalTo(baseBtn);
        }];
        
        if ([action isEqualToString:@"edit_deck"]) {
            // Add image, add target / action
            [baseBtn setTitle:@"Edit Deck" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-Edit"]];
            [baseBtn addTarget:self action:@selector(editDeck:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"add_card"]) {
            if (_deck) {
                baseBtn.userInfo = @{@"deck":_deck};
            }
            [baseBtn setTitle:@"Add Card" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddCard"]];
            [baseBtn addTarget:self action:@selector(doNewCard:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"follow_deck"]) {
            
            followBtn = baseBtn;
            
            [followBtn setTitle:@"Follow" forState:UIControlStateNormal];
            if (_deck && [_deck is_deck_followed]) {
                [followBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-FollowOn"]];
                [followBtn setTitle:@"Unfollow" forState:UIControlStateNormal];
            } else {
                [followBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddCard"]];
            }
            [followBtn addTarget:self action:@selector(doFollow:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        if ([action isEqualToString:@"advert_deck"]) {
            [baseBtn setTitle:@"Add My Deck" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddMyDeck"]];
            [baseBtn addTarget:self action:@selector(requestPromoDeckAdd:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"view_promos"]) {
            [baseBtn setTitle:@"Promo Decks" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-Edit"]];
            [baseBtn placeCounter:0];
            //[baseBtn placeAlertCounter:0];
            [baseBtn addTarget:self action:@selector(goToPromoDecks:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"add_user"]) {
            [baseBtn setTitle:@"Add Member" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddCard"]];
        }
        if ([action isEqualToString:@"place_deck"]) {
            [baseBtn setTitle:@"Inadek This!" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-Place"]];
            [baseBtn addTarget:self action:@selector(doPlaceInDeck:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([action isEqualToString:@"delete_deck"]) {
            [baseBtn setTitle:@"Delete" forState:UIControlStateNormal];
            [baseBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-Delete"]];
        }
        

        
        
        [actionButtons addObject:baseBtn];
    }
    
    if ([actionButtons count] > 0) {
        int spacer = self.view.bounds.size.width;
        spacer = spacer / [actionButtons count];
        int spacer_location = spacer / 2;
        NSLog(@"Spacer: %d", spacer_location);
        
        for (UIButton *button in actionButtons) {
            [self.view addSubview:button];
            [button makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(actionRadius * 2));
                make.height.equalTo(@((actionRadius * 2) + 25));
                MASConstraint *bottom = make.bottom.equalTo(self.view.bottom).with.offset((actionRadius * 2) + 25);
                make.left.equalTo(self.view).with.offset(spacer_location - actionRadius);
                
                [actionButtonAnimations addObject:bottom];
            }];
            spacer_location = spacer_location + spacer;
        }
    }
    
    for (MASConstraint *anim in actionButtonAnimations) {
        POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
        popupAnimation.toValue = @(-20);
        popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
        popupAnimation.springBounciness = 8;
        popupAnimation.springSpeed = 20;
        [anim pop_addAnimation:popupAnimation forKey:@"test"];
    }
    

    
}

- (void)reloadCardsAfterEdit:(id)sender
{
    [self reloadCards];
}

- (void)editDeck:(id)sender
{
    CreateDeckViewController *createDeckVC = [[CreateDeckViewController alloc] initWithDeck:self.deck andPage:@"page2"];
    [createDeckVC setTitle:@"Edit Deck"];
    [createDeckVC setEditingMode:YES];
    
    [self closeAndTransitionTo:createDeckVC withType:0 buildNavController:YES];
}

- (void)editThisCard:(id)sender
{
    if (currentCard) {
        CreateCardViewController *createCardVC = [[CreateCardViewController alloc] initWithCard:currentCard.card andPage:@"page2"];
        [createCardVC setTitle:@"Edit Card"];
        [createCardVC setEditingMode:YES];
        
        [self closeAndTransitionTo:createCardVC withType:0 buildNavController:YES closeMe:NO];
    }
}

- (void)editCard:(Card *)thisCard
{
    CreateCardViewController *createCardVC = [[CreateCardViewController alloc] initWithCard:thisCard andPage:@"page2"];
    [createCardVC setTitle:@"Edit Card"];
    [createCardVC setEditingMode:YES];
    
    [self closeAndTransitionTo:createCardVC withType:0 buildNavController:YES closeMe:NO];
}

- (void)doAddContact:(CardActionButton *)sender
{
    [self addCardToContacts:currentCard.card];
}

- (void)doNewCard:(CardActionButton *)sender
{
    CreateCardViewController *createCardVC = [[CreateCardViewController alloc] init];
    createCardVC.card = nil;
    if (sender.userInfo && [sender.userInfo objectForKey:@"deck"]) {
        createCardVC.into_deck = [sender.userInfo objectForKey:@"deck"];
    }
    [createCardVC setTitle:@"New Card"];
    
    [self closeAndTransitionTo:createCardVC withType:0 buildNavController:YES closeMe:NO];
}

- (void)doPlaceInDeck:(id)sender
{
    NSArray *myDecks = [[DeckManager sharedManager] myLocalDecks];
    
    if (myDecks.count > 0) {
        PlaceInDeckViewController *placeDeckVC = [[PlaceInDeckViewController alloc] init];
        placeDeckVC.deck = self.deck;
        [placeDeckVC setTitle:@"Place in Deck"];
        
        [self closeAndTransitionTo:placeDeckVC withType:0 buildNavController:YES closeMe:NO];
    } else {
        UIAlertView *followMsg = [[UIAlertView alloc] initWithTitle:@"Deck in Deck" message:@"In order to drop this deck into one of your own decks, you must have created at least one deck first. Go ahead and create a deck and then you can return to this deck to drop it in." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [followMsg show];
    }

}

- (void)doFollow:(id)sender {

    if (_deck && [_deck is_deck_followed]) {
        [_deck set_is_followed:NO];
        [followBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-AddCard"]];
        [followBtn setTitle:@"Follow" forState:UIControlStateNormal];
    } else {
        [self.deck set_is_followed:YES];
        [followBtn.myImage setImage:[UIImage imageNamed:@"Btn-DeckAction-FollowOn"]];
        [followBtn setTitle:@"Unfollow" forState:UIControlStateNormal];
        
        UIAlertView *followMsg = [[UIAlertView alloc] initWithTitle:@"Following Deck" message:@"You are now following updates to this deck. When others edit or add to this deck you will be notified." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [followMsg show];
    }
    
    [self.deck save];
    
    [[DeckManager sharedManager] followDeck:_deck];
}

- (void)openShareActions:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    NSString *text = [_deck get_shareText];
    NSURL *url = [_deck get_shareUrl];
    
    if (text) {
        [sharingItems addObject:text];
    }
    
    if (url) {
        [sharingItems addObject:url];
    }

    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

- (void)dismissDeck:(id)sender
{
    
    if (backOutOfInaDeck) {
        [self returnFromSubdeck];
        backOutOfInaDeck = NO;
        return;
    }
    
    if (deckView) {
        POPSpringAnimation *slideIn = [POPSpringAnimation new];
        slideIn.toValue = @(self.view.bounds.size.width);
        slideIn.springBounciness = 4;
        slideIn.springSpeed = 25;
        slideIn.property = [POPAnimatableProperty mas_offsetProperty];
        [deckView.leftEdgeMC pop_addAnimation:slideIn forKey:@"PopDeckSlideInAnimation"];
        // Now move it to where it needs to be immediately
        
        POPSpringAnimation *slideTitle = [POPSpringAnimation new];
        slideTitle.toValue = @(100);
        slideTitle.springBounciness = 4;
        slideTitle.springSpeed = 25;
        slideTitle.property = [POPAnimatableProperty mas_offsetProperty];
        [deckTitle.deckTitleMS pop_addAnimation:slideTitle forKey:@"PopDeckSlideTitleAnimation"];
        
        [UIView animateWithDuration:0.3 animations:^{
            [deckTitle setAlpha:0.0];
        } completion:^(BOOL finished) {
            //
        }];
        
    }
    [self fadeMe:self withStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)disabledCardTapped:(id)sender
{
    if (deckActionsShown) {
        deckActionsShown = NO;
        [self hideDeckActions:nil];
    }
    
    if (cardCollectionView) {
        [self hideCardCollection];
    }
}

- (void)goToPromoDecks:(id)sender
{
//    PromoDecksViewController *promoVC = [[PromoDecksViewController alloc] init];
//    [promoVC setTitle:@"Promo Decks"];
//    
//    [self closeAndTransitionTo:promoVC withType:0 buildNavController:YES];

    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Promotional Decks" message:@"Your deck does not currently have any promotional decks." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [av show];
}

- (void)requestPromoDeckAdd:(id)sender
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Request Submitted" message:@"Your request to be included in this deck has been submitted. You will be contacted by the deck owner shortly if approved." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [av show];
    
}

- (void)addCardToContacts:(Card *)card
{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You previously denied Inadek access to your address book, so we are not able to add to contacts. To fix this you must visit the Settings app and re-enable access to Contacts for Inadek." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [av show];
        
        NSLog(@"Denied");
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
        
        [self addContactNow:card];
        
        
    } else { //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted){
                
                [self showAlert:@"We cannot add to contacts unless you accept our request to access the Address Book." withTitle:@"Unable to Add Contact"];
                
                return;
            }
            //5
            [self addContactNow:card];
        });
    }
}

- (void)addContactNow:(Card *)card;
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    ABRecordRef person = ABPersonCreate();
    
    NSDictionary *contactCardInfo = [card prepareForContacts];
    
//    
//    
//    
//    [dict setObject:[name_parts objectAtIndex:0] forKey:@"first_name"];
//    [dict setObject:[name_parts objectAtIndex:1] forKey:@"last_name"];
//} else if (name_parts.count == 1) {
//    [dict setObject:[name_parts objectAtIndex:0] forKey:@"first_name"];
//}
//
//for (CardField *c in [self cardFrontFields]) {
//    if ([c fieldIsEmail]) {
//        [dict setObject:c.field_value forKey:@"email"];
//    }
//    if ([c fieldIsHomePhone]) {
//        [dict setObject:c.field_value forKey:@"home_phone"];
//    }
//    if ([c fieldIsMobilePhone]) {
//        [dict setObject:c.field_value forKey:@"mobile_phone"];
//    }
//    if ([c fieldIsBusinessPhone]) {
//        [dict setObject:c.field_value forKey:@"business_phone"];
//    }
//}
//
//for (CardTrigger *t in [[self triggers] allObjects]) {
//    if ([t.trigger_type isEqualToString:@"phone"]) {
//        [dict setObject:t.value forKey:@"home_phone"];
//    }
//    
//    if ([t.trigger_type isEqualToString:@"email"]) {
//        [dict setObject:t.value forKey:@"email"];
//    
//    
    
    
    
    // Add First and Last
    if ([contactCardInfo objectForKey:@"first_name"]) {
        ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)[contactCardInfo objectForKey:@"first_name"], nil);
    }
        
    if ([contactCardInfo objectForKey:@"last_name"]) {
        ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFStringRef)[contactCardInfo objectForKey:@"last_name"], nil);
    }
        
    if ([contactCardInfo objectForKey:@"email"]) {
        ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)([contactCardInfo objectForKey:@"email"]), kABHomeLabel, NULL);
        ABRecordSetValue(person, kABPersonEmailProperty, multiEmail, NULL);
    }
    
    if ([contactCardInfo objectForKey:@"home_phone"]) {
        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)[contactCardInfo objectForKey:@"home_phone"], kABPersonPhoneMainLabel, NULL);
    }
    
    if ([contactCardInfo objectForKey:@"mobile_phone"]) {
        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)[contactCardInfo objectForKey:@"mobile_phone"], kABPersonPhoneMobileLabel, NULL);
    }
    
    if ([contactCardInfo objectForKey:@"business_phone"]) {
        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)[contactCardInfo objectForKey:@"business_phone"], kABPersonPhoneMainLabel, NULL);
    }
    
    
    ABAddressBookAddRecord(addressBookRef, person, nil);
    ABAddressBookSave(addressBookRef, nil);
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Contact Added" message:@"This card has been added to your contacts." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    [av show];
}

- (void)showAlert:(NSString *)alertText withTitle:(NSString *)title {
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:title message:alertText delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertMsg show];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
