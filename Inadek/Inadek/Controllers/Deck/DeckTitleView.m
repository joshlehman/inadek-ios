//
//  DeckTitleView.m
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckTitleView.h"

@implementation DeckTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)initWithDeck:(Deck *)deck andParent:(id)parentVC
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        UILabel *deckTitle = UILabel.new;
        [deckTitle setStyleClass:@"lbl-deck-title"];
        [deckTitle setText:deck.name];
        
        [self addSubview:deckTitle];
        [deckTitle makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@28);
            make.width.equalTo(self);
            make.height.equalTo(@18);
            _deckTitleMS = make.left.equalTo(@0);
        }];
        
        
        UILabel *deckSubtitle = UILabel.new;
        [deckSubtitle setStyleClass:@"lbl-deck-subtitle"];
        
        if (deck.updated_at) {
            
            NSString *timeUpdated = @"Updated ";
 
            timeUpdated = [timeUpdated stringByAppendingString:[deck.updated_at distanceOfTimeInWords]];
            timeUpdated = [[[timeUpdated substringToIndex:1] uppercaseString] stringByAppendingString:[[timeUpdated substringFromIndex:1] lowercaseString]];

            [deckSubtitle setText:timeUpdated];
            
            [self addSubview:deckSubtitle];
            [deckSubtitle makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(deckTitle.bottom).with.offset(-2);
                make.width.equalTo(self);
                make.height.equalTo(@16);
                make.centerX.equalTo(deckTitle);
            }];
            
        }
        
        
        UIButton *backButton = UIButton.new;
        [backButton setImage:[UIImage imageNamed:@"Btn-Back-White"] forState:UIControlStateNormal];
        [backButton setContentEdgeInsets:UIEdgeInsetsMake(18, 15, 18, 15)];
        [backButton setContentMode:UIViewContentModeScaleToFill];
        [backButton addTarget:parentVC action:@selector(dismissDeck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backButton];
        [backButton makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).with.offset(13);
            make.left.equalTo(self);
            make.width.equalTo(@56);
            make.height.equalTo(@66);
        }];
        
        UIButton *showCards = UIButton.new;
        [showCards setImage:[UIImage imageNamed:@"Btn-AllCards"] forState:UIControlStateNormal];
        [showCards setContentEdgeInsets:UIEdgeInsetsMake(12, 0, 0, 9)];
        [showCards setContentMode:UIViewContentModeScaleToFill];
        [showCards addTarget:parentVC action:@selector(showCardCollection) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:showCards];
        [showCards makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).with.offset(13);
            make.right.equalTo(self);
            make.width.equalTo(@58);
            make.height.equalTo(@55);
        }];
        
    }
    return self;
}

- (id)initWithCard:(Card *)card andParent:(id)parentVC
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        UILabel *deckTitle = UILabel.new;
        [deckTitle setStyleClass:@"lbl-deck-title"];
        [deckTitle setText:card.name];
        
        [self addSubview:deckTitle];
        [deckTitle makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@28);
            make.width.equalTo(self);
            make.height.equalTo(@18);
            _deckTitleMS = make.left.equalTo(@0);
        }];
        
        
        UILabel *deckSubtitle = UILabel.new;
        [deckSubtitle setStyleClass:@"lbl-deck-subtitle"];
        
        if (card.updated_at) {
            
            NSString *timeUpdated = @"Updated ";
            
            timeUpdated = [timeUpdated stringByAppendingString:[card.updated_at distanceOfTimeInWords]];
            timeUpdated = [[[timeUpdated substringToIndex:1] uppercaseString] stringByAppendingString:[[timeUpdated substringFromIndex:1] lowercaseString]];
            
            [deckSubtitle setText:timeUpdated];
            
            [self addSubview:deckSubtitle];
            [deckSubtitle makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(deckTitle.bottom).with.offset(-2);
                make.width.equalTo(self);
                make.height.equalTo(@16);
                make.centerX.equalTo(deckTitle);
            }];
            
        }
        
        
        UIButton *backButton = UIButton.new;
        [backButton setImage:[UIImage imageNamed:@"Btn-Back-White"] forState:UIControlStateNormal];
        [backButton setContentEdgeInsets:UIEdgeInsetsMake(18, 15, 18, 15)];
        [backButton setContentMode:UIViewContentModeScaleToFill];
        [backButton addTarget:parentVC action:@selector(dismissDeck:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backButton];
        [backButton makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).with.offset(13);
            make.left.equalTo(self);
            make.width.equalTo(@56);
            make.height.equalTo(@66);
        }];
    }
    return self;
}

@end
