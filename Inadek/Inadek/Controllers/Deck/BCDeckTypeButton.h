//
//  BCDeckTypeButton.h
//  Inadek
//
//  Created by Josh Lehman on 1/16/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import "BCButton.h"
#import "CreateDeckViewController.h"

@class CreateDeckViewController;

@interface BCDeckTypeButton : UIView

@property (nonatomic, retain) CreateDeckViewController *parentViewController;

- (void)drawButtonWithType:(NSDictionary *)typeDictionary;

@end
