//
//  BCDeckTypeButton.m
//  Inadek
//
//  Created by Josh Lehman on 1/16/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCDeckTypeButton.h"

@implementation BCDeckTypeButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIView *background = UIView.new;
        [background setBackgroundColor:[UIColor whiteColor]];
        [background setStyleClass:@"box-deck-cell"];
        
        [self addSubview:background];
        
        [background mas_makeConstraints:^(MASConstraintMaker *make){
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(3,3,3,3));
        }];
        
        [self setClipsToBounds:YES];
        
    }
    return self;
}

- (void)drawButtonWithType:(NSDictionary *)typeDictionary
{
    UIImage *existing = [UIImage imageNamed:[NSString stringWithFormat:@"DeckType-%@", [typeDictionary objectForKey:@"cid"]]];
    if (existing) {
        // Load the decktype specific image
        UIImageView *iv = [[UIImageView alloc] initWithImage:existing];
        [iv setContentMode:UIViewContentModeScaleAspectFit];
        [self addSubview:iv];
        
        [iv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(20, 30, 40, 30));
        }];
    } else {
        // Load a default image
        UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DeckType-base"]];
        [iv setContentMode:UIViewContentModeScaleAspectFit];
        [self addSubview:iv];
        
        [iv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(20, 30, 40, 30));
        }];
    }
    
    UILabel *title = UILabel.new;
    [title setText:[NSString stringWithFormat:@"%@ %@", [typeDictionary objectForKey:@"name"], @"Deck"]];
    [title setStyleClass:@"lbl-new-button-sm"];
    [title setTextAlignment:NSTextAlignmentCenter];
    
    [self addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self);
        make.height.equalTo(@30);
        make.bottom.equalTo(self).with.offset(-10);
        make.centerX.equalTo(self);
    }];
    
    BCButton *buttonOverlay = BCButton.new;
    
    [buttonOverlay addTarget:self action:@selector(didTapButtonForHighlight:) forControlEvents:UIControlEventTouchDown];
    [buttonOverlay addTarget:self action:@selector(didUnTapButtonForHighlight:) forControlEvents:UIControlEventTouchUpInside];
    [buttonOverlay addTarget:self action:@selector(didUnTapButtonForHighlight:) forControlEvents:UIControlEventTouchUpOutside];
    [buttonOverlay addTarget:self action:@selector(selectDeckType:) forControlEvents:UIControlEventTouchUpInside];
    
    buttonOverlay.userInfo = @{@"cid":[typeDictionary objectForKey:@"cid"]};
    
    [self addSubview:buttonOverlay];
    [buttonOverlay.layer setCornerRadius:2.0];
    [buttonOverlay mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(2, 2, 2, 2));
    }];
    
}

- (void)selectDeckType:(BCButton *)sender
{
    NSDictionary *userInfo = sender.userInfo;
    if ([userInfo objectForKey:@"cid"]) {
        [self.parentViewController startDeckType:[userInfo objectForKey:@"cid"]];
    } else {
        [self.parentViewController startDeckType:@"general"];
    }
}

- (void)didTapButtonForHighlight:(id)sender
{
    [sender setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
}

- (void)didUnTapButtonForHighlight:(id)sender
{
    [UIView animateWithDuration:0.4 animations:^{
        [sender setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0]];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
