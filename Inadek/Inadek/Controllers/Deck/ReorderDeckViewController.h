//
//  ReorderDeckViewController.h
//  Inadek
//
//  Created by Josh Lehman on 11/7/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"

@class DeckViewController, Deck;

@interface ReorderDeckViewController : BCViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    BOOL canMoveCards;
}

@property(nonatomic, retain) UICollectionView *deckCV;
@property(nonatomic, retain) NSMutableArray *cards;
@property(nonatomic, retain) DeckViewController *delegate;
@property(nonatomic, retain) Deck *deck;

- (void)loadCards:(NSMutableArray *)cards;
- (void)moveCardInDeck:(UIGestureRecognizer *)gesture;

- (void)setCanMoveCards:(BOOL)canMove;

@end
