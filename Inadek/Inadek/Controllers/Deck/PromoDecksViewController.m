//
//  PromoDecksViewController.m
//  Inadek
//
//  Created by Josh Lehman on 2/8/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "PromoDecksViewController.h"

@implementation PromoDecksViewController

- (void)viewDidLoad
{
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    [self showPromoDeckRequests];
}

- (void)showPromoDeckRequests
{
    UIView *container = UIView.new;
    [container setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:container];
    
    [container makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    _detailsSV = UIScrollView.new;
    [_detailsSV setBackgroundColor:[UIColor clearColor]];
    [container addSubview:_detailsSV];
    
    [_detailsSV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(container);
        make.left.equalTo(container);
        make.right.equalTo(container);
        make.bottom.equalTo(container.bottom).with.offset(-80);
    }];
    
    scrollHolder = UIView.new;
    [_detailsSV addSubview:scrollHolder];
    [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_detailsSV);
        make.width.equalTo(_detailsSV);
    }];
    
    int total_height = 0;
    
    UILabel *pendingSummary = UILabel.new;
    [pendingSummary setStyleClass:@"lbl-hero"];
    [pendingSummary setNumberOfLines:0];
    [pendingSummary setTextAlignment:NSTextAlignmentCenter];
    [pendingSummary setText:@"Review promotional deck requests and decide whether to approve or deny. If no action taken within 10 days, request will be denied."];
    
    [self.view addSubview:pendingSummary];
    [pendingSummary makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@20);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.equalTo(@60);
    }];
    
    total_height = total_height + 100;

    NSArray *fieldsArray = [[NSArray alloc] initWithObjects:
                   @{@"style":[NSNumber numberWithInt:kFieldStyleSelect],
                     @"title":@"Pending",
                     @"name":@"requests",
                     @"value":@"4 Requests",
                     @"action_callback":@"doChangeDeckType:"},
                   
                   nil];
    
    BCFieldSetView *promoRequests = [[BCFieldSetView alloc] initWithTitle:nil dataFields:fieldsArray andStyle:@"white"];
    promoRequests.delegate = self;
    
    [scrollHolder addSubview:promoRequests];
    [promoRequests makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(pendingSummary.bottom).with.offset(15);
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([promoRequests fieldHeight]);
    }];
    
    total_height += [[promoRequests fieldHeight] integerValue] + 20;
    
    
    fieldsArray = [[NSArray alloc] initWithObjects:
                           @{@"style":[NSNumber numberWithInt:kFieldStyleSelectDeck],
                             @"title":@"Bob's Surf Shop",
                             @"subtitle":@"$20 / 20k views, Ending in 8 Days",
                             @"action_callback":@"doChangeDeckType:"},
                           @{@"style":[NSNumber numberWithInt:kFieldStyleSelectDeck],
                             @"title":@"The Dive Bar",
                             @"subtitle":@"$32 / 20k views, Unlimited",
                             @"action_callback":@"doChangeDeckType:"},
                           @{@"style":[NSNumber numberWithInt:kFieldStyleSelectDeck],
                             @"title":@"Baseball Prospectus",
                             @"subtitle":@"$45 / 20k views, 35% of Budget",
                             @"action_callback":@"doChangeDeckType:"},
                   
                            nil];
    
    BCFieldSetView *promoActive = [[BCFieldSetView alloc] initWithTitle:@"CURRENT PROMO DECKS" dataFields:fieldsArray andStyle:@"white"];
    promoRequests.delegate = self;
    
    [scrollHolder addSubview:promoActive];
    [promoActive makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(promoRequests.bottom).with.offset(15);
        make.width.equalTo(scrollHolder.width);
        make.height.equalTo([promoActive fieldHeight]);
    }];
    
    total_height += [[promoActive fieldHeight] integerValue] + 20;
    
    
    current_total_height = total_height;
    
    [scrollHolder makeConstraints:^(MASConstraintMaker *make) {
        scrollHolderHeightCS = make.height.equalTo(@0).with.offset(total_height);
    }];
    
}

@end
