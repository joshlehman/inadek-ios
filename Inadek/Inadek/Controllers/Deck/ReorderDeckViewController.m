//
//  ReorderDeckViewController.m
//  Inadek
//
//  Created by Josh Lehman on 11/7/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "ReorderDeckViewController.h"
#import "DeckViewController.h"
#import "DeckCVCell.h"
#import "Card+Core.h"
#import "Deck+Core.h"
#import "AppDelegate.h"

@interface ReorderDeckViewController ()

@end

@implementation ReorderDeckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.itemSize = CGSizeMake(90.0, 120.0);
    [flowLayout setSectionInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    
    self.deckCV = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.deckCV.backgroundColor = [UIColor clearColor];
    
    // Register the collection cell
    [self.deckCV registerClass:[DeckCVCell class] forCellWithReuseIdentifier:@"cardCell"];
    
    [self.deckCV setDataSource:self];
    [self.deckCV setDelegate:self];
    self.deckCV.alwaysBounceHorizontal = YES;
    [self.view addSubview:self.deckCV];
    [self.deckCV makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
}

- (void)refreshData
{
    //[self.deckCV reloadData];
}

- (void)loadCards:(NSMutableArray *)cards {
    self.cards = cards;
    [self.deckCV reloadData];
}

- (void)setCanMoveCards:(BOOL)canMove
{
    canMoveCards = canMove;
}

- (void)moveCardInDeck:(UIGestureRecognizer *)gesture
{
    
    if (!canMoveCards) {
        return;
    }
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            NSIndexPath *indexPathOfCentralCell = [_deckCV indexPathForItemAtPoint:[gesture locationInView:_deckCV]];
            if (indexPathOfCentralCell) {
                NSLog(@"STARTED movement");
                [_deckCV beginInteractiveMovementForItemAtIndexPath:indexPathOfCentralCell];
                break;
            } else {
                break;
            }
            
        }
        case UIGestureRecognizerStateChanged:
        {
            NSLog(@"CHANGED movement");
            [_deckCV updateInteractiveMovementTargetPosition:[gesture locationInView:gesture.view]];
            break;
            
        }
        case UIGestureRecognizerStateEnded:
        {
            NSLog(@"ENDED movement");
            [_deckCV endInteractiveMovement];
            [self refreshData];
            break;
        }
            
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 6;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSLog(@"Count: %lu", (unsigned long)_cards.count);
    return _cards.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Setup cell identifier
    static NSString *cellIdentifier = @"cardCell";
    
    /* Uncomment this block to use subclass-based cells */
    DeckCVCell *cell = (DeckCVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    Card *card = [_cards objectAtIndex:indexPath.row];
    [cell setSize:@"small"];
    [cell buildWithCard:card];
    
    [cell.layer setCornerRadius:4];
    
    NSLog(@"....... created cell");
    
    // Return the cell
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    
    if (_deck && _cards) {
        id card = [_cards objectAtIndex:sourceIndexPath.row];
        [_cards removeObjectAtIndex:sourceIndexPath.row];
        [_cards insertObject:card atIndex:destinationIndexPath.row];
        
        [[DeckManager sharedManager] updateCardSort:_cards forDeck:_deck];
    }
    
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    id currentCell = [collectionView cellForItemAtIndexPath:indexPath];
    if ([currentCell isKindOfClass:[DeckCVCell class]]) {
        
        if (self.delegate) {
            [(DeckViewController *)self.delegate goIntoDeckAtIndex:(indexPath.row + 1)];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
