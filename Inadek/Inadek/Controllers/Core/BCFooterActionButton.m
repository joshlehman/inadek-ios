//
//  BCFooterActionButton.m
//  Inadek
//
//  Created by Josh Lehman on 1/17/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCFooterActionButton.h"

@implementation BCFooterActionButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setStyleClass:@"box-shadow-upper"];
        
        [self setBackgroundColor:[UIColor colorWithHexString:@"FFFFFF"]];
        continueBtn = BCButton.new;
        [continueBtn buildType:@"orange"];
        [continueBtn setTitle:@"CREATE" forState:UIControlStateNormal];
        [continueBtn addTarget:self action:@selector(doContinueAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:continueBtn];
        [continueBtn makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(20, 20, 20, 20));
        }];
        
    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    [continueBtn setTitle:title forState:UIControlStateNormal];
}

- (void)doContinueAction:(id)sender {
    [self.delegate footerActionButton:self didContinue:YES];
}

@end
