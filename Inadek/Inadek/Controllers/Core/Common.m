//
//  Common.m
//  Inadek
//
//  Created by Josh Lehman on 1/12/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "Common.h"
#import "UIImage+BCImage.h"

#define kFieldStyleText                     20
#define kFieldStyleTextView                 21
#define kFieldStylePassword                 22
#define kFieldStyleMultiselect              23
#define kFieldStyleSelect                   24
#define kFieldStyleSelectText               35
#define kFieldStyleSwitch                   25
#define kFieldStyleTextLowercase            26
#define kFieldStyleTextEmail                27
#define kFieldStyleTextSelect               28
#define kFieldStyleTextSelectPhone          29
#define kFieldStyleTextSelectEmail          30
#define kFieldStyleAddButton                31
#define kFieldStyleTextCapped               32
#define kFieldStyleSelectDeck               33
#define kFieldStyleReorderButton            34
#define kFieldStyleDeleteButton             36

@implementation Common

+ (NSString *)generateGUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    return (__bridge NSString *)string;
}

+ (NSString *)sanitizedString:(id)inputVal
{
    if ([inputVal isKindOfClass:[NSNull class]]) {
        return @"";
    } else {
        return inputVal;
    }
}

+ (NSNumber *)sanitizedNumber:(id)inputVal
{
    if ([inputVal isKindOfClass:[NSNull class]]) {
        return [NSNumber numberWithInt:0];
    } else {
        if ([inputVal isKindOfClass:[NSNumber class]]) {
            return inputVal;
        } else if ([inputVal isKindOfClass:[NSString class]]) {
            
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *myNumber = [f numberFromString:inputVal];
            
            if (myNumber == nil) {
                return [NSNumber numberWithInt:0];
            } else {
                return myNumber;
            }

        } else {
            return [NSNumber numberWithInt:0];
        }
    }
}

+ (NSNumber *)sanitizedBool:(id)inputVal
{
    if ([inputVal isKindOfClass:[NSNull class]]) {
        return [NSNumber numberWithInt:0];
    } else {
        if ([inputVal isKindOfClass:[NSNumber class]]) {
            return inputVal;
        } else if ([inputVal isKindOfClass:[NSString class]]) {
            if ([inputVal isEqualToString:@"1"]) {
                return [NSNumber numberWithInt:1];
            } else {
                return [NSNumber numberWithInt:0];
            }
        } else {
            return inputVal;
        }
    }
}

+ (NSDate *)sanitizedDate:(id)inputVal
{
    if (inputVal && ![inputVal isKindOfClass:[NSNull class]]) {
        return [NSDate dateWithISO8601String:inputVal];
    } else {
        return nil;
    }
}



+ (NSString *)currentDeckTypeAsString:(Deck *)deck
{
    if (!deck) {
        return @"";
    } else {
        NSDictionary *deckTypeDetails = [[DeckManager sharedManager] deckTypeDetailsById:deck.type_id];
        if (deckTypeDetails) {
            return [deckTypeDetails objectForKey:@"name"];
        }
    }
    
    return @"";
}

+ (NSString *)currentCardTypeAsString:(Card *)card
{
    if (!card) {
        return @"";
    } else {
        NSDictionary *cardTypeDetails = [[DeckManager sharedManager] cardTypeDetailsById:card.type_id];
        if (cardTypeDetails) {
            return [cardTypeDetails objectForKey:@"name"];
        }
    }
    
    return @"";
}

+ (NSArray *)currentCardTypeDefaultFields:(Card *)card
{
    
    NSMutableArray *defaultFields = NSMutableArray.new;
    if (!card) {
        return @"";
    } else {
        NSDictionary *cardTypeDetails = [[DeckManager sharedManager] cardTypeDetailsById:card.type_id];
        if (cardTypeDetails) {
            for (NSDictionary *card_field in [cardTypeDetails objectForKey:@"card_type_fields"]) {
                
                NSString *placeholder = [card_field objectForKey:@"default_placeholder"];
                NSString *value = [card_field objectForKey:@"default_value"];
                NSString *value_type = [card_field objectForKey:@"value_type"];
                
                
                NSNumber *style = [NSNumber numberWithInt:kFieldStyleSelectText];
                if ([value_type rangeOfString:@"|"].location == NSNotFound) {
                    if ([value_type isEqualToString:@"switch"]) {
                        style = [NSNumber numberWithInt:kFieldStyleSwitch];
                    }
                } else {
                    style = [NSNumber numberWithInt:kFieldStyleSelect];
                }
                
                NSDictionary *cardFieldDictionary = @{@"style":style,
                                                      @"value":value,
                                                      @"placeholder":placeholder,
                                                      @"title":[card_field objectForKey:@"name"],
                                                      @"name":[card_field objectForKey:@"name"],
                                                      @"has_title":[Common sanitizedNumber:@1],
                                                      @"action_callback":@"doChangeField:",
                                                      @"action_object":card_field};
                
                [defaultFields addObject:cardFieldDictionary];
            }
        }
    }
    
    return defaultFields;
}

+ (NSString *)defaultPlaceholderForCardType:(Card *)card withDefault:(NSString *)defaultValue
{
    if (!card) {
        return defaultValue;
    } else {
        NSDictionary *cardTypeDetails = [[DeckManager sharedManager] cardTypeDetailsById:card.type_id];
        if (cardTypeDetails && [cardTypeDetails objectForKey:@"title_placeholder"]) {
            if ([[cardTypeDetails objectForKey:@"title_placeholder"] isEqualToString:@""]) {
                return defaultValue;
            } else {
                return [cardTypeDetails objectForKey:@"title_placeholder"];
            }
        }
    }
    
    return defaultValue;
}

+ (NSString *)currentDeckCategoriesAsString:(Deck *)deck
{
    if (!deck) {
        return @"";
    } else {
        if ([[deck.categories allObjects] count] > 1) {
            return [NSString stringWithFormat:@"%lu %@", (unsigned long)[[deck.categories allObjects] count], @"Categories"];
        } else if ([[deck.categories allObjects] count] == 1) {
            DeckCategory *cat = [[deck.categories allObjects] objectAtIndex:0];
            return cat.name;
        } else {
            return @"";
        }
    }
    
    return @"";
}

+ (NSString *)currentCardDecksAsString:(Card *)card
{
    if (!card) {
        return @"";
    } else {
        if ([[card.decks allObjects] count] > 1) {
            return [NSString stringWithFormat:@"%lu %@", (unsigned long)[[card.decks allObjects] count], @"Decks"];
        } else if ([[card.decks allObjects] count] == 1) {
            Deck *deck = [[card.decks allObjects] objectAtIndex:0];
            return deck.name;
        } else {
            return @"";
        }
    }
    
    return @"";
}

+ (NSArray *) triggersForDeckTypeId:(NSNumber *)type_id
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *retData = NSArray.new;
    NSMutableArray *filtered = NSMutableArray.new;
    
    if ([defaults objectForKey:@"triggers"]) {
        for (NSDictionary *t in [defaults objectForKey:@"triggers"]) {
            if ([t objectForKey:@"deck_types"]) {
                for (NSNumber *t_id in [t objectForKey:@"deck_types"]) {
                    if ([t_id integerValue] == [type_id integerValue]) {
                        [filtered addObject:t];
                        break;
                    }
                }
            }
        }
    }
    
    return filtered;
        
}

+ (NSArray *) triggersForCardTypeId:(NSNumber *)type_id
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *retData = NSArray.new;
    NSMutableArray *filtered = NSMutableArray.new;
    
    if ([defaults objectForKey:@"triggers"]) {
        for (NSDictionary *t in [defaults objectForKey:@"triggers"]) {
            if ([t objectForKey:@"card_types"]) {
                for (NSNumber *t_id in [t objectForKey:@"card_types"]) {
                    if ([t_id integerValue] == [type_id integerValue]) {
                        [filtered addObject:t];
                        break;
                    }
                }
            }
        }
    }
    
    return filtered;
    
}

+ (NSDictionary *)fieldWithId:(NSNumber *)id_value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:@"fields"]) {
        
        for (NSDictionary *t in [defaults objectForKey:@"fields"]) {
            if([[t objectForKey:@"id"] integerValue] == [id_value integerValue]) {
                return t;
            }
        }
    }
    
    return nil;
}

+ (NSArray *) fieldsForCardTypeId:(NSNumber *)type_id
{
    

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *retData = NSArray.new;
    NSMutableArray *filtered = NSMutableArray.new;
    
    NSMutableArray *restricted_fields = NSMutableArray.new;
    
    if ([defaults objectForKey:@"card_types"]) {
        for (NSDictionary *t in [defaults objectForKey:@"card_types"]) {
            NSLog(@"card type: %@", t);
            if ([t objectForKey:@"restricted_field_ids"] && ([[t objectForKey:@"id"] integerValue] == [type_id integerValue])) {
                for (NSNumber *i in [t objectForKey:@"restricted_field_ids"]) {
                    [restricted_fields addObject:i];
                }
                break;
            }
        }
    }

    if ([defaults objectForKey:@"card_types"]) {
        for (NSDictionary *t in [defaults objectForKey:@"card_types"]) {
            NSLog(@"card type: %@", t);
            if ([t objectForKey:@"card_type_fields"] && ([[t objectForKey:@"id"] integerValue] == [type_id integerValue])) {
                for (NSDictionary *i in [t objectForKey:@"card_type_fields"]) {
                    [filtered addObject:i];
                }
                break;
            }
        }
    }
    
    //if ([filtered count] == 0) {
        if ([defaults objectForKey:@"fields"]) {
            
            for (NSDictionary *t in [defaults objectForKey:@"fields"]) {
                if(![restricted_fields containsObject:[t objectForKey:@"id"]]) {
                    [filtered addObject:t];
                }
            }
        }
    //}

    
    return filtered;
    
}

+ (NSString *)triggerPlaceholderText:(NSString *)trigger_type
{
    if ([trigger_type isEqualToString:@"trigger_phone"]) {
        return @"x (xxx) xxx-xxxx";
    } else if ([trigger_type isEqualToString:@"trigger_email"]) {
        return @"";
    } else if ([trigger_type isEqualToString:@"trigger_map"]) {
        return @"Enter an address";
    } else if ([trigger_type isEqualToString:@"trigger_web"]) {
        return @"Enter web address";
    } else {
        return @"";
    }
    
    return @"";
}

+ (NSString *)triggerSummaryText:(NSString *)trigger_type
{
    if ([trigger_type isEqualToString:@"trigger_phone"]) {
        return @"The Phone Number action will place an easy call button on your deck cover or card.";
    } else if ([trigger_type isEqualToString:@"trigger_email"]) {
        return @"The Email action will place a quick link to send an email to the address you specify here.";
    } else if ([trigger_type isEqualToString:@"trigger_map"]) {
        return @"The Map action will take the address you enter and connect your deck to this physical location.";
    } else if ([trigger_type isEqualToString:@"trigger_web"]) {
        return @"The Website action will take the web address you enter create a quick launch button on your deck.";
    } else {
        return @"";
    }
    
    return @"";
}

+ (UIKeyboardType)triggerKeyboardType:(NSString *)trigger_type
{
    if ([trigger_type isEqualToString:@"trigger_phone"]) {
        return UIKeyboardTypePhonePad;
    } else if ([trigger_type isEqualToString:@"trigger_email"]) {
        return UIKeyboardTypeEmailAddress;
    } else if ([trigger_type isEqualToString:@"trigger_map"]) {
        return UIKeyboardTypeDefault;
    } else if ([trigger_type isEqualToString:@"trigger_web"]) {
        return UIKeyboardTypeURL;
    } else {
        return UIKeyboardTypeDefault;
    }
    
    return UIKeyboardTypeDefault;
}

+ (UIKeyboardType)fieldKeyboardType:(NSString *)value_type
{
    if ([value_type isEqualToString:@"phone"]) {
        return UIKeyboardTypePhonePad;
    } else if ([value_type isEqualToString:@"email"]) {
        return UIKeyboardTypeEmailAddress;
    } else if ([value_type isEqualToString:@"address"]) {
        return UIKeyboardTypeDefault;
    } else if ([value_type isEqualToString:@"web_address"]) {
        return UIKeyboardTypeURL;
    } else {
        return UIKeyboardTypeDefault;
    }
    
    return UIKeyboardTypeDefault;
}

+ (NSNumber *)fieldStyle:(NSString *)value_type
{
    
    if ([value_type isEqualToString:@"phone"]) {
        return [NSNumber numberWithInt:kFieldStyleText];
    } else if ([value_type isEqualToString:@"email"]) {
        return [NSNumber numberWithInt:kFieldStyleText];
    } else if ([value_type isEqualToString:@"address"]) {
        return [NSNumber numberWithInt:kFieldStyleText];
    } else if ([value_type isEqualToString:@"web_address"]) {
        return [NSNumber numberWithInt:kFieldStyleText];
    } else if ([value_type isEqualToString:@"web"]) {
        return [NSNumber numberWithInt:kFieldStyleText];
    } else {
        return [NSNumber numberWithInt:kFieldStyleTextView];
    }
    
    return [NSNumber numberWithInt:kFieldStyleTextView];
    
}

+ (NSString *)fieldStyleTitlePlaceholder:(NSString *)value_type
{
    if ([value_type isEqualToString:@"phone"]) {
        return @"Phone Type (work, home, etc)";
    } else if ([value_type isEqualToString:@"email"]) {
        return @"Email Title (work, home, etc)";
    } else if ([value_type isEqualToString:@"address"]) {
        return @"Address Type (work, home, etc)";
    } else if ([value_type isEqualToString:@"web_address"]) {
        return @"Display Text";
    } else if ([value_type isEqualToString:@"stat"]) {
        return @"Statistic Name";
    } else {
        return @"Optional Title";
    }
    
    return @"Optional Title";
}

+ (BOOL)isFieldTypePhotos:(NSString *)fieldType
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *aField = [fieldType stringByReplacingOccurrencesOfString:@"field_" withString:@""];
    
    if ([defaults objectForKey:@"fields"]) {
        for (NSDictionary *t in [defaults objectForKey:@"fields"]) {
            if ([[t objectForKey:@"cid"] isEqualToString:aField]) {
                if ([t objectForKey:@"value_type"] && [[t objectForKey:@"value_type"] isEqualToString:@"images"]) {
                    return YES;
                } else {
                    return NO;
                }
            }
        }
    }
    
    return NO;
}


+ (NSDictionary *)triggerDetailsForType:(NSString *)trigger_type
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:@"triggers"]) {
        for (NSDictionary *t in [defaults objectForKey:@"triggers"]) {
            if ([[t objectForKey:@"trigger_type"] isEqualToString:trigger_type]) {
                return t;
            }
        }
    }
    
    return nil;
}

+ (UIColor *) getAverageColor:(UIImage *)image
{
    if (image) {
        UIColor *thisColor = [image averageColor];
        
        if (thisColor) {
            const CGFloat *componentColors = CGColorGetComponents(thisColor.CGColor);
            
            CGFloat darknessScore = (((componentColors[0]*255) * 299) + ((componentColors[1]*255) * 587) + ((componentColors[2]*255) * 114)) / 1000;
            
            if (darknessScore >= 200) {
                return [thisColor darkerColor];
            }
            
            return thisColor;
        } else {
            return [UIColor blackColor];
        }

        
    } else {
        return [UIColor blackColor];
    }
}

+ (NSString *) hexForColor:(UIColor *)color
{
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}

+ (NSString *) getHexForAverageColor:(UIImage *)image
{
    if (image) {
        UIColor *color = [self getAverageColor:image];
        if (color) {
            return [self hexForColor:color];
        } else {
            return @"FFFFFF";
        }
    } else {
        return @"FFFFFF";
    }
}

+ (UIImage *) scaleAndRotateImage:(UIImage *)image
{
    if (image == nil) {
        return nil;
    }
    
    int kMaxResolution = 1600; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


+ (UIImage *)cropImage:(UIImage *)image withRect:(CGRect)cropRect
{
    
    if (image == nil) {
        return nil;
    }
    
    UIGraphicsBeginImageContext(cropRect.size);
    [image drawAtPoint:CGPointMake(-cropRect.origin.x, -cropRect.origin.y)];
    UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cropped;
}

+ (UIImage *)colorizeImage:(UIImage *)image withColor:(UIColor *)color {
    UIGraphicsBeginImageContext(image.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -area.size.height);
    
    CGContextSaveGState(context);
    CGContextClipToMask(context, area, image.CGImage);
    
    [color set];
    CGContextFillRect(context, area);
    
    CGContextRestoreGState(context);
    
    CGContextSetBlendMode(context, kCGBlendModeMultiply);
    
    CGContextDrawImage(context, area, image.CGImage);
    
    UIImage *colorizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return colorizedImage;
}

+ (void)log:(NSString *)string
{
    DLog(@"\r\r▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽\r%@\r△ △ △ △ △ △ △ △ △ △\r\r",string);
}

+ (void)log:(NSString *)string withObject:(id)object
{
    DLog(@"\r\r▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽ ▽\r%@: %@\r△ △ △ △ △ △ △ △ △ △\r\r",string,object);
}

+ (id)nilSafe:(id)object
{
    if (object) {
        return object;
    } else {
        return @"";
    }
}

+ (id)nilSafe:(id)object withNull:(BOOL)isNull
{
    if (object) {
        return object;
    } else {
        return [NSNull null];
    }
}

@end
