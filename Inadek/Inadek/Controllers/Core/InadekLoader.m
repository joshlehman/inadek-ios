//
//  InadekLoader.m
//  Inadek
//
//  Created by Josh Lehman on 1/19/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "InadekLoader.h"

@implementation InadekLoader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)loadSpinner:(NSString *)style andSize:(NSString *)size andTitle:(NSString *)title
{
    
    spinnerIsShown = YES;
    
    if (title) {
        UILabel *titleLabel = UILabel.new;
        [titleLabel setText:title];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        
        if ([size isEqualToString:@"large"]) {
            [titleLabel setStyleClass:@"lbl-loading-lg"];
        } else if ([size isEqualToString:@"small"]) {
            [titleLabel setStyleClass:@"lbl-loading-sm"];
        } else {
            [titleLabel setStyleClass:@"lbl-loading"];
        }
        
        if ([style isEqualToString:@"white"]) {
            [titleLabel setStyleCSS:@"color: #ffffff;"];
        }
        
        
        [self addSubview:titleLabel];
        [titleLabel makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(self);
            make.height.equalTo(@30);
            make.bottom.equalTo(self.bottom);
            make.centerX.equalTo(self.centerX);
        }];
    }
    
    UIImage *baseImage = UIImage.new;
    UIImageView *loaderViews = UIImageView.new;
    
    if ([style isEqualToString:@"white"]) {
        baseImage = [UIImage imageNamed:@"Anim-White0000"];
        
        loaderViews.animationImages = [NSArray arrayWithObjects:
                                       [UIImage imageNamed:@"Anim-White0000"],
                                       [UIImage imageNamed:@"Anim-White0001"],
                                       [UIImage imageNamed:@"Anim-White0002"],
                                       [UIImage imageNamed:@"Anim-White0003"],
                                       [UIImage imageNamed:@"Anim-White0004"],
                                       [UIImage imageNamed:@"Anim-White0005"],
                                       [UIImage imageNamed:@"Anim-White0006"],
                                       [UIImage imageNamed:@"Anim-White0007"],
                                       [UIImage imageNamed:@"Anim-White0008"],
                                       [UIImage imageNamed:@"Anim-White0009"],
                                       [UIImage imageNamed:@"Anim-White0010"],
                                       [UIImage imageNamed:@"Anim-White0011"],
                                       [UIImage imageNamed:@"Anim-White0012"],
                                       [UIImage imageNamed:@"Anim-White0013"],
                                       [UIImage imageNamed:@"Anim-White0014"]
                                       , nil];
        
    } else if ([style isEqualToString:@"grey"]) {
        baseImage = [UIImage imageNamed:@"Anim-Grey0000"];
        
        loaderViews.animationImages = [NSArray arrayWithObjects:
                                       [UIImage imageNamed:@"Anim-Grey0000"],
                                       [UIImage imageNamed:@"Anim-Grey0001"],
                                       [UIImage imageNamed:@"Anim-Grey0002"],
                                       [UIImage imageNamed:@"Anim-Grey0003"],
                                       [UIImage imageNamed:@"Anim-Grey0004"],
                                       [UIImage imageNamed:@"Anim-Grey0005"],
                                       [UIImage imageNamed:@"Anim-Grey0006"],
                                       [UIImage imageNamed:@"Anim-Grey0007"],
                                       [UIImage imageNamed:@"Anim-Grey0008"],
                                       [UIImage imageNamed:@"Anim-Grey0009"],
                                       [UIImage imageNamed:@"Anim-Grey0010"],
                                       [UIImage imageNamed:@"Anim-Grey0011"],
                                       [UIImage imageNamed:@"Anim-Grey0012"],
                                       [UIImage imageNamed:@"Anim-Grey0013"],
                                       [UIImage imageNamed:@"Anim-Grey0014"]
                                       , nil];
        
    }
    
    loaderViews.animationDuration = 0.8;
    
    [self addSubview:loaderViews];
    
    if ([size isEqualToString:@"large"]) {
        [loaderViews makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@75);
            make.height.equalTo(@82);
            make.centerX.equalTo(self);
            
            if (title) {
                make.top.equalTo(@10);
            } else {
                make.centerY.equalTo(self);
            }
        }];
    } else if ([size isEqualToString:@"medium"]) {
        [loaderViews makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@50);
            make.height.equalTo(@54);
            make.centerX.equalTo(self);
            make.centerY.equalTo(self);
        }];
    } else if ([size isEqualToString:@"small"]) {
        [loaderViews makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@30);
            make.height.equalTo(@33);
            make.centerX.equalTo(self);
            make.centerY.equalTo(self);
        }];
    }
    
    [loaderViews startAnimating];
    
    
}

- (void)hideSpinner {
    spinnerIsShown = NO;
    [UIView animateWithDuration:0.3 animations:^{
        [self setAlpha:0.0];
    } completion:^(BOOL finished) {
        //[self removeFromSuperview];
    }];
}

- (BOOL)isShown
{
    if (spinnerIsShown) {
        return YES;
    } else {
        return NO;
    }
}

@end
