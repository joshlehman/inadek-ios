//
//  BCFieldSetView.h
//  Inadek
//
//  Created by Josh Lehman on 1/18/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import "BCCellButton.h"

#define kFieldTypeLoginEmail                10
#define kFieldTypeLoginUsername             11
#define kFieldTypeLoginPassword             12

#define kFieldStyleText                     20
#define kFieldStyleTextView                 21
#define kFieldStylePassword                 22
#define kFieldStyleMultiselect              23
#define kFieldStyleSelect                   24
#define kFieldStyleSelectText               35
#define kFieldStyleSwitch                   25
#define kFieldStyleTextLowercase            26
#define kFieldStyleTextEmail                27
#define kFieldStyleTextSelect               28
#define kFieldStyleTextSelectPhone          29
#define kFieldStyleTextSelectEmail          30
#define kFieldStyleAddButton                31
#define kFieldStyleTextCapped               32
#define kFieldStyleSelectDeck               33
#define kFieldStyleReorderButton            34
#define kFieldStyleDeleteButton             36
#define kFieldStylePhotos                   40

@protocol BCFieldSetDelegate;

@interface BCFieldSetView : UIView <UITextFieldDelegate, UITextViewDelegate> {
    NSNumber *viewHeight;
    id myFirstResponder;
    NSMutableArray *textFields;
    NSMutableArray *baseFields;
    id currentEditingField;
    
    NSString *currentStyle;
}

@property (nonatomic,assign) id<BCFieldSetDelegate> delegate;    // weak reference

@property (nonatomic, retain) UILabel *groupTitle;
@property (nonatomic, retain) NSArray *fields;
@property (nonatomic, retain) NSString *overlayStyle;

- (id)initWithTitle:(NSString *)title dataFields:(NSArray *)fields andStyle:(NSString *)style;
- (void)rebuildFields:(NSArray *)fields;

- (NSNumber *)fieldHeight;
- (NSNumber *)fieldHeightDebug;
- (void)makeIndexFirstResponder:(int)index;
- (NSString *)valueForFieldNamed:(NSString *)name;
- (void)updateValueForFieldNamed:(NSString *)name withValue:(NSString *)value;

@end

@protocol BCFieldSetDelegate <NSObject>
@required

- (void)fieldSetView:(BCFieldSetView *)view didCallAction:(NSString *)selector withInfo:(NSDictionary *)userInfo;
- (void)fieldSetView:(BCFieldSetView *)view didChangeField:(NSString *)field withValue:(id)value;
- (void)fieldSetView:(BCFieldSetView *)view didBeginEditing:(id)textField;

@end
