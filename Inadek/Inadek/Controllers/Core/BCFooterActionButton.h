//
//  BCFooterActionButton.h
//  Inadek
//
//  Created by Josh Lehman on 1/17/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCButton.h"

@protocol BCFooterActionButtonDelegate;

@interface BCFooterActionButton : UIView {
    BCButton *continueBtn;
}

@property (nonatomic,assign) id<BCFooterActionButtonDelegate> delegate;    // weak reference

@property (nonatomic, retain) BCButton *saveButton;
@property (nonatomic, retain) BCButton *addButton;
@property (nonatomic) BOOL hasAddAction;

- (void)setTitle:(NSString *)title;

@end

@protocol BCFooterActionButtonDelegate <NSObject>
@optional

- (void)footerActionButton:(BCFooterActionButton *)button didContinue:(BOOL)didContinue;
- (void)footerActionButton:(BCFooterActionButton *)button didAddAction:(BOOL)didContinue;

@end

