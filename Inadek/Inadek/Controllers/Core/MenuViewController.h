//
//  MenuViewController.h
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "BCViewController.h"

@class BCBadgeButtonView;

@interface MenuViewController : BCViewController

@property (nonatomic, retain) IBOutlet UIButton *popularBtn;
@property (nonatomic, retain) IBOutlet UIButton *profileBtn;
@property (nonatomic, retain) IBOutlet UIButton *decksBtn;
@property (nonatomic, retain) IBOutlet BCBadgeButtonView *noticesBtn;

- (IBAction)gotoPopular:(id)sender;
- (IBAction)gotoFavorites:(id)sender;
- (IBAction)gotoMyDecks:(id)sender;
- (IBAction)gotoNews:(id)sender;
- (IBAction)createNew:(id)sender;

- (void)setBadge:(NSNumber *)number;

@end
