//
//  MenuViewController.m
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "PopularViewController.h"
#import "FavoritesViewController.h"
#import "MyDecksViewController.h"
#import "NewsViewController.h"
#import "BCBadgeButtonView.h"

#import "InadekAPIManager.h"

@interface MenuViewController ()

@end

@implementation MenuViewController


- (void)setCurrent:(UIButton *)object
{
    [self.popularBtn setSelected:NO];
    [self.profileBtn setSelected:NO];
    [self.decksBtn setSelected:NO];
    [self.noticesBtn setSelected:NO];
    
    [object setSelected:YES];
}

- (IBAction)gotoPopular:(id)sender
{
    PopularViewController *newVC = [[PopularViewController alloc] init];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [self setCurrent:self.popularBtn];
    [appDelegate.transitionVC transitionStackedViewController:newVC];
}

- (IBAction)gotoFavorites:(id)sender
{
    FavoritesViewController *newVC = [[FavoritesViewController alloc] init];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [self setCurrent:self.profileBtn];
    [appDelegate.transitionVC transitionStackedViewController:newVC];
    
    [[InadekAPIManager sharedManager] fetchDecks:^(NSArray *decks) {
        NSLog(@"Fetch done!");
    }];
}

- (IBAction)gotoMyDecks:(id)sender
{
    MyDecksViewController *newVC = [[MyDecksViewController alloc] init];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [self setCurrent:self.decksBtn];
    [appDelegate.transitionVC transitionStackedViewController:newVC];
}

- (IBAction)gotoNews:(id)sender
{
    NewsViewController *newVC = [[NewsViewController alloc] init];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [self setCurrent:self.noticesBtn];
    [appDelegate.transitionVC transitionStackedViewController:newVC];
}

- (IBAction)createNew:(id)sender
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.transitionVC transitionTo:@"addNew" withParams:@{}];
}




- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self placeSubviewNib:@"_MenuView"];
    
    [self.noticesBtn buildBadge];
    
    [self.view setBackgroundColor:[UIColor greenColor]];
    // Do any additional setup after loading the view.
}

- (void)setBadge:(NSNumber *)number
{
    if ([number integerValue] == 0) {
        [self.noticesBtn hideBadge];
    } else {
        [self.noticesBtn showBadge];
        [self.noticesBtn updateNumber:number];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
