//
//  CreatePopupViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "CreateDeckViewController.h"
#import "CreateCardViewController.h"

#import <pop/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>

@interface CreatePopupViewController : BCViewController {
    MASConstraint *newDeckMC;
    MASConstraint *newCardMC;
    
    BOOL allowCardCreate;
}

- (id)init:(BOOL)showCreateCard;

@end
