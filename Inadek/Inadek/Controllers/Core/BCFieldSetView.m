//
//  BCFieldSetView.m
//  Inadek
//
//  Created by Josh Lehman on 1/18/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCFieldSetView.h"
#import "BCTextView.h"
#import <QuartzCore/QuartzCore.h>
#import "BCPhoneFormat.h"

#define kFieldSpacerOffset      -1
#define kExtraPadding           24;

@implementation BCFieldSetView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)initWithTitle:(NSString *)title dataFields:(NSArray *)fields andStyle:(NSString *)style;
{
    self = [self initWithFrame:CGRectMake(0, 0, 0, 0)];
    if (self) {
        
        currentStyle = style;
        viewHeight = [NSNumber numberWithInt:10];
        textFields = [[NSMutableArray alloc] init];
        
        if (title && ![title isEqualToString:@""]) {
            _groupTitle = UILabel.new;
            [_groupTitle setText:title];
            if ([style isEqualToString:@"overlay"]) {
                [_groupTitle setStyleClass:@"lbl-fieldtitle-overlay"];
            } else {
                [_groupTitle setStyleClass:@"lbl-fieldtitle"];
            }
            [self addSubview:_groupTitle];
            [_groupTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(20);
                make.right.equalTo(self).with.offset(-20);
                make.height.equalTo(@30);
                make.top.equalTo(self.top);
            }];
            viewHeight = [NSNumber numberWithInt:40];
        }
        
        if (fields && [fields count] > 0) {
            _fields = fields;
        }
        
        id priorField = nil;
        baseFields = NSMutableArray.new;
        for (NSDictionary *field in _fields) {
            priorField = [self addField:field afterField:priorField withStyle:style];
            [baseFields addObject:priorField];
        }
        
    }
    
    return self;
}

- (void)rebuildFields:(NSArray *)fields
{
    
    if (baseFields) {
        for (UIView *field in baseFields) {
            [field removeFromSuperview];
        }
    }
    
    viewHeight = [NSNumber numberWithInt:10];
    
    if (_groupTitle) {
        viewHeight = [NSNumber numberWithInt:40];
    }
    
    textFields = nil;
    textFields = [[NSMutableArray alloc] init];
    
    baseFields = nil;
    baseFields = NSMutableArray.new;
    
    if (fields && [fields count] > 0) {
        _fields = fields;
    }
    
    id priorField = nil;
    for (NSDictionary *field in _fields) {
        priorField = [self addField:field afterField:priorField withStyle:currentStyle];
        [baseFields addObject:priorField];
    }
    
}

- (UIView *)addField:(NSDictionary *)field afterField:(UIView *)priorField withStyle:(NSString *)style
{
    UIView *thisField = UIView.new;
    
    int defaultTextHeight = 44;
    NSNumber *defaultTextHeightNum = [NSNumber numberWithInt:defaultTextHeight];
    
    int defaultDeckCellHeight = 60;
    
    int defaultTextviewHeight = 120;
    NSNumber *defaultTextViewHeightNum = [NSNumber numberWithInt:defaultTextviewHeight];
    
    UIView *lastfield = nil;
    if (priorField) {
        lastfield = priorField;
    }
    
    [self addSubview:thisField];
    
    switch ([[field objectForKey:@"style"] integerValue]) {
        case kFieldStyleTextView:
        {
            BCTextView *tf = BCTextView.new;
            [tf setStyleClass:@"textarea-normal-small"];
            tf.delegate = self;
            
            if ([field objectForKey:@"value"]) {
                [tf setText:[field objectForKey:@"value"]];
            }
            
            if ([field objectForKey:@"placeholder"]) {
                [tf setPlaceholder:[field objectForKey:@"placeholder"]];
            }
        
            
            if ([[field objectForKey:@"style"] integerValue] == kFieldStyleTextLowercase) {
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeNone];
                [tf setAutocorrectionType:UITextAutocorrectionTypeNo];
            }
            
            if ([[field objectForKey:@"style"] integerValue] == kFieldStyleTextCapped) {
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeWords];
            }
            
            [self setFieldReturnKeyType:tf withType:[field objectForKey:@"return_key"]];
            
            [thisField addSubview:tf];
            
            NSString *placeholder = @"";
            
            [textFields addObject:@{@"object":tf,@"name":[field objectForKey:@"name"],@"placeholder":placeholder}];
            
            [tf makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(16);
                make.right.equalTo(self).with.offset(-16);
                make.height.equalTo(defaultTextViewHeightNum);
                make.top.equalTo(@0);
            }];
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            if (!lastfield) {
                
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextViewHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextViewHeightNum integerValue])];
            
            break;
        }
        case kFieldStyleText:
        case kFieldStylePassword:
        case kFieldStyleTextLowercase:
        case kFieldStyleTextCapped:
        case kFieldStyleTextEmail:
        {
            UITextField *tf = UITextField.new;
            [tf setStyleClass:@"field-normal"];
            tf.delegate = self;
            [tf setClearButtonMode:UITextFieldViewModeWhileEditing];
            if ([field objectForKey:@"placeholder"]) {
                [tf setPlaceholder:[field objectForKey:@"placeholder"]];
            }
            
            if ([field objectForKey:@"value"]) {
                [tf setText:[field objectForKey:@"value"]];
            }
            
            if ([[field objectForKey:@"style"] integerValue] == kFieldStylePassword) {
                [tf setKeyboardType:UIKeyboardTypeDefault];
                [tf setAutocorrectionType:UITextAutocorrectionTypeNo];
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeNone];
                [tf setClearButtonMode:UITextFieldViewModeAlways];
                [tf setSecureTextEntry:YES];
            }
            
            if ([[field objectForKey:@"style"] integerValue] == kFieldStyleTextLowercase) {
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeNone];
                [tf setAutocorrectionType:UITextAutocorrectionTypeNo];
            }
            
            if ([[field objectForKey:@"style"] integerValue] == kFieldStyleTextCapped) {
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeWords];
            }
            
            if ([[field objectForKey:@"style"] integerValue] == kFieldStyleTextEmail) {
                [tf setKeyboardType:UIKeyboardTypeEmailAddress];
                [tf setAutocorrectionType:UITextAutocorrectionTypeNo];
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeNone];
            }
            
            if ([field objectForKey:@"keyboard_type"] && [[field objectForKey:@"keyboard_type"] integerValue] == UIKeyboardTypeURL) {
                [tf setKeyboardType:UIKeyboardTypeURL];
                [tf setAutocorrectionType:UITextAutocorrectionTypeNo];
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeNone];
            }
            
            if ([field objectForKey:@"keyboard_type"] && [[field objectForKey:@"keyboard_type"] integerValue] == UIKeyboardTypePhonePad) {
                [tf setKeyboardType:UIKeyboardTypePhonePad];
                [tf setAutocorrectionType:UITextAutocorrectionTypeNo];
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeNone];
            }
            
            if ([field objectForKey:@"keyboard_type"] && [[field objectForKey:@"keyboard_type"] integerValue] == UIKeyboardTypeEmailAddress) {
                [tf setKeyboardType:UIKeyboardTypeEmailAddress];
                [tf setAutocorrectionType:UITextAutocorrectionTypeNo];
                [tf setAutocapitalizationType:UITextAutocapitalizationTypeNone];
            }
            
            [self setFieldReturnKeyType:tf withType:[field objectForKey:@"return_key"]];
            
            [thisField addSubview:tf];
            
            NSString *placeholder = @"";
            if ([field objectForKey:@"placeholder"]) {
                placeholder = [field objectForKey:@"placeholder"];
            }
            
            [textFields addObject:@{@"object":tf,@"name":[field objectForKey:@"name"],@"placeholder":placeholder}];
            
            [tf makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(20);
                make.right.equalTo(self).with.offset(-20);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            if (!lastfield) {

            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
         
        }
        case kFieldStyleSelectText:
        {
            UILabel *fieldTitle = UILabel.new;
            UILabel *fieldContent = UILabel.new;
            [fieldContent setNumberOfLines:0];
            [fieldContent setLineBreakMode:NSLineBreakByTruncatingMiddle];
            
            
            [fieldContent setStyleClass:@"field-placeholder"];
            if ([field objectForKey:@"placeholder"]) {
                [fieldContent setText:[field objectForKey:@"placeholder"]];
            }
            
            
            if ([field objectForKey:@"value"] && ![[field objectForKey:@"value"] isEqualToString:@""]) {
                [fieldContent setStyleClass:@"field-value-text"];
                [fieldContent setText:[field objectForKey:@"value"]];
            }
            
            NSString *placeholder = @"";
            if ([field objectForKey:@"placeholder"]) {
                placeholder = [field objectForKey:@"placeholder"];
            }
            
            [textFields addObject:@{@"object":fieldContent,@"name":[field objectForKey:@"name"],@"placeholder":placeholder}];
            
            
            UIImageView *disclosure = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-Disclose"]];
            [thisField addSubview:disclosure];
            [disclosure makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@28);
                make.height.equalTo(@33);
                make.centerY.equalTo(thisField);
                make.right.equalTo(thisField);
            }];
            
            BCCellButton *fieldSelector = BCCellButton.new;
            
            if ([field objectForKey:@"action_callback"]) {
                if ([field objectForKey:@"action_object"]) {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"],@"object":[field objectForKey:@"action_object"]}];
                } else {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"]}];
                }
                [fieldSelector addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
            [thisField addSubview:fieldSelector];
            [fieldSelector makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(thisField);
            }];
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [self layoutIfNeeded];
            
            CGSize maxSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width - 48, CGFLOAT_MAX);
            CGSize fieldContentSize = [fieldContent sizeThatFits:maxSize];
            
            int fieldHeightSize = [defaultTextHeightNum integerValue];
            int requiredFieldHeight = fieldContentSize.height + kExtraPadding;
            BOOL addOffset = NO;
            if (requiredFieldHeight > [defaultTextHeightNum integerValue]) {
                fieldHeightSize = fieldContentSize.height + kExtraPadding;
                addOffset = YES;
            }
            
            [fieldTitle setStyleClass:@"field-select-text"];
            
            BOOL addWidthOffset = 0;
            if ([field objectForKey:@"name"] && ![[field objectForKey:@"name"] isEqualToString:@""]) {
                [fieldTitle setText:[field objectForKey:@"name"]];
                addWidthOffset = -110;
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self).with.offset(-24);
                make.width.equalTo(@110);
                make.height.greaterThanOrEqualTo(@10);
                make.top.equalTo(@13);
            }];
            
            
            [thisField addSubview:fieldContent];
            [fieldContent makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(20);
                make.right.equalTo(self).with.offset(-28 - addWidthOffset);
                make.height.greaterThanOrEqualTo(defaultTextHeightNum);
                if (addOffset) {
                    make.top.equalTo(@0).with.offset(12);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(@(fieldHeightSize));
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + fieldHeightSize)];
            
            break;
        }
            
        case kFieldStyleSelect:
        {

            UILabel *fieldTitle = UILabel.new;
            UILabel *fieldContent = UILabel.new;
            [fieldContent setLineBreakMode:NSLineBreakByTruncatingMiddle];
            
            [fieldTitle setStyleClass:@"field-select"];
            if ([field objectForKey:@"title"]) {
                [fieldTitle setText:[field objectForKey:@"title"]];
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(20);
                make.width.equalTo(@80);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            [fieldContent setStyleClass:@"field-placeholder"];
            if ([field objectForKey:@"placeholder"]) {
                [fieldContent setText:[field objectForKey:@"placeholder"]];
            }
            
            if ([field objectForKey:@"value"] && ![[field objectForKey:@"value"] isEqualToString:@""]) {
                [fieldContent setStyleClass:@"field-value"];
                [fieldContent setText:[field objectForKey:@"value"]];
            }
            
            [thisField addSubview:fieldContent];
            [fieldContent makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(fieldTitle.right).with.offset(10);
                make.right.equalTo(self).with.offset(-28);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            NSString *placeholder = @"";
            if ([field objectForKey:@"placeholder"]) {
                placeholder = [field objectForKey:@"placeholder"];
            }
            
            [textFields addObject:@{@"object":fieldContent,@"name":[field objectForKey:@"name"],@"placeholder":placeholder}];
            
            
            UIImageView *disclosure = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-Disclose"]];
            [thisField addSubview:disclosure];
            [disclosure makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@28);
                make.height.equalTo(@33);
                make.centerY.equalTo(thisField);
                make.right.equalTo(thisField);
            }];
            
            BCCellButton *fieldSelector = BCCellButton.new;
            
            if ([field objectForKey:@"action_callback"]) {
                if ([field objectForKey:@"action_object"]) {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"],@"object":[field objectForKey:@"action_object"]}];
                } else {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"]}];
                }
                [fieldSelector addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
            [thisField addSubview:fieldSelector];
            [fieldSelector makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(thisField);
            }];
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
        }

        case kFieldStylePhotos:
        {
            
            UILabel *fieldTitle = UILabel.new;
            UILabel *fieldContent = UILabel.new;
            [fieldContent setLineBreakMode:NSLineBreakByTruncatingMiddle];
            
            [fieldTitle setStyleClass:@"field-value-photos"];
            if ([field objectForKey:@"title"]) {
                [fieldTitle setText:[field objectForKey:@"title"]];
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(20);
                make.width.equalTo(@120);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            [fieldContent setStyleClass:@"field-placeholder"];
            if ([field objectForKey:@"placeholder"]) {
                [fieldContent setText:[field objectForKey:@"placeholder"]];
            }
            
            if ([field objectForKey:@"value"] && ![[field objectForKey:@"value"] isEqualToString:@""]) {
                [fieldContent setStyleClass:@"field-value"];
                [fieldContent setText:[field objectForKey:@"value"]];
            }
            
            [thisField addSubview:fieldContent];
            [fieldContent makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(fieldTitle.right).with.offset(10);
                make.right.equalTo(self).with.offset(-28);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            NSString *placeholder = @"";
            if ([field objectForKey:@"placeholder"]) {
                placeholder = [field objectForKey:@"placeholder"];
            }
            
            [textFields addObject:@{@"object":fieldContent,@"name":[field objectForKey:@"name"],@"placeholder":placeholder}];
            
            
            UIImageView *disclosure = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-Disclose"]];
            [thisField addSubview:disclosure];
            [disclosure makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@28);
                make.height.equalTo(@33);
                make.centerY.equalTo(thisField);
                make.right.equalTo(thisField);
            }];
            
            BCCellButton *fieldSelector = BCCellButton.new;
            
            if ([field objectForKey:@"action_callback"]) {
                if ([field objectForKey:@"action_object"]) {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"],@"object":[field objectForKey:@"action_object"]}];
                } else {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"]}];
                }
                [fieldSelector addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
            [thisField addSubview:fieldSelector];
            [fieldSelector makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(thisField);
            }];
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
        }
        
        case kFieldStyleMultiselect:
        {
            
            UILabel *fieldTitle = UILabel.new;
            UILabel *fieldContent = UILabel.new;
            
            [fieldTitle setStyleClass:@"field-select"];
            if ([field objectForKey:@"title"]) {
                [fieldTitle setText:[field objectForKey:@"title"]];
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(20);
                make.right.equalTo(self).with.offset(-20);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            [fieldContent setStyleClass:@"field-placeholder"];
            if ([field objectForKey:@"placeholder"]) {
                [fieldContent setText:[field objectForKey:@"placeholder"]];
            }
            
            if ([field objectForKey:@"value"] && ![[field objectForKey:@"value"] isEqualToString:@""]) {
                [fieldContent setStyleClass:@"field-value"];
                [fieldContent setText:[field objectForKey:@"value"]];
            }
            
            [thisField addSubview:fieldContent];
            [fieldContent makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(self).with.offset(20);
                make.right.equalTo(self).with.offset(-28);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            NSString *placeholder = @"";
            if ([field objectForKey:@"placeholder"]) {
                placeholder = [field objectForKey:@"placeholder"];
            }
            
            [textFields addObject:@{@"object":fieldContent,@"name":[field objectForKey:@"name"],@"placeholder":placeholder}];
            
            UIImageView *disclosure = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-Disclose"]];
            [thisField addSubview:disclosure];
            [disclosure makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@28);
                make.height.equalTo(@33);
                make.centerY.equalTo(thisField);
                make.right.equalTo(thisField);
            }];
            
            BCCellButton *fieldSelector = BCCellButton.new;
            
            if ([field objectForKey:@"action_callback"]) {
                if ([field objectForKey:@"action_object"]) {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"],@"object":[field objectForKey:@"action_object"]}];
                } else {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"]}];
                }
                [fieldSelector addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [thisField addSubview:fieldSelector];
            [fieldSelector makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(thisField);
            }];
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
            
        }
            
        case kFieldStyleTextSelect:
        case kFieldStyleTextSelectPhone:
        case kFieldStyleTextSelectEmail:
        {
            
            UILabel *fieldTitle = UILabel.new;
            UITextField *fieldContent = UITextField.new;
            
            [fieldContent setAutocorrectionType:UITextAutocorrectionTypeNo];
            [fieldContent setAutocapitalizationType:UITextAutocapitalizationTypeNone];
            [fieldContent setClearButtonMode:UITextFieldViewModeWhileEditing];
            
            if ([[field objectForKey:@"style"] integerValue] == kFieldStyleTextSelectPhone) {
                [fieldContent setKeyboardType:UIKeyboardTypePhonePad];
            } else if ([[field objectForKey:@"style"] integerValue] == kFieldStyleTextSelectEmail) {
                [fieldContent setKeyboardType:UIKeyboardTypeEmailAddress];
            } else {
                [fieldContent setKeyboardType:UIKeyboardTypeDefault];
            }
            
            
            [fieldTitle setStyleClass:@"field-select"];
            if ([field objectForKey:@"title"]) {
                [fieldTitle setText:[field objectForKey:@"title"]];
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(20);
                make.right.equalTo(self).with.offset(-20);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            [fieldContent setStyleClass:@"field-value-blk"];
            
            if ([field objectForKey:@"placeholder"]) {
                [fieldContent setPlaceholder:[field objectForKey:@"placeholder"]];
            }
            
            if ([field objectForKey:@"value"] && ![[field objectForKey:@"value"] isEqualToString:@""]) {
                [fieldContent setText:[field objectForKey:@"value"]];
            }
            
            [thisField addSubview:fieldContent];
            [fieldContent makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(self).with.offset(20);
                make.right.equalTo(self).with.offset(-20);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            NSString *placeholder = @"";
            if ([field objectForKey:@"placeholder"]) {
                placeholder = [field objectForKey:@"placeholder"];
            }
            
            [textFields addObject:@{@"object":fieldContent,@"name":[field objectForKey:@"name"],@"placeholder":placeholder}];
            
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
        }
            
        case kFieldStyleSelectDeck:
        {
            UILabel *deckTitle = UILabel.new;
            [deckTitle setStyleClass:@"field-select-deck"];
            
            if ([field objectForKey:@"title"]) {
                [deckTitle setText:[field objectForKey:@"title"]];
            }
            
            [thisField addSubview:deckTitle];
            [deckTitle makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(thisField).with.offset(12);
                make.left.equalTo(thisField).with.offset(20);
                make.right.equalTo(thisField).with.offset(10);
                make.height.equalTo(@18);
            }];
            
            
            UILabel *subDeckTitle = UILabel.new;
            [subDeckTitle setStyleClass:@"field-select-deck-subtitle"];
            
            if ([field objectForKey:@"subtitle"]) {
                [subDeckTitle setText:[field objectForKey:@"subtitle"]];
            }
            
            [thisField addSubview:subDeckTitle];
            [subDeckTitle makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(deckTitle.bottom).with.offset(0);
                make.left.equalTo(thisField).with.offset(20);
                make.right.equalTo(thisField).with.offset(10);
                make.height.equalTo(@18);
            }];
        
            
            UIImageView *disclosure = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-Disclose"]];
            [thisField addSubview:disclosure];
            [disclosure makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@28);
                make.height.equalTo(@33);
                make.centerY.equalTo(thisField);
                make.right.equalTo(thisField);
            }];
            
            BCCellButton *fieldSelector = BCCellButton.new;
            
            if ([field objectForKey:@"action_callback"]) {
                if ([field objectForKey:@"action_object"]) {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"],@"object":[field objectForKey:@"action_object"]}];
                } else {
                    [fieldSelector setUserInfo:@{@"action":[field objectForKey:@"action_callback"]}];
                }
                [fieldSelector addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
            [thisField addSubview:fieldSelector];
            [fieldSelector makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(thisField);
            }];
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(@(defaultDeckCellHeight));
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + defaultDeckCellHeight)];
            break;
        }
        case kFieldStyleReorderButton:
        {
            BCCellButton *fieldTitle = BCCellButton.new;
            
            [fieldTitle setStyleClass:@"field-select-reorder"];
            fieldTitle.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            fieldTitle.contentEdgeInsets = UIEdgeInsetsMake(0, 52, 0, 0);
            
            UIImageView *btn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-Reorder"]];
            [fieldTitle addSubview:btn];
            [btn makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(fieldTitle);
                make.width.equalTo(fieldTitle.height);
                make.left.equalTo(fieldTitle.left).with.offset(10);
                make.top.equalTo(fieldTitle);
            }];
            
            if ([field objectForKey:@"title"]) {
                [fieldTitle setTitle:[field objectForKey:@"title"] forState:UIControlStateNormal];
            }
            
            if ([field objectForKey:@"action_callback"]) {
                if ([field objectForKey:@"action_object"]) {
                    [fieldTitle setUserInfo:@{@"action":[field objectForKey:@"action_callback"],@"object":[field objectForKey:@"action_object"]}];
                } else {
                    [fieldTitle setUserInfo:@{@"action":[field objectForKey:@"action_callback"]}];
                }
                [fieldTitle addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(thisField).with.offset(0);
                make.right.equalTo(thisField).with.offset(0);
                make.height.equalTo(thisField);
                make.top.equalTo(@0);
            }];
            
            [thisField setBackgroundColor:[UIColor clearColor]];
            
            [thisField.layer setBorderColor:[UIColor clearColor].CGColor];
            [thisField.layer setBorderWidth:0.0f];
                        
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
        }
        
        case kFieldStyleAddButton:
        {
            BCCellButton *fieldTitle = BCCellButton.new;
            
            [fieldTitle setStyleClass:@"field-select-add"];
            if ([field objectForKey:@"title"]) {
                [fieldTitle setTitle:[field objectForKey:@"title"] forState:UIControlStateNormal];
            }

            if ([field objectForKey:@"action_callback"]) {
                if ([field objectForKey:@"action_object"]) {
                    [fieldTitle setUserInfo:@{@"action":[field objectForKey:@"action_callback"],@"object":[field objectForKey:@"action_object"]}];
                } else {
                    [fieldTitle setUserInfo:@{@"action":[field objectForKey:@"action_callback"]}];
                }
                [fieldTitle addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(thisField).with.offset(0);
                make.right.equalTo(thisField).with.offset(0);
                make.height.equalTo(thisField);
                make.top.equalTo(@0);
            }];
            

            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
        }
            
        case kFieldStyleDeleteButton:
        {
            BCCellButton *fieldTitle = BCCellButton.new;
            
            [fieldTitle setStyleClass:@"field-select-add"];
            [fieldTitle setStyleCSS:@"color: #242424;"];
            if ([field objectForKey:@"title"]) {
                [fieldTitle setTitle:[field objectForKey:@"title"] forState:UIControlStateNormal];
            }
            
            if ([field objectForKey:@"action_callback"]) {
                if ([field objectForKey:@"action_object"]) {
                    [fieldTitle setUserInfo:@{@"action":[field objectForKey:@"action_callback"],@"object":[field objectForKey:@"action_object"]}];
                } else {
                    [fieldTitle setUserInfo:@{@"action":[field objectForKey:@"action_callback"]}];
                }
                [fieldTitle addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(thisField).with.offset(0);
                make.right.equalTo(thisField).with.offset(0);
                make.height.equalTo(thisField);
                make.top.equalTo(@0);
            }];
            
            
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
        }
            
        case kFieldStyleSwitch:
        {
            UILabel *fieldTitle = UILabel.new;
            
            [fieldTitle setStyleClass:@"field-select"];
            if ([field objectForKey:@"title"]) {
                [fieldTitle setText:[field objectForKey:@"title"]];
            }
            
            [thisField addSubview:fieldTitle];
            [fieldTitle makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(20);
                make.right.equalTo(self).with.offset(-20);
                make.height.equalTo(defaultTextHeightNum);
                make.top.equalTo(@0);
            }];
            
            UISwitch *fieldSwitch = UISwitch.new;
            [fieldSwitch setOnTintColor:[UIColor colorWithHexString:@"#F15400"]];
            if ([field objectForKey:@"value"]) {
                if ([[field objectForKey:@"value"] integerValue] == 1) {
                    [fieldSwitch setOn:YES];
                }
            }
            
            [thisField addSubview:fieldSwitch];
            [fieldSwitch makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(thisField.right).with.offset(-12);
                make.top.equalTo(thisField.top).with.offset(6);
            }];
            
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
            } else {
                [thisField setBackgroundColor:[UIColor whiteColor]];
            }
            
            if ([style isEqualToString:@"overlay"]) {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            } else {
                [thisField.layer setBorderColor:[UIColor colorWithWhite:0.0 alpha:0.2].CGColor];
                [thisField.layer setBorderWidth:1.0f];
            }
            
            [thisField makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.left).with.offset(-1);
                make.right.equalTo(self.right).with.offset(1);
                make.height.equalTo(defaultTextHeightNum);
                if (lastfield) {
                    make.top.equalTo(lastfield.bottom).with.offset(kFieldSpacerOffset);
                } else if (_groupTitle) {
                    make.top.equalTo(_groupTitle.bottom);
                } else {
                    make.top.equalTo(@0);
                }
            }];
            
            viewHeight = [NSNumber numberWithInt:([viewHeight integerValue] + [defaultTextHeightNum integerValue])];
            
            break;
        }
        
            
        default:
            break;
    }
    
    return thisField;
    
}

- (void)doAction:(id)sender
{
    if ([sender isKindOfClass:[BCCellButton class]] && self.delegate != nil) {
        [self.delegate fieldSetView:self didCallAction:[[sender userInfo] objectForKey:@"action"] withInfo:[sender userInfo]];
    }
}

- (void)updateValueForFieldNamed:(NSString *)name withValue:(NSString *)value
{
    if (!name || [name isEqualToString:@""]) {
        return;
    }
    
    if (!value) {
        return;
    }
    
    for (NSDictionary *field in textFields) {
        if ([[field objectForKey:@"name"] isEqualToString:name]) {
            if ([[field objectForKey:@"object"] isKindOfClass:[UITextField class]]) {
                UITextField *tv = [field objectForKey:@"object"];
                tv.text = value;
                if ([value isEqualToString:@""]) {
                    
                }
            } else if ([[field objectForKey:@"object"] isKindOfClass:[BCTextView class]]) {
                BCTextView *tv = [field objectForKey:@"object"];
                tv.text = value;
                if ([value isEqualToString:@""]) {
                    
                }
            } else if ([[field objectForKey:@"object"] isKindOfClass:[UILabel class]]) {
                UILabel *tv = [field objectForKey:@"object"];
                tv.text = value;
                [tv setStyleClass:@"field-value"];
                if ([value isEqualToString:@""] && [field objectForKey:@"placeholder"]) {
                    tv.text = [field objectForKey:@"placeholder"];
                    [tv setStyleClass:@"field-placeholder"];
                }
            }
        }
    }
}

- (NSString *)valueForFieldNamed:(NSString *)name
{
    NSString *val = @"";
    for (NSDictionary *field in textFields) {
        if ([[field objectForKey:@"name"] isEqualToString:name]) {
            if ([[field objectForKey:@"object"] isKindOfClass:[UITextField class]]) {
                UITextField *tv = [field objectForKey:@"object"];
                val = tv.text;
            } else if ([[field objectForKey:@"object"] isKindOfClass:[BCTextView class]]) {
                BCTextView *tv = [field objectForKey:@"object"];
                val = tv.text;
            }
        }
    }
    
    return val;
}

- (NSNumber *)fieldHeight
{
    if (viewHeight) {
        return [NSNumber numberWithInt:([viewHeight integerValue] + kFieldSpacerOffset)];
    } else {
        return [NSNumber numberWithInt:10];
    }
}

- (NSNumber *)fieldHeightDebug
{
    if (viewHeight) {
        return [NSNumber numberWithInt:([viewHeight integerValue] + kFieldSpacerOffset)];
    } else {
        return [NSNumber numberWithInt:10];
    }
}

- (void)makeIndexFirstResponder:(int)index
{
    if ([textFields count] > index) {
        if ([[[textFields objectAtIndex:index] objectForKey:@"object"] isKindOfClass:[UITextField class]]) {
            [[[textFields objectAtIndex:index] objectForKey:@"object"] becomeFirstResponder];
        } else if ([[[textFields objectAtIndex:index] objectForKey:@"object"] isKindOfClass:[BCTextView class]]) {
            [[[textFields objectAtIndex:index] objectForKey:@"object"] becomeFirstResponder];
        }
    }
}

- (void)setFieldReturnKeyType:(id)field withType:(NSString *)type
{
    if (field == nil) {
        return;
    }
    
    if (type == nil) {
        if ([field isKindOfClass:[UITextField class]]) {
            [field setReturnKeyType:UIReturnKeyNext];
        }
        return;
    }
    if ([type isEqualToString:@"done"]) {
        if ([field isKindOfClass:[UITextField class]]) {
            [field setReturnKeyType:UIReturnKeyDone];
        }
    } else if ([type isEqualToString:@"next"]) {
        if ([field isKindOfClass:[UITextField class]]) {
            [field setReturnKeyType:UIReturnKeyNext];
        }
    } else if ([type isEqualToString:@"go"]) {
        if ([field isKindOfClass:[UITextField class]]) {
            [field setReturnKeyType:UIReturnKeyGo];
        }
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentEditingField = textField;
    
    UIView *keyboardTopView = UIView.new;
    keyboardTopView.frame = CGRectMake(0, 0, self.bounds.size.width, 40);
    [keyboardTopView setBackgroundColor:[UIColor colorWithHexString:@"cfd3d5"]];
    [keyboardTopView setStyleClass:@"box-keyboard-accessory"];
    [keyboardTopView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    // drop shadow
    [keyboardTopView.layer setShadowColor:[UIColor blackColor].CGColor];
    [keyboardTopView.layer setShadowOpacity:0.06];
    [keyboardTopView.layer setShadowRadius:1.0];
    [keyboardTopView.layer setShadowOffset:CGSizeMake(0.0, -2.0)];
    
    CALayer *TopBorder = [CALayer layer];
    TopBorder.frame = CGRectMake(0.0f, 0.0f, keyboardTopView.bounds.size.width, 0.5f);
    TopBorder.backgroundColor = [UIColor colorWithHexString:@"6c6e6f"].CGColor;
    [TopBorder setOpacity:0.4];
    [keyboardTopView.layer addSublayer:TopBorder];
    
    
    //[textField setInputAccessoryView:keyboardTopView];
    
    int count = 0;
    for (NSDictionary *field in textFields) {
        if ([field objectForKey:@"object"] && [field objectForKey:@"object"] == textField) {
            if ([self.delegate respondsToSelector:@selector(fieldSetView:didBeginEditing:)]) {
                [self.delegate fieldSetView:self didBeginEditing:textField];
            }
        }
        count++;
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    BCPhoneFormat *fmt = [BCPhoneFormat instance];
    
    if (textField.keyboardType == UIKeyboardTypePhonePad) {
        // Call any number of times
        NSString *numberString = textField.text;// the phone number to format
        NSString *formattedNumber = [fmt format:numberString];
        [textField setText:formattedNumber];
    }
    
    int count = 0;
    for (NSDictionary *field in textFields) {
        if ([field objectForKey:@"object"] && [field objectForKey:@"object"] == textField) {
            if ([self.delegate respondsToSelector:@selector(fieldSetView:didChangeField:withValue:)]) {
                [self.delegate fieldSetView:self didChangeField:[field objectForKey:@"name"] withValue:textField.text];
                NSLog(@"CURRENT FIELD");
            }
        }
        count++;
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    int count = 0;
    for (NSDictionary *field in textFields) {
        if ([field objectForKey:@"object"] && [field objectForKey:@"object"] == textField) {
            if ([_fields count] > count && [_fields objectAtIndex:count]) {
                NSDictionary *field = [_fields objectAtIndex:count];
                if (field && [field objectForKey:@"return_key"]) {
                    if ([[field objectForKey:@"return_key"] isEqualToString:@"done"] || [[field objectForKey:@"return_key"] isEqualToString:@"go"]) {
                        // Fire delegate submit method
                        [textField resignFirstResponder];
                    } else if ([[field objectForKey:@"return_key"] isEqualToString:@"next"]) {
                        // Go to next
                        if ([textFields count] > (count + 1) && [textFields objectAtIndex:(count + 1)]) {
                            NSDictionary *tf = [textFields objectAtIndex:(count +1)];
                            if ([[tf objectForKey:@"object"] isKindOfClass:[UITextField class]]) {
                                [[tf objectForKey:@"object"] becomeFirstResponder];
                            }
                        } else {
                            [textField resignFirstResponder];
                        }
                    }
                } else {
                    if ([textFields count] > (count + 1) && [textFields objectAtIndex:(count + 1)]) {
                        NSDictionary *tf = [textFields objectAtIndex:(count +1)];
                        if ([[tf objectForKey:@"object"] isKindOfClass:[UITextField class]]) {
                            [[tf objectForKey:@"object"] becomeFirstResponder];
                        }
                    } else {
                        [textField resignFirstResponder];
                    }
                }
            } else {
                // We're on the last field
            }
            NSLog(@"CURRENT FIELD");
        }
        count++;
    }
    return YES;
}

- (void)didTapButtonForHighlight:(id)sender
{
    [sender setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.1]];
}

- (void)didUnTapButtonForHighlight:(id)sender
{
    [UIView animateWithDuration:0.4 animations:^{
        [sender setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0]];
    }];
}

@end
