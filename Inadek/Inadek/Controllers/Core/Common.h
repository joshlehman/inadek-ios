//
//  Common.h
//  Inadek
//
//  Created by Josh Lehman on 1/12/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ISO8601/ISO8601.h>

#import "Deck+Core.h"
#import "Card+Core.h"
#import "DeckCategory+Core.h"
#import "DeckManager.h"
#import "UIColor+BCColor.h"

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

#define kBCNavPositionLeft      1
#define kBCNavPositionRight     2

#define kBCNavButtonStyleBack       10
#define kBCNavButtonStyleBackWhite  11
#define kBCNavButtonStyleClose      20
#define kBCNavButtonStyleAddWhite   30
#define kBCNavButtonStyleText       40
#define kBCNavButtonStyleTextSmall  41

#define kProfileTypeFan             1
#define kProfileTypeFriend          2
#define kProfileTypeGeneral         3

#define kRefreshDecks               1
#define kRefreshCards               2

@interface Common : NSObject


+ (NSString *)generateGUID;

+ (NSString *)sanitizedString:(id)inputVal;
+ (NSNumber *)sanitizedNumber:(id)inputVal;
+ (NSNumber *)sanitizedBool:(id)inputVal;
+ (NSDate *)sanitizedDate:(id)inputVal;


+ (NSString *)currentDeckTypeAsString:(Deck *)deck;
+ (NSString *)currentDeckCategoriesAsString:(Deck *)deck;
+ (NSString *)currentCardDecksAsString:(Card *)card;
+ (NSString *)currentCardTypeAsString:(Card *)card;
+ (NSArray *)currentCardTypeDefaultFields:(Card *)card;
+ (NSString *)defaultPlaceholderForCardType:(Card *)card withDefault:(NSString *)defaultValue;

+ (NSArray *)triggersForDeckTypeId:(NSNumber *)type_id;
+ (NSArray *)triggersForCardTypeId:(NSNumber *)type_id;
+ (NSArray *)fieldsForCardTypeId:(NSNumber *)type_id;
+ (NSDictionary *)fieldWithId:(NSNumber *)id_value;
+ (NSString *)triggerPlaceholderText:(NSString *)trigger_type;
+ (UIKeyboardType)triggerKeyboardType:(NSString *)trigger_type;
+ (UIKeyboardType)fieldKeyboardType:(NSString *)value_type;
+ (NSString *)triggerSummaryText:(NSString *)trigger_type;
+ (NSDictionary *)triggerDetailsForType:(NSString *)trigger_type;
+ (NSNumber *)fieldStyle:(NSString *)value_type;
+ (NSString *)fieldStyleTitlePlaceholder:(NSString *)value_type;

+ (NSString *) hexForColor:(UIColor *)color;
+ (UIColor *) getAverageColor:(UIImage *)image;
+ (NSString *) getHexForAverageColor:(UIImage *)image;

+ (UIImage *)scaleAndRotateImage:(UIImage *)image;
+ (UIImage *)cropImage:(UIImage *)image withRect:(CGRect)cropRect;
+ (UIImage *)colorizeImage:(UIImage *)image withColor:(UIColor *)color;

+ (BOOL)isFieldTypePhotos:(NSString *)fieldType;

+ (void)log:(NSString *)string;
+ (void)log:(NSString *)string withObject:(id)obj;

+ (id)nilSafe:(id)object;

@end
