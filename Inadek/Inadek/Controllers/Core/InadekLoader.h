//
//  InadekLoader.h
//  Inadek
//
//  Created by Josh Lehman on 1/19/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <PixateFreestyle/PixateFreestyle.h>

@interface InadekLoader : UIView {
    BOOL spinnerIsShown;
}

- (void)loadSpinner:(NSString *)style andSize:(NSString *)size andTitle:(NSString *)title;
- (BOOL)isShown;
- (void)hideSpinner;

@end
