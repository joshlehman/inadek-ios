//
//  CreatePopupViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CreatePopupViewController.h"
#import "CreateCardViewController.h"

@interface CreatePopupViewController ()

@end

@implementation CreatePopupViewController

- (id)init:(BOOL)showCreateCard
{
    self = [super init];
    if (self) {
        if (showCreateCard) {
            allowCardCreate = YES;
        } else {
            allowCardCreate = NO;
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self makeBackgroundView:[UIColor colorWithWhite:0.0 alpha:0.8] animate:@"fadein" withCloseAction:YES];
    
    UIView *newDeckButton = [[UIView alloc] initWithFrame:CGRectZero];
    UIView *newCardButton = [[UIView alloc] initWithFrame:CGRectZero];
    UILabel *header = UILabel.new;
    UIImageView *ivNewCard = UIImageView.new;
    UIImageView *ivNewDeck = UIImageView.new;
    UIButton *btnNewCard = UIButton.new;
    UIButton *btnNewDeck = UIButton.new;
    UILabel *lblNewCard = UILabel.new;
    UILabel *lblNewDeck = UILabel.new;
    UILabel *lblDisabled = UILabel.new;
    
    if (allowCardCreate) {
        [btnNewCard setBackgroundColor:[UIColor clearColor]];
    } else {
        [btnNewCard setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.7]];
    }

    [btnNewDeck setBackgroundColor:[UIColor clearColor]];
    
    [lblNewCard setStyleClass:@"lbl-new-button"];
    [lblNewCard setText:@"NEW CARD"];
    [lblNewCard setTextAlignment:NSTextAlignmentCenter];
    
    [lblNewDeck setStyleClass:@"lbl-new-button"];
    [lblNewDeck setText:@"NEW DECK"];
    [lblNewDeck setTextAlignment:NSTextAlignmentCenter];

    [ivNewCard setImage:[UIImage imageNamed:@"BigBtn-NewCard"]];
    [ivNewCard setContentMode:UIViewContentModeScaleAspectFit];
    
    [ivNewDeck setImage:[UIImage imageNamed:@"BigBtn-NewDeck"]];
    [ivNewDeck setContentMode:UIViewContentModeScaleAspectFit];
    
    [header setStyleClass:@"lbl-new-header"];
    [header setText:@"What would you like to create?"];
    [header setTextAlignment:NSTextAlignmentCenter];
    
    [newCardButton.layer setCornerRadius:3.0];
    [newCardButton setBackgroundColor:[UIColor whiteColor]];
    
    [newDeckButton.layer setCornerRadius:3.0];
    [newDeckButton setBackgroundColor:[UIColor whiteColor]];
    
    [newCardButton addSubview:lblNewCard];
    [newDeckButton addSubview:lblNewDeck];
    [newCardButton addSubview:ivNewCard];
    [newDeckButton addSubview:ivNewDeck];
    [newCardButton addSubview:btnNewCard];
    [newDeckButton addSubview:btnNewDeck];
    
    [self.view addSubview:newCardButton];
    [self.view addSubview:newDeckButton];
    [self.view addSubview:header];
    
    [newCardButton makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@150);
        make.height.equalTo(@170);
        make.centerX.equalTo(self.view.centerX);
        newCardMC = make.top.equalTo(self.view.centerY).with.offset(-180 - (self.view.bounds.size.height / 2)); // 180
    }];
    
    [newDeckButton makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@150);
        make.height.equalTo(@170);
        make.centerX.equalTo(self.view.centerX);
        newDeckMC = make.top.equalTo(self.view.centerY).with.offset(10 - (self.view.bounds.size.height)); // 10
    }];
    
    [header makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.width);
        make.height.equalTo(@30);
        make.bottom.equalTo(newCardButton.top).with.offset(-20);
    }];
    
    [lblNewDeck makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(newDeckButton);
        make.height.equalTo(@30);
        make.centerX.equalTo(newDeckButton);
        make.bottom.equalTo(newDeckButton).with.offset(-15);
    }];
    
    [lblNewCard makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(newCardButton);
        make.height.equalTo(@30);
        make.centerX.equalTo(newCardButton);
        make.bottom.equalTo(newCardButton).with.offset(-15);
    }];
    
    [ivNewDeck makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(newDeckButton).with.offset(-60);
        make.height.equalTo(newDeckButton).with.offset(-60);
        make.centerX.equalTo(newDeckButton);
        make.top.equalTo(newDeckButton).with.offset(16);
    }];
    
    [ivNewCard makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(newCardButton).with.offset(-60);
        make.height.equalTo(newCardButton).with.offset(-60);
        make.centerX.equalTo(newCardButton);
        make.top.equalTo(newCardButton).with.offset(20);
    }];
    
    [btnNewCard makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(newCardButton);
        make.height.equalTo(newCardButton);
        make.centerX.equalTo(self.view.centerX);
        make.top.equalTo(newCardButton);
    }];
    
    if (allowCardCreate) {
        [btnNewCard addTarget:self action:@selector(doNewCard:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [btnNewDeck makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(newDeckButton);
        make.height.equalTo(newDeckButton);
        make.centerX.equalTo(self.view.centerX);
        make.top.equalTo(newDeckButton);
    }];
    
    [btnNewDeck addTarget:self action:@selector(doNewDeck:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)allowCards:(BOOL)allow
{
    
}

- (void)viewDidAppear:(BOOL)animated
{
    
    // 20, 10 - Super springy
    // 5, 10 - Slower entry speed, with only one slight spring back
    // 10, 10 - A slight bounce, but feels a little slow
    // 10, 26 - A little too fast on entry
    // 10, 18 -
    
    [Common log:@"Deck Count" withObject:@([[Deck all] count])];
    
    [Common log:@"Decks" withObject:[[DeckManager sharedManager] allDeckNames]];
    
    POPSpringAnimation *slideInCard = [POPSpringAnimation new];
    slideInCard.toValue = @(-180);
    slideInCard.springBounciness = 10;
    slideInCard.springSpeed = 18;
    slideInCard.property = [POPAnimatableProperty mas_offsetProperty];
    [newCardMC pop_addAnimation:slideInCard forKey:@"PopDeckSlideInAnimation"];
    
    POPSpringAnimation *slideInDeck = [POPSpringAnimation new];
    slideInDeck.toValue = @(10);
    slideInDeck.springBounciness = 10;
    slideInDeck.springSpeed = 18;
    slideInDeck.property = [POPAnimatableProperty mas_offsetProperty];
    [newDeckMC pop_addAnimation:slideInDeck forKey:@"PopDeckSlideInAnimation"];
}

- (void)doNewDeck:(id)sender
{
    CreateDeckViewController *createDeckVC = [[CreateDeckViewController alloc] init];
    createDeckVC.deck = nil;
    [createDeckVC setTitle:@"New Deck"];
    
    [self closeAndTransitionTo:createDeckVC withType:0 buildNavController:YES];
}

- (void)doNewCard:(id)sender
{
    CreateCardViewController *createCardVC = [[CreateCardViewController alloc] init];
    createCardVC.card = nil;
    [createCardVC setTitle:@"New Card"];
    
    [self closeAndTransitionTo:createCardVC withType:0 buildNavController:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
