//
//  CardViewComments.h
//  Inadek
//
//  Created by Josh Lehman on 2/1/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import "InadekLoader.h"

@class BCViewController, CardView, DeckView, Deck, Card;

@interface CardViewComments : UIView <UITableViewDataSource, UITableViewDelegate> {
    UITableView *commentsTV;
    int commentBoxOffset;
    
    UIView *commentsBox;

    BOOL isViewShown;
    
    MASConstraint *commentsTVBottom;
}

@property (nonatomic, retain) BCViewController *baseViewController;
@property (nonatomic, retain) CardView *baseCardView;
@property (nonatomic, retain) DeckView *baseDeckView;
@property (nonatomic, retain) InadekLoader *loader;

- (id)initWithBaseView:(id)baseCard andDeck:(Deck *)deck;
- (id)initWithBaseView:(id)baseCard andCard:(Card *)card;
- (BOOL)isShown;
- (void)setIsShown:(BOOL)shown;
- (void)moveViewUp:(CGSize)size;
- (void)updateFieldText:(NSString *)value;

- (void)reloadComments:(Card *)card;


@end
