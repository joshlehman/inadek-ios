//
//  BCDeckEditImage.m
//  Inadek
//
//  Created by Josh Lehman on 1/16/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCCardEditImage.h"
#import <QuartzCore/QuartzCore.h>
#import <Masonry/Masonry.h>
#import <QuartzCore/QuartzCore.h>
#import "InadekLoader.h"
#import "BCButton.h"

@implementation BCCardEditImage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)buildButtons
{
    UILabel *addLabel = UILabel.new;
    [addLabel setStyleClass:@"lbl-instruction"];
    [addLabel setText:@"You may add a cover photo to your card. Select from the below options:"];
    [addLabel setTextAlignment:NSTextAlignmentCenter];
    [addLabel setNumberOfLines:0];
    
    [self addSubview:addLabel];
    [addLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.height.equalTo(@40);
        make.width.equalTo(@260);
        make.top.equalTo(@10);
    }];

    UIButton *addCRPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [addCRPhoto setImage:[UIImage imageNamed:@"Btn-NewCRPhoto"] forState:UIControlStateNormal];
    [addCRPhoto setContentMode:UIViewContentModeScaleAspectFit];
    [addCRPhoto setStyleClass:@"lbl-camera"];
    [addCRPhoto setBackgroundColor:[UIColor clearColor]];
    [addCRPhoto setTitle:@"Camera Roll" forState:UIControlStateNormal];
    [addCRPhoto setTitleEdgeInsets:UIEdgeInsetsMake(40, -50, 0, 0)];
    [addCRPhoto setImageEdgeInsets:UIEdgeInsetsMake(-30, 23, 0, 0)];
    [addCRPhoto addTarget:self action:@selector(doSelectPhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:addCRPhoto];
    [addCRPhoto mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@100);
        make.height.equalTo(@80);
        make.top.equalTo(@80);
        make.right.equalTo(self.centerX).with.offset(-5);
    }];
    
    
    UIButton *addNewPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [addNewPhoto setImage:[UIImage imageNamed:@"Btn-NewPhoto"] forState:UIControlStateNormal];
    [addNewPhoto setContentMode:UIViewContentModeScaleAspectFit];
    [addNewPhoto setStyleClass:@"lbl-camera"];
    [addNewPhoto setBackgroundColor:[UIColor clearColor]];
    [addNewPhoto setTitle:@"Take Photo" forState:UIControlStateNormal];
    [addNewPhoto setTitleEdgeInsets:UIEdgeInsetsMake(40, -50, 0, 0)];
    [addNewPhoto setImageEdgeInsets:UIEdgeInsetsMake(-30, 23, 0, 0)];
    [addNewPhoto addTarget:self action:@selector(doTakePhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:addNewPhoto];
    [addNewPhoto mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@100);
        make.height.equalTo(@80);
        make.top.equalTo(@80);
        make.left.equalTo(self.centerX).with.offset(5);
    }];
}

- (void)loadImage:(NSString *)image_url
{
    
    for (UIView *v in [self subviews]) {
        if ([v isKindOfClass:[UILabel class]]) {
            [v removeFromSuperview];
        } else if ([v isKindOfClass:[UIButton class]]) {
            [v removeFromSuperview];
        }
    }
    
    if (_imageHolder) {
        [_imageHolder removeFromSuperview];
    }
    
    _imageHolder = UIImageView.new;
    [_imageHolder.layer setCornerRadius:3.0f];
    [_imageHolder setClipsToBounds:YES];
    [self addSubview:_imageHolder];
    [_imageHolder makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.equalTo(self.width);
    }];
    
    if (!_loader) {
        _loader = [[InadekLoader alloc] initWithFrame:CGRectZero];
    }
    
    [_loader loadSpinner:@"white" andSize:@"medium" andTitle:nil];
    [_loader setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:_loader];
    [_loader makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    if (_imageHolder) {
        [_imageHolder setContentMode:UIViewContentModeScaleAspectFill];
        [_imageHolder sd_setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [_loader hideSpinner];
        }];
    }
}

- (void)addImageEditButton
{
    BCButton *editBtn = BCButton.new;
    [editBtn setStyleClass:@"lbl-new-button-sm-white"];
    [editBtn setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.8]];
    [editBtn setTitle:@"Change" forState:UIControlStateNormal];
    
    [self addSubview:editBtn];
    [editBtn makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@80);
        make.height.equalTo(@34);
        make.right.equalTo(self).with.offset(-10);
        make.bottom.equalTo(self).with.offset(-15);
    }];
    
    [editBtn addTarget:self action:@selector(showSelector:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)showSelector:(id)sender
{
    if (self.delegate) {
        [self.delegate cardEditImage:self selectImage:@"show_picker"];
    }
}

- (void)showImage:(UIImage *)image
{
    // Remove label and button views if showing
    for (UIView *v in [self subviews]) {
        if ([v isKindOfClass:[UILabel class]]) {
            [v removeFromSuperview];
        } else if ([v isKindOfClass:[UIButton class]]) {
            [v removeFromSuperview];
        }
    }
    
    if (_imageHolder) {
        [_imageHolder removeFromSuperview];
    }
    
    _imageHolder = UIImageView.new;
    [_imageHolder.layer setCornerRadius:3.0f];
    [_imageHolder setClipsToBounds:YES];
    [self addSubview:_imageHolder];
    [_imageHolder makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.equalTo(self.width);
    }];
    
    [_imageHolder setImage:image];
}

- (void)doSelectPhoto:(id)sender
{
    if (self.delegate) {
        [self.delegate cardEditImage:self selectImage:@"cameraroll"];
    }
}

- (void)doTakePhoto:(id)sender
{
    if (self.delegate) {
        [self.delegate cardEditImage:self selectImage:@"camera"];
    }
}

@end
