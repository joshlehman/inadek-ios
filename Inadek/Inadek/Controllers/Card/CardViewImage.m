//
//  CardViewImage.m
//  Inadek
//
//  Created by Josh Lehman on 1/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardViewImage.h"

@implementation CardViewImage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _imageView = UIImageView.new;
        [_imageView setBackgroundColor:[UIColor colorWithHexString:@"DDDDDD"]];
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:_imageView];
        [_imageView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(-1, -1, -1, -1));
        }];
    }
    return self;
}

- (void)loadImage:(NSString *)url
{
    if (!_loader) {
        _loader = [[InadekLoader alloc] initWithFrame:CGRectZero];
    }
    
    [_loader loadSpinner:@"white" andSize:@"medium" andTitle:nil];
    [_loader setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:_loader];
    [_loader makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    if (_imageView) {
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [_imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [_loader hideSpinner];
        }];
    }
}

- (void)loadLocalImageData:(id)image
{
    if (_loader) {
        [_loader removeFromSuperview];
    }
    
    if (_imageView) {
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        if ([image isKindOfClass:[UIImage class]]) {
            [_imageView setImage:image];
        } else if ([image isKindOfClass:[NSData class]]) {
            [_imageView setImage:[UIImage imageWithData:image]];
        }
    }
}



@end
