//
//  CardView.m
//  Inadek
//
//  Created by Josh Lehman on 2/6/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardView.h"
#import "CardViewImage.h"
#import "CardVieWComments.h"
#import "Card+Core.h"
#import "CardField+Core.h"
#import "DeckViewController.h"
#import "MBProgressHUD.h"

#import "BCPhoneFormat.h"

#import "BCGalleryImageView.h"

#import "JTSImageViewController.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>

#import "BCLabel.h"

#define kTopOffset      70
#define kSidePadding    10

#define kViewTagReturn  90

@implementation CardView {
    UIView *genericOverlay;
}

- (id) initWithMiniView:(BCViewController *)parentVC andCard:(Card *)card
{
    self = [super init];
    if (self) {
        
        if (parentVC) {
            _cardHolderVC = parentVC;
        }
        
        UIView *grey = UIView.new;
        [grey setBackgroundColor:[UIColor colorWithWhite:0.90 alpha:1.0]];
        [self addSubview:grey];
        [grey.layer setCornerRadius:1.0];

        
        if (card.image_thumb_url) {
            
            [grey makeConstraints:^(MASConstraintMaker *make) {
                make.top.left.right.equalTo(self).with.insets(UIEdgeInsetsMake(5, 5, 5, 5));
                make.height.equalTo(grey.width);
            }];
            
            _cardImage = [[CardViewImage alloc] initWithFrame:CGRectZero];
            [_cardImage setBackgroundColor:[UIColor redColor]];
            [grey addSubview:_cardImage];
            [_cardImage makeConstraints:^(MASConstraintMaker *make) {
                make.top.left.bottom.right.equalTo(grey);
            }];
            
            [_cardImage loadImage:card.image_thumb_url];
        } else {
            
            [grey makeConstraints:^(MASConstraintMaker *make) {
                make.top.left.right.equalTo(self).with.insets(UIEdgeInsetsMake(5, 5, 5, 5));
                make.height.equalTo(@0);
            }];
            
        }
        
        UILabel *title = UILabel.new;
        [title setStyleClass:@"lbl-cell-title"];
        [title setNumberOfLines:0];
        [title setText:card.name];
        [self addSubview:title];
        [title makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(grey.bottom).with.offset(5);
            make.left.equalTo(grey).with.offset(5);
            make.right.equalTo(grey).with.offset(-5);
            make.height.equalTo(@18);
        }];
        
        // drop shadow
        [self.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.layer setShadowOpacity:0.6];
        [self.layer setShadowRadius:2.0];
        [self.layer setShadowOffset:CGSizeMake(0.0, 1.0)];
        
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.layer setCornerRadius:3.0];
        [self setBackgroundColor:[UIColor whiteColor]];
        
        [self setUserInteractionEnabled:YES];
        
    }
    
    return self;
}

- (id) initWithCard:(BCViewController *)parentVC andCard:(Card *)card
{
    self = [super init];
    if (self) {

        self = [self initWithFrame:CGRectZero];
        
        self.card = card;
        self.cardHolderVC = parentVC;
        
    }
    
    return self;
}

- (void)buildInitialCardLayout
{
    int hasImageOffset = 0;
    if (self.card.image_url && ![self.card.image_url isEqualToString:@""]) {
        hasImageOffset = 180;
    } else if (self.card.imageObj) {
        hasImageOffset = 180;
    }
    
    
    // Front of card, view
    _card_front = UIView.new;
    [self addSubview:_card_front];
    [_card_front makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_card_front setBackgroundColor:[UIColor whiteColor]];
    [_card_front.layer setCornerRadius:3.0];
    [_card_front setClipsToBounds:YES];
    
    _sv_card_front = UIScrollView.new;
    [_sv_card_front setDelegate:self];
    [_sv_card_front setScrollEnabled:YES];
    
    _cf_holder = UIView.new;
    [_cf_holder setBackgroundColor:[UIColor clearColor]];
    
    [_sv_card_front setBackgroundColor:[UIColor clearColor]];
    [_card_front addSubview:_sv_card_front];
    
    [_sv_card_front makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_card_front).with.insets(UIEdgeInsetsMake(hasImageOffset, 0, 0, 0));
    }];
    
    [_sv_card_front addSubview:_cf_holder];
    
    [_cf_holder makeConstraints:^(MASConstraintMaker *make) {
        make.edges.width.equalTo(_sv_card_front);
    }];
    
    side = @"front";
    
    // Back of card, view
    _card_back = UIView.new;
    [_card_back setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_card_back];
    [_card_back makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_card_back setBackgroundColor:[UIColor whiteColor]];
    [_card_back setHidden:YES];
    [_card_back.layer setCornerRadius:3.0];
    [_card_back setClipsToBounds:YES];
    
    _sv_card_back = UIScrollView.new;
    _cb_holder = UIView.new;
    
    [_sv_card_back setBackgroundColor:[UIColor clearColor]];
    [_card_back addSubview:_sv_card_back];
    
    [_sv_card_back makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_card_back);
    }];
    
    [_sv_card_back addSubview:_cb_holder];
    
    [_cb_holder makeConstraints:^(MASConstraintMaker *make) {
        make.edges.width.equalTo(_sv_card_back);
    }];
    
    // drop shadow
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.layer setShadowOpacity:0.6];
    [self.layer setShadowRadius:2.0];
    [self.layer setShadowOffset:CGSizeMake(0.0, 1.0)];
    
    //[self setClipsToBounds:YES];
    
    [self.layer setCornerRadius:3.0];
    
    if (self.card.image_url && ![self.card.image_url isEqualToString:@""]) {
        _cardImage = [[CardViewImage alloc] initWithFrame:CGRectZero];
        [_cardImage setBackgroundColor:[UIColor redColor]];
        [_card_front insertSubview:_cardImage belowSubview:_sv_card_front];
        [_cardImage makeConstraints:^(MASConstraintMaker *make) {
            cardImageTop = make.top.equalTo(self).with.offset(0);
            make.left.right.equalTo(self);
            make.height.equalTo(self.width);
        }];
        
        [_cardImage loadImage:self.card.image_url];
        
        imageOverlay = UIView.new;
        [imageOverlay setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
        [_card_front insertSubview:imageOverlay aboveSubview:_cardImage];
        [imageOverlay makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_cardImage);
        }];
        [imageOverlay setAlpha:0.0];
    } else if (self.card.imageObj) {
        _cardImage = [[CardViewImage alloc] initWithFrame:CGRectZero];
        [_cardImage setBackgroundColor:[UIColor redColor]];
        [_card_front insertSubview:_cardImage belowSubview:_sv_card_front];
        [_cardImage makeConstraints:^(MASConstraintMaker *make) {
            cardImageTop = make.top.equalTo(self).with.offset(0);
            make.left.right.equalTo(self);
            make.height.equalTo(self.width);
        }];
        
        [_cardImage loadLocalImageData:self.card.imageObj];
        
        imageOverlay = UIView.new;
        [imageOverlay setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
        [_card_front insertSubview:imageOverlay aboveSubview:_cardImage];
        [imageOverlay makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_cardImage);
        }];
        [imageOverlay setAlpha:0.0];
    }
    
    
    cardFrontContent = UIView.new;
    [cardFrontContent setBackgroundColor:[UIColor whiteColor]];
    [_cf_holder addSubview:cardFrontContent];
    [cardFrontContent makeConstraints:^(MASConstraintMaker *make) {
        cardContentTop = make.top.equalTo(@0);
        make.left.right.equalTo(_cf_holder);
        make.height.equalTo(@1000);
    }];
    //
    //        [_cf_holder updateConstraints:^(MASConstraintMaker *make) {
    //            make.bottom.equalTo(cardFrontContent.bottom).with.offset(0);
    //        }];
    //
    
    cardCoverDetails = UIView.new;
    [cardCoverDetails setBackgroundColor:[UIColor whiteColor]];
    [cardFrontContent addSubview:cardCoverDetails];
    [cardCoverDetails makeConstraints:^(MASConstraintMaker *make) {
        cardCoverDetailsTop = make.top.equalTo(cardFrontContent.top);
        make.left.equalTo(cardFrontContent);
        make.right.equalTo(cardFrontContent);
        make.height.equalTo(@100);
    }];
    
    
    
    // TITLE
    
    UILabel *title = nil;
    
    if (_card.name && ![_card.name isEqualToString:@""]) {
        
        title = UILabel.new;
        [title setStyleClass:@"lbl-card-name"];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_card.name];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineHeightMultiple:0.8];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [_card.name length])];
        
        title.attributedText = attributedString ;
        
        [title setNumberOfLines:0];
        [cardCoverDetails addSubview:title];
        [title makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(cardCoverDetails).with.offset(10);
            make.width.lessThanOrEqualTo(@200);
            make.height.greaterThanOrEqualTo(@32);
            make.centerX.equalTo(cardCoverDetails);
        }];
    }
    
    // SUBTITLE
    UILabel *subTitle = UILabel.new;
    [subTitle setStyleClass:@"lbl-card-subtitle"];
    
    if (_card.base_color) {
        [subTitle setTextColor:[UIColor colorWithHexString:_card.base_color]];
    }
    
    if (_card.updated_at) {
        
        NSString *timeUpdated = @"UPDATED ";
        
        timeUpdated = [timeUpdated stringByAppendingString:[_card.updated_at distanceOfTimeInWords]];
        timeUpdated = [[[timeUpdated substringToIndex:1] uppercaseString] stringByAppendingString:[[timeUpdated substringFromIndex:1] uppercaseString]];
        
        [subTitle setText:timeUpdated];
        
    } else {
        [subTitle setText:@"UPDATED"];
    }
    
    [cardCoverDetails addSubview:subTitle];
    [subTitle makeConstraints:^(MASConstraintMaker *make) {
        if (title) {
            make.top.equalTo(title.bottom);
        } else {
            make.top.equalTo(cardCoverDetails.top).with.offset(14);
        }
        make.width.equalTo(cardCoverDetails);
        make.height.equalTo(@18);
        make.centerX.equalTo(cardCoverDetails);
    }];
    
    UIButton *cardAddAction = UIButton.new;
    [cardAddAction setImage:[UIImage imageNamed:@"Btn-DeckAction-AddSmall"] forState:UIControlStateNormal];
    [cardCoverDetails addSubview:cardAddAction];
    [cardAddAction makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@40);
        make.height.equalTo(@40);
        make.left.equalTo(cardCoverDetails).with.offset(5);
        make.top.equalTo(@0).with.offset(3);
    }];
    
    [cardAddAction addTarget:self.cardHolderVC action:@selector(showCardActions:) forControlEvents:UIControlEventTouchUpInside];
    
//    UIButton *cardFavAction = UIButton.new;
//    [cardFavAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Like"] forState:UIControlStateNormal];
//    [cardCoverDetails addSubview:cardFavAction];
//    [cardFavAction makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(@40);
//        make.height.equalTo(@40);
//        make.left.equalTo(cardCoverDetails).with.offset(5);
//        make.top.equalTo(@0).with.offset(3);
//    }];
    
    UIButton *cardCommentAction = UIButton.new;
    [cardCommentAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Comment"] forState:UIControlStateNormal];
    [cardCoverDetails addSubview:cardCommentAction];
    [cardCommentAction makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@40);
        make.height.equalTo(@40);
        make.right.equalTo(cardCoverDetails).with.offset(-5);
        make.top.equalTo(@0).with.offset(3);
    }];
    
    [cardCommentAction addTarget:self action:@selector(openComments:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    swipeLeftGR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeCard:)];
    [swipeLeftGR setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self addGestureRecognizer:swipeLeftGR];
    
    swipeRightGR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeCard:)];
    [swipeRightGR setDirection:UISwipeGestureRecognizerDirectionRight];
    [self addGestureRecognizer:swipeRightGR];
    
    swipeUpGR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeCard:)];
    [swipeUpGR setDirection:UISwipeGestureRecognizerDirectionUp];
    [self addGestureRecognizer:swipeUpGR];
    
    tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCard:)];
    tapGR.delegate = self;
    [self addGestureRecognizer:tapGR];
    
    panGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panCard:)];
    panGR.delegate = self;
    [panGR setEnabled:YES];
    [self addGestureRecognizer:panGR];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self buildCardFront:cardFrontContent];
    
    
    [_cf_holder updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(cardFrontContent.bottom).with.offset(100);
    }];
    
    
    cardBackContent = UIView.new;
    [cardBackContent setBackgroundColor:[UIColor whiteColor]];
    [_cb_holder addSubview:cardBackContent];
    [cardBackContent makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.right.equalTo(_cb_holder);
        make.height.equalTo(@1000);
    }];
    
    
    [self buildCardBack:cardBackContent];
    
    [_cb_holder updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(cardBackContent.bottom).with.offset(100);
    }];

}

- (void)reloadCardLayout:(Card *)card
{
    if (card) {
        self.card = card;
        
        int hasImageOffset = 0;
        if (self.card.image_url && ![self.card.image_url isEqualToString:@""]) {
            hasImageOffset = 180;
        }
        
        if (self.card.image) {
            hasImageOffset = 180;
        }
        
        if (self.card.image) {
            
            if (!_cardImage) {
                _cardImage = [[CardViewImage alloc] initWithFrame:CGRectZero];
                [_cardImage setBackgroundColor:[UIColor redColor]];
                [_card_front insertSubview:_cardImage belowSubview:_sv_card_front];
                [_cardImage makeConstraints:^(MASConstraintMaker *make) {
                    cardImageTop = make.top.equalTo(self).with.offset(0);
                    make.left.right.equalTo(self);
                    make.height.equalTo(self.width);
                }];
            }
            
            [_cardImage loadLocalImageData:self.card.image];
            
            if (!imageOverlay) {
                imageOverlay = UIView.new;
                [imageOverlay setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
                [_card_front insertSubview:imageOverlay aboveSubview:_cardImage];
                [imageOverlay makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(_cardImage);
                }];
                [imageOverlay setAlpha:0.0];
            }
            
        } else if (self.card.image_url && ![self.card.image_url isEqualToString:@""]) {
            
            if (!_cardImage) {
                _cardImage = [[CardViewImage alloc] initWithFrame:CGRectZero];
                [_cardImage setBackgroundColor:[UIColor redColor]];
                [_card_front insertSubview:_cardImage belowSubview:_sv_card_front];
                [_cardImage makeConstraints:^(MASConstraintMaker *make) {
                    cardImageTop = make.top.equalTo(self).with.offset(0);
                    make.left.right.equalTo(self);
                    make.height.equalTo(self.width);
                }];
            }
            
            [_cardImage loadImage:self.card.image_url];
            
            if (!imageOverlay) {
                imageOverlay = UIView.new;
                [imageOverlay setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
                [_card_front insertSubview:imageOverlay aboveSubview:_cardImage];
                [imageOverlay makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(_cardImage);
                }];
                [imageOverlay setAlpha:0.0];
            }
        }
        
        
        for (UIView *v in [cardFrontContent subviews]) {
            [v removeFromSuperview];
        }
        
        for (UIView *v in [cardBackContent subviews]) {
            [v removeFromSuperview];
        }
        
        cardCoverDetails = UIView.new;
        [cardCoverDetails setBackgroundColor:[UIColor whiteColor]];
        [cardFrontContent addSubview:cardCoverDetails];
        [cardCoverDetails makeConstraints:^(MASConstraintMaker *make) {
            cardCoverDetailsTop = make.top.equalTo(cardFrontContent.top);
            make.left.equalTo(cardFrontContent);
            make.right.equalTo(cardFrontContent);
            make.height.equalTo(@100);
        }];
        
        // TITLE
        
        UILabel *title = nil;
        
        if (_card.name && ![_card.name isEqualToString:@""]) {
            
            title = UILabel.new;
            [title setStyleClass:@"lbl-card-name"];
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_card.name];
            NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
            [paragrahStyle setLineHeightMultiple:0.8];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [_card.name length])];
            
            title.attributedText = attributedString ;
            
            [title setNumberOfLines:0];
            [cardCoverDetails addSubview:title];
            [title makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(cardCoverDetails).with.offset(10);
                make.width.lessThanOrEqualTo(@200);
                make.height.greaterThanOrEqualTo(@32);
                make.centerX.equalTo(cardCoverDetails);
            }];
        }
        
        // SUBTITLE
        UILabel *subTitle = UILabel.new;
        [subTitle setStyleClass:@"lbl-card-subtitle"];
        
        if (card.base_color) {
            [subTitle setTextColor:[UIColor colorWithHexString:card.base_color]];
        }
        
        if (card.updated_at) {
            
            NSString *timeUpdated = @"UPDATED ";
            
            timeUpdated = [timeUpdated stringByAppendingString:[card.updated_at distanceOfTimeInWords]];
            timeUpdated = [[[timeUpdated substringToIndex:1] uppercaseString] stringByAppendingString:[[timeUpdated substringFromIndex:1] uppercaseString]];
            
            [subTitle setText:timeUpdated];
            
        } else {
            [subTitle setText:@"UPDATED"];
        }
        
        [cardCoverDetails addSubview:subTitle];
        [subTitle makeConstraints:^(MASConstraintMaker *make) {
            if (title) {
                make.top.equalTo(title.bottom);
            } else {
                make.top.equalTo(cardCoverDetails.top).with.offset(14);
            }
            make.width.equalTo(cardCoverDetails);
            make.height.equalTo(@18);
            make.centerX.equalTo(cardCoverDetails);
        }];

        UIButton *cardAddAction = UIButton.new;
        [cardAddAction setImage:[UIImage imageNamed:@"Btn-DeckAction-AddSmall"] forState:UIControlStateNormal];
        [cardCoverDetails addSubview:cardAddAction];
        [cardAddAction makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.left.equalTo(cardCoverDetails).with.offset(5);
            make.top.equalTo(@0).with.offset(3);
        }];
        
//        UIButton *cardFavAction = UIButton.new;
//        [cardFavAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Like"] forState:UIControlStateNormal];
//        [cardCoverDetails addSubview:cardFavAction];
//        [cardFavAction makeConstraints:^(MASConstraintMaker *make) {
//            make.width.equalTo(@40);
//            make.height.equalTo(@40);
//            make.left.equalTo(cardCoverDetails).with.offset(5);
//            make.top.equalTo(@0).with.offset(3);
//        }];
        
        UIButton *cardCommentAction = UIButton.new;
        [cardCommentAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Comment"] forState:UIControlStateNormal];
        [cardCoverDetails addSubview:cardCommentAction];
        [cardCommentAction makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.right.equalTo(cardCoverDetails).with.offset(-5);
            make.top.equalTo(@0).with.offset(3);
        }];
        
        [cardCommentAction addTarget:self action:@selector(openComments:) forControlEvents:UIControlEventTouchUpInside];
        
        [self buildCardFront:cardFrontContent];
        
        [_cf_holder updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(cardFrontContent.bottom).with.offset(100);
        }];
        
        
        [self buildCardBack:cardBackContent];
        
        [_cb_holder updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(cardBackContent.bottom).with.offset(100);
        }];

        
    }
}

- (void)buildCardFront:(UIView *)holderView
{
    UIView *priorView = nil;
    if (self.card && _card_front) {
        
        if ([self.card cardBackFields].count > 0)  {
            UIImageView *cardBackIndicator = UIImageView.new;
            [cardBackIndicator setImage:[UIImage imageNamed:@"Icon-OnBack"]];
            [_card_front addSubview:cardBackIndicator];
            [cardBackIndicator makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.equalTo(@31);
                make.bottom.equalTo(_card_front).with.offset(-10);
                make.right.equalTo(_card_front).with.offset(-10);
            }];
        }
        
        if (_card.related_deck)  {
            
            UIImageView *cardBackIndicator = UIImageView.new;
            [cardBackIndicator setImage:[UIImage imageNamed:@"Icon-InadekDeck"]];
            [_card_front addSubview:cardBackIndicator];
            [cardBackIndicator makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.equalTo(@70);
                make.centerX.equalTo(_card_front);
                make.bottom.equalTo(_card_front).with.offset(-10);
            }];
            
            UILabel *inDeckNotice = UILabel.new;
            
            inDeckNotice.text = @"OPEN INADEK";
            [inDeckNotice setStyleClass:@"lbl-card-subtitle"];
            [inDeckNotice setStyleCSS:@"color: #242424;"];
            
            [_card_front addSubview:inDeckNotice];
            [inDeckNotice makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(holderView);
                make.top.equalTo(cardBackIndicator).with.offset(10);
                make.height.equalTo(@15);
            }];
        }
        
        
        if ([_card.triggers allObjects] && [[_card.triggers allObjects] count] > 0) {
            int count = 0;
            
            NSMutableArray *triggerButtons = NSMutableArray.new;
            
            for (CardTrigger *trig in [_card.triggers allObjects]) {
                
                BCButton *rounded = BCButton.new;
                [rounded buildType:@"deck.action"];
                [rounded setTitle:[trig.action_verb uppercaseString] forState:UIControlStateNormal];
                
                if ([trig is_url] && trig.value && ![trig.value isEqualToString:@""]) {
                    // Open a URL
                    [rounded setUserInfo:@{@"url":trig.value}];
                    [rounded addTarget:self action:@selector(openURL:) forControlEvents:UIControlEventTouchUpInside];
                } else if ([trig is_phone] && trig.value && ![trig.value isEqualToString:@""]) {
                    // Open a URL
                    [rounded setUserInfo:@{@"phone":trig.value}];
                    [rounded addTarget:self action:@selector(startPhoneCall:) forControlEvents:UIControlEventTouchUpInside];
                } else if ([trig is_email] && trig.value && ![trig.value isEqualToString:@""]) {
                    // Open a URL
                    [rounded setUserInfo:@{@"email":trig.value}];
                    [rounded addTarget:self action:@selector(openEmail:) forControlEvents:UIControlEventTouchUpInside];
                } else if ([trig is_map] && trig.value && ![trig.value isEqualToString:@""]) {
                    // Open a URL
                    [rounded setUserInfo:@{@"location":trig.value}];
                    [rounded addTarget:self action:@selector(openLocation:) forControlEvents:UIControlEventTouchUpInside];
                }
                
                [rounded.layer setCornerRadius:16.0f];
                
                if (_card.base_color) {
                    [rounded setColor:[UIColor colorWithHexString:_card.base_color]];
                    [rounded setBackgroundColor:[UIColor colorWithHexString:_card.base_color]];
                }
                
                [triggerButtons addObject:rounded];
                
                count++;
                if (count == 2) { break; }
            }
            
            if ([triggerButtons count] == 2) {
                
                [holderView addSubview:[triggerButtons objectAtIndex:0]];
                [[triggerButtons objectAtIndex:0] makeConstraints:^(MASConstraintMaker *make) {
                    make.width.equalTo(@100);
                    make.height.equalTo(@32);
                    make.centerX.equalTo(holderView).with.offset(-55);
                    make.top.equalTo(holderView).with.offset(80);
                }];
                
                [holderView addSubview:[triggerButtons objectAtIndex:1]];
                [[triggerButtons objectAtIndex:1] makeConstraints:^(MASConstraintMaker *make) {
                    make.width.equalTo(@100);
                    make.height.equalTo(@32);
                    make.centerX.equalTo(holderView).with.offset(55);
                    make.top.equalTo(holderView).with.offset(80);
                }];
                
                priorView = [triggerButtons objectAtIndex:0];
                
            } else if ([triggerButtons count] == 1) {
                [holderView addSubview:[triggerButtons objectAtIndex:0]];
                [[triggerButtons objectAtIndex:0] makeConstraints:^(MASConstraintMaker *make) {
                    make.width.equalTo(@120);
                    make.height.equalTo(@32);
                    make.centerX.equalTo(holderView);
                    make.top.equalTo(holderView).with.offset(80);
                }];
                
                priorView = [triggerButtons objectAtIndex:0];
            }
            
        } else {
            UIView *spacer = UIView.new;
            [holderView addSubview:spacer];
            [spacer makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(holderView);
                make.top.equalTo(@60);
                make.height.equalTo(@10);
            }];
            
            priorView = spacer;
        }

        
        for (CardField *field in [self.card cardFrontFields]) {
            DLog(@"Got a field: %@", field);
            
            if ([field.field_cid isEqualToString:@"text"]) {
                priorView = [self buildTextFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"stat"]) {
                priorView = [self buildStatFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"header"]) {
                priorView = [self buildHeaderFieldView:priorView withField:field intoView:holderView];
            
            } else if ([field.field_cid isEqualToString:@"phone_number_01"]) {
                priorView = [self buildPhoneFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"phone_number_02"]) {
                priorView = [self buildPhoneFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"phone_number_03"]) {
                priorView = [self buildPhoneFieldView:priorView withField:field intoView:holderView];
            
            } else if ([field.field_cid isEqualToString:@"email_address"]) {
                priorView = [self buildEmailFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"web_address"]) {
                priorView = [self buildWebFieldView:priorView withField:field intoView:holderView];

            
            } else if ([field.field_cid isEqualToString:@"address_01"]) {
                priorView = [self buildTextFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"address_02"]) {
                priorView = [self buildTextFieldView:priorView withField:field intoView:holderView];
            
            } else if ([field fieldIsPhotoGallery]) {
                priorView = [self buildGalleryView:priorView withField:field intoView:holderView];
            }
        
            
        }
    }
}

- (void)buildCardBack:(UIView *)holderView
{
    if (self.card) {
        
        // Hack fix for some strange autolayout confusion
        UIView *priorView = nil;
        
        for (CardField *field in [self.card cardBackFields]) {

            
            DLog(@"Got a field: %@", field);
            
            if ([field.field_cid isEqualToString:@"text"]) {
                priorView = [self buildTextFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"stat"]) {
                priorView = [self buildStatFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"web_content"]) {
                priorView = [self buildWebContentFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"header"]) {
                priorView = [self buildHeaderFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"phone_number_01"]) {
                priorView = [self buildPhoneFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"phone_number_02"]) {
                priorView = [self buildPhoneFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"phone_number_03"]) {
                priorView = [self buildPhoneFieldView:priorView withField:field intoView:holderView];
                
            } else if ([field.field_cid isEqualToString:@"email_address"]) {
                priorView = [self buildEmailFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"web_address"]) {
                priorView = [self buildWebFieldView:priorView withField:field intoView:holderView];
                
                
            } else if ([field.field_cid isEqualToString:@"address_01"]) {
                priorView = [self buildTextFieldView:priorView withField:field intoView:holderView];
            } else if ([field.field_cid isEqualToString:@"address_02"]) {
                priorView = [self buildTextFieldView:priorView withField:field intoView:holderView];
                
            } else if ([field fieldIsPhotoGallery]) {
                priorView = [self buildGalleryView:priorView withField:field intoView:holderView];
            }
        
        }
        
        if ([self.card isMine]) {
            _cardActions = [[CardViewActions alloc] initWithFullControls:self];
            [_card_back addSubview:_cardActions];
            [_cardActions makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(_card_back);
                make.left.right.equalTo(_card_back);
                make.height.equalTo(@54);
            }];
        }
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self layoutIfNeeded];
    
    int hasImageOffset = 0;
    if (_cardImage) {
        hasImageOffset = 180;
        [cardContentTop setOffset:_cardImage.frame.size.height - hasImageOffset];
    }
    
    [cardCommentsTop setOffset:_card_front.frame.size.height];
    
    if (cardBackContent && [cardBackContent subviews].count > 0) {
        
        id last_view = [[cardBackContent subviews] objectAtIndex:(cardBackContent.subviews.count - 1)];
        
        if ([last_view isKindOfClass:[UIView class]]) {
            [cardBackContent updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@([(UIView *)last_view frame].origin.y + [(UIView *)last_view frame].size.height + 80));
            }];
        }
    } else {
        [cardBackContent updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@20);
        }];
    }
    
    if (cardFrontContent && [cardFrontContent subviews].count > 0) {
        
        id last_view = [[cardFrontContent subviews] objectAtIndex:(cardFrontContent.subviews.count - 1)];
        
        if ([last_view isKindOfClass:[UIView class]]) {
            [cardFrontContent updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@([(UIView *)last_view frame].origin.y + [(UIView *)last_view frame].size.height + 80));
            }];
        }
    } else {
        [cardFrontContent updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@20);
        }];
    }
}

- (void)rebuildMyView:(CardView *)newCV
{
    [self reloadCardLayout:newCV.card];
    
    if ([side isEqualToString:@"back"]) {
        [self tapCard:nil];
    }
}

- (void)openURL:(BCButton *)sender
{
    
    NSDictionary *ui = [sender userInfo];
    SFSafariViewController *webVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:[ui objectForKey:@"url"]]];
    [(DeckViewController *)_cardHolderVC presentViewController:webVC animated:YES completion:^{
        //
    }];
}

- (void)startPhoneCall:(BCButton *)sender
{
    
    NSDictionary *ui = [sender userInfo];
    
    NSString *phoneNumber = [ui objectForKey:@"phone"];
    
    NSString *cleanedPhone = [[phoneNumber componentsSeparatedByCharactersInSet:
                               [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                              componentsJoinedByString:@""];
    
    NSString *phone = [@"telprompt://" stringByAppendingString:cleanedPhone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone]];
    
}

- (void)openEmail:(BCButton *)sender
{
    
    NSDictionary *ui = [sender userInfo];
    NSString *emailAddress = [ui objectForKey:@"email"];
    
    NSString *mailtoEmail = [@"mailto:" stringByAppendingString:emailAddress];
    NSString *url = [mailtoEmail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
    
}

- (void)openLocation:(BCButton *)sender
{
    
    NSDictionary *ui = [sender userInfo];
    NSString *location = [[ui objectForKey:@"location"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *mapUrl = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@", location];
    
    SFSafariViewController *webVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:mapUrl]];
    [(DeckViewController *)_cardHolderVC presentViewController:webVC animated:YES completion:^{
        //
    }];

    
}

- (void)doAddComment:(id)sender
{
    if (commentField && _cardHolderVC) {
        [(UITextView *)sender resignFirstResponder];
        NSString *comment = commentField.text;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_cardHolderVC.view animated:YES];
        hud.labelText = @"Adding Comment";
        [[InadekAPIManager sharedManager] postComment:@{@"object":self.card,@"comment":comment} callback:^(Card * card) {
            //
            if (cardComments) {
                [cardComments reloadComments:card];
            }
            [MBProgressHUD hideHUDForView:_cardHolderVC.view animated:YES];
            [self closeComments:nil];
        } failureCallback:^(id responseObject) {
            //
            [MBProgressHUD hideHUDForView:_cardHolderVC.view animated:YES];
            [self closeComments:nil];
        }];
    }
}


- (void)openComments:(id)sender
{
    
    if (keyboardAccessoryView) {
        [keyboardAccessoryView removeFromSuperview];
        keyboardAccessoryView = nil;
    }
    
    keyboardAccessoryView = UIView.new;
    [keyboardAccessoryView setBackgroundColor:[UIColor whiteColor]];
    [keyboardAccessoryView setStyleClass:@"box-shadow-upper"];
    
    UIView *commentFieldHolder = UIView.new;
    [keyboardAccessoryView addSubview:commentFieldHolder];
    [commentFieldHolder setStyleClass:@"box-rounded-entry-field"];
    [commentFieldHolder makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(keyboardAccessoryView).with.insets(UIEdgeInsetsMake(5, 5, 5, 5));
    }];
    
    commentField = UITextView.new;
    commentField.delegate = self;
    [commentField setBackgroundColor:[UIColor clearColor]];
    [commentField setStyleCSS:@"font-size: 15px;"];
    [commentField setReturnKeyType:UIReturnKeyDone];
    [commentFieldHolder addSubview:commentField];
    [commentField makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(commentFieldHolder).with.insets(UIEdgeInsetsMake(4, 4, 4, 80));
    }];
    
    UIButton *post = UIButton.new;
    [post setTitle:@"Save" forState:UIControlStateNormal];
    [post setStyleCSS:@"color: #F15400;"];
    [commentFieldHolder addSubview:post];
    [post makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(commentFieldHolder);
        make.width.equalTo(@80);
    }];
    
    [post addTarget:self action:@selector(doAddComment:) forControlEvents:UIControlEventTouchUpInside];

    
    
    
    // 20, 10 - Super springy
    // 5, 10 - Slower entry speed, with only one slight spring back
    // 10, 10 - A slight bounce, but feels a little slow
    // 10, 26 - A little too fast on entry
    // 10, 18 -
    
    if (genericOverlay) {
        [genericOverlay removeFromSuperview];
        genericOverlay = nil;
    }
    
    genericOverlay = UIView.new;
    [genericOverlay setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
    [genericOverlay setAlpha:0];
    [_card_front addSubview:genericOverlay];
    [genericOverlay makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_card_front);
    }];
    
    cardComments = [[CardViewComments alloc] initWithBaseView:self andCard:_card];
    [cardComments setAlpha:0.0];
    
    [_card_front addSubview:cardComments];
    [cardComments makeConstraints:^(MASConstraintMaker *make) {
        cardCommentsTop = make.top.equalTo(_card_front.top).with.offset(_card_front.frame.size.height);
        make.bottom.equalTo(_card_front.bottom);
        make.left.right.equalTo(_card_front);
    }];
    
    if (!cardCoverDetails) {
        return;
    }
    
    [cardComments setIsShown:YES];
    
    int toOffset = 60;
    
    // 0 - deckCoverDetails.bounds.origin.x
    
    POPBasicAnimation *slideInCard = [POPBasicAnimation new];
    slideInCard.toValue = @(toOffset);
    slideInCard.duration = 0.2;
    slideInCard.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    slideInCard.property = [POPAnimatableProperty mas_offsetProperty];
    [cardCommentsTop pop_addAnimation:slideInCard forKey:@"PopDeckSlideInAnimation"];
    
    [UIView animateWithDuration:0.2 animations:^{
        [cardComments setAlpha:1.0];
        [genericOverlay setAlpha:1.0];
    }];
    
}

- (void)closeComments:(id)sender
{
    if (commentField) {
        [commentField resignFirstResponder];
    }
    
    [self endEditing:YES];
    int toOffset = 0;
    
    POPSpringAnimation *slideInCard = [POPSpringAnimation new];
    slideInCard.toValue = @(toOffset);
    slideInCard.springBounciness = 5;
    slideInCard.springSpeed = 48;
    slideInCard.property = [POPAnimatableProperty mas_offsetProperty];
    [cardCommentsTop pop_addAnimation:slideInCard forKey:@"PopDeckSlideInAnimation"];
    
    [UIView animateWithDuration:0.2 animations:^{
        [cardComments setAlpha:0.0];
        if (genericOverlay) {
            [genericOverlay setAlpha:0.0];
        }
    } completion:^(BOOL finished) {
        if (genericOverlay) {
            [genericOverlay removeFromSuperview];
            genericOverlay = nil;
        }
        [cardComments removeFromSuperview];
        cardComments = nil;
    }];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)swipeCard:(UISwipeGestureRecognizer *)gestureRecognizer
{
    if (ignoreGestures || [cardComments isShown]) return;
    
    // If they swipe back (right), they return to home view, if they swipe forward (left) they dive into the deck
    if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [(DeckViewController *)_cardHolderVC transitionToNextCard];
    } else if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        // Take em back...
        [(DeckViewController *)_cardHolderVC transitionToPrevCard];
    } else if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionUp) {
        //[(DeckViewController *)_cardHolderVC showCardCollection];
    }
}

- (void)animateCardAway:(int)direction
{
    if (direction == 1) {
        // Move the card to the right
        
        POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
        animation.springBounciness = 15;
        animation.springSpeed = 30;
        animation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.9, 0.9)];
        animation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            [self.layer setOpacity:1.0];
            CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
            transform = CGAffineTransformScale(transform, 1.0, 1.0);
            self.transform = transform;
        };
        [self.layer pop_addAnimation:animation forKey:@"scale"];
        
        POPSpringAnimation *animation2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
        animation2.springBounciness = 15;
        animation2.springSpeed = 30;
        animation2.toValue = [NSValue valueWithCGPoint:CGPointMake(_cardHolderVC.view.bounds.size.width, self.center.y)];
        [self.layer pop_addAnimation:animation2 forKey:@"rotate"];
        
        POPBasicAnimation *animation3 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
        animation3.duration = 0.3;
        animation3.toValue = @(0.0);
        animation3.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            [self removeFromSuperview];
            [self.layer setOpacity:1.0];
            [(DeckViewController *)_cardHolderVC transitionIsComplete];
        };
        
        [self.layer pop_addAnimation:animation3 forKey:@"opacity"];
        
    } else {
        // Move the card to the left
        
        POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
        animation.springBounciness = 15;
        animation.springSpeed = 30;
        animation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            [self.layer setOpacity:1.0];
            CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
            transform = CGAffineTransformScale(transform, 1.0, 1.0);
            self.transform = transform;
        };
        
        animation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.9, 0.9)];
        
        [self.layer pop_addAnimation:animation forKey:@"scale"];
        
        POPSpringAnimation *animation2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
        animation2.springBounciness = 15;
        animation2.springSpeed = 30;
        animation2.toValue = [NSValue valueWithCGPoint:CGPointMake(0, self.center.y)];
        [self.layer pop_addAnimation:animation2 forKey:@"rotate"];
        
        POPBasicAnimation *animation3 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
        animation3.duration = 0.3;
        animation3.toValue = @(0.0);
        animation3.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            [self removeFromSuperview];
            [self.layer setOpacity:1.0];
            [(DeckViewController *)_cardHolderVC transitionIsComplete];
        };
        [self.layer pop_addAnimation:animation3 forKey:@"opacity"];
    }
}

- (void)animateCardBack
{
    
    POPBasicAnimation *animation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.duration = 0.2;
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.85, 0.85)];
    [self.layer pop_addAnimation:animation forKey:@"scale_back"];
    
    POPBasicAnimation *animation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    animation2.duration = 0.2;
    animation2.toValue = [NSValue valueWithCGSize:CGSizeMake(0.0, 0.0)];
    [self.layer pop_addAnimation:animation2 forKey:@"opacity_back"];
    
    
}

- (void)animateDeckBack
{
    
    ignoreGestures = YES;
    
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.springBounciness = 20;
    animation.springSpeed = 30;
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.85, 0.85)];
    [self.layer pop_addAnimation:animation forKey:@"scale_back"];
    
    POPBasicAnimation *animation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    animation2.duration = 0.2;
    animation2.toValue = [NSValue valueWithCGSize:CGSizeMake(0.5, 0.5)];
    [self.layer pop_addAnimation:animation2 forKey:@"opacity_back"];
    
}

- (void)animateDeckBackUp
{
    
    ignoreGestures = YES;
    
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.springBounciness = 20;
    animation.springSpeed = 30;
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.85, 0.85)];
    [self.layer pop_addAnimation:animation forKey:@"scale_back"];
    
    POPBasicAnimation *animation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    animation2.duration = 0.2;
    animation2.toValue = [NSValue valueWithCGSize:CGSizeMake(0.5, 0.5)];
    [self.layer pop_addAnimation:animation2 forKey:@"opacity_back"];
    
    POPSpringAnimation *animation3 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    animation3.springBounciness = 20;
    animation3.springSpeed = 30;
    animation3.toValue = @(self.center.y - 30);
    [self.layer pop_addAnimation:animation3 forKey:@"slide_up"];
    
    cardIsSlidUp = YES;
    
}

- (void)animateDeckForward
{
    
    ignoreGestures = NO;
    
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.springBounciness = 20;
    animation.springSpeed = 30;
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0, 1.0)];
    [self.layer pop_addAnimation:animation forKey:@"scale_back"];
    
    POPBasicAnimation *animation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    animation2.duration = 0.2;
    animation2.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0, 1.0)];
    [self.layer pop_addAnimation:animation2 forKey:@"opacity_back"];
    
    if (cardIsSlidUp) {
        POPSpringAnimation *animation3 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        animation3.springBounciness = 20;
        animation3.springSpeed = 30;
        animation3.toValue = @(self.center.y + 30);
        [self.layer pop_addAnimation:animation3 forKey:@"slide_up"];
        cardIsSlidUp = NO;
    }
    
    [self enableInteraction];
    
}

- (void)disableInteraction
{
    UIButton *returnBtn = UIButton.new;
    [returnBtn setTag:kViewTagReturn];
    [returnBtn setBackgroundColor:[UIColor clearColor]];
    [self addSubview:returnBtn];
    [returnBtn makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    if (self.cardHolderVC && [self.cardHolderVC respondsToSelector:@selector(disabledCardTapped:)]) {
        [returnBtn addTarget:self.cardHolderVC action:@selector(disabledCardTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

- (void)enableInteraction
{
    [[self viewWithTag:kViewTagReturn] removeFromSuperview];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    // Disallow recognition of tap gestures in the segmented control.
    if (([touch.view isKindOfClass:[UIButton class]])) {//change it to your condition
        return NO;
    }
    return YES;
}

- (void)panCard:(UIPanGestureRecognizer *)gesture
{
    
    if (ignoreGestures || [cardComments isShown]) return;
    
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        CGPoint velocity = [gesture velocityInView:self];
        
        BOOL highVelocity = NO;
        
        if (velocity.x > 500) {
            [(DeckViewController *)_cardHolderVC transitionToPrevCard];
            highVelocity = YES;
        } else if (velocity.x < -500) {
            [(DeckViewController *)_cardHolderVC transitionToNextCard];
            highVelocity = YES;
        }
        
        CGPoint translation = [gesture translationInView:self.superview];
        
        if (translation.x > 30 && fabs(translation.y) < 40) {
            // GO BACK
            [(DeckViewController *)_cardHolderVC transitionToPrevCard];
        } else if (translation.x < -30 && fabs(translation.y) < 40) {
            // GO FORWARD
            [(DeckViewController *)_cardHolderVC transitionToNextCard];
        }

        return;
    }
    
    return;
    
//    CGPoint translation = [gesture translationInView:self.superview];
//    if (_baseVC) {
//        [_baseVC panCards:translation.x];
//    }

}

- (void)tapCard:(UIGestureRecognizer *)gestureRecognizer
{
    
    if (ignoreGestures || [cardComments isShown]) return;
    
    if (_card && _card.related_deck) {
        
        [(DeckViewController *)_cardHolderVC goIntoSubdeck:_card.related_deck];
        
        return;
    }
    
    [UIView transitionWithView:self
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        if ([side isEqualToString:@"back"]) {
                            [_card_front setHidden:NO];
                            [_card_back setHidden:YES];
                            side = @"front";
                        } else {
                            [_card_front setHidden:YES];
                            [_card_back setHidden:NO];
                            side = @"back";
                        }
                    } completion:^(BOOL finished) {
                        
                    }];
}

- (void)respondToLongPress
{
    if (longPressGR == nil) {
        longPressGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didLongPress:)];
        [longPressGR setMinimumPressDuration:0.5];
        longPressGR.delegate = self;
        [self addGestureRecognizer:longPressGR];
    }
    
    UITapGestureRecognizer *tpMe = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPress:)];
    [tpMe setNumberOfTapsRequired:1];
    [tpMe setDelegate:self];
    [self addGestureRecognizer:tpMe];
}

- (void)didLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    [(DeckViewController *)_cardHolderVC startMovingCard:YES andCard:self];
}

- (void)didPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    [(DeckViewController *)_cardHolderVC transitionToCard:self.card];
}

- (void)isFloating:(BOOL)floating
{
    if (floating) {
        [self.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.layer setShadowOpacity:0.6];
        [self.layer setShadowRadius:10.0];
        [self.layer setShadowOffset:CGSizeMake(0.0, 4.0)];
    } else {
        
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    
    CGFloat percentage = offsetY / 180;
    
    if (percentage > 0 && percentage < 1) {
        if (_cardImage && cardImageTop) {
            [cardImageTop setOffset:0 - ((90 * percentage) - (percentage * 10))];
            if (imageOverlay) {
                [imageOverlay setAlpha:percentage];
            }
        }
        DLog(@"Percentage (b): %f", percentage);
        
    }
}

- (void)keyboardWillHide:(NSNotification*)notification {
    
    if ([cardComments isShown]) {
        [cardComments moveViewUp:CGSizeMake(0, 0)];
    }
    
}

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    if ([cardComments isShown]) {
        [cardComments moveViewUp:kbSize];
        [commentField becomeFirstResponder];
    }
}

#pragma mark - UITextField / UITextView Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    if (keyboardAccessoryView) {
        [keyboardAccessoryView setFrame:CGRectMake(20, 0, [[UIScreen mainScreen] bounds].size.width, 50)];
        [textField setInputAccessoryView:keyboardAccessoryView];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (cardComments) {
        [cardComments updateFieldText:textField.text];
    }
    [textField resignFirstResponder];
    return YES;
}

- (UIView *) buildHeaderFieldView:(UIView *)prior withField:(CardField *)field intoView:(UIView *)holderView
{
    
    UILabel *value = UILabel.new;
    [value setStyleClass:@"lbl-card-headervalue"];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:field.field_value];
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyle setLineHeightMultiple:1.1];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [field.field_value length])];
    
    value.attributedText = attributedString;
    
    
    [value setNumberOfLines:0];
    [holderView addSubview:value];
    [value makeConstraints:^(MASConstraintMaker *make) {
        if (prior) {
            make.top.equalTo(prior.bottom).with.offset(15);
        } else {
            make.top.equalTo(holderView).with.offset(20);
        }
        make.left.equalTo(holderView).with.offset(20);
        make.right.equalTo(holderView).with.offset(-20);
        make.height.greaterThanOrEqualTo(@30);
    }];
    
    
    return value;
    
}
- (UIView *) buildGalleryView:(UIView *)prior withField:(CardField *)field intoView:(UIView *)holderView
{
    UIView *imageHolder = UIView.new;
    [imageHolder setBackgroundColor:[UIColor clearColor]];
    
    [holderView addSubview:imageHolder];
    
    
    [imageHolder makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(holderView).with.insets(UIEdgeInsetsMake(0, 20, 0, 20));
        make.height.greaterThanOrEqualTo(@45);
        if (prior) {
            make.top.equalTo(prior.bottom).with.offset(15);
        } else {
            make.top.equalTo(holderView).with.offset(20);
        }
        make.height.greaterThanOrEqualTo(@80);
        
    }];
    
    UIView *priorImage = nil;
    
    int location = 1;
    int row_count = 0;
    int cell_height = 100;
    
    for (CardImage *ci in [field.cardimages allObjects]) {
        BCGalleryImageView *iv = BCGalleryImageView.new;
        [imageHolder addSubview:iv];
        [iv setUserInteractionEnabled:YES];
        [iv setCardImageSet:[field.cardimages allObjects]];
        
        if (ci.image) {
            [iv loadLocalImage:ci.image];
            [iv addTarget:self cardImage:ci];
        } else if (ci.image_thumb_url) {
            [iv loadImage:ci.image_thumb_url];
            [iv addTarget:self cardImage:ci];
        }
        
        if (location == 1) {
            row_count = row_count + 1;
        }
        
        [iv setBackgroundColor:[UIColor grayColor]];
        [iv makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(imageHolder).multipliedBy(0.25).with.offset(-1);
            make.height.equalTo(iv.width);
            if (priorImage) {
                if (location == 1) {
                    make.left.equalTo(imageHolder);
                    make.top.equalTo(priorImage.bottom).with.offset(1);
                } else {
                    make.left.equalTo(priorImage.right).with.offset(1);
                    make.top.equalTo(priorImage);
                }
                
            } else {
                make.left.equalTo(imageHolder);
                make.top.equalTo(imageHolder);
            }
        }];
        
        priorImage = iv;
        
        if (location == 4) {
            location = 1;
        } else {
            location = location + 1;
        }
    }
    
    [imageHolder updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(row_count * cell_height));
    }];
    
    return imageHolder;
    
}

- (void)didTapImage:(BCButton *)sender
{
    
    NSMutableArray *fullSet = NSMutableArray.new;
    if (sender && sender.userInfo && [sender.userInfo objectForKey:@"set"]) {
        fullSet = [sender.userInfo objectForKey:@"set"];
    }
    
    NSMutableArray *photos = [NSMutableArray new];
    
    for (CardImage *ci in fullSet) {
        IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:ci.image_url]];
        [photos addObject:photo];
    }
    
    
    if (sender && sender.userInfo && [sender.userInfo objectForKey:@"cardimage"]) {
        
        CardImage *ci = [sender.userInfo objectForKey:@"cardimage"];
        
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
        browser.displayActionButton = NO;
        browser.displayArrowButton = NO;
        browser.displayCounterLabel = YES;
        
        [self.cardHolderVC presentViewController:browser animated:YES completion:nil];
        
        
//        // Create image info
//        JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
//        
//        if (ci.image) {
//            imageInfo.image = ci.image;
//        } else if (ci.image_url) {
//            imageInfo.imageURL = [NSURL URLWithString:ci.image_url];
//        }
//        
//        
//        CGPoint point = [sender.superview convertPoint:sender.superview.frame.origin toView:self.cardHolderVC.view];
//        
//        imageInfo.referenceRect = CGRectMake(point.x, point.y, sender.superview.frame.size.width, sender.superview.frame.size.height);
//        imageInfo.referenceView = self;
//        
//        // Setup view controller
//        JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
//                                               initWithImageInfo:imageInfo
//                                               mode:JTSImageViewControllerMode_Image
//                                               backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
//        
//        // Present the view controller.
//        [imageViewer showFromViewController:self.cardHolderVC transition:JTSImageViewControllerTransition_FromOriginalPosition];
        
    }

}

- (UIView *) buildEmailFieldView:(UIView *)prior withField:(CardField *)field intoView:(UIView *)holderView
{
    if (field.title && ![field.title isEqualToString:@""]) {
        
        NSString *title_text = [field.title uppercaseString];
        UILabel *title = UILabel.new;
        [title setText:title_text];
        
        [title setStyleClass:@"lbl-card-texttitle"];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:title_text];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineHeightMultiple:1.1];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [title_text length])];
        
        title.attributedText = attributedString;
        
        [title setNumberOfLines:0];
        [holderView addSubview:title];
        [title makeConstraints:^(MASConstraintMaker *make) {
            if (prior) {
                make.top.equalTo(prior.bottom).with.offset(16);
            } else {
                make.top.equalTo(holderView).with.offset(20);
            }
            make.left.equalTo(holderView).with.offset(20);
            make.right.equalTo(holderView).with.offset(-20);
            make.height.greaterThanOrEqualTo(@20);
        }];
        
        prior = title;
        
    }
    
    
    BCLabel *value = BCLabel.new;
    [value setStyleClass:@"lbl-card-phonevalue"];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:field.field_value];
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyle setLineHeightMultiple:1.1];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [field.field_value length])];
    
    value.attributedText = attributedString;
    
    
    [value setNumberOfLines:0];
    [holderView addSubview:value];
    [value makeConstraints:^(MASConstraintMaker *make) {
        if (prior) {
            make.top.equalTo(prior.bottom).with.offset(4);
        } else {
            make.top.equalTo(holderView).with.offset(20);
        }
        make.left.equalTo(holderView).with.offset(20);
        make.right.equalTo(holderView).with.offset(-20);
        make.height.greaterThanOrEqualTo(@20);
    }];
    
    
    return value;
    
}

- (UIView *) buildWebFieldView:(UIView *)prior withField:(CardField *)field intoView:(UIView *)holderView
{
    
    NSString *display_text = @"";
    
    if (field.title && ![field.title isEqualToString:@""]) {
        
        display_text = field.title;
        
    } else {
        
        display_text = field.field_value;
        
    }
    
    
    BCLabel *value = BCLabel.new;
    [value setStyleClass:@"lbl-card-webvalue"];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:display_text];
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyle setLineHeightMultiple:1.1];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [display_text length])];
    
    value.attributedText = attributedString;
    
    
    [value setNumberOfLines:0];
    [holderView addSubview:value];
    [value makeConstraints:^(MASConstraintMaker *make) {
        if (prior) {
            make.top.equalTo(prior.bottom).with.offset(4);
        } else {
            make.top.equalTo(holderView).with.offset(20);
        }
        make.left.equalTo(holderView).with.offset(20);
        make.right.equalTo(holderView).with.offset(-20);
        make.height.greaterThanOrEqualTo(@20);
    }];
    
    
    BCButton *goWeb = BCButton.new;
    [holderView addSubview:goWeb];
    [goWeb setStyleCSS:@"border-radius: 6px;"];
    [goWeb makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(value.top);
        make.left.equalTo(value).with.offset(-10);
        make.height.equalTo(value);
        make.width.equalTo(value).with.offset(20);
    }];
    
    NSString *url = field.field_value;
    if (url && [url rangeOfString:@"http"].location == NSNotFound) {
        url = [NSString stringWithFormat:@"http://%@", url];
    }
    
    goWeb.userInfo = @{@"url":url};
    
    [goWeb addTarget:self action:@selector(openURL:) forControlEvents:UIControlEventTouchUpInside];

    return value;
    
    
}

- (UIView *) buildPhoneFieldView:(UIView *)prior withField:(CardField *)field intoView:(UIView *)holderView
{
    if (field.title && ![field.title isEqualToString:@""]) {
        
        NSString *title_text = [field.title uppercaseString];
        BCLabel *title = BCLabel.new;
        [title setText:title_text];
        
        [title setStyleClass:@"lbl-card-texttitle"];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:title_text];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineHeightMultiple:1.1];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [title_text length])];
        
        title.attributedText = attributedString;
        
        [title setNumberOfLines:0];
        [holderView addSubview:title];
        [title makeConstraints:^(MASConstraintMaker *make) {
            if (prior) {
                make.top.equalTo(prior.bottom).with.offset(16);
            } else {
                make.top.equalTo(holderView).with.offset(20);
            }
            make.left.equalTo(holderView).with.offset(20);
            make.right.equalTo(holderView).with.offset(-20);
            make.height.greaterThanOrEqualTo(@20);
        }];
        
        prior = title;
        
    }
    
    
    
    BCLabel *value = BCLabel.new;
    [value setStyleClass:@"lbl-card-phonevalue"];
    
    
    BCPhoneFormat *fmt = [BCPhoneFormat instance];
    // Call any number of times
    NSString *numberString = field.field_value;// the phone number to format
    NSString *formattedNumber = [fmt format:numberString];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:formattedNumber];
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyle setLineHeightMultiple:1.1];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [formattedNumber length])];
    
    value.attributedText = attributedString;
    
    
    [value setNumberOfLines:0];
    [holderView addSubview:value];
    [value makeConstraints:^(MASConstraintMaker *make) {
        if (prior) {
            make.top.equalTo(prior.bottom).with.offset(4);
        } else {
            make.top.equalTo(holderView).with.offset(20);
        }
        make.left.equalTo(holderView).with.offset(20);
        make.right.equalTo(holderView).with.offset(-60);
        make.height.greaterThanOrEqualTo(@20);
    }];
    
    BCButton *phoneCall = BCButton.new;
    [holderView addSubview:phoneCall];
    [phoneCall makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(value.right);
        make.right.equalTo(holderView);
        make.height.equalTo(@50);
        make.top.equalTo(value).with.offset(-20);
    }];
    
    [phoneCall setImage:[UIImage imageNamed:@"Btn-CardAction-Phone"] forState:UIControlStateNormal];
    
    phoneCall.userInfo = @{@"phone":field.field_value};
    
    [phoneCall addTarget:self action:@selector(startPhoneCall:) forControlEvents:UIControlEventTouchUpInside];
    
    return value;
    
}

- (UIView *) buildWebContentFieldView:(UIView *)prior withField:(CardField *)field intoView:(UIView *)holderView
{
    
    BOOL has_title = NO;
    if (field.title && ![field.title isEqualToString:@""]) {
        
        has_title = YES;
        
        NSString *title_text = field.title;
        BCLabel *title = BCLabel.new;
        [title setText:title_text];
        
        [title setStyleClass:@"lbl-card-textvalue"];
        [title setStyleCSS:@"text-align: center; font-weight: 600;"];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:title_text];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineHeightMultiple:1.1];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [title_text length])];
        
        title.attributedText = attributedString;
        
        [title setPreferredMaxLayoutWidth:[holderView bounds].size.width];
        
        [title setNumberOfLines:0];
        [holderView addSubview:title];
        [title makeConstraints:^(MASConstraintMaker *make) {
            if (prior) {
                make.top.equalTo(prior.bottom).with.offset(2);
            } else {
                make.top.equalTo(holderView).with.offset(20);
            }
            make.left.equalTo(holderView).with.offset(20);
            make.right.equalTo(holderView).with.offset(-20);
            make.height.greaterThanOrEqualTo(@20);
        }];
        
        prior = title;
        
    }
    
    UIWebView *webview = UIWebView.new;
    [webview setBackgroundColor:[UIColor grayColor]];
    
    [holderView addSubview:webview];
    [webview makeConstraints:^(MASConstraintMaker *make) {
        if (has_title && prior) {
            make.top.equalTo(prior.bottom).with.offset(10);
        } else {
            make.top.equalTo(holderView.top).with.offset(30);
        }
        make.left.right.equalTo(holderView);
        make.bottom.equalTo(holderView);
        
    }];
    
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:field.field_value]]];
    
    return webview;
    
}


- (UIView *) buildStatFieldView:(UIView *)prior withField:(CardField *)field intoView:(UIView *)holderView
{
    
    BOOL has_title = NO;
    if (field.title && ![field.title isEqualToString:@""]) {
        
        has_title = YES;
        
        NSString *title_text = field.title;
        BCLabel *title = BCLabel.new;
        [title setText:title_text];
        
        [title setStyleClass:@"lbl-card-textvalue"];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:title_text];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineHeightMultiple:1.1];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [title_text length])];
        
        title.attributedText = attributedString;
        
        [title setPreferredMaxLayoutWidth:[holderView bounds].size.width];
        
        [title setNumberOfLines:0];
        [holderView addSubview:title];
        [title makeConstraints:^(MASConstraintMaker *make) {
            if (prior) {
                make.top.equalTo(prior.bottom).with.offset(2);
            } else {
                make.top.equalTo(holderView).with.offset(20);
            }
            make.left.equalTo(holderView).with.offset(20);
            make.right.equalTo(holderView).with.offset(-160);
            make.height.greaterThanOrEqualTo(@20);
        }];
        
    }
    
    
    BCLabel *value = BCLabel.new;
    [value setStyleClass:@"lbl-card-textvalue"];
    [value setStyleCSS:@"text-align: right;"];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:field.field_value];
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyle setLineHeightMultiple:1.1];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [field.field_value length])];
    
    value.attributedText = attributedString;
    
    [value setPreferredMaxLayoutWidth:[holderView bounds].size.width];
    [value setNumberOfLines:0];
    [holderView addSubview:value];
    [value makeConstraints:^(MASConstraintMaker *make) {
        if (prior) {
            if (has_title) {
                make.left.equalTo(holderView.right).with.offset(-160);
                make.right.equalTo(holderView).with.offset(-20);
            } else {
                make.left.equalTo(holderView).with.offset(20);
                make.right.equalTo(holderView).with.offset(-20);
            }
            make.top.equalTo(prior.bottom).with.offset(2);
        } else {
            make.top.equalTo(holderView).with.offset(80);
        }
        
        make.height.greaterThanOrEqualTo(@20);
    }];
    
    
    return value;
    
}

- (UIView *) buildTextFieldView:(UIView *)prior withField:(CardField *)field intoView:(UIView *)holderView
{
    
    BOOL has_title = NO;
    BCLabel *title = BCLabel.new;
    
    if (field.title && ![field.title isEqualToString:@""]) {
        
        has_title = YES;
        
        NSString *title_text = [field.title uppercaseString];
        [title setText:title_text];
        
        [title setStyleClass:@"lbl-card-texttitle"];
        
        if ([field.field_value isEqualToString:@""]) {
            [title setStyleCSS:@"color: #242424;"];
        }
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:title_text];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineHeightMultiple:1.1];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [title_text length])];
        
        title.attributedText = attributedString;
        
        [title setPreferredMaxLayoutWidth:[holderView bounds].size.width];
        
        [title setNumberOfLines:0];
        [holderView addSubview:title];
        [title makeConstraints:^(MASConstraintMaker *make) {
            if (prior) {
                make.top.equalTo(prior.bottom).with.offset(16);
            } else {
                make.top.equalTo(holderView).with.offset(20);
            }
            make.left.equalTo(holderView).with.offset(20);
            make.right.equalTo(holderView).with.offset(-20);
            make.height.greaterThanOrEqualTo(@20);
        }];
        
        prior = title;
        
    }
    
    if ([field.field_value isEqualToString:@""]) {
        if (has_title) {
            return title;
        }
    }
    
    BCLabel *value = BCLabel.new;
    [value setStyleClass:@"lbl-card-textvalue"];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:field.field_value];
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyle setLineHeightMultiple:1.1];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [field.field_value length])];
    
    value.attributedText = attributedString;
    
    [value setPreferredMaxLayoutWidth:[holderView bounds].size.width];
    [value setNumberOfLines:0];
    [holderView addSubview:value];
    [value makeConstraints:^(MASConstraintMaker *make) {
        if (prior) {
            if (has_title) {
                make.top.equalTo(prior.bottom).with.offset(4);
            } else {
                make.top.equalTo(prior.bottom).with.offset(12);
            }
        } else {
            make.top.equalTo(holderView).with.offset(80);
        }
        make.left.equalTo(holderView).with.offset(20);
        make.right.equalTo(holderView).with.offset(-20);
        make.height.greaterThanOrEqualTo(@20);
    }];
    
    
    return value;
    
}

- (void)editCard:(id)sender
{
    if (_cardHolderVC && [_cardHolderVC respondsToSelector:@selector(editCard:)]) {
        [(DeckViewController *)_cardHolderVC editCard:_card];
    }
}

@end
