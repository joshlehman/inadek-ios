//
//  CardViewComments.m
//  Inadek
//
//  Created by Josh Lehman on 2/1/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardViewComments.h"
#import "DeckViewController.h"
#import "CommentTVCell.h"

@implementation CardViewComments {
    UITextField *commentBoxField;
    NSMutableArray *comments;
    Card *thisCard;
    UILabel *cardCommentCount;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)initWithBaseView:(id)baseCard andDeck:(Deck *)deck
{
    self = [self initWithFrame:CGRectZero];
    if (self) {
        
        BOOL isDeck = NO;
        commentBoxOffset = 0;
        [self setBackgroundColor:[UIColor whiteColor]];
        
        if ([baseCard isKindOfClass:[DeckView class]]) {
            isDeck = YES;
            _baseViewController = [(DeckView *)baseCard cardHolderVC];
            _baseDeckView = (DeckView *)baseCard;
        } else if ([baseCard isKindOfClass:[CardView class]]) {
            _baseViewController = [(CardView *)baseCard cardHolderVC];
            _baseCardView = (CardView *)baseCard;
        }
        
        commentsTV = UITableView.new;
        commentsTV.delegate = self;
        commentsTV.dataSource = self;
        commentsTV.estimatedRowHeight = 44.0;
        commentsTV.rowHeight = UITableViewAutomaticDimension;
        [commentsTV setBackgroundColor:[UIColor colorWithWhite:0.98 alpha:1.0]];
        [commentsTV setTag:1];
        [commentsTV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        UIView *header = UIView.new;
        [header setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:header];
        [header makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@40);
        }];
        
        UIView *topBorder = UIView.new;
        
        [topBorder setBackgroundColor:[UIColor colorWithWhite:0.92 alpha:1.0]];
        
        [self addSubview:topBorder];
        [topBorder makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(header.bottom).with.offset(0);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@1);
        }];
        
        [self addSubview:commentsTV];
        [commentsTV makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topBorder.bottom);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
        
        if (deck) {
            UILabel *deckTitle = UILabel.new;
            [deckTitle setStyleClass:@"lbl-deck-title-dark"];
            [deckTitle setText:deck.name];
            [self addSubview:deckTitle];
            [deckTitle makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(header).with.offset(2);
                make.left.right.equalTo(header);
                make.height.equalTo(@16);
            }];
            
            UILabel *deckCommentCount = UILabel.new;
            [deckCommentCount setStyleClass:@"lbl-deck-subtitle-dark"];
            [deckCommentCount setText:@"No Comments"];
            [self addSubview:deckCommentCount];
            [deckCommentCount makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(deckTitle.bottom);
                make.left.right.equalTo(header);
                make.height.equalTo(@16);
            }];
            
            UIButton *closeButton = UIButton.new;
            [closeButton setImage:[UIImage imageNamed:@"Btn-DeckAction-Close"] forState:UIControlStateNormal];
            [self addSubview:closeButton];
            [closeButton makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self).with.offset(-3);
                make.top.equalTo(self).with.offset(-3);
                make.width.equalTo(@40);
                make.height.equalTo(@40);
            }];
            
            if (isDeck) {
                [closeButton addTarget:self.baseDeckView action:@selector(closeComments:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                [closeButton addTarget:self.baseCardView action:@selector(closeComments:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        
        
    }
    
    return self;
}

- (id)initWithBaseView:(id)baseCard andCard:(Card *)card
{
    self = [self initWithFrame:CGRectZero];
    if (self) {
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        BOOL isDeck = NO;
        commentBoxOffset = 0;
        thisCard = card;
        comments = [thisCard sortedComments];
        
        if ([baseCard isKindOfClass:[CardView class]]) {
            _baseViewController = [(CardView *)baseCard cardHolderVC];
            _baseCardView = (CardView *)baseCard;
        }
        
        commentsTV = UITableView.new;
        commentsTV.delegate = self;
        commentsTV.dataSource = self;
        commentsTV.estimatedRowHeight = 44.0;
        commentsTV.rowHeight = UITableViewAutomaticDimension;
        [commentsTV setBackgroundColor:[UIColor colorWithWhite:0.98 alpha:1.0]];
        [commentsTV setTag:1];
        [commentsTV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        UIView *header = UIView.new;
        [header setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:header];
        [header makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@44);
        }];
        
        UIView *topBorder = UIView.new;
        
        [topBorder setBackgroundColor:[UIColor colorWithWhite:0.92 alpha:1.0]];
        
        [self addSubview:topBorder];
        [topBorder makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(header.bottom).with.offset(0);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@1);
        }];
        
        [self addSubview:commentsTV];
        [commentsTV makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topBorder.bottom);
            make.left.equalTo(self);
            make.right.equalTo(self);
            commentsTVBottom = make.bottom.equalTo(self).with.offset(0 - commentBoxOffset);
        }];
        
        if (card) {
            UILabel *cardTitle = UILabel.new;
            [cardTitle setStyleClass:@"lbl-deck-title-dark"];
            [cardTitle setText:card.name];
            [self addSubview:cardTitle];
            [cardTitle makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(header).with.offset(6);
                make.left.right.equalTo(header);
                make.height.equalTo(@16);
            }];
            
            cardCommentCount = UILabel.new;
            [cardCommentCount setStyleClass:@"lbl-deck-subtitle-dark"];
            if (comments.count > 1) {
                [cardCommentCount setText:[NSString stringWithFormat:@"%lu Comments", (unsigned long)comments.count]];
            } else if (comments.count == 1) {
                    [cardCommentCount setText:[NSString stringWithFormat:@"%lu Comment", (unsigned long)comments.count]];
            } else {
                [cardCommentCount setText:@"No Comments"];
            }
            
            [self addSubview:cardCommentCount];
            [cardCommentCount makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(cardTitle.bottom);
                make.left.right.equalTo(header);
                make.height.equalTo(@16);
            }];
            
            UIButton *closeButton = UIButton.new;
            [closeButton setImage:[UIImage imageNamed:@"Btn-DeckAction-Close"] forState:UIControlStateNormal];
            [self addSubview:closeButton];
            [closeButton makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self).with.offset(-3);
                make.top.equalTo(self).with.offset(-3);
                make.width.equalTo(@40);
                make.height.equalTo(@40);
            }];
            
            if (isDeck) {
                [closeButton addTarget:self.baseDeckView action:@selector(closeComments:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                [closeButton addTarget:self.baseCardView action:@selector(closeComments:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        commentsBox = UIView.new;
        [commentsBox setStyleClass:@"box-shadow-upper"];
        [self addSubview:commentsBox];
        [commentsBox makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.equalTo(@44);
        }];
        
        UIView *commentFieldHolder = UIView.new;
        [commentsBox addSubview:commentFieldHolder];
        [commentFieldHolder setStyleClass:@"box-rounded-entry-field"];
        [commentFieldHolder makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(commentsBox).with.insets(UIEdgeInsetsMake(5, 5, 5, 5));
        }];
        
        commentBoxField = UITextField.new;
        [commentBoxField setDelegate:baseCard];
        [commentBoxField setStyleCSS:@"font-size: 14px;"];
        [commentBoxField setBorderStyle:UITextBorderStyleNone];
        [commentFieldHolder addSubview:commentBoxField];
        [commentBoxField makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(commentFieldHolder).with.insets(UIEdgeInsetsMake(5, 8, 5, 8));
        }];
        [commentBoxField setPlaceholder:@"Write a comment..."];
        
        
    }
    
    return self;
}

- (void)reloadComments:(Card *)card
{
    if (card) {
        thisCard = card;
        comments = [thisCard sortedComments];
        [self loadComments];
        
        if (comments.count > 1) {
            [cardCommentCount setText:[NSString stringWithFormat:@"%lu Comments", (unsigned long)comments.count]];
        } else if (comments.count == 1) {
            [cardCommentCount setText:[NSString stringWithFormat:@"%lu Comment", (unsigned long)comments.count]];
        } else {
            [cardCommentCount setText:@"No Comments"];
        }
    }
}

- (void)loadComments
{
    if (comments && [comments count] > 0) {
        [commentsTV setHidden:NO];
        [commentsTV reloadData];
    
        [commentsTV setNeedsLayout];
        [commentsTV layoutIfNeeded];
        
    } else {
        [commentsTV setHidden:YES];
    }
}

- (BOOL)isShown
{
    if (isViewShown) {
        return YES;
    } else {
        return NO;
    }
}

- (void)updateFieldText:(NSString *)value
{
    [commentBoxField setText:value];
}

- (void)setIsShown:(BOOL)shown {
    isViewShown = shown;
}

- (void)moveViewUp:(CGSize)size
{
    commentsTVBottom.offset = 0 - size.height + 10;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [comments count];    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"CommentIdentifier";
    
    CommentTVCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[CommentTVCell alloc] initWithFrame:CGRectZero];
    }
    
    if ([comments count] > indexPath.row) {
        [cell buildCell:[comments objectAtIndex:indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



@end
