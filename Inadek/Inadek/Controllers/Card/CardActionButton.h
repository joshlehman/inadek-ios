//
//  CardActionButton.h
//  Inadek
//
//  Created by Josh Lehman on 2/7/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <QuartzCore/QuartzCore.h>
#import <Masonry/Masonry.h>
#import "Common.h"


@interface CardActionButton : UIButton {
    BOOL isHighlighted;
}

@property (nonatomic, retain) UIImageView *myImage;
@property (nonatomic, retain) UIImage *baseImage;
@property (nonatomic, retain) NSDictionary *userInfo;

- (void)placeCounter:(int)number;
- (void)placeAlertCounter:(int)number;

@end
