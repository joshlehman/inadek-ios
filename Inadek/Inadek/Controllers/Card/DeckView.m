//
//  DeckView.m
//  Inadek
//
//  Created by Josh Lehman on 1/7/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckView.h"
#import "DeckTrigger+Core.h"

#import "AccountManager.h"

#define kTopOffset      70
#define kSidePadding    10

#define kViewTagReturn  90

@implementation DeckView

- (id)initWithOwner:(BCViewController *)parentVC andView:(UIView *)view toSide:(int)side shouldSlideUp:(BOOL)slideUp
{
    self = [self initWithFrame:CGRectZero];
    _base = UIView.new;
    [_base setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_base];
    [_base makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    self.cardHolderVC = parentVC;
    
    // drop shadow
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.layer setShadowOpacity:0.6];
    [self.layer setShadowRadius:2.0];
    [self.layer setShadowOffset:CGSizeMake(0.0, 1.0)];
    
    [self setBackgroundColor:[UIColor whiteColor]];
    //[self setClipsToBounds:YES];
    
    [view addSubview:self];
    [self.layer setCornerRadius:3.0];
    [_base.layer setCornerRadius:3.0];
    [_base setClipsToBounds:YES];

    [self mas_makeConstraints:^(MASConstraintMaker *make){
        if (side == 0) {
            if (slideUp) {
                //NSLog(@"Offest: %@", self.cardHolderVC.view.bounds.size.height);
                self.leftEdgeMC = make.top.equalTo(@(0)).with.offset(1000);
                make.left.equalTo(view).with.offset(10);
            } else {
                make.top.equalTo(@(kTopOffset)); //.with.offset(kTopOffset);
                self.leftEdgeMC = make.left.equalTo(view).with.offset(self.cardHolderVC.view.bounds.size.width);
            }
        } else {
            if (slideUp) {
                //NSLog(@"Offest: %@", self.cardHolderVC.view.bounds.size.height);
                self.leftEdgeMC = make.top.equalTo(@(0)).with.offset(1000);
                make.left.equalTo(view).with.offset(10);
            } else {
                make.top.equalTo(@(kTopOffset)); //.with.offset(kTopOffset);
                self.leftEdgeMC = make.left.equalTo(view).with.offset(0 - self.cardHolderVC.view.bounds.size.width);
            }
        }
        make.width.equalTo(view).with.offset(-(kSidePadding * 2));
        make.height.equalTo(view).with.offset(0 - kTopOffset - kSidePadding);
    }];
    
    return self;
    
}

- (void)buildCardInDeck:(Deck *)aDeck
{
    if (aDeck) {
        
        swipeLeftGR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDeck:)];
        [swipeLeftGR setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self addGestureRecognizer:swipeLeftGR];
        
        swipeRightGR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDeck:)];
        [swipeRightGR setDirection:UISwipeGestureRecognizerDirectionRight];
        [self addGestureRecognizer:swipeRightGR];
        
        swipeUpGR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDeck:)];
        [swipeUpGR setDirection:UISwipeGestureRecognizerDirectionUp];
        [self addGestureRecognizer:swipeUpGR];
        
        tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDeck:)];
//        [tapGR setCancelsTouchesInView:NO];
        
        ignoreGestures = NO;
        
        _deck = aDeck;
        
        if (_deck.base_color) {
            //[self setBackgroundColor:[UIColor colorWithHexString:_deck.base_color]];
        }
        [self setBackgroundColor:[UIColor colorWithHexString:@"FDFDFD"]];
        
        _cardImage = [[CardViewImage alloc] initWithFrame:CGRectZero];
        [_cardImage setBackgroundColor:[UIColor redColor]];
        [_base addSubview:_cardImage];
        [_cardImage makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
            make.height.equalTo(self.width);
        }];
        
        [_cardImage loadImage:_deck.image_url];
        
        deckCoverDetails = UIView.new;
        [deckCoverDetails setBackgroundColor:[UIColor whiteColor]];
        [_base addSubview:deckCoverDetails];
        [deckCoverDetails makeConstraints:^(MASConstraintMaker *make) {
            deckCoverDetailsTop = make.top.equalTo(_cardImage.bottom);
            make.left.equalTo(_base);
            make.right.equalTo(_base);
            make.bottom.equalTo(_base);
        }];
        
        // TITLE
        UILabel *title = UILabel.new;
        [title setStyleClass:@"lbl-card-name"];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_deck.name];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineHeightMultiple:0.8];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [_deck.name length])];
        
        title.attributedText = attributedString ;

        [title setNumberOfLines:0];
        [deckCoverDetails addSubview:title];
        [title makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(deckCoverDetails).with.offset(10);
            make.width.lessThanOrEqualTo(@200);
            make.height.greaterThanOrEqualTo(@32);
            make.centerX.equalTo(deckCoverDetails);
        }];
        
        
        // SUBTITLE
        UILabel *subTitle = UILabel.new;
        [subTitle setStyleClass:@"lbl-card-subtitle"];
        
        if (_deck.base_color) {
            [subTitle setTextColor:[UIColor colorWithHexString:_deck.base_color]];
        }
        
        [subTitle setText:[NSString stringWithFormat:@"%@%@", [[_deck cardCountAsString] uppercaseString], @", RECENTLY UPDATED"]];
        [deckCoverDetails addSubview:subTitle];
        [subTitle makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(title.bottom);
            make.width.equalTo(deckCoverDetails);
            make.height.equalTo(@18);
            make.centerX.equalTo(deckCoverDetails);
        }];
        
        // Build buttons based on triggers
        
        if ([_deck.triggers allObjects] && [[_deck.triggers allObjects] count] > 0) {
            int count = 0;
            
            NSMutableArray *triggerButtons = NSMutableArray.new;
            
            for (DeckTrigger *trig in [_deck.triggers allObjects]) {
                
                BCButton *rounded = BCButton.new;
                [rounded buildType:@"deck.action"];
                [rounded setTitle:[trig.action_verb uppercaseString] forState:UIControlStateNormal];
                
                if ([trig is_url] && trig.value && ![trig.value isEqualToString:@""]) {
                    // Open a URL
                    [rounded setUserInfo:@{@"url":trig.value}];
                    [rounded addTarget:self action:@selector(openURL:) forControlEvents:UIControlEventTouchUpInside];
                } else if ([trig is_phone] && trig.value && ![trig.value isEqualToString:@""]) {
                    // Open a URL
                    [rounded setUserInfo:@{@"phone":trig.value}];
                    [rounded addTarget:self action:@selector(startPhoneCall:) forControlEvents:UIControlEventTouchUpInside];
                } else if ([trig is_email] && trig.value && ![trig.value isEqualToString:@""]) {
                    // Open a URL
                    [rounded setUserInfo:@{@"email":trig.value}];
                    [rounded addTarget:self action:@selector(openEmail:) forControlEvents:UIControlEventTouchUpInside];
                } else if ([trig is_map] && trig.value && ![trig.value isEqualToString:@""]) {
                    // Open a URL
                    [rounded setUserInfo:@{@"location":trig.value}];
                    [rounded addTarget:self action:@selector(openLocation:) forControlEvents:UIControlEventTouchUpInside];
                }
                
                [rounded.layer setCornerRadius:16.0f];
                
                if (_deck.base_color) {
                    [rounded setColor:[UIColor colorWithHexString:_deck.base_color]];
                    [rounded setBackgroundColor:[UIColor colorWithHexString:_deck.base_color]];
                }
                
                [triggerButtons addObject:rounded];
                
                count++;
                if (count == 2) { break; }
            }
            
            if ([triggerButtons count] == 2) {
                
                [deckCoverDetails addSubview:[triggerButtons objectAtIndex:0]];
                [[triggerButtons objectAtIndex:0] makeConstraints:^(MASConstraintMaker *make) {
                    make.width.equalTo(@100);
                    make.height.equalTo(@32);
                    make.centerX.equalTo(deckCoverDetails).with.offset(-55);
                    make.top.equalTo(subTitle.bottom).with.offset(15);
                }];
                
                [deckCoverDetails addSubview:[triggerButtons objectAtIndex:1]];
                [[triggerButtons objectAtIndex:1] makeConstraints:^(MASConstraintMaker *make) {
                    make.width.equalTo(@100);
                    make.height.equalTo(@32);
                    make.centerX.equalTo(deckCoverDetails).with.offset(55);
                    make.top.equalTo(subTitle.bottom).with.offset(15);
                }];
                
            } else if ([triggerButtons count] == 1) {
                [deckCoverDetails addSubview:[triggerButtons objectAtIndex:0]];
                [[triggerButtons objectAtIndex:0] makeConstraints:^(MASConstraintMaker *make) {
                    make.width.equalTo(@120);
                    make.height.equalTo(@32);
                    make.centerX.equalTo(deckCoverDetails);
                    make.top.equalTo(subTitle.bottom).with.offset(15);
                }];
            }
            
        }
        
        
        // CREATOR
        UILabel *creator = UILabel.new;
        [creator setStyleClass:@"lbl-card-creator"];
        
        if (_deck.owner_name) {
            NSString *owner_name = [NSString stringWithFormat:@"BY %@", _deck.owner_name];
            [creator setText:[owner_name uppercaseString]];
        } else {
            [creator setText:@"BY INADEK"];
        }
        
        [deckCoverDetails addSubview:creator];
        [creator makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(deckCoverDetails.bottom).with.offset(-15);
            make.width.equalTo(@140);
            make.height.equalTo(@18);
            make.left.equalTo(deckCoverDetails).with.offset(20);
        }];
        
        UIButton *deckAddAction = UIButton.new;
        [deckAddAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Add"] forState:UIControlStateNormal];
        [deckCoverDetails addSubview:deckAddAction];
        [deckAddAction makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.right.equalTo(deckCoverDetails).with.offset(-10);
            make.bottom.equalTo(deckCoverDetails).with.offset(-8);
        }];
        
        [deckAddAction addTarget:self.cardHolderVC action:@selector(showDeckActions:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *deckShareAction = UIButton.new;
        [deckShareAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Share"] forState:UIControlStateNormal];
        [deckCoverDetails addSubview:deckShareAction];
        [deckShareAction makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.right.equalTo(deckAddAction.left).with.offset(-16);
            make.bottom.equalTo(deckCoverDetails).with.offset(-8);
        }];
        
        [deckShareAction addTarget:self.cardHolderVC action:@selector(openShareActions:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        deckFavAction = UIButton.new;
        [deckFavAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Like"] forState:UIControlStateNormal];
        [deckCoverDetails addSubview:deckFavAction];
        [deckFavAction makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.left.equalTo(deckCoverDetails).with.offset(5);
            make.top.equalTo(@0).with.offset(3);
        }];
        
        if (self.deck && [[AccountManager sharedManager] isFavoriteDeck:self.deck]) {
            [deckFavAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Like-On"] forState:UIControlStateNormal];
        }
        
        [deckFavAction addTarget:self action:@selector(setFavorite:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *deckCommentAction = UIButton.new;
        [deckCommentAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Comment"] forState:UIControlStateNormal];
        [deckCoverDetails addSubview:deckCommentAction];
        [deckCommentAction makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.right.equalTo(deckCoverDetails).with.offset(-5);
            make.top.equalTo(@0).with.offset(3);
        }];
        
        [deckCommentAction addTarget:self action:@selector(openComments:) forControlEvents:UIControlEventTouchUpInside];
        [deckCommentAction setHidden:YES];
        
        deckComments = [[CardViewComments alloc] initWithBaseView:self andDeck:_deck];
        [deckComments setAlpha:0.0];
        
        [_base addSubview:deckComments];
        [deckComments makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(title.top).with.offset(-5);
            make.bottom.equalTo(deckCoverDetails.bottom);
            make.left.equalTo(_base);
            make.right.equalTo(_base);
        }];
        
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    // Disallow recognition of tap gestures in the segmented control.
    if (([touch.view isKindOfClass:[UIButton class]])) {//change it to your condition
        return NO;
    }
    return YES;
}

- (void)openURL:(BCButton *)sender
{
    
    NSDictionary *ui = [sender userInfo];
    SFSafariViewController *webVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:[ui objectForKey:@"url"]]];
    [(DeckViewController *)_cardHolderVC presentViewController:webVC animated:YES completion:^{
        //
    }];
}

- (void)startPhoneCall:(BCButton *)sender
{
    
    NSDictionary *ui = [sender userInfo];
    
    NSString *phoneNumber = [ui objectForKey:@"phone"];
    
    NSString *cleanedPhone = [[phoneNumber componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                           componentsJoinedByString:@""];
    
    NSString *phone = [@"tel://" stringByAppendingString:cleanedPhone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone]];
    
}

- (void)openEmail:(BCButton *)sender
{
    
    NSDictionary *ui = [sender userInfo];
    NSString *emailAddress = [ui objectForKey:@"email"];
    
    NSString *mailtoEmail = [@"mailto:" stringByAppendingString:emailAddress];
    NSString *url = [mailtoEmail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
    
}

- (void)openLocation:(BCButton *)sender
{
    
    NSDictionary *ui = [sender userInfo];
    NSString *location = [[ui objectForKey:@"location"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *mapUrl = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@", location];
    
    SFSafariViewController *webVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:mapUrl]];
    [(DeckViewController *)_cardHolderVC presentViewController:webVC animated:YES completion:^{
        //
    }];
    
}


- (void)swipeDeck:(UISwipeGestureRecognizer *)gestureRecognizer
{
    if (ignoreGestures) return;
    
    // If they swipe back (right), they return to home view, if they swipe forward (left) they dive into the deck
    if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [(DeckViewController *)_cardHolderVC goIntoDeck:nil];
    } else if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        // Take em back...
        [(DeckViewController *)_cardHolderVC dismissDeck:nil];
    } else if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionUp) {
        [(DeckViewController *)_cardHolderVC showCardCollection];
    }
}

- (void)tapDeck:(UIGestureRecognizer *)gestureRecognizer
{
    if (ignoreGestures) return;
}

- (void)setFavorite:(id)sender
{
    if (self.deck && [[AccountManager sharedManager] isFavoriteDeck:self.deck]) {
        [self.deck set_is_favorite:NO];
        [deckFavAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Like"] forState:UIControlStateNormal];
    } else {
        [self.deck set_is_favorite:YES];
        [deckFavAction setImage:[UIImage imageNamed:@"Btn-DeckAction-Like-On"] forState:UIControlStateNormal];
    }
    
    [self.deck save];
    
    [[DeckManager sharedManager] favoriteDeck:self.deck];
}

- (void)openComments:(id)sender
{
    
    // 20, 10 - Super springy
    // 5, 10 - Slower entry speed, with only one slight spring back
    // 10, 10 - A slight bounce, but feels a little slow
    // 10, 26 - A little too fast on entry
    // 10, 18 -
    
    if (!deckCoverDetailsTop) {
        return;
    }
    
    int toOffset = 30 - deckCoverDetails.frame.origin.y;
    
    // 0 - deckCoverDetails.bounds.origin.x
    
    POPSpringAnimation *slideInCard = [POPSpringAnimation new];
    slideInCard.toValue = @(toOffset);
    slideInCard.springBounciness = 10;
    slideInCard.springSpeed = 18;
    slideInCard.property = [POPAnimatableProperty mas_offsetProperty];
    [deckCoverDetailsTop pop_addAnimation:slideInCard forKey:@"PopDeckSlideInAnimation"];
    
    [UIView animateWithDuration:0.2 animations:^{
        [deckComments setAlpha:1.0];
    }];
    
}

- (void)closeComments:(id)sender
{
    int toOffset = 0;
    
    POPSpringAnimation *slideInCard = [POPSpringAnimation new];
    slideInCard.toValue = @(toOffset);
    slideInCard.springBounciness = 5;
    slideInCard.springSpeed = 48;
    slideInCard.property = [POPAnimatableProperty mas_offsetProperty];
    [deckCoverDetailsTop pop_addAnimation:slideInCard forKey:@"PopDeckSlideInAnimation"];
    
    [UIView animateWithDuration:0.2 animations:^{
        [deckComments setAlpha:0.0];
    }];
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)loadCardSubviews
{
    // Load up the image
    
    // Load up the card actions, if this is a deck cover
    
    // Load up the deck actions if this is a deck cover
    
    // appeal to higher power!
}


- (void)layoutSubviews {
    // resize your layers based on the view's new frame
    
    CALayer *TopBorder = [CALayer layer];
    TopBorder.frame = CGRectMake(0.0f, 0.0f, _base.bounds.size.width, 0.5f);
    TopBorder.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2].CGColor;
    [TopBorder setOpacity:0.2];
    [_base.layer addSublayer:TopBorder];
}

- (void)animateDeckAway
{
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.springBounciness = 15;
    animation.springSpeed = 30;
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.9, 0.9)];
    [self.layer pop_addAnimation:animation forKey:@"scale"];
    
    POPSpringAnimation *animation2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
    animation2.springBounciness = 15;
    animation2.springSpeed = 30;
    animation2.toValue = [NSValue valueWithCGPoint:CGPointMake(0, self.center.y)];
    [self.layer pop_addAnimation:animation2 forKey:@"rotate"];
    
    POPBasicAnimation *animation3 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    animation3.duration = 0.3;
    animation3.toValue = @(0.0);
    [self.layer pop_addAnimation:animation3 forKey:@"opacity"];
}

- (void)animateDeckBack
{
    
    ignoreGestures = YES;
    
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.springBounciness = 20;
    animation.springSpeed = 30;
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.85, 0.85)];
    [self.layer pop_addAnimation:animation forKey:@"scale_back"];
    
    POPBasicAnimation *animation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    animation2.duration = 0.2;
    animation2.toValue = [NSValue valueWithCGSize:CGSizeMake(0.5, 0.5)];
    [self.layer pop_addAnimation:animation2 forKey:@"opacity_back"];

}

- (void)animateDeckBackUp
{
    
    ignoreGestures = YES;
    
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.springBounciness = 20;
    animation.springSpeed = 30;
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.85, 0.85)];
    [self.layer pop_addAnimation:animation forKey:@"scale_back"];
    
    POPBasicAnimation *animation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    animation2.duration = 0.2;
    animation2.toValue = [NSValue valueWithCGSize:CGSizeMake(0.5, 0.5)];
    [self.layer pop_addAnimation:animation2 forKey:@"opacity_back"];
    
    POPSpringAnimation *animation3 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    animation3.springBounciness = 20;
    animation3.springSpeed = 30;
    animation3.toValue = @(self.center.y - 30);
    [self.layer pop_addAnimation:animation3 forKey:@"slide_up"];
    
    deckIsSlidUp = YES;
    
}

- (void)animateDeckForward
{
    
    ignoreGestures = NO;
    
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.springBounciness = 20;
    animation.springSpeed = 30;
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0, 1.0)];
    [self.layer pop_addAnimation:animation forKey:@"scale_back"];
    
    POPBasicAnimation *animation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    animation2.duration = 0.2;
    animation2.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0, 1.0)];
    [self.layer pop_addAnimation:animation2 forKey:@"opacity_back"];
    
    if (deckIsSlidUp) {
        POPSpringAnimation *animation3 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        animation3.springBounciness = 20;
        animation3.springSpeed = 30;
        animation3.toValue = @(self.center.y + 30);
        [self.layer pop_addAnimation:animation3 forKey:@"slide_up"];
        deckIsSlidUp = NO;
    }
    
    [self enableInteraction];
    
}

- (void)disableInteraction
{
    UIButton *returnBtn = UIButton.new;
    [returnBtn setTag:kViewTagReturn];
    [returnBtn setBackgroundColor:[UIColor clearColor]];
    [self addSubview:returnBtn];
    [returnBtn makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    if (self.cardHolderVC && [self.cardHolderVC respondsToSelector:@selector(disabledCardTapped:)]) {
        [returnBtn addTarget:self.cardHolderVC action:@selector(disabledCardTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

- (void)enableInteraction
{
    [[self viewWithTag:kViewTagReturn] removeFromSuperview];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
