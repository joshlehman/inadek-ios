//
//  CardViewActions.h
//  Inadek
//
//  Created by Josh Lehman on 1/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CardView;

@interface CardViewActions : UIView

@property (nonatomic,retain) CardView *delegate;

- (id) initWithFullControls:(CardView *)aDelegate;

@end
