//
//  CardViewImage.h
//  Inadek
//
//  Created by Josh Lehman on 1/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import "InadekLoader.h"

@interface CardViewImage : UIView

@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) InadekLoader *loader;

- (void)loadImage:(NSString *)url;
- (void)loadLocalImageData:(id)image;

@end
