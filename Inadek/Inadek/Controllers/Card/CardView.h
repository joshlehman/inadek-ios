//
//  CardView.h
//  Inadek
//
//  Created by Josh Lehman on 2/6/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCViewController.h"
#import "BCButton.h"

@class Card, CardViewImage, CardViewActions, CardViewComments;

@interface CardView : UIView <UIGestureRecognizerDelegate, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate> {
    UILongPressGestureRecognizer *longPressGR;
    
    UIView *cardCoverDetails;
    UIView *cardFrontContent;
    UIView *cardBackContent;
    MASConstraint *cardCoverDetailsTop;
    MASConstraint *cardContentTop;
    MASConstraint *cardCommentsTop;
    MASConstraint *cardImageTop;
    CardViewComments *cardComments;
    
    UISwipeGestureRecognizer *swipeLeftGR;
    UISwipeGestureRecognizer *swipeRightGR;
    UISwipeGestureRecognizer *swipeUpGR;
    UITapGestureRecognizer *tapGR;
    UIPanGestureRecognizer *panGR;
    
    BOOL ignoreGestures;
    BOOL cardIsSlidUp;
    
    UIView *keyboardAccessoryView;
    UITextView *commentField;
    
    UIView *imageOverlay;
    
    NSString *side;
}

@property (nonatomic, retain) BCViewController *cardHolderVC;
@property (nonatomic, retain) Card *card;
@property (nonatomic, retain) CardViewImage *cardImage;
@property (nonatomic, retain) CardViewActions *cardActions;

@property (nonatomic, retain) UIView *card_front;
@property (nonatomic, retain) UIView *card_back;

@property (nonatomic, retain) UIScrollView *sv_card_front;
@property (nonatomic, retain) UIScrollView *sv_card_back;

@property (nonatomic, retain) UIView *cf_holder;
@property (nonatomic, retain) UIView *cb_holder;

- (id) initWithMiniView:(BCViewController *)parentVC andCard:(Card *)card;
- (id) initWithCard:(BCViewController *)parentVC andCard:(Card *)card;
- (void)respondToLongPress;
- (void)isFloating:(BOOL)floating;
- (void)animateCardAway:(int)direction;
- (void)animateCardBack;

- (void)animateDeckBack;
- (void)animateDeckBackUp;
- (void)animateDeckForward;
- (void)disableInteraction;

- (void)rebuildMyView:(CardView *)newCV;
- (void)buildInitialCardLayout;

/* Actions we redirect elsewhere */
- (void)editCard:(id)sender;

- (void)didTapImage:(BCButton *)sender;

@end
