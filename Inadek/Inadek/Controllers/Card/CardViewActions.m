//
//  CardViewActions.m
//  Inadek
//
//  Created by Josh Lehman on 1/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardViewActions.h"
#import "CardView.h"

@implementation CardViewActions

- (id) initWithFullControls:(CardView *)aDelegate
{
    self = [self init];
    if (self) {
        
        self.delegate = aDelegate;
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIView *bg = UIView.new;
        [bg setBackgroundColor:[UIColor whiteColor]];
        [bg setStyleClass:@"box-shadow-upper"];
        [self addSubview:bg];
        [bg makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(10, 0, 0, 0));
        }];
        
        UIButton *editCard = UIButton.new;
        [editCard setStyleClass:@"lbl-new-button-sm"];
        [editCard setTitle:@"Edit Card" forState:UIControlStateNormal];
        [bg addSubview:editCard];
        [editCard makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bg).with.offset(-10);
            make.centerY.equalTo(bg);
            make.width.equalTo(@100);
            make.height.equalTo(@44);
        }];
        
        [editCard addTarget:_delegate action:@selector(editCard:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
