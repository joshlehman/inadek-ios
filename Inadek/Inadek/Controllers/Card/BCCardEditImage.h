//
//  BCDeckEditImage.h
//  Inadek
//
//  Created by Josh Lehman on 1/16/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <PixateFreestyle/PixateFreestyle.h>

@class InadekLoader;

@protocol BCCardEditImageDelegate;

@interface BCCardEditImage : UIView

@property (nonatomic, retain) UIImageView *imageHolder;
@property (nonatomic,assign) id<BCCardEditImageDelegate> delegate;    // weak reference
@property (nonatomic, retain) InadekLoader *loader;

- (void)buildButtons;
- (void)showImage:(UIImage *)image;
- (void)loadImage:(NSString *)image_url;

- (void)addImageEditButton;

@end

@protocol BCCardEditImageDelegate <NSObject>
@optional

- (void)cardEditImage:(BCCardEditImage *)image selectImage:(NSString *)mode;

@end