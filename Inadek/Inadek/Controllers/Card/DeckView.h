//
//  DeckView.h
//  Inadek
//
//  Created by Josh Lehman on 1/7/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import <Masonry/Masonry.h>

#import <Pop/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>

#import "BCViewController.h"
#import "BCButton.h"

#import "CardViewActions.h"
#import "CardViewImage.h"
#import "CardViewComments.h"

#import "DeckViewController.h"

#import "Deck+Core.h"

@import SafariServices;


@interface DeckView : UIView <UIGestureRecognizerDelegate> {
    UIView *deckCoverDetails;
    CardViewComments *deckComments;
    
    MASConstraint *deckCoverDetailsTop;
    
    UISwipeGestureRecognizer *swipeLeftGR;
    UISwipeGestureRecognizer *swipeRightGR;
    UISwipeGestureRecognizer *swipeUpGR;
    UITapGestureRecognizer *tapGR;
    
    BOOL ignoreGestures;
    BOOL deckIsSlidUp;
    
    UIButton *deckFavAction;
}

@property (nonatomic, retain) BCViewController *cardHolderVC;
@property (nonatomic, retain) MASConstraint *leftEdgeMC;
@property (nonatomic, retain) CardViewActions *cardActions;
@property (nonatomic, retain) CardViewImage *cardImage;

@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) UIView *base;


- (id)initWithOwner:(BCViewController *)parentVC andView:(UIView *)view toSide:(int)side shouldSlideUp:(BOOL)slideUp;

- (void)buildCardInDeck:(Deck *)aDeck;

- (void)loadViews;

- (void)openComments:(id)sender;
- (void)closeComments:(id)sender;

- (void)animateDeckBack;
- (void)animateDeckBackUp;
- (void)animateDeckForward;
- (void)animateDeckAway;
- (void)disableInteraction;

@end
