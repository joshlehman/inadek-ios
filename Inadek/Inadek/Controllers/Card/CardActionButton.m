//
//  CardActionButton.m
//  Inadek
//
//  Created by Josh Lehman on 2/7/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardActionButton.h"

@implementation CardActionButton

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        if (_myImage && _myImage.image && !isHighlighted) {
            _baseImage = _myImage.image;
            NSLog(@"Setting image again...");
            [_myImage setAlpha:0.3];
        }
        isHighlighted = YES;
    } else {
        isHighlighted = NO;
        if (_baseImage) {
            [_myImage setAlpha:1.0];
        }
    }
    
}

- (void)placeCounter:(int)number
{
    UILabel *counter = UILabel.new;
    [counter setText:[NSString stringWithFormat:@"%d",number]];
    [counter setStyleClass:@"lbl-deck-action-button"];
    [self addSubview:counter];
    [counter makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(_myImage);
    }];
}

- (void)placeAlertCounter:(int)number
{
    UILabel *alertCounter = UILabel.new;
    [alertCounter setText:[NSString stringWithFormat:@"%d",number]];
    [alertCounter setStyleClass:@"lbl-deck-action-button"];
    [alertCounter setBackgroundColor:[UIColor redColor]];
    [alertCounter.layer setCornerRadius:10.0f];
    [alertCounter setClipsToBounds:YES];
    [self addSubview:alertCounter];
    [alertCounter makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(_myImage);
        make.width.equalTo(@20);
        make.height.equalTo(@20);
    }];
}

@end
