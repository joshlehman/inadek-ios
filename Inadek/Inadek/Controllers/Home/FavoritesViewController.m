//
//  FavoritesViewController.m
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "FavoritesViewController.h"

#import "AppDelegate.h"
#import "InadekCVFlowLayout.h"
#import "DeckCVCell.h"
#import "ProfileHeader.h"
#import "SearchHeaderRVCell.h"
#import "UIProfileButton.h"

#import "CreateCardViewController.h"

#import "ProfileManager.h"

@interface FavoritesViewController () {
    CGSize cvCellSize;
    UIEdgeInsets cvEdgeInsets;
    CGSize cvSectionHeader;
    CGSize svSearchHeader;
    NSMutableArray *popularCategories;
    
    UIRefreshControl *refreshControl;
    
    UILabel *noResultsMessage;
}

@end

@implementation FavoritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor redColor]];
    [self setDefaultSizes];
    [self setupCollectionView];
    [self setupTopView];
    
    [self refreshData:@"profiles"];
}

- (void)doNewProfileCard:(id)sender
{
    CreateCardViewController *createCardVC = [[CreateCardViewController alloc] initWithCard:nil andPage:@"page2"];
    createCardVC.card.type_id = [Card getTypeIdFor:@"profile"];
    [createCardVC setTitle:@"New Profile Card"];
    
    [self transitionTo:createCardVC withType:0 buildNavController:YES];
}

- (void)doNewGroup:(id)sender
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Missing Users" message:@"There are not enough Inadek users in your area to begin a group." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    [av show];
}

- (void)refreshData:(NSString *)thisArea
{
    __block NSArray *popularDecks = nil;
    
    [self showLoading];
    
    self.collectionGroups = @[@{@"cards":@[],@"decks":@[]}];
    
    if ([thisArea isEqualToString:@"favs"]) {
        
        [self refreshFavorites:^(id responseObject) {
            
            [self showNoResultsMessage:@"You do not currently have any favorite decks or cards."];
            if (responseObject && [responseObject count] >= 1) {
                
                NSDictionary *favoriteDecks = [responseObject objectAtIndex:0];
                
                if ([favoriteDecks objectForKey:@"decks"] && [[favoriteDecks objectForKey:@"decks"] count] > 0) {
                    [self hideNoResultsMessage];
                    self.collectionGroups = responseObject;
                }
            }
            [decksCV reloadData];
            [self hideLoading];
            [refreshControl endRefreshing];
            
        } failureCallback:^(id responseObject) {
            
            // Empty the controller, and show error
            
        }];
        
    } else if ([thisArea isEqualToString:@"fans"]) {
        
        [self refreshFans:^(id responseObject) {
            
            [self showNoResultsMessage:@"You do not currently have any fans."];
            if (responseObject && [responseObject count] >= 1) {
                
                NSDictionary *profileCards = [responseObject objectAtIndex:0];
                
                if ([profileCards objectForKey:@"cards"] && [[profileCards objectForKey:@"cards"] count] > 0) {
                    [self hideNoResultsMessage];
                    self.collectionGroups = responseObject;
                }
            }
            
            [decksCV reloadData];
            [self hideLoading];
            [refreshControl endRefreshing];
            
        } failureCallback:^(id responseObject) {
            
            // Empty the controller, and show error
            
        }];
        
    } else if ([thisArea isEqualToString:@"groups"]) {
        
        [self refreshFans:^(id responseObject) {
            
            self.collectionGroups = responseObject;
            [decksCV reloadData];
            [self hideLoading];
            [refreshControl endRefreshing];
            
        } failureCallback:^(id responseObject) {
            
            // Empty the controller, and show error
            
        }];
        
    } else if ([thisArea isEqualToString:@"profiles"]) {
        
        [self refreshProfiles:^(id responseObject) {
            
            [self showNoResultsMessage:@"You do not currently have any profile cards."];
            if (responseObject && [responseObject count] >= 1) {
                
                NSDictionary *profileCards = [responseObject objectAtIndex:0];
                
                if ([profileCards objectForKey:@"cards"] && [[profileCards objectForKey:@"cards"] count] > 0) {
                    [self hideNoResultsMessage];
                    self.collectionGroups = responseObject;
                }
            }
            
            [decksCV reloadData];
            [self hideLoading];
            [refreshControl endRefreshing];
            
        } failureCallback:^(id responseObject) {
            
            // Empty the controller, and show error
            
        }];
        
    }
    
    
    
    
    
    //    // TEST: Create data for collection views
    //    NSMutableArray *firstSection = [[NSMutableArray alloc] init];
    //    [firstSection addObject:@{@"my":@"thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //
    //    self.collectionGroups = [[NSArray alloc] initWithObjects:firstSection, nil];
}

- (void)hideNoResultsMessage
{
    if (noResultsMessage) {
        [noResultsMessage removeFromSuperview];
        noResultsMessage = nil;
    }
}

- (void)showNoResultsMessage:(NSString *)message
{
    if (noResultsMessage) {
        [noResultsMessage removeFromSuperview];
        noResultsMessage = nil;
    }
    
    noResultsMessage = UILabel.new;
    [noResultsMessage setStyleClass:@"lbl-message"];
    [self.view addSubview:noResultsMessage];
    [noResultsMessage makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@200);
        make.height.equalTo(@80);
        make.width.equalTo(self.view).with.insets(UIEdgeInsetsMake(20, 20, 20, 20));
        make.centerX.equalTo(self.view);
    }];
    
    [noResultsMessage setNumberOfLines:0];
    [noResultsMessage setText:message];
}

- (void)refreshFavorites:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    
    [[DeckManager sharedManager] localDecksWithBlock:@"favorite" callback:^(id responseObject) {
        
        callback(responseObject);
        
    } failureCallback:^(id responseObject) {
        
        failureCallback(nil);
        
    }];
    
}

- (void)refreshFans:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    
    [[ProfileManager sharedManager] localProfilesWithBlock:@"fans" callback:^(id responseObject) {
        
        callback(responseObject);
        
    } failureCallback:^(id responseObject) {
        
        failureCallback(nil);
        
    }];
    
}

- (void)refreshProfiles:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    
    [[DeckManager sharedManager] localCardsWithBlock:@"profiles" callback:^(id responseObject) {
        
        callback(responseObject);
        
    } failureCallback:^(id responseObject) {
        
        failureCallback(nil);
        
    }];
    
}

- (void)setupTopView
{
    ProfileHeader *profile = ProfileHeader.new;
    [profile buildView];
    [profile setSelectedArea:@"profiles"];
    [self.view addSubview:profile];
    
    [profile makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@180);
    }];
    
    
}


- (void)setupCollectionView
{
    
    
    InadekCVFlowLayout *flowLayout = [[InadekCVFlowLayout alloc] init];
    [flowLayout setHasSearchHeader:NO];
    [flowLayout setItemSize:cvCellSize];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setSectionInset:cvEdgeInsets];
    [flowLayout setMinimumLineSpacing:15.0];
    
    /* REGISTER CELLS */
    decksCV = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    [decksCV registerClass:[DeckCVCell class] forCellWithReuseIdentifier:@"deckCell"];
    
    
    [decksCV setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    [decksCV setDataSource:self];
    [decksCV setDelegate:self];
    decksCV.alwaysBounceVertical = YES;
    
    [self.view addSubview:decksCV];
    [decksCV mas_makeConstraints:^(MASConstraintMaker *make){
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(180, 0, 0, 0));
    }];
    
    UIView *topShadow = UIView.new;
    [self.view addSubview:topShadow];
    [topShadow makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@2);
    }];
    [topShadow setStyleClass:@"box-shadow"];
    
//    refreshControl = [[UIRefreshControl alloc] init ];
//    refreshControl.tintColor = [UIColor colorWithHexString:@"F15400"];
//    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
//    [decksCV addSubview:refreshControl];
//    
//    [self showLoading];
    
}


- (void)showLoading
{
    self.loadingView = [[InadekLoader alloc] initWithFrame:CGRectZero];
    [self.loadingView loadSpinner:@"grey" andSize:@"large" andTitle:nil];
    [self.loadingView setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    
    [self.view addSubview:self.loadingView];
    [self.loadingView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(180, 0, 0, 0));
    }];
}

- (void)hideLoading
{
    if (self.loadingView && [self.loadingView isShown]) {
        // Hide it
        [self.loadingView hideSpinner];
    }
}



- (void)setDefaultSizes
{
    if (IS_IPHONE_6P) {
        cvCellSize = CGSizeMake(180, 220);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake(320, 200);
    }
    
    else if (IS_IPHONE_6) {
        cvCellSize = CGSizeMake(160, 200);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake(320, 200);
    }
    
    else {
        cvCellSize = CGSizeMake(139, 180);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake(320, 200);
    }
}

#pragma mark -- COLLECTION VIEW DELEGATE METHODS --

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.collectionGroups count];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if ([self.collectionGroups objectAtIndex:section]) {
        if ([[self.collectionGroups objectAtIndex:section] objectForKey:@"decks"]) {
            DLog(@"Decks...");
            return [[[self.collectionGroups objectAtIndex:section] objectForKey:@"decks"] count];
        } else if ([[self.collectionGroups objectAtIndex:section] objectForKey:@"cards"]) {
            DLog(@"Cards...");
            return [[[self.collectionGroups objectAtIndex:section] objectForKey:@"cards"] count];
        } else {
            return 0;
        }
    } else {
        return 0;
    }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    // Setup cell identifier
    static NSString *cellIdentifier = @"deckCell";
    
    /* Uncomment this block to use subclass-based cells */
    DeckCVCell *cell = (DeckCVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    if ([[self.collectionGroups objectAtIndex:indexPath.section] objectForKey:@"decks"]) {
        
        NSMutableArray *data = [[self.collectionGroups objectAtIndex:indexPath.section] objectForKey:@"decks"];
        Deck *deck = [data objectAtIndex:indexPath.row];
        
        [cell buildWithDeck:deck];
        
        [cell.layer setCornerRadius:4];
    
    } else if ([[self.collectionGroups objectAtIndex:indexPath.section] objectForKey:@"cards"]) {
        
        NSMutableArray *data = [[self.collectionGroups objectAtIndex:indexPath.section] objectForKey:@"cards"];
        Card *card = [data objectAtIndex:indexPath.row];
        
        [cell buildWithCard:card];
        
        [cell.layer setCornerRadius:4];
    
    }
    
    
    
    // Return the cell
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    id currentCell = [collectionView cellForItemAtIndexPath:indexPath];
    if ([currentCell isKindOfClass:[DeckCVCell class]]) {
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        if ([(DeckCVCell *)currentCell isDeck]) {
            Deck *thisDeck = [(DeckCVCell *)currentCell cellDeck];
            
            NSDictionary *deckParams = NSDictionary.new;
            if (thisDeck) {
                deckParams = @{@"deck":thisDeck};
            }
            
            [appDelegate.transitionVC transitionTo:@"deck" withParams:deckParams];
        } else {
            Card *thisCard = [(DeckCVCell *)currentCell cellCard];
            
            NSDictionary *cardParams = NSDictionary.new;
            if (thisCard) {
                cardParams = @{@"card":thisCard};
            }
            
            [appDelegate.transitionVC transitionTo:@"card" withParams:cardParams];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return cvCellSize;
}


- (void)reloadProfileSection:(id)sender
{
    if ([(UIProfileButton *)sender userInfo]) {
        NSString *selectedArea = [[(UIProfileButton *)sender userInfo] objectForKey:@"area"];
        area = selectedArea;
        [self refreshData:area];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
