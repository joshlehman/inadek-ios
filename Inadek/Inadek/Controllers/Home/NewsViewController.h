//
//  NewsViewController.h
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "BCTransitionViewController.h"
#import "InadekLoader.h"
#import "DeckManager.h"

@interface NewsViewController : BCViewController <UITableViewDataSource, UITableViewDelegate> {
    UITableView *messagesTV;
    
    NSMutableArray *notices;
}

@property (nonatomic, retain) InadekLoader *loadingView;


@end
