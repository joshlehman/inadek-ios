//
//  NewsViewController.m
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "NewsViewController.h"

#import "MessageTVCell.h"

@interface NewsViewController () {
    UILabel *noResultsMessage;
    UIRefreshControl *refreshControl;
}

@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    messagesTV = [[UITableView alloc] init];
    messagesTV.dataSource = self;
    messagesTV.delegate = self;
    messagesTV.estimatedRowHeight = 44.0;
    messagesTV.rowHeight = UITableViewAutomaticDimension;
    [messagesTV setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    [messagesTV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:messagesTV];
    [messagesTV mas_makeConstraints:^(MASConstraintMaker *make){
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    refreshControl = [[UIRefreshControl alloc] init ];
    refreshControl.tintColor = [UIColor colorWithHexString:@"F15400"];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [messagesTV addSubview:refreshControl];
    
    UIView *shadow = UIView.new;
    [shadow setStyleClass:@"box-shadow"];
    [self.view addSubview:shadow];
    [shadow makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@1);
        make.left.right.equalTo(self.view);
        make.top.equalTo(messagesTV.top);
    }];
    
    
    noResultsMessage = UILabel.new;
    [noResultsMessage setStyleClass:@"lbl-message"];
    [self.view addSubview:noResultsMessage];
    [noResultsMessage makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@50);
        make.height.equalTo(@80);
        make.width.equalTo(self.view).with.insets(UIEdgeInsetsMake(20, 20, 20, 20));
        make.centerX.equalTo(self.view);
    }];
    
    [noResultsMessage setNumberOfLines:0];
    [noResultsMessage setText:@"You do not currently have any messages."];
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    // Do any additional setup after loading the view.
    
    [self refreshData];
    
    [self markNotificationsAsRead];
}

- (void)refreshData
{

    [self showLoading];
    
    [[InadekAPIManager sharedManager] refreshNotifications:^(id responseObject) {
        
        notices = [[AccountNotification allNotifications] mutableCopy];
        [self hideLoading];
        [refreshControl endRefreshing];
        [messagesTV reloadData];
        
        [(MenuViewController *)self.baseViewController.menuVC setBadge:[AccountNotification unreadNotificationCount]];
        
    } failureCallback:^(id responseObject) {
        
        notices = NSMutableArray.new;
        [self hideLoading];
        [refreshControl endRefreshing];
        [messagesTV reloadData];
        // PRESENT AN ERROR MESSAGE OF SOME SORT
    }];
    
    
}

- (void)showLoading
{
    [noResultsMessage removeFromSuperview];
    
    self.loadingView = [[InadekLoader alloc] initWithFrame:CGRectZero];
    [self.loadingView loadSpinner:@"grey" andSize:@"large" andTitle:nil];
    [self.loadingView setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    
    [self.view addSubview:self.loadingView];
    [self.loadingView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)hideLoading
{
    if (self.loadingView && [self.loadingView isShown]) {
        // Hide it
        [self.loadingView hideSpinner];
    }
}

- (void)markNotificationsAsRead
{
    [[InadekAPIManager sharedManager] markNotificationsRead:^(id responseObject) {
        [(MenuViewController *)self.baseViewController.menuVC setBadge:[AccountNotification unreadNotificationCount]];
    } failureCallback:^(id responseObject) {
        //
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notices.count;  // Only one "row" per section - with the NSArray of article titles
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *MyIdentifier = @"MessageIdentifier";
    
    MessageTVCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[MessageTVCell alloc] initWithFrame:CGRectZero];
    }
    
    if ([notices count] > indexPath.row) {
        [cell buildCell:[notices objectAtIndex:indexPath.row]];
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageTVCell *cell = (MessageTVCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    Deck *deck = [cell getNotificationDeckDestination];
    Card *card = [cell getNotificationCardDestination];
    
    if (deck && card) {
        // Going to a specific deck and card
        NSDictionary *deckParams = NSDictionary.new;
        if (deck) {
            deckParams = @{@"deck":deck};
        }
        
        [self.baseViewController transitionTo:@"deck" withParams:deckParams];
    } else if (deck) {
        // Going to the start of a specific deck
        NSDictionary *deckParams = NSDictionary.new;
        if (deck) {
            deckParams = @{@"deck":deck};
        }
        
        [self.baseViewController transitionTo:@"deck" withParams:deckParams];
    } else if (card) {
        // Going to a single card
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    } else {
        // Going nowhere in life
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
        return;
    }
    
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
