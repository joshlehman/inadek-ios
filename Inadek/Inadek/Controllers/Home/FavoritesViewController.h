//
//  FavoritesViewController.h
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "BCTransitionViewController.h"
#import "InadekLoader.h"
#import "DeckManager.h"

@interface FavoritesViewController : BCViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate>
{
    UICollectionView *decksCV;
    NSString *area;
}

@property (nonatomic, retain) NSArray *collectionGroups;
@property (nonatomic, retain) InadekLoader *loadingView;

- (void)reloadProfileSection:(id)sender;
- (void)doNewProfileCard:(id)sender;
- (void)doNewGroup:(id)sender;

@end
