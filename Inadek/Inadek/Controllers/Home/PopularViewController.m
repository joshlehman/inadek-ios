//
//  PopularViewController.m
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "PopularViewController.h"

#import "AppDelegate.h"
//#import "InadekCVFlowLayout.h"
//#import "DeckCVCell.h"
//#import "SectionHeaderRVCell.h"
//#import "SectionFooterRVCell.h"
//#import "SearchHeaderRVCell.h"
//#import "SectionBackgroundRVCell.h"
#import "DeckRowTVCell.h"
#import "CategoryListTVCell.h"

#import "Common.h"

#define kMaxDecksInPopular  6

@interface PopularViewController () {
    CGSize cvCellSize;
    UIEdgeInsets cvEdgeInsets;
    CGSize cvSectionHeader;
    CGSize cvSectionFooter;
    CGSize svSearchHeader;
    NSMutableArray *popularCategories;
    
    UIRefreshControl *refreshControl;
    
    BOOL isSearchActive;
}

@property(nonatomic, retain) UICollectionView *decksCV;

@end

@implementation PopularViewController

- (void)showPopup:(id)sender
{
    UIView *myPopup = UIView.new;
    [myPopup setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *hideMe = UIButton.new;
    [hideMe setTitle:@"Hide Popup" forState:UIControlStateNormal];
    [hideMe setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [hideMe addTarget:self.baseViewController action:@selector(hideCurrentPopupView:) forControlEvents:UIControlEventTouchUpInside];
    [myPopup addSubview:hideMe];
    
    [hideMe makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(myPopup.centerX);
        make.centerY.equalTo(myPopup.centerY);
    }];
    
    [self.baseViewController presentPopupView:myPopup];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.transitionVC setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isSearchActive = NO;
    
    [self addNotificationObserver:@[@kRefreshDecks]];
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"F15400"]];
    //[self setDefaultSizes];
    [self setupTableView];
    [self setupSearchView];
    
    [self refreshData:NO];
    
    /*
    UIButton *show = UIButton.new;
    [show setTitle:@"Show Popup" forState:UIControlStateNormal];
    [show addTarget:self action:@selector(showPopup:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:show];
    
    [show makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.centerX);
        make.centerY.equalTo(self.view.centerY);
    }];
    */
    
    // Do any additional setup after loading the view.
}

/* Called when decks need to be updated locally */
- (void)didUpdateDecks:(NSNotification *)notification
{
    [Common log:@"Update decks"];
    [self refreshData:YES];
}

- (void)presentCategoriesView
{
    
}

- (void)refreshDataNow
{
    [self refreshData:YES];
}

- (void)refreshData:(BOOL)immediate
{
    
    isSearchActive = NO;
    
    [self showLoading];
    
    if (immediate) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"last_updated"]) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:nil forKey:@"last_updated"];
            [defaults synchronize];
        }
    }
    
    __block NSArray *popularDecks = nil;
    [[DeckManager sharedManager] localDecksWithBlock:@"popular" callback:^(id responseObject) {
        
        self.collectionGroups = responseObject;
        [popularTV reloadData];
        [self hideLoading];
        [refreshControl endRefreshing];
        
    } failureCallback:^(id responseObject) {
        popularDecks = NSArray.new;
        [popularTV reloadData];
        [self hideLoading];
        [refreshControl endRefreshing];
        // PRESENT AN ERROR MESSAGE OF SOME SORT
    }];
    
    [[InadekAPIManager sharedManager] refreshNotifications:^(id responseObject) {
        
        [(MenuViewController *)self.baseViewController.menuVC setBadge:[AccountNotification unreadNotificationCount]];
        
    } failureCallback:^(id responseObject) {
        
    }];
    
    
    
    
//    // TEST: Create data for collection views
//    NSMutableArray *firstSection = [[NSMutableArray alloc] init];
//    [firstSection addObject:@{@"my":@"thing"}];
//    [firstSection addObject:@{@"my":@"other thing"}];
//    [firstSection addObject:@{@"my":@"other thing"}];
//    [firstSection addObject:@{@"my":@"other thing"}];
//    [firstSection addObject:@{@"my":@"other thing"}];
//    [firstSection addObject:@{@"my":@"other thing"}];
//    
//    self.collectionGroups = [[NSArray alloc] initWithObjects:firstSection, nil];
}

- (void)refreshDataBySearch:(NSString *)search_term
{
    [self showLoading];
    
    isSearchActive = YES;
    
    __block NSArray *popularDecks = nil;
    [[DeckManager sharedManager] searchForDecks:search_term callback:^(id responseObject) {
        
        self.collectionGroups = responseObject;
        [popularTV reloadData];
        [popularTV setContentOffset:CGPointZero animated:NO];
        [self hideLoading];
        [refreshControl endRefreshing];
        
    } failureCallback:^(id responseObject) {
        popularDecks = NSArray.new;
        [popularTV reloadData];
        [self hideLoading];
        [refreshControl endRefreshing];
        // PRESENT AN ERROR MESSAGE OF SOME SORT
    }];
}

- (void)refreshDataByCategory:(NSNumber *)category_id
{
    [self showLoading];
    
    isSearchActive = YES;
    
    __block NSArray *popularDecks = nil;
    [[DeckManager sharedManager] searchForCategoryDecks:category_id callback:^(id responseObject) {
        
        self.collectionGroups = responseObject;
        [popularTV reloadData];
        [popularTV setContentOffset:CGPointZero animated:NO];
        [self hideLoading];
        [refreshControl endRefreshing];
        
    } failureCallback:^(id responseObject) {
        popularDecks = NSArray.new;
        [popularTV reloadData];
        [self hideLoading];
        [refreshControl endRefreshing];
        // PRESENT AN ERROR MESSAGE OF SOME SORT
    }];
}

- (void)setupSearchView
{
    _searchView = [[InadekSearchView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_searchView];
    [_searchView initSearchBarDelegate:self];
    
    [_searchView mas_makeConstraints:^(MASConstraintMaker *make){
        make.height.equalTo(@40);
        make.left.equalTo(self.view.left);
        make.right.equalTo(self.view.right);
        make.top.equalTo(self.view.top);
    }];
}


- (void)setupTableView
{
    popularTV = [[UITableView alloc] init];
    popularTV.dataSource = self;
    popularTV.delegate = self;
    [popularTV setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    [popularTV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:popularTV];
    [popularTV mas_makeConstraints:^(MASConstraintMaker *make){
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(40, 0, 0, 0));
    }];
    
    [popularTV registerClass:[DeckRowTVCell class] forCellReuseIdentifier:@"deckRowCell"];
    [popularTV registerClass:[CategoryListTVCell class] forCellReuseIdentifier:@"categoryRowCell"];
    
    refreshControl = [[UIRefreshControl alloc] init ];
    refreshControl.tintColor = [UIColor colorWithHexString:@"F15400"];
    [refreshControl addTarget:self action:@selector(refreshDataNow) forControlEvents:UIControlEventValueChanged];
    [popularTV addSubview:refreshControl];
    
//    categoriesTV = [[UITableView alloc] init];
//    categoriesTV.dataSource = self;
//    categoriesTV.delegate = self;
//    [categoriesTV setBackgroundColor:[UIColor colorWithHexString:@"DDDDDD"]];
//    [categoriesTV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    
//    [self.view addSubview:categoriesTV];
//    [categoriesTV mas_makeConstraints:^(MASConstraintMaker *make){
//        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(40, 0, 0, 0));
//    }];
}

- (void)setupCollectionView
{
    
//    InadekCVFlowLayout *flowLayout = [[InadekCVFlowLayout alloc] init];
//    [flowLayout setHasSearchHeader:NO];
//    [flowLayout setItemSize:cvCellSize];
//    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
//    [flowLayout setSectionInset:cvEdgeInsets];
//    [flowLayout setMinimumLineSpacing:15.0];
//    
//    /* REGISTER CELLS */
//    
//    
//    
//    popularCV = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
//    [popularCV registerClass:[DeckCVCell class] forCellWithReuseIdentifier:@"deckCell"];
//    
//    [popularCV registerClass:[SectionHeaderRVCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionHeader"];
//    [popularCV registerClass:[SectionFooterRVCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"sectionFooter"];
//
//    [popularCV setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
//    [popularCV setDataSource:self];
//    [popularCV setDelegate:self];
//    popularCV.alwaysBounceVertical = YES;
//    
//    [self.view addSubview:popularCV];
//    [popularCV mas_makeConstraints:^(MASConstraintMaker *make){
//        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(40, 0, 0, 0));
//    }];
//    
//    refreshControl = [[UIRefreshControl alloc] init ];
//    refreshControl.tintColor = [UIColor colorWithHexString:@"F15400"];
//    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
//    [popularCV addSubview:refreshControl];
//    
//    [self showLoading];
    
}


- (void)showLoading
{
    self.loadingView = [[InadekLoader alloc] initWithFrame:CGRectZero];
    [self.loadingView loadSpinner:@"grey" andSize:@"large" andTitle:nil];
    [self.loadingView setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    
    [self.view addSubview:self.loadingView];
    [self.loadingView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)hideLoading
{
    if (self.loadingView && [self.loadingView isShown]) {
        // Hide it
        [self.loadingView hideSpinner];
    }
}



- (void)setDefaultSizes
{
    if (IS_IPHONE_6P) {
        cvCellSize = CGSizeMake(180, 220);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 50);
        cvSectionFooter = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 60);
    }
    
    else if (IS_IPHONE_6) {
        cvCellSize = CGSizeMake(166, 206);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 50);
        cvSectionFooter = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 60);
    }
    
    else {
        cvCellSize = CGSizeMake(139, 180);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 50);
        cvSectionFooter = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 60);
    }
}

#pragma mark -- COLLECTION VIEW DELEGATE METHODS --

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    return 10;
//}
//
//-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    return [self.collectionGroups count];
//}
//
//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    
//    
//    if ([self.collectionGroups objectAtIndex:section]) {
//        if ([[[self.collectionGroups objectAtIndex:section] objectForKey:@"decks"] count] > kMaxDecksInPopular) {
//            return kMaxDecksInPopular;
//        } else {
//            return [[[self.collectionGroups objectAtIndex:section] objectForKey:@"decks"] count];
//        }
//    } else {
//        return 0;
//    }
//    
//}
//
//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    
//    // Setup cell identifier
//    static NSString *cellIdentifier = @"deckCell";
//    
//    /* Uncomment this block to use subclass-based cells */
//    DeckCVCell *cell = (DeckCVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
//    cell.layer.shouldRasterize = YES;
//    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
//    
//    NSMutableArray *data = [[self.collectionGroups objectAtIndex:indexPath.section] objectForKey:@"decks"];
//    Deck *deck = [data objectAtIndex:indexPath.row];
//    
//    [cell buildWithDeck:deck];
//    
//    [cell.layer setCornerRadius:4];
//    
//    // Return the cell
//    return cell;
//    
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    id currentCell = [collectionView cellForItemAtIndexPath:indexPath];
//    if ([currentCell isKindOfClass:[DeckCVCell class]]) {
//        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//        
//        Deck *thisDeck = [(DeckCVCell *)currentCell cellDeck];
//        
//        NSDictionary *deckParams = NSDictionary.new;
//        if (thisDeck) {
//            deckParams = @{@"deck":thisDeck};
//        }
//        
//        [appDelegate.transitionVC transitionTo:@"deck" withParams:deckParams];
//    }
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return cvCellSize;
//}
//
//
//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
//    if (kind == UICollectionElementKindSectionHeader) {
//        
//        SectionHeaderRVCell *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionHeader" forIndexPath:indexPath];
//        
//        reusableview.delegate = self;
//        if ([self.collectionGroups count] > indexPath.section) {
//            NSString *title = [[self.collectionGroups objectAtIndex:indexPath.section] objectForKey:@"title"];
//            reusableview.sectionTitle.text = title;
//            
//            reusableview.sectionTotal.text = [NSString stringWithFormat:@"%@", [self cardCountInGroupString:[[self.collectionGroups objectAtIndex:indexPath.section] objectForKey:@"decks"]]];
//        }
//        
//        if (reusableview == nil) {
//            reusableview = [[SectionHeaderRVCell alloc] initWithFrame:CGRectMake(0, 0, cvSectionHeader.width, cvSectionHeader.height)];
//        }
//        
//        
//        return reusableview;
//    } else if (kind == UICollectionElementKindSectionFooter) {
//        
//        SectionFooterRVCell *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"sectionFooter" forIndexPath:indexPath];
//        
//        reusableview.delegate = self;
//        if ([self.collectionGroups count] > indexPath.section) {
//            NSString *title = [[self.collectionGroups objectAtIndex:indexPath.section] objectForKey:@"title"];
//            [reusableview.gotoArea setTitle:[NSString stringWithFormat:@"More %@ Decks", title] forState:UIControlStateNormal];
//        }
//        
//        if (reusableview == nil) {
//            reusableview = [[SectionFooterRVCell alloc] initWithFrame:CGRectMake(0, 0, cvSectionFooter.width, cvSectionFooter.height)];
//        }
//        
//        return reusableview;
//    }
//    
//    return nil;
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
//    
//    if (section < [self.collectionGroups count]) {
//        CGSize headerSize = cvSectionHeader;
//        return headerSize;
//    } else {
//        CGSize headerSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 0);
//        return headerSize;
//    }
//    
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
//    
//    if (section < [self.collectionGroups count]) {
//        CGSize headerSize = cvSectionFooter;
//        return headerSize;
//    } else {
//        CGSize headerSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 0);
//        return headerSize;
//    }
//    
//}

#pragma mark -- SearchBar Delegate Methods

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""]) {
        if (isSearchActive) {
            [self refreshData:NO];
        }
    }
}


- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    [self presentCategoriesView];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    // Do the search...
    NSLog(@"Seart search.......");
    [self refreshDataBySearch:searchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    NSLog(@"Search cancelled");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)cardCountInGroupString:(NSArray *)decksArray
{
    //return @"54 Cards";
    
    int total = 0;
    for (Deck *d in decksArray) {
        total = total + [d.card_count integerValue];
    }
    
    if (total == 0) {
        return [NSString stringWithFormat:@"No Cards"];
    } else if (total == 1) {
        return [NSString stringWithFormat:@"%@ Card", @(total)];
    } else {
        return [NSString stringWithFormat:@"%@ Cards", @(total)];
    }

}



#pragma mark -- TABLE VIEW DELEGATE METHODS --

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSDictionary *grouping = [self.collectionGroups objectAtIndex:section];
    
    if ([grouping objectForKey:@"type"] && [[grouping objectForKey:@"type"] isEqualToString:@"category_cell"]) {
        
        return nil;
        
    }
    
    NSString *title = [grouping objectForKey:@"title"];
    UIView *headerCell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 36)];
    [headerCell setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    
    UILabel *sectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 200, 36)];
    [sectionTitle setText:title];
    [sectionTitle setStyleClass:@"lbl-cv-section-header"];
    
    [headerCell addSubview:sectionTitle];
    
    UIView *overline = UIView.new;
    [overline setBackgroundColor:[UIColor colorWithHexString:@"D8D8D8"]];
    [overline setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 1)];
    [headerCell addSubview:overline];
    
    UILabel *sectionTotal = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 120, 11, 100, 36)];
    [sectionTotal setText:[NSString stringWithFormat:@"%@", [self cardCountInGroupString:[[self.collectionGroups objectAtIndex:section] objectForKey:@"decks"]]]];
    
    [sectionTotal setStyleClass:@"lbl-cv-section-header-count"];
    [sectionTotal setTextAlignment:NSTextAlignmentRight];
    
    [headerCell addSubview:sectionTotal];
    
    return headerCell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.collectionGroups count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;  // Only one "row" per section - with the NSArray of article titles
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary *cellData = [self.collectionGroups objectAtIndex:[indexPath section]];  // Note we're using section, not row here
    
    if ([cellData objectForKey:@"type"] && [[cellData objectForKey:@"type"] isEqualToString:@"category_cell"]) {
        
        CategoryListTVCell *cell = [[CategoryListTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"categoryRowCell"];
        
        cell.label.text = [cellData objectForKey:@"title"];
        
        if ([cellData objectForKey:@"direction"] && [[cellData objectForKey:@"direction"] isEqualToString:@"back"]) {
            [cell makeBackButton];
        }
        
        return cell;
    
    } else if ([cellData objectForKey:@"type"] && [[cellData objectForKey:@"type"] isEqualToString:@"back"]) {
            
            CategoryListTVCell *cell = [[CategoryListTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"categoryRowCell"];
            
            cell.label.text = [cellData objectForKey:@"title"];
            [cell makeBackButton];
        
            return cell;
        
    } else {
        
        DeckRowTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deckRowCell"];
        
        NSArray *decks = [cellData objectForKey:@"decks"];
        if ([cellData objectForKey:@"summary"]) {
            [cell setSummaryText:[cellData objectForKey:@"summary"]];
        }
        [cell setCollectionData:decks];
        return cell;
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *grouping = [self.collectionGroups objectAtIndex:[indexPath section]];
    
    if ([grouping objectForKey:@"type"] && [[grouping objectForKey:@"type"] isEqualToString:@"category_cell"]) {
        
        [self refreshDataByCategory:[grouping objectForKey:@"category_id"]];
    
    } else if ([grouping objectForKey:@"type"] && [[grouping objectForKey:@"type"] isEqualToString:@"back"]) {
        
        [self refreshData:NO];
        
    } else {
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    NSDictionary *grouping = [self.collectionGroups objectAtIndex:section];
    
    if ([grouping objectForKey:@"type"] && [[grouping objectForKey:@"type"] isEqualToString:@"category_cell"]) {
        
        return 0.0;
        
    } else if ([grouping objectForKey:@"type"] && [[grouping objectForKey:@"type"] isEqualToString:@"back"]) {
        
        return 0.0;
        
    }
    
    return 48.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *cellData = [self.collectionGroups objectAtIndex:[indexPath section]];  // Note we're using section, not row here
    NSString *tmpText = @"";
    
    if ([cellData objectForKey:@"type"] && [[cellData objectForKey:@"type"] isEqualToString:@"category_cell"]) {
        
        if ([cellData objectForKey:@"direction"] && [[cellData objectForKey:@"direction"] isEqualToString:@"back"]) {
            return 44;
        }
        
        return 60;
    } else if ([cellData objectForKey:@"type"] && [[cellData objectForKey:@"type"] isEqualToString:@"back"]) {
        
        return 44;
    }
    
    if ([cellData objectForKey:@"summary"]) {
       tmpText = [cellData objectForKey:@"summary"];
    }
    
    if (tmpText && ![tmpText isEqualToString:@""]) {
        
        UILabel *tmpLabel = UILabel.new;
        [tmpLabel setNumberOfLines:0];
        [tmpLabel setStyleClass:@"lbl-deck-summary"];
        [tmpLabel setText:tmpText];
        
        CGSize maxSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width - 40, CGFLOAT_MAX);
        CGSize requiredSize = [tmpLabel sizeThatFits:maxSize];
        
        tmpLabel = nil;
        
        return 196.0 + requiredSize.height + 40;
        
    } else {
        
        return 196.0 + 40;
        
    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
