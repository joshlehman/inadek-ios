//
//  PopularViewController.h
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "BCTransitionViewController.h"
#import "InadekLoader.h"
#import "DeckManager.h"

#import "InadekSearchView.h"

@interface PopularViewController : BCViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    UITableView *popularTV;
    UITableView *categoriesTV;
}
@property (nonatomic, retain) InadekSearchView *searchView;
@property (nonatomic, retain) NSArray *collectionGroups;
@property (nonatomic, retain) InadekLoader *loadingView;

- (void)didUpdateDecks:(NSNotification *)notification;


@end
