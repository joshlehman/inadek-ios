//
//  MyDecksViewController.m
//  Inadek
//
//  Created by Josh Lehman on 11/24/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "MyDecksViewController.h"

#import "AppDelegate.h"
//#import "InadekCVFlowLayout.h"
//#import "DeckCVCell.h"
//#import "SectionHeaderRVCell.h"
//#import "SectionFooterRVCell.h"
//#import "SearchHeaderRVCell.h"
//#import "SectionBackgroundRVCell.h"
#import "DeckRowTVCell.h"

#import "Common.h"

@interface MyDecksViewController () {
    CGSize cvCellSize;
    UIEdgeInsets cvEdgeInsets;
    CGSize cvSectionHeader;
    CGSize cvSectionFooter;
    CGSize svSearchHeader;
    NSMutableArray *popularCategories;
    
    UIRefreshControl *refreshControl;
}

@property(nonatomic, retain) UICollectionView *decksCV;

@end

@implementation MyDecksViewController

- (void)showPopup:(id)sender
{
    UIView *myPopup = UIView.new;
    [myPopup setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *hideMe = UIButton.new;
    [hideMe setTitle:@"Hide Popup" forState:UIControlStateNormal];
    [hideMe setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [hideMe addTarget:self.baseViewController action:@selector(hideCurrentPopupView:) forControlEvents:UIControlEventTouchUpInside];
    [myPopup addSubview:hideMe];
    
    [hideMe makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(myPopup.centerX);
        make.centerY.equalTo(myPopup.centerY);
    }];
    
    [self.baseViewController presentPopupView:myPopup];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addNotificationObserver:@[@kRefreshDecks]];
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"F15400"]];
    //[self setDefaultSizes];
    [self setupTableView];
    
    [self refreshData];
    
    /*
     UIButton *show = UIButton.new;
     [show setTitle:@"Show Popup" forState:UIControlStateNormal];
     [show addTarget:self action:@selector(showPopup:) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:show];
     
     [show makeConstraints:^(MASConstraintMaker *make) {
     make.centerX.equalTo(self.view.centerX);
     make.centerY.equalTo(self.view.centerY);
     }];
     */
    
    // Do any additional setup after loading the view.
}

/* Called when decks need to be updated locally */
- (void)didUpdateDecks:(NSNotification *)notification
{
    [Common log:@"Update decks"];
    [self refreshData];
}

- (void)refreshData
{
    [self showLoading];
    
    __block NSArray *popularDecks = nil;
    [[DeckManager sharedManager] localDecksWithBlock:@"mine" callback:^(id responseObject) {
        
        self.collectionGroups = responseObject;
        [popularTV reloadData];
        [self hideLoading];
        [refreshControl endRefreshing];
        
    } failureCallback:^(id responseObject) {
        popularDecks = NSArray.new;
        [popularTV reloadData];
        [self hideLoading];
        [refreshControl endRefreshing];
        // PRESENT AN ERROR MESSAGE OF SOME SORT
    }];
    
    
    
    
    //    // TEST: Create data for collection views
    //    NSMutableArray *firstSection = [[NSMutableArray alloc] init];
    //    [firstSection addObject:@{@"my":@"thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //    [firstSection addObject:@{@"my":@"other thing"}];
    //
    //    self.collectionGroups = [[NSArray alloc] initWithObjects:firstSection, nil];
}


- (void)setupTableView
{
    popularTV = [[UITableView alloc] init];
    popularTV.dataSource = self;
    popularTV.delegate = self;
    [popularTV setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    [popularTV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:popularTV];
    [popularTV mas_makeConstraints:^(MASConstraintMaker *make){
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [popularTV registerClass:[DeckRowTVCell class] forCellReuseIdentifier:@"deckRowCell"];
    
    refreshControl = [[UIRefreshControl alloc] init ];
    refreshControl.tintColor = [UIColor colorWithHexString:@"F15400"];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [popularTV addSubview:refreshControl];
}

- (void)showLoading
{
    self.loadingView = [[InadekLoader alloc] initWithFrame:CGRectZero];
    [self.loadingView loadSpinner:@"grey" andSize:@"large" andTitle:nil];
    [self.loadingView setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    
    [self.view addSubview:self.loadingView];
    [self.loadingView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)hideLoading
{
    if (self.loadingView && [self.loadingView isShown]) {
        // Hide it
        [self.loadingView hideSpinner];
    }
}



- (void)setDefaultSizes
{
    if (IS_IPHONE_6P) {
        cvCellSize = CGSizeMake(180, 220);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 50);
        cvSectionFooter = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 60);
    }
    
    else if (IS_IPHONE_6) {
        cvCellSize = CGSizeMake(166, 206);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 50);
        cvSectionFooter = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 60);
    }
    
    else {
        cvCellSize = CGSizeMake(139, 180);
        cvEdgeInsets = UIEdgeInsetsMake(15, 15, 10, 15);
        cvSectionHeader = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 50);
        cvSectionFooter = CGSizeMake([[UIScreen mainScreen] bounds].size.width, 60);
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)cardCountInGroupString:(NSArray *)decksArray
{
    //return @"54 Cards";
    
    int total = 0;
    for (Deck *d in decksArray) {
        total = total + [d.card_count integerValue];
    }
    
    if (total == 0) {
        return [NSString stringWithFormat:@"No Cards"];
    } else if (total == 1) {
        return [NSString stringWithFormat:@"%@ Card", @(total)];
    } else {
        return [NSString stringWithFormat:@"%@ Cards", @(total)];
    }
    
}



#pragma mark -- TABLE VIEW DELEGATE METHODS --

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSDictionary *grouping = [self.collectionGroups objectAtIndex:section];
    
    NSString *title = [grouping objectForKey:@"title"];
    UIView *headerCell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 36)];
    [headerCell setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
    
    UILabel *sectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 200, 36)];
    [sectionTitle setText:title];
    [sectionTitle setStyleClass:@"lbl-cv-section-header"];
    
    [headerCell addSubview:sectionTitle];
    
    UIView *overline = UIView.new;
    [overline setBackgroundColor:[UIColor colorWithHexString:@"D8D8D8"]];
    [overline setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 1)];
    [headerCell addSubview:overline];
    
    UILabel *sectionTotal = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 120, 11, 100, 36)];
    [sectionTotal setText:[NSString stringWithFormat:@"%@", [self cardCountInGroupString:[[self.collectionGroups objectAtIndex:section] objectForKey:@"decks"]]]];
    
    [sectionTotal setStyleClass:@"lbl-cv-section-header-count"];
    [sectionTotal setTextAlignment:NSTextAlignmentRight];
    
    [headerCell addSubview:sectionTotal];
    
    return headerCell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.collectionGroups count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;  // Only one "row" per section - with the NSArray of article titles
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DeckRowTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deckRowCell"];
    NSDictionary *cellData = [self.collectionGroups objectAtIndex:[indexPath section]];  // Note we're using section, not row here
    NSArray *decks = [cellData objectForKey:@"decks"];
    if ([cellData objectForKey:@"summary"]) {
        [cell setSummaryText:[cellData objectForKey:@"summary"]];
    }
    [cell setCollectionData:decks];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 48.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *cellData = [self.collectionGroups objectAtIndex:[indexPath section]];  // Note we're using section, not row here
    NSString *tmpText = @"";
    
    if ([cellData objectForKey:@"summary"]) {
        tmpText = [cellData objectForKey:@"summary"];
    }
    
    if (tmpText && ![tmpText isEqualToString:@""]) {
        
        UILabel *tmpLabel = UILabel.new;
        [tmpLabel setNumberOfLines:0];
        [tmpLabel setStyleClass:@"lbl-deck-summary"];
        [tmpLabel setText:tmpText];
        
        CGSize maxSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width - 40, CGFLOAT_MAX);
        CGSize requiredSize = [tmpLabel sizeThatFits:maxSize];
        
        tmpLabel = nil;
        
        return 196.0 + requiredSize.height + 40;
        
    } else {
        
        return 196.0;
        
    }
    
    
}

@end
