//
//  LoginViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import "BCButton.h"
#import "BCFieldSetView.h"
#import "BCTransitionViewController.h"
#import "InadekLoader.h"
#import "InadekLocationManager.h"

@interface LoginViewController : BCViewController <UITextFieldDelegate> {
    BCButton *emailLogin;
    BCButton *facebookLogin;
    BCButton *existingLogin;
    
    BOOL is_facebook_login;
}

@property (nonatomic, retain) BCFieldSetView *loginFields;
@property (nonatomic, retain) BCFieldSetView *accountFields;
@property (nonatomic, retain) BCFieldSetView *accountFieldsFacebook;




@end
