//
//  IntroViewController.h
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCViewController.h"
#import <pop/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>

@interface IntroViewController : BCViewController

@end
