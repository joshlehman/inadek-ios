//
//  LoginViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "LoginViewController.h"
#import <pop/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController () {
    NSMutableArray *popupAnimations;
    UIView *heroTextHolder;
    UIView *loginOptionsHolder;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    heroTextHolder = UIView.new;
    popupAnimations = NSMutableArray.new;
    
    is_facebook_login = NO;
    
    UIImageView *heroImage = UIImageView.new;
    [heroImage setImage:[self applyBlurOnImage:[UIImage imageNamed:@"city_splash_bg"] withRadius:19.0f]];
    [heroImage setContentMode:UIViewContentModeScaleAspectFill];
    
    
    [self.view addSubview:heroImage];
    
    [heroImage makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(-20, -20, -20, -20));
    }];
    
    UIView *brighter = UIView.new;
    [brighter setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.4]];
    [self.view addSubview:brighter];
    [brighter makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    UILabel *heroIntro = UILabel.new;
    [heroIntro setText:@"Inadek lets you share and organize all the important people, places and things in your life. \r\rSign up for a FREE account."];
    [heroIntro setStyleClass:@"lbl-hero"];
    [heroIntro setNumberOfLines:0];
    [heroIntro setTextAlignment:NSTextAlignmentCenter];
    
    [self.view addSubview:heroTextHolder];
    [heroTextHolder addSubview:heroIntro];

    [heroIntro makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@240);
        make.height.equalTo(@110);
        make.centerX.equalTo(self.view);
        make.top.equalTo(@60);
    }];
    

    facebookLogin = BCButton.new;
    [facebookLogin buildType:@"blue"];
    [facebookLogin setTitle:@"SIGN IN VIA FACEBOOK" forState:UIControlStateNormal];
    [facebookLogin addTarget:self action:@selector(showFacebookLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    existingLogin = BCButton.new;
    [existingLogin buildType:@"clear"];
    [existingLogin setTitle:@"Already have an email login? SIGN IN" forState:UIControlStateNormal];
    [existingLogin setBackgroundColor:[UIColor clearColor]];
    [existingLogin addTarget:self action:@selector(showLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    emailLogin = BCButton.new;
    [emailLogin buildType:@"grey"];
    [emailLogin setTitle:@"REGISTER VIA EMAIL" forState:UIControlStateNormal];
    [emailLogin addTarget:self action:@selector(showNewAccount:) forControlEvents:UIControlEventTouchUpInside];
    
    __block MASConstraint *animated_login_options = nil;
    
    [self.view addSubview:facebookLogin];
    [facebookLogin makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@280);
        make.centerX.equalTo(self.view);
        animated_login_options = make.bottom.equalTo(self.view).with.offset(-144);
    }];
    
    [self.view addSubview:emailLogin];
    [emailLogin makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@280);
        make.centerX.equalTo(self.view);
        make.top.equalTo(facebookLogin.bottom).with.offset(15);
    }];
    
    [self.view addSubview:existingLogin];
    [existingLogin makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@280);
        make.centerX.equalTo(self.view);
        make.top.equalTo(emailLogin.bottom).with.offset(20);
    }];
    
    [popupAnimations addObject:@{@"anim":animated_login_options,@"key":@"login-options"}];
    
    [self addLoginFields];
    [self addNewAccountFields];
    [self addNewFacebookAccountFields];
    
}

- (void) addNewFacebookAccountFields
{
    NSArray *fieldsArray = [[NSArray alloc] initWithObjects:
                            @{@"style":[NSNumber numberWithInt:kFieldStyleTextLowercase],@"placeholder":@"Username",@"name":@"username"},
                            @{@"style":[NSNumber numberWithInt:kFieldStyleTextEmail],@"placeholder":@"Email Address",@"name":@"email_address"},
                            nil];
    
    self.accountFieldsFacebook = [[BCFieldSetView alloc] initWithTitle:@"NEW ACCOUNT" dataFields:fieldsArray andStyle:@"overlay"];
    
    __block MASConstraint *animated_newfacebookaccount = nil;
    
    [self.view addSubview:_accountFieldsFacebook];
    [_accountFieldsFacebook makeConstraints:^(MASConstraintMaker *make) {
        animated_newfacebookaccount = make.top.equalTo(@0).with.offset(-280);
        make.width.equalTo(self.view.width);
        make.height.equalTo([_accountFieldsFacebook fieldHeight]);
    }];
    
    BCButton *continueBtn = BCButton.new;
    [continueBtn buildType:@"orange"];
    [continueBtn setTitle:@"CREATE ACCOUNT" forState:UIControlStateNormal];
    [continueBtn addTarget:self action:@selector(completeNewAccount:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:continueBtn];
    [continueBtn makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@180);
        make.left.equalTo(_accountFieldsFacebook.left).with.offset(20);
        make.top.equalTo(_accountFieldsFacebook.bottom).with.offset(20);
    }];
    
    BCButton *cancelBtn = BCButton.new;
    [cancelBtn buildType:@"cancel"];
    [cancelBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelNewAccount:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:cancelBtn];
    [cancelBtn makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@80);
        make.left.equalTo(continueBtn.right).with.offset(20);
        make.top.equalTo(_accountFieldsFacebook.bottom).with.offset(20);
    }];
    
    
    [popupAnimations addObject:@{@"anim":animated_newfacebookaccount,@"key":@"new-facebook-account"}];
}

- (void) addNewAccountFields
{
    NSArray *fieldsArray = [[NSArray alloc] initWithObjects:
                            @{@"style":[NSNumber numberWithInt:kFieldStyleTextLowercase],@"placeholder":@"Username",@"name":@"username"},
                            @{@"style":[NSNumber numberWithInt:kFieldStyleTextEmail],@"placeholder":@"Email Address",@"name":@"email_address"},
                            @{@"style":[NSNumber numberWithInt:kFieldStylePassword],@"placeholder":@"Password",@"name":@"password"},
                            @{@"style":[NSNumber numberWithInt:kFieldStylePassword],@"placeholder":@"Re-enter Password",@"name":@"confirm_password",@"return_key":@"done"},
                            nil];
    
    self.accountFields = [[BCFieldSetView alloc] initWithTitle:@"NEW ACCOUNT" dataFields:fieldsArray andStyle:@"overlay"];
    
    __block MASConstraint *animated_newaccount = nil;
    
    [self.view addSubview:_accountFields];
    [_accountFields makeConstraints:^(MASConstraintMaker *make) {
        animated_newaccount = make.top.equalTo(@0).with.offset(-280);
        make.width.equalTo(self.view.width);
        make.height.equalTo([_accountFields fieldHeight]);
    }];
    
    BCButton *continueBtn = BCButton.new;
    [continueBtn buildType:@"orange"];
    [continueBtn setTitle:@"CREATE ACCOUNT" forState:UIControlStateNormal];
    [continueBtn addTarget:self action:@selector(completeNewAccount:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:continueBtn];
    [continueBtn makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@180);
        make.left.equalTo(_accountFields.left).with.offset(20);
        make.top.equalTo(_accountFields.bottom).with.offset(20);
    }];
    
    BCButton *cancelBtn = BCButton.new;
    [cancelBtn buildType:@"cancel"];
    [cancelBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelNewAccount:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:cancelBtn];
    [cancelBtn makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@80);
        make.left.equalTo(continueBtn.right).with.offset(20);
        make.top.equalTo(_accountFields.bottom).with.offset(20);
    }];
    
    
    [popupAnimations addObject:@{@"anim":animated_newaccount,@"key":@"new-account"}];
    
}

- (void) addLoginFields
{
    NSArray *fieldsArray = [[NSArray alloc] initWithObjects:
                            @{@"style":[NSNumber numberWithInt:kFieldStyleTextLowercase],@"placeholder":@"Username or Email",@"name":@"username",@"return_key":@"next"},
                            @{@"style":[NSNumber numberWithInt:kFieldStylePassword],@"placeholder":@"Password",@"name":@"password",@"return_key":@"done"},
                            nil];
    
    self.loginFields = [[BCFieldSetView alloc] initWithTitle:@"SIGN IN" dataFields:fieldsArray andStyle:@"overlay"];
    
    __block MASConstraint *animated_login = nil;
    
    [self.view addSubview:_loginFields];
    [_loginFields makeConstraints:^(MASConstraintMaker *make) {
        animated_login = make.top.equalTo(@0).with.offset(-200);
        make.width.equalTo(self.view.width);
        make.height.equalTo([_loginFields fieldHeight]);
    }];
    
    BCButton *continueBtn = BCButton.new;
    [continueBtn buildType:@"orange"];
    [continueBtn setTitle:@"COMPLETE SIGN IN" forState:UIControlStateNormal];
    [continueBtn addTarget:self action:@selector(completeLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:continueBtn];
    [continueBtn makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@180);
        make.left.equalTo(_loginFields.left).with.offset(20);
        make.top.equalTo(_loginFields.bottom).with.offset(30);
    }];
    
    BCButton *cancelBtn = BCButton.new;
    [cancelBtn buildType:@"cancel"];
    [cancelBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:cancelBtn];
    [cancelBtn makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@80);
        make.left.equalTo(continueBtn.right).with.offset(20);
        make.top.equalTo(_loginFields.bottom).with.offset(30);
    }];
    
    
    [popupAnimations addObject:@{@"anim":animated_login,@"key":@"login"}];
    
}

- (void)completeLogin:(id)sender
{
    [self.view endEditing:YES];
    
    InadekLoader *spinner = [[InadekLoader alloc] initWithFrame:CGRectZero];
    
    [spinner loadSpinner:@"white" andSize:@"large" andTitle:@"Welcome Back to Inadek"];
    
    [self.view addSubview:spinner];
    
    __block MASConstraint *animated_cst = nil;
    [spinner makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@200);
        make.height.equalTo(@130);
        animated_cst = make.top.equalTo(self.view.bottom).with.offset(0);
    }];
    
    
    __block POPSpringAnimation *spinnerAnimation = [POPSpringAnimation new];
    spinnerAnimation.toValue = @(-350);
    spinnerAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    spinnerAnimation.springBounciness = 4;
    spinnerAnimation.springSpeed = 6;
    
    [animated_cst pop_addAnimation:spinnerAnimation forKey:@"offset"];
    
    
    
    __block POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(-280);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.springBounciness = 4;
    popupAnimation.springSpeed = 10;
    
    __block MASConstraint *thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"login"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
    
    
    NSDictionary *accountDetails = @{@"login":[_loginFields valueForFieldNamed:@"username"],
                                     @"password":[_loginFields valueForFieldNamed:@"password"]};
    
    [[InadekAPIManager sharedManager] loginAccount:accountDetails callback:^(id responseObject) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if ([defaults objectForKey:@"push_device_token"]) {
            [[InadekAPIManager sharedManager] registerDevice:[defaults objectForKey:@"push_device_token"]];
        }
        
        [[InadekLocationManager sharedManager] getCurrentLocation];
        [self.baseViewController transitionAfterLogin];
        
    } failureCallback:^(id responseObject) {
        UIAlertView *popup = [[UIAlertView alloc] initWithTitle:@"Login Error" message:[responseObject objectForKey:@"error"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [popup show];
        
        popupAnimation.toValue = @(90);
        [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
        
        spinnerAnimation.toValue = @(0);
        [animated_cst pop_addAnimation:spinnerAnimation forKey:@"offset"];
    }];
    
    //[self.baseViewController transitionAfterLogin];
}

- (void)completeNewAccount:(id)sender
{
    [self.view endEditing:YES];
    
    InadekLoader *spinner = [[InadekLoader alloc] initWithFrame:CGRectZero];
    
    [spinner loadSpinner:@"white" andSize:@"large" andTitle:@"Creating Your Account"];
    
    [self.view addSubview:spinner];
    
    __block MASConstraint *animated_cst = nil;
    [spinner makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@200);
        make.height.equalTo(@130);
        animated_cst = make.top.equalTo(self.view.bottom).with.offset(0);
    }];
    
    
    __block POPSpringAnimation *spinnerAnimation = [POPSpringAnimation new];
    spinnerAnimation.toValue = @(-350);
    spinnerAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    spinnerAnimation.springBounciness = 4;
    spinnerAnimation.springSpeed = 6;

    [animated_cst pop_addAnimation:spinnerAnimation forKey:@"offset"];
    
    
    
    __block POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(-280);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.springBounciness = 4;
    popupAnimation.springSpeed = 10;
    
    __block MASConstraint *thisCst = nil;
    
    if (is_facebook_login) {
        for (NSDictionary *anim in popupAnimations) {
            if ([[anim objectForKey:@"key"] isEqualToString:@"new-facebook-account"]) {
                thisCst = [anim objectForKey:@"anim"];
            }
        }
    } else {
        for (NSDictionary *anim in popupAnimations) {
            if ([[anim objectForKey:@"key"] isEqualToString:@"new-account"]) {
                thisCst = [anim objectForKey:@"anim"];
            }
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
    
    NSDictionary *accountDetails = nil;
    
    if (is_facebook_login) {
        accountDetails = @{@"username":[_accountFieldsFacebook valueForFieldNamed:@"username"],
                           @"email":[_accountFieldsFacebook valueForFieldNamed:@"email_address"],
                           @"facebook_id":[FBSDKAccessToken currentAccessToken].userID,
                           @"password":[FBSDKAccessToken currentAccessToken].userID,
                           @"password_confirmation":[FBSDKAccessToken currentAccessToken].userID};
    } else {
        accountDetails = @{@"username":[_accountFields valueForFieldNamed:@"username"],
                           @"email":[_accountFields valueForFieldNamed:@"email_address"],
                           @"password":[_accountFields valueForFieldNamed:@"password"],
                           @"password_confirmation":[_accountFields valueForFieldNamed:@"confirm_password"]};
    }

    
    [[InadekAPIManager sharedManager] createAccount:accountDetails callback:^(id responseObject) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if ([defaults objectForKey:@"push_device_token"]) {
            [[InadekAPIManager sharedManager] registerDevice:[defaults objectForKey:@"push_device_token"]];
        }
        
        [[InadekLocationManager sharedManager] getCurrentLocation];
        [self.baseViewController transitionAfterLogin];
        
    } failureCallback:^(id responseObject) {
        UIAlertView *popup = [[UIAlertView alloc] initWithTitle:@"New Account Error" message:[responseObject objectForKey:@"error"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [popup show];
        
        popupAnimation.toValue = @(25);
        [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
        
        spinnerAnimation.toValue = @(0);
        [animated_cst pop_addAnimation:spinnerAnimation forKey:@"offset"];
    }];
    
    //[self.baseViewController transitionAfterLogin];
}

- (void)cancelLogin:(id)sender
{
    [self.view endEditing:YES];
    
    POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(-200);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.springBounciness = 4;
    popupAnimation.springSpeed = 10;
    
    MASConstraint *thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"login"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
    
    POPSpringAnimation *popupAnimation2 = [POPSpringAnimation new];
    popupAnimation2.toValue = @(-144);
    popupAnimation2.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation2.springBounciness = 4;
    popupAnimation2.springSpeed = 10;
    
    thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"login-options"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation2 forKey:@"offset"];
    
    [UIView animateWithDuration:0.4 animations:^{
        [heroTextHolder setAlpha:1.0];
        [loginOptionsHolder setAlpha:1.0];
    }];
}

- (void)cancelNewAccount:(id)sender
{
    [self.view endEditing:YES];
    
    POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(-280);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.springBounciness = 4;
    popupAnimation.springSpeed = 10;
    
    MASConstraint *thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"new-account"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
    
    POPSpringAnimation *popupAnimation2 = [POPSpringAnimation new];
    popupAnimation2.toValue = @(-144);
    popupAnimation2.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation2.springBounciness = 4;
    popupAnimation2.springSpeed = 10;
    
    thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"login-options"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation2 forKey:@"offset"];

    thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"new-facebook-account"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
    
    [UIView animateWithDuration:0.4 animations:^{
        [heroTextHolder setAlpha:1.0];
        [loginOptionsHolder setAlpha:1.0];
    }];
}

- (void)showFacebookLogin:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                
                // Look up account via userId, if exists sign in if not sign up...
                [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
                
                [[InadekAPIManager sharedManager] loginFacebookAccount:@{@"facebook_user_id":[FBSDKAccessToken currentAccessToken].userID} callback:^(id responseObject) {
                    
                    if ([responseObject objectForKey:@"account"]) {
                        [[InadekLocationManager sharedManager] getCurrentLocation];
                        [self.baseViewController transitionAfterLogin];
                        return;
                    }
                    
                    if ([FBSDKAccessToken currentAccessToken]) {
                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                      id result, NSError *error) {
                             if (!error) {
                                 NSLog(@"fetched user:%@", result);

                                 is_facebook_login = YES;
                                 
                                 NSString *usernameString = [[result objectForKey:@"name"] lowercaseString];
                                 usernameString = [usernameString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                 
                                 NSMutableDictionary *presetsDict = NSMutableDictionary.new;
                                 [presetsDict setObject:[result objectForKey:@"email"] forKey:@"email"];
                                 [presetsDict setObject:usernameString forKey:@"username"];
                                 
                                 [self showNewFacebookAccount:nil withPresets:presetsDict];
                                 
                             } else {

                             }
                         }];
                    }

                    
                } failureCallback:^(id responseObject) {
                    UIAlertView *popup = [[UIAlertView alloc] initWithTitle:@"New Account Error" message:[responseObject objectForKey:@"error"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [popup show];
                }];
            }
        }
    }];
}

- (void)showLogin:(id)sender
{
    
    POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(90);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.springBounciness = 4;
    popupAnimation.springSpeed = 10;
    
    MASConstraint *thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"login"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
    
    POPSpringAnimation *popupAnimation2 = [POPSpringAnimation new];
    popupAnimation2.toValue = @(100);
    popupAnimation2.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation2.springBounciness = 4;
    popupAnimation2.springSpeed = 10;
    
    thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"login-options"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation2 forKey:@"offset"];
    
    [UIView animateWithDuration:0.4 animations:^{
        [heroTextHolder setAlpha:0.0];
        [loginOptionsHolder setAlpha:0.0];
    }];
    
    [_loginFields makeIndexFirstResponder:0];
}

- (void)showNewFacebookAccount:(id)sender withPresets:(NSDictionary *)presets
{
    
    [self.accountFieldsFacebook updateValueForFieldNamed:@"username" withValue:[presets objectForKey:@"username"]];
    [self.accountFieldsFacebook updateValueForFieldNamed:@"email_address" withValue:[presets objectForKey:@"email"]];
    
    POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(25);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.springBounciness = 4;
    popupAnimation.springSpeed = 10;
    
    MASConstraint *thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"new-facebook-account"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
    
    POPSpringAnimation *popupAnimation2 = [POPSpringAnimation new];
    popupAnimation2.toValue = @(100);
    popupAnimation2.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation2.springBounciness = 4;
    popupAnimation2.springSpeed = 10;
    
    thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"login-options"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation2 forKey:@"offset"];
    
    [UIView animateWithDuration:0.4 animations:^{
        [heroTextHolder setAlpha:0.0];
        [loginOptionsHolder setAlpha:0.0];
    }];
    
    [_accountFieldsFacebook makeIndexFirstResponder:0];
}

- (void)showNewAccount:(id)sender
{
    POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(25);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.springBounciness = 4;
    popupAnimation.springSpeed = 10;
    
    MASConstraint *thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"new-account"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation forKey:@"offset"];
    
    POPSpringAnimation *popupAnimation2 = [POPSpringAnimation new];
    popupAnimation2.toValue = @(100);
    popupAnimation2.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation2.springBounciness = 4;
    popupAnimation2.springSpeed = 10;
    
    thisCst = nil;
    for (NSDictionary *anim in popupAnimations) {
        if ([[anim objectForKey:@"key"] isEqualToString:@"login-options"]) {
            thisCst = [anim objectForKey:@"anim"];
        }
    }
    
    [thisCst pop_addAnimation:popupAnimation2 forKey:@"offset"];
    
    [UIView animateWithDuration:0.4 animations:^{
        [heroTextHolder setAlpha:0.0];
        [loginOptionsHolder setAlpha:0.0];
    }];
    
    [_accountFields makeIndexFirstResponder:0];
}

- (UIImage *)applyBlurOnImage: (UIImage *)imageToBlur withRadius: (CGFloat)blurRadius
{
    
    CIImage *originalImage = [CIImage imageWithCGImage: imageToBlur.CGImage];
    CIFilter *filter = [CIFilter filterWithName: @"CIGaussianBlur" keysAndValues: kCIInputImageKey, originalImage, @"inputRadius", @(blurRadius), nil];
    CIImage *outputImage = filter.outputImage;
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef outImage = [context createCGImage: outputImage fromRect: [outputImage extent]]; return [UIImage imageWithCGImage: outImage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
