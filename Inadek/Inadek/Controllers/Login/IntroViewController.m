//
//  IntroViewController.m
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "IntroViewController.h"
#import "AppDelegate.h"

@interface IntroViewController () {
    MASConstraint *welcomeTop;
    MASConstraint *continueBtnMC;
}

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self makeBackgroundView:[UIColor colorWithHexString:@"F15400"] animate:@"fadein" withCloseAction:NO];
    
    UIButton *continueBtn = UIButton.new;
    [continueBtn setStyleClass:@"btn-white"];
    [continueBtn setTitle:@"Continue" forState:UIControlStateNormal];
    [self.view addSubview:continueBtn];
    [continueBtn makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@40);
        make.width.equalTo(@220);
        make.centerX.equalTo(self.view);
        continueBtnMC = make.bottom.equalTo(self.view).with.offset(40);
    }];
    
    [continueBtn addTarget:self action:@selector(skipIntro:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [self animateTo:@"page1"];
}

- (void)animateTo:(NSString *)page
{
    if ([page isEqualToString:@"page1"]) {
        UILabel *welcome = UILabel.new;
        [welcome setStyleClass:@"lbl-welcome"];
        [welcome setTextAlignment:NSTextAlignmentCenter];
        [welcome setText:@"Welcome, Randy"];
        [self.view addSubview:welcome];
        [welcome makeConstraints:^(MASConstraintMaker *make) {
            welcomeTop = make.top.equalTo(self.view).with.offset(100);
            make.width.equalTo(self.view);
            make.height.equalTo(@100);
            make.centerX.equalTo(self.view);
        }];
        
        
        POPDecayAnimation *driftIn = [POPDecayAnimation new];
        driftIn.property = [POPAnimatableProperty mas_offsetProperty];
        driftIn.deceleration = 0.998;
        driftIn.velocity = @(-120.0f);
        [welcomeTop pop_addAnimation:driftIn forKey:@"PopDeckDriftAnimation"];
        
        POPSpringAnimation *springIn = [POPSpringAnimation new];
        springIn.property = [POPAnimatableProperty mas_offsetProperty];
        springIn.toValue = @(-30);
        springIn.springBounciness = 2;
        springIn.springSpeed = 40;

        [continueBtnMC pop_addAnimation:springIn forKey:@"PopDeckSlideInAnimation"];
    }
}

- (void)skipIntro:(id)sender
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"no" forKey:@"show_intro"];
    [defaults synchronize];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.transitionVC transitionAfterLogin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
