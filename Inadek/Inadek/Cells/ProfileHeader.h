//
//  ProfileHeaderRVCell.h
//  Inadek
//
//  Created by Josh Lehman on 12/3/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//
#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>

@class UIProfileButton;

@interface ProfileHeader : UIView {
    UIProfileButton *btnGroups;
    UIProfileButton *btnProfiles;
    UIProfileButton *btnFavs;
    UIProfileButton *btnFans;
}

@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) UILabel *sectionTitle;
@property (nonatomic, retain) UILabel *sectionTotal;
@property (nonatomic, retain) NSString *area;
@property (nonatomic, retain) NSMutableArray *buttons;



- (void)buildView;
- (void)setSelectedArea:(NSString *)area;

@end
