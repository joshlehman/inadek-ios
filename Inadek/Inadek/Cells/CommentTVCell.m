//
//  CommentTVCell.m
//  Inadek
//
//  Created by Josh Lehman on 12/14/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "CommentTVCell.h"
#import "CardComment.h"
#import "DeckComment.h"

@implementation CommentTVCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self.contentView setBackgroundColor:[UIColor colorWithWhite:0.98 alpha:1.0]];
        
        UIView *bottomBorder = [[UIView alloc] init];
        [bottomBorder setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.08]];
        [self.contentView addSubview:bottomBorder];
        [bottomBorder makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.bottom.equalTo(self.contentView);
            make.width.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
        }];
        
        _comment = UILabel.new;
        [_comment setStyleClass:@"lbl-card-textvalue"];
        [_comment setStyleCSS:@"text-size: 11px;"];
        _name = UILabel.new;
        [_name setStyleClass:@"lbl-card-texttitle"];
        
        [_comment setNumberOfLines:0];
        [_name setNumberOfLines:0];
        
        [self.contentView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        [self.contentView addSubview:_comment];
        [self.contentView addSubview:_name];
        
        _comment.translatesAutoresizingMaskIntoConstraints = NO;
        _comment.numberOfLines = 0;
        _comment.lineBreakMode = NSLineBreakByWordWrapping;
        
        [_name makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(12, 16, 3, 10));
        }];
        
        [_comment makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_name.bottom).with.offset(5);
            make.left.right.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(3, 16, 3, 10));
            make.bottom.equalTo(self.contentView).with.offset(-15);
        }];
        
        [self setClipsToBounds:NO];
        
    }
    return self;
}

- (void)buildCell:(id)comment_object
{
    if ([comment_object isKindOfClass:[CardComment class]]) {
        CardComment *this = comment_object;
        
        _comment.text = this.comment;
        _name.text = [this.name uppercaseString];
        
    } else if ([comment_object isKindOfClass:[DeckComment class]]) {
        DeckComment *this = comment_object;
        
        _comment.text = this.comment;
        _name.text = [this.name uppercaseString];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
