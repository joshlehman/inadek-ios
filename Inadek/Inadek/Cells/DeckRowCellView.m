//
//  DeckRowCellView.m
//  Inadek
//
//  Created by Josh Lehman on 6/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckRowCellView.h"
#import "DeckCVCell.h"
#import "Deck+Core.h"
#import "AppDelegate.h"


@implementation DeckRowCellView

- (id) init
{
    self = [super init];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithHexString:@"F0EEEA"]];
        
        self.summaryText = UILabel.new;
        [_summaryText setNumberOfLines:0];
        [_summaryText setBackgroundColor:[UIColor clearColor]];
        [_summaryText setStyleClass:@"lbl-deck-summary"];
        [self addSubview:self.summaryText];
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.itemSize = CGSizeMake(134.0, 170.0);
        [flowLayout setSectionInset:UIEdgeInsetsMake(0, 16, 0, 16)];
        
        self.deckCV = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        self.deckCV.backgroundColor = [UIColor colorWithHexString:@"F0EEEA"];
        
        // Register the collection cell
        [self.deckCV registerClass:[DeckCVCell class] forCellWithReuseIdentifier:@"deckCell"];
        
        [self.deckCV setDataSource:self];
        [self.deckCV setDelegate:self];
        self.deckCV.alwaysBounceVertical = NO;
        [self addSubview:self.deckCV];
        [self.deckCV makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.top.equalTo(_summaryText.bottom).with.offset(10);
        }];
    }
    
    return self;
    
}

- (void)setSummary:(NSString *)summary
{
    
    [_summaryText setText:summary];
    CGSize maxSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width - 40, CGFLOAT_MAX);
    CGSize requiredSize = [_summaryText sizeThatFits:maxSize];
    _summaryText.frame = CGRectMake(20, 10, requiredSize.width, requiredSize.height);
    
}

- (void)setDecksData:(NSArray *)decks
{
    _decksData = decks;
    [_deckCV reloadData];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _decksData.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Setup cell identifier
    static NSString *cellIdentifier = @"deckCell";
    
    /* Uncomment this block to use subclass-based cells */
    DeckCVCell *cell = (DeckCVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    Deck *deck = [_decksData objectAtIndex:indexPath.row];
    
    [cell buildWithDeck:deck];
    
    [cell.layer setCornerRadius:4];
    
    // Return the cell
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    id currentCell = [collectionView cellForItemAtIndexPath:indexPath];
    if ([currentCell isKindOfClass:[DeckCVCell class]]) {
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;

        Deck *thisDeck = [(DeckCVCell *)currentCell cellDeck];

        NSDictionary *deckParams = NSDictionary.new;
        if (thisDeck) {
            deckParams = @{@"deck":thisDeck};
        }

        [appDelegate.transitionVC transitionTo:@"deck" withParams:deckParams];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
