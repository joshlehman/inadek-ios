//
//  ImageCVCell.h
//  Inadek
//
//  Created by Josh Lehman on 12/2/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//
#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "InadekLoader.h"
#import "CardImage.h"

@interface ImageCVCell : UICollectionViewCell

@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) CardImage *cardImage;

@property (nonatomic, retain) UIImageView *image;
@property (nonatomic, retain) InadekLoader *loader;


- (void)loadImage:(NSString *)url;

- (void)buildWithImage:(CardImage *)image;
- (CardImage *)cellImage;



@end
