//
//  CategoryTableViewCell.m
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CategoryTableViewCell.h"

@implementation CategoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        self.label = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.label setStyleClass:@"lbl-selectorcell"];
        
        [self addSubview:self.label];
        [self.label makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 20, 0, 50));
        }];
        
        self.check = UIButton.new;
        [_check setImage:[UIImage imageNamed:@"Btn-Check"] forState:UIControlStateNormal];
        [_check setAlpha:0.0];
        [self addSubview:_check];
        
        [_check makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@27);
            make.height.equalTo(@27);
            make.left.equalTo(self.label.right).with.offset(7);
            make.top.equalTo(@4);
        }];
        
        UIView *background = UIView.new;
        [background setBackgroundColor:[UIColor clearColor]];
        self.selectedBackgroundView = background;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setChecked:(BOOL)checked
{
    if (checked) {
        is_checked = YES;
        [self.label setStyleClass:@"lbl-selectorcell-on"];
        [_check setAlpha:1.0];
    } else {
        is_checked = NO;
        [self.label setStyleClass:@"lbl-selectorcell"];
        [_check setAlpha:0.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        if (is_checked) {
            [self setChecked:NO];
        } else {
            [self setChecked:YES];
        }
    }
}

- (BOOL)isChecked
{
    if (is_checked) {
        return YES;
    } else {
        return NO;
    }
}

@end
