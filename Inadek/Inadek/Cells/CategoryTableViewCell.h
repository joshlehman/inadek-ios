//
//  CategoryTableViewCell.h
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import "DeckCategory.h"

@class DeckCategory, Deck;

@interface CategoryTableViewCell : UITableViewCell {
    BOOL is_checked;
}

@property (nonatomic, retain) UIButton *check;
@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain) UILabel *subLabel;
@property (nonatomic, retain) DeckCategory *category;
@property (nonatomic, retain) Deck *deck;


- (void)setChecked:(BOOL)is_checked;
- (BOOL)isChecked;
@end
