//
//  SectionBackgroundRVCell.m
//  Inadek
//
//  Created by Josh Lehman on 6/4/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "SectionBackgroundRVCell.h"
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>

@implementation SectionBackgroundRVCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImage *backgroundImage = [UIImage imageNamed:@"cat_popular"];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = backgroundImage;
        [self addSubview:imageView];
        [imageView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

@end
