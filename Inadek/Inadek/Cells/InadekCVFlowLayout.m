//
//  InadekCVFlowLayout.m
//  Inadek
//
//  Created by Josh Lehman on 12/3/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "InadekCVFlowLayout.h"

#import "SectionBackgroundRVCell.h"

@implementation InadekCVFlowLayout

- (id)init
{
    self = [super init];
    if (self) {
        [self setupLayout];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout
{
    [self registerClass:[SectionBackgroundRVCell class] forDecorationViewOfKind:@"SectionBackground"];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
        
    NSMutableArray *allAttributes = [[NSMutableArray alloc] initWithCapacity:[self.collectionView numberOfSections]];
    
    if ([self.collectionView numberOfSections] > 0) {
        
        NSMutableArray *other_attributes = [[super layoutAttributesForElementsInRect:rect] mutableCopy];
        
        for (UICollectionViewLayoutAttributes *object in other_attributes) {
            [allAttributes addObject:object];
        }
        
//        [allAttributes addObject:[self layoutAttributesForDecorationViewOfKind:@"SectionBackground" atIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];
        
        return allAttributes;
        
    }
    
    return [[super layoutAttributesForElementsInRect:rect] mutableCopy];
        
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:
(NSString*)decorationViewKind atIndexPath:(NSIndexPath *)indexPath
{
    if ([decorationViewKind isEqualToString:@"SectionBackground"]) {
        
        UICollectionViewLayoutAttributes *headerAttributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:@"SectionBackground" withIndexPath:indexPath];
        
        headerAttributes.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 200);
        
        return headerAttributes;
        
    } else {
        return nil;
    }
}


@end
