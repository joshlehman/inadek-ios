//
//  BCGalleryImageView.h
//  Inadek
//
//  Created by Josh Lehman on 11/18/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "InadekLoader.h"
#import "CardImage.h"
#import "BCButton.h"

@interface BCGalleryImageView : UIImageView

@property (nonatomic, retain) CardImage *cardImage;
@property (nonatomic, retain) NSArray *cardImageSet;
@property (nonatomic, retain) BCButton *tapButton;
@property (nonatomic, retain) InadekLoader *loader;

- (void)loadLocalImage:(UIImage *)image;
- (void)loadImage:(NSString *)url;
- (void)addTarget:(id)target cardImage:(CardImage *)cardImage;

@end
