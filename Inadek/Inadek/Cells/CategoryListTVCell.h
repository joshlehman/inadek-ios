//
//  CategoryListTVCell.h
//  Inadek
//
//  Created by Josh Lehman on 3/21/16.
//  Copyright © 2016 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <PixateFreestyle/PixateFreestyle.h>


@interface CategoryListTVCell : UITableViewCell {
    UIImageView *disclosure;
    UIImageView *disclosureBack;
}

@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain) UILabel *backLabel;
@property (nonatomic, retain) NSNumber *category_id;

- (void)makeBackButton;

@end
