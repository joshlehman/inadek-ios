//
//  SectionBackgroundRVCell.h
//  Inadek
//
//  Created by Josh Lehman on 6/4/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>

@interface SectionBackgroundRVCell : UICollectionReusableView

@end
