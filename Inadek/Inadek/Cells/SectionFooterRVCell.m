//
//  SectionFooterRVCell.m
//  Inadek
//
//  Created by Josh Lehman on 12/3/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "SectionFooterRVCell.h"

@implementation SectionFooterRVCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *background = UIView.new;
        
        
        [background setBackgroundColor:[UIColor colorWithHexString:@"FFFFFF"]];
        [self addSubview:background];
        [background makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(10, 0, 0, 0));
        }];
        
        UIView *bottomBorder = [[UIView alloc] init];
        [bottomBorder setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.2]];
        [background addSubview:bottomBorder];
        [bottomBorder makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.bottom.equalTo(background);
            make.width.equalTo(background);
            make.left.equalTo(@0);
        }];
        
        UIView *topBorder = [[UIView alloc] init];
        [topBorder setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.2]];
        [background addSubview:topBorder];
        [topBorder makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.top.equalTo(background);
            make.width.equalTo(background);
            make.left.equalTo(@0);
        }];
        
        _gotoArea = UIButton.new;
        [_gotoArea setStyleClass:@"lbl-moredecks-button-sm"];
        _gotoArea.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _gotoArea.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 40);
        
        [self addSubview:_gotoArea];
        [_gotoArea makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(background).with.insets(UIEdgeInsetsMake(1, 0, 1, 0));
        }];
        
        [_gotoArea setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        
        UIImageView *disclose = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-DiscloseOrange"]];
        [background addSubview:disclose];
        [disclose makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@22);
            make.height.equalTo(@26);
            make.right.equalTo(background).with.offset(-10);
            make.centerY.equalTo(background);
        }];
        
    }
    return self;
}

@end
