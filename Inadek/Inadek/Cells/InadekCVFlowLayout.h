//
//  InadekCVFlowLayout.h
//  Inadek
//
//  Created by Josh Lehman on 12/3/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InadekCVFlowLayout : UICollectionViewFlowLayout

@property (nonatomic) BOOL hasSearchHeader;

@end
