//
//  MessageTVCell.h
//  Inadek
//
//  Created by Josh Lehman on 2/27/16.
//  Copyright © 2016 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>

@class AccountNotification, Deck, Card;

@interface MessageTVCell : UITableViewCell

@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *message;
@property (nonatomic, retain) AccountNotification *notice;

- (void)buildCell:(id)message_object;

- (Deck *)getNotificationDeckDestination;
- (Card *)getNotificationCardDestination;

@end
