//
//  BCGalleryImageView.m
//  Inadek
//
//  Created by Josh Lehman on 11/18/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "BCGalleryImageView.h"
#import "BCButton.h"

@implementation BCGalleryImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)loadLocalImage:(UIImage *)image
{
    
    [self setClipsToBounds:YES];
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    if (image != nil) {
        [self setContentMode:UIViewContentModeScaleAspectFill];
        [self setImage:image];
    } else {
        NSLog(@"Something is...... missing.");
    }
}

- (void)loadImage:(NSString *)url
{
    if (self.image) {
        self.image = nil;
    }
    
    [self setClipsToBounds:YES];
    
    if (!_loader) {
        _loader = [[InadekLoader alloc] initWithFrame:CGRectZero];
    }
    
    [_loader loadSpinner:@"grey" andSize:@"small" andTitle:nil];
    [_loader setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:_loader];
    [_loader makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self setContentMode:UIViewContentModeScaleAspectFill];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager.imageCache clearMemory];
    
    [manager downloadImageWithURL:[NSURL URLWithString:url]
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [self setImage:image];
                                [_loader hideSpinner];
                            }
                        }];
    
}

- (void)addTarget:(id)target cardImage:(CardImage *)cardImage
{
    if (_tapButton) {
        [_tapButton removeFromSuperview];
        _tapButton = nil;
    }
    
    _tapButton = BCButton.new;
    [_tapButton setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_tapButton];
    if (cardImage) {
        _tapButton.userInfo = @{@"cardimage":cardImage, @"set":_cardImageSet};
    }
    [_tapButton makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    if (target && [target respondsToSelector:@selector(didTapImage:)]) {
        [_tapButton addTarget:target action:@selector(didTapImage:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}


@end
