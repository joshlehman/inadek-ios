//
//  DeckRowTVCell.h
//  Inadek
//
//  Created by Josh Lehman on 6/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>

@class DeckRowCellView;

@interface DeckRowTVCell : UITableViewCell {
    
}

@property (nonatomic, retain) DeckRowCellView *containerView;

- (void)setCollectionData:(NSArray *)data;
- (void)setSummaryText:(NSString *)text;

@end
