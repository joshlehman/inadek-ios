//
//  SectionHeaderRVCell.m
//  Inadek
//
//  Created by Josh Lehman on 12/3/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "ProfileHeader.h"
#import "AccountManager.h"
#import "UIProfileButton.h"

@implementation ProfileHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)setSelectedArea:(NSString *)area
{
    _area = area;
    
    [btnProfiles setStyleClass:@"btn-profile-area"];
    [btnFavs setStyleClass:@"btn-profile-area"];
    [btnFans setStyleClass:@"btn-profile-area"];
    [btnGroups setStyleClass:@"btn-profile-area"];
    
    if ([area isEqualToString:@"profiles"]) {
        [btnProfiles setStyleClass:@"btn-profile-area-selected"];
    } else if ([area isEqualToString:@"favs"]) {
        [btnFavs setStyleClass:@"btn-profile-area-selected"];
    } else if ([area isEqualToString:@"fans"]) {
        [btnFans setStyleClass:@"btn-profile-area-selected"];
    } else if ([area isEqualToString:@"groups"]) {
        [btnGroups setStyleClass:@"btn-profile-area-selected"];
    }
}

- (void)buildView
{
    [self setBackgroundColor:[UIColor colorWithHexString:@"282727"]];
    [self setStyleClass:@"box-shadow"];
    [self setStyleCSS:@"background-color: #282727;"];
    
    UILabel *realname = UILabel.new;
    [realname setStyleClass:@"lbl-profile-name"];
    realname.text = [[AccountManager sharedManager] currentAccount].username;
    [self addSubview:realname];
    [realname makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).with.offset(15);
        make.right.equalTo(self);
        make.height.equalTo(@36);
        make.left.equalTo(self).with.offset(20);
    }];
    
    UILabel *username = UILabel.new;
    [username setStyleClass:@"lbl-profile-username"];
    username.text = [[AccountManager sharedManager] currentAccount].email;
    [self addSubview:username];
    [username makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(realname.bottom).with.offset(-6);
        make.right.equalTo(self);
        make.height.equalTo(@22);
        make.left.equalTo(self).with.offset(20);
    }];
    
    UIButton *newGroup = UIButton.new;
    [newGroup setStyleClass:@"btn-glass"];
    [newGroup setTitle:@"+ Start Group" forState:UIControlStateNormal];
    [self addSubview:newGroup];
    [newGroup makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@110);
        make.height.equalTo(@36);
        make.left.equalTo(self).with.offset(20);
        make.top.equalTo(username.bottom).with.offset(5);
    }];
    
    [newGroup addTarget:self.delegate action:@selector(doNewGroup:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *newProfile = UIButton.new;
    [newProfile setStyleClass:@"btn-glass"];
    [newProfile setTitle:@"+ Profile Card" forState:UIControlStateNormal];
    [self addSubview:newProfile];
    [newProfile makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@110);
        make.height.equalTo(@36);
        make.left.equalTo(newGroup.right).with.offset(15);
        make.top.equalTo(username.bottom).with.offset(5);
    }];
    
    [newProfile addTarget:self.delegate action:@selector(doNewProfileCard:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    NSString *profileCount = [[AccountManager sharedManager] profileCardCountString];
    NSString *favCount = [[AccountManager sharedManager] favDeckCountString];
    NSString *fanCount = [[AccountManager sharedManager] fanCountString];
    NSString *groupCount = [[AccountManager sharedManager] groupDeckCountString];
    
    btnProfiles = UIProfileButton.new;
    [btnProfiles setUserInfo:@{@"area":@"profiles"}];
    
    if (_area && [_area isEqualToString:@"profiles"]) {
        [btnProfiles setStyleClass:@"btn-profile-area-selected"];
    } else {
        [btnProfiles setBackgroundColor:[UIColor clearColor]];
        [btnProfiles setStyleClass:@"btn-profile-area"];
    }
    
    [btnProfiles addTarget:self.delegate action:@selector(reloadProfileSection:) forControlEvents:UIControlEventTouchUpInside];
    [btnProfiles addTarget:self action:@selector(changeButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *content = [self buildButton:profileCount andTitle:@"PROFILES" atIndex:0];
    [btnProfiles addSubview:content];
    [content makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(btnProfiles);
    }];
    
    [_buttons addObject:btnProfiles];
    
    
    btnFavs = UIProfileButton.new;
    [btnFavs setUserInfo:@{@"area":@"favs"}];
    
    if (_area && [_area isEqualToString:@"favs"]) {
        [btnFavs setStyleClass:@"btn-profile-area-selected"];
    } else {
        [btnFavs setStyleClass:@"btn-profile-area"];
    }
    
    [btnFavs addTarget:self.delegate action:@selector(reloadProfileSection:) forControlEvents:UIControlEventTouchUpInside];
    [btnFavs addTarget:self action:@selector(changeButton:) forControlEvents:UIControlEventTouchUpInside];
    
    content = [self buildButton:favCount andTitle:@"YOUR FAVS" atIndex:1];
    [btnFavs addSubview:content];
    [content makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(btnFavs);
    }];
    
    [_buttons addObject:btnFavs];
    
    btnFans = UIProfileButton.new;
    [btnFans setUserInfo:@{@"area":@"fans"}];
    
    if (_area && [_area isEqualToString:@"fans"]) {
        [btnFans setStyleClass:@"btn-profile-area-selected"];
    } else {
        [btnFans setStyleClass:@"btn-profile-area"];
    }
    
    [btnFans addTarget:self.delegate action:@selector(reloadProfileSection:) forControlEvents:UIControlEventTouchUpInside];
    [btnFans addTarget:self action:@selector(changeButton:) forControlEvents:UIControlEventTouchUpInside];
    
    content = [self buildButton:fanCount andTitle:@"FANS" atIndex:2];
    [btnFans addSubview:content];
    [content makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(btnFans);
    }];
    
    [_buttons addObject:btnFans];
    
    btnGroups = UIProfileButton.new;
    [btnGroups setUserInfo:@{@"area":@"groups"}];
    
    if (_area && [_area isEqualToString:@"groups"]) {
        [btnGroups setStyleClass:@"btn-profile-area-selected"];
    } else {
        [btnGroups setStyleClass:@"btn-profile-area"];
    }
    
    [btnGroups addTarget:self.delegate action:@selector(reloadProfileSection:) forControlEvents:UIControlEventTouchUpInside];
    [btnGroups addTarget:self action:@selector(changeButton:) forControlEvents:UIControlEventTouchUpInside];
    
    content = [self buildButton:groupCount andTitle:@"GROUPS" atIndex:3];
    [btnGroups addSubview:content];
    [content makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(btnGroups);
    }];
    
    [_buttons addObject:btnGroups];
    
    int profileTop = 125;
    
    [self addSubview:btnProfiles];
    [btnProfiles makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(profileTop));
        make.left.equalTo(@-1);
        make.height.equalTo(@55);
        make.width.equalTo(self).multipliedBy(0.25).with.offset(1);
    }];
    
    [self addSubview:btnFavs];
    [btnFavs makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(profileTop));
        make.left.equalTo(btnProfiles.right).with.offset(-1);
        make.height.equalTo(@55);
        make.width.equalTo(self).multipliedBy(0.25).with.offset(1);
    }];
    
    [self addSubview:btnFans];
    [btnFans makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(profileTop));
        make.left.equalTo(btnFavs.right).with.offset(-1);
        make.height.equalTo(@55);
        make.width.equalTo(self).multipliedBy(0.25).with.offset(1);
    }];
    
    [self addSubview:btnGroups];
    [btnGroups makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(profileTop));
        make.left.equalTo(btnFans.right).with.offset(-1);
        make.height.equalTo(@55);
        make.width.equalTo(self).multipliedBy(0.25).with.offset(2);
    }];
}

- (void)changeButton:(id)sender
{
    NSDictionary *params = [(UIProfileButton *)sender userInfo];
    if ([params objectForKey:@"area"]) {
        NSString *area = [params objectForKey:@"area"];
        [self setSelectedArea:area];
    }
}

- (UIView *)buildButton:(NSString *)total andTitle:(NSString *)title atIndex:(int)index
{
    UILabel *totLabel = UILabel.new;
    [totLabel setStyleClass:@"lbl-profile-total"];
    totLabel.text = total;
    
    UILabel *titleLabel = UILabel.new;
    [titleLabel setStyleClass:@"lbl-profile-title"];
    titleLabel.text = title;
    
    UIView *label = UIView.new;
    [label setUserInteractionEnabled:NO];
    [label setStyleClass:@"btn-profile-button-background"];
    [label addSubview:totLabel];
    [label addSubview:titleLabel];
    
    [totLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(label);
        make.height.equalTo(@36);
    }];
    
    [titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(label);
        make.height.equalTo(@30);
    }];
    
    if (index == 0) {
        UIView *top = UIView.new;
        top.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.0];
        UIView *bottom = UIView.new;
        bottom.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.0];
        
        [label addSubview:top];
        [top makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(label);
            make.height.equalTo(@1);
        }];
        
        [label addSubview:bottom];
        [bottom makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.equalTo(label);
            make.height.equalTo(@1);
        }];
        
    } else {
        UIView *top = UIView.new;
        top.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.0];
        UIView *bottom = UIView.new;
        bottom.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.0];
        UIView *left = UIView.new;
        left.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.0];
        
        [label addSubview:top];
        [top makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(label);
            make.height.equalTo(@1);
        }];
        
        [label addSubview:bottom];
        [bottom makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.equalTo(label);
            make.height.equalTo(@1);
        }];
    
        [label addSubview:left];
        [left makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.top.left.equalTo(label);
            make.width.equalTo(@1);
        }];
    }
    
    return label;
}

@end
