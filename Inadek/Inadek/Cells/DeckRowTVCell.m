//
//  DeckRowTVCell.m
//  Inadek
//
//  Created by Josh Lehman on 6/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckRowTVCell.h"
#import "DeckRowCellView.h"

@implementation DeckRowTVCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _containerView = [[DeckRowCellView alloc] init];
        [self.contentView addSubview:_containerView];
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCollectionData:(NSArray *)data
{
    [_containerView setDecksData:data];
}

- (void)setSummaryText:(NSString *)text
{
    [_containerView setSummary:text];
}

@end
