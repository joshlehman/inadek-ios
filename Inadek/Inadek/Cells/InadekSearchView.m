//
//  SearchView.m
//  Inadek
//
//  Created by Josh Lehman on 12/3/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "InadekSearchView.h"

@implementation InadekSearchView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setBackgroundColor:[UIColor clearColor]];
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectZero];
        
        [searchBar setSearchBarStyle:UISearchBarStyleMinimal];
        [searchBar setTranslucent:YES];
        [searchBar setTintColor:[UIColor grayColor]];
        [searchBar setBackgroundColor:[UIColor whiteColor]];
        
        
        [self addSubview:searchBar];
        [searchBar mas_makeConstraints:^(MASConstraintMaker *make){
            make.edges.equalTo(self);
        }];
        
        [self setStyleClass:@"box-shadow"];
        
    }
    return self;
}

- (void) initSearchBarDelegate:(id)delegate
{
    searchBar.delegate = delegate;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
