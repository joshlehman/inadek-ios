//
//  ImageCVCell.m
//  Inadek
//
//  Created by Josh Lehman on 12/2/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "ImageCVCell.h"
#import "BCButton.h"

@implementation ImageCVCell {
    BCButton *xButton;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *background = UIView.new;
        [background setBackgroundColor:[UIColor whiteColor]];
        [background setStyleClass:@"box-deck-cell"];
        
        [self addSubview:background];
        
        [background mas_makeConstraints:^(MASConstraintMaker *make){
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(3, 3, 3, 3));
        }];
        
        _image = UIImageView.new;
        [_image setStyleClass:@"box-rounded-inner"];
        [_image setStyleCSS:@"background-color: #fff;"];
        [_image setClipsToBounds:YES];
        
        [background addSubview:_image];
        
        [_image mas_makeConstraints:^(MASConstraintMaker *make){
            make.top.equalTo(background.top).with.offset(3);
            make.left.equalTo(background.left).with.offset(3);
            make.right.equalTo(background.right).with.offset(-3);
            make.bottom.equalTo(background.bottom).with.offset(-3);
        }];
        
        xButton = BCButton.new;
        [xButton setImage:[UIImage imageNamed:@"Btn-Image-Delete"] forState:UIControlStateNormal];
        [self addSubview:xButton];
        [xButton makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@28);
            make.top.equalTo(self).with.offset(5);
            make.right.equalTo(self).with.offset(-5);
        }];
        
        [self setClipsToBounds:YES];
        
    }
    return self;
}

- (void)loadLocalImage:(UIImage *)image
{
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    if (_image && image != nil) {
        [_image setContentMode:UIViewContentModeScaleAspectFill];
        [_image setImage:image];
    } else {
        NSLog(@"Something is...... missing.");
    }
}

- (void)loadImage:(NSString *)url
{
    if (_image.image) {
        _image.image = nil;
    }
    
    if (!_loader) {
        _loader = [[InadekLoader alloc] initWithFrame:CGRectZero];
    }
    
    [_loader loadSpinner:@"grey" andSize:@"small" andTitle:nil];
    [_loader setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:_loader];
    [_loader makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    if (_image) {
        [_image setContentMode:UIViewContentModeScaleAspectFill];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager.imageCache clearMemory];
        
        [manager downloadImageWithURL:[NSURL URLWithString:url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    [_image setImage:image];
                                    [_loader hideSpinner];
                                }
                            }];
        
    }
}

- (void)buildWithImage:(CardImage *)thisImage
{
    _cardImage = nil;
    if (_image.image) {
        _image.image = nil;
    }
    
    _cardImage = thisImage;
    
    if (xButton) {
        xButton.userInfo = @{@"cardimage":_cardImage};
        [xButton setHidden:YES];
        [xButton removeTarget:self.delegate action:@selector(didRemoveImage:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (_cardImage.image_thumb_url && ![_cardImage.image_thumb_url isEqualToString:@""]) {
        [self loadImage:_cardImage.image_thumb_url];
        [xButton setHidden:NO];
        if (self.delegate) {
            [xButton addTarget:self.delegate action:@selector(didRemoveImage:) forControlEvents:UIControlEventTouchUpInside];
        }
    } else if (_cardImage.image && [_cardImage.image isKindOfClass:[UIImage class]]) {
        [self loadLocalImage:_cardImage.image];
        [xButton setHidden:NO];
        if (self.delegate) {
            [xButton addTarget:self.delegate action:@selector(didRemoveImage:) forControlEvents:UIControlEventTouchUpInside];
        }
    } else {
        [_image setImage:nil];
    }
}


@end
