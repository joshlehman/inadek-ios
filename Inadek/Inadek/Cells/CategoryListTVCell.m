//
//  CategoryListTVCell.m
//  Inadek
//
//  Created by Josh Lehman on 3/21/16.
//  Copyright © 2016 Inadek. All rights reserved.
//

#import "CategoryListTVCell.h"

@implementation CategoryListTVCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        self.label = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.label setStyleClass:@"lbl-selectorcell"];
        [self.label setStyleCSS:@"font-weight: 600; font-size: 17px;"];
        
        [self addSubview:self.label];
        [self.label makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 20, 0, 50));
        }];
                
        [self setBackgroundColor:[UIColor clearColor]];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView *topBorder = [[UIView alloc] init];
        [topBorder setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.12]];
        [self addSubview:topBorder];
        [topBorder makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.top.equalTo(self);
            make.width.equalTo(self);
            make.left.equalTo(self);
        }];
        
        disclosure = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-Disclose-Dark"]];
        [self addSubview:disclosure];
        [disclosure makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@28);
            make.height.equalTo(@33);
            make.centerY.equalTo(self);
            make.right.equalTo(self).with.offset(-10);
        }];
        
    }
    return self;
}

- (void)makeBackButton
{
    
    disclosureBack = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-Back-Dark"]];
    [self addSubview:disclosureBack];
    [disclosureBack makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@28);
        make.height.equalTo(@33);
        make.centerY.equalTo(self);
        make.left.equalTo(self).with.offset(10);
    }];
    
    [self.label updateConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 43, 0, 50));
    }];
    
    if (disclosure) {
        [disclosure removeFromSuperview];
    }
    
    [self setBackgroundColor:[UIColor whiteColor]];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected) {
        [self setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    } else {
        [self setBackgroundColor:[UIColor clearColor]];
    }

    // Configure the view for the selected state
}

@end
