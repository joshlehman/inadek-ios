//
//  SectionFooterRVCell.h
//  Inadek
//
//  Created by Josh Lehman on 12/3/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//
#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>

@interface SectionFooterRVCell : UICollectionReusableView

@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) UIButton *gotoArea;

@end
