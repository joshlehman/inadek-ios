//
//  MessageTVCell.m
//  Inadek
//
//  Created by Josh Lehman on 2/27/16.
//  Copyright © 2016 Inadek. All rights reserved.
//

#import "MessageTVCell.h"

#import "AccountNotification.h"
#import "Card.h"
#import "Deck.h"

@implementation MessageTVCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self.contentView setBackgroundColor:[UIColor colorWithWhite:0.98 alpha:1.0]];
        
        UIView *bottomBorder = [[UIView alloc] init];
        [bottomBorder setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.08]];
        [self.contentView addSubview:bottomBorder];
        [bottomBorder makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@1);
            make.bottom.equalTo(self.contentView);
            make.width.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
        }];
        
        _message = UILabel.new;
        [_message setStyleClass:@"lbl-card-textvalue"];
        [_message setStyleCSS:@"font-size: 12px;"];
        _title = UILabel.new;
        [_title setStyleClass:@"lbl-card-textvalue"];
        [_title setStyleCSS:@"font-size: 11px; font-weight: 700;"];
        
        [_message setNumberOfLines:0];
        [_title setNumberOfLines:0];
        
        [self.contentView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        [self.contentView addSubview:_message];
        [self.contentView addSubview:_title];
        
        _message.translatesAutoresizingMaskIntoConstraints = NO;
        _message.numberOfLines = 0;
        _message.lineBreakMode = NSLineBreakByWordWrapping;
        
        [_title makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(12, 16, 3, 10));
        }];
        
        [_message makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_title.bottom).with.offset(5);
            make.left.right.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(3, 16, 3, 10));
            make.bottom.equalTo(self.contentView).with.offset(-15);
        }];
        
        [self setClipsToBounds:NO];
        
    }
    return self;
}

- (void)buildCell:(id)message_object
{
    if ([message_object isKindOfClass:[AccountNotification class]]) {
        AccountNotification *this = message_object;
        
        _message.text = this.message;
        _title.text = [this.title uppercaseString];
        _notice = this;
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (Deck *)getNotificationDeckDestination
{
    if (self.notice) {
        if (self.notice.deck) {
            return self.notice.deck;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

- (Card *)getNotificationCardDestination
{
    if (self.notice) {
        if (self.notice.card) {
            return self.notice.card;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

@end
