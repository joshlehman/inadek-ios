//
//  DeckRowCellView.h
//  Inadek
//
//  Created by Josh Lehman on 6/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeckRowCellView : UIView <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, retain) UICollectionView *deckCV;
@property (strong, nonatomic) NSArray *decksData;
@property (nonatomic, retain) UILabel *summaryText;

- (void)setDecksData:(NSArray *)decks;
- (void)setSummary:(NSString *)summary;

@end
