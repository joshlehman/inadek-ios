//
//  SectionHeaderRVCell.m
//  Inadek
//
//  Created by Josh Lehman on 12/3/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "SectionHeaderRVCell.h"

@implementation SectionHeaderRVCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _sectionTitle = UILabel.new;
        [_sectionTitle setText:@"Entertainment"];
        [_sectionTitle setStyleClass:@"lbl-cv-section-header"];
        
        [self addSubview:_sectionTitle];
        
        [_sectionTitle mas_makeConstraints:^(MASConstraintMaker *make){
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(30, 19, 0, 120));
        }];
        
        _sectionTotal = UILabel.new;
        [_sectionTotal setText:@"231 Cards"];
        [_sectionTotal setStyleClass:@"lbl-cv-section-header-count"];
        [_sectionTotal setTextAlignment:NSTextAlignmentRight];
        
        [self addSubview:_sectionTotal];
        
        [_sectionTotal mas_makeConstraints:^(MASConstraintMaker *make){
            make.top.equalTo(_sectionTitle.top).with.offset(1);
            make.right.equalTo(self).with.offset(-19);
            make.height.equalTo(_sectionTitle.height);
            make.width.equalTo(@100);
        }];
        
    }
    return self;
}

@end
