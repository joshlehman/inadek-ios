//
//  DeckCVCell.m
//  Inadek
//
//  Created by Josh Lehman on 12/2/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//

#import "DeckCVCell.h"

@implementation DeckCVCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *background = UIView.new;
        [background setBackgroundColor:[UIColor whiteColor]];
        [background setStyleClass:@"box-deck-cell"];
        
        [self addSubview:background];
        
        [background mas_makeConstraints:^(MASConstraintMaker *make){
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(1, 3, 3, 3));
        }];
        
        _image = UIImageView.new;
        [_image setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        [_image setStyleClass:@"box-rounded-inner"];
        [_image setStyleCSS:@"background-color: #eee;"];
        [_image setClipsToBounds:YES];
        
        [background addSubview:_image];
        
        [_image mas_makeConstraints:^(MASConstraintMaker *make){
            make.top.equalTo(background.top).with.offset(3);
            make.left.equalTo(background.left).with.offset(3);
            make.right.equalTo(background.right).with.offset(-3);
            make.height.equalTo(_image.width);
        }];
        
        _title = [[UILabel alloc] init];
        _title.numberOfLines = 0;
        _title.lineBreakMode = NSLineBreakByWordWrapping;
        _title.preferredMaxLayoutWidth = 90;
        
        [_title setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [_title setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [_title setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [_title setStyleClass:@"lbl-cell-title"];

        
        [self addSubview:_title];
        
        [_title mas_makeConstraints:^(MASConstraintMaker *make){
            make.top.equalTo(_image.bottom).with.offset(5);
            make.left.equalTo(_image.left).with.offset(5);
            make.width.equalTo(@90);
        }];
        
        _locked = UIImageView.new;
        _locked.hidden = NO;
        [_locked setImage:[UIImage imageNamed:@"Icon-Locked-Sm"]];
        
        [self addSubview:_locked];
        
        [_locked mas_makeConstraints:^(MASConstraintMaker *make){
            make.right.equalTo(self.right).with.offset(-8);
            make.top.equalTo(_title.top);
            make.width.equalTo(@16);
            make.height.equalTo(@16);
        }];
        
        _favs = UIView.new;
        _favs.hidden = NO;
    
        [self addSubview:_favs];
        
        [_favs makeConstraints:^(MASConstraintMaker *make){
            make.right.equalTo(self.right).with.offset(-8);
            make.top.equalTo(_title.top).with.offset(20);
            make.width.equalTo(@30);
            make.height.equalTo(@16);
        }];
        
        _favIv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-TinyStar"]];
        [_favs addSubview:_favIv];
        [_favIv makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_favs);
            make.height.equalTo(@13);
            make.width.equalTo(@13);
            make.top.equalTo(_favs).with.offset(1);
        }];
        
        
        _favs_count = UILabel.new;
        [_favs_count setText:@""];
        [_favs_count setStyleClass:@"lbl-cell-fav-count"];
        [_favs_count setText:@""];
        [_favs addSubview:_favs_count];
        [_favs_count makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_favs).with.insets(UIEdgeInsetsMake(0, 0, 0, 15));
        }];
        
        [self setClipsToBounds:YES];
        
    }
    return self;
}

- (void)setFavorite:(BOOL)isFavorite
{
    if (isFavorite) {
        if (_favIv) {
            _favIv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-TinyStarOn"]];
        }
    } else {
        if (_favIv) {
            _favIv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Btn-TinyStar"]];
        }
    }
}

- (void)loadLocalImage:(UIImage *)image
{
    if (_image && image != nil) {
        [_image setContentMode:UIViewContentModeScaleAspectFill];
        [_image setImage:image];
    }
}

- (void)loadImage:(NSString *)url
{
    if (_image.image) {
        _image.image = nil;
    }
    
    if (!_loader) {
        _loader = [[InadekLoader alloc] initWithFrame:CGRectZero];
    }
    
    [_loader loadSpinner:@"grey" andSize:@"small" andTitle:nil];
    [_loader setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:_loader];
    [_loader makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    if (_image) {
        [_image setContentMode:UIViewContentModeScaleAspectFill];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager.imageCache clearMemory];
        
        [manager downloadImageWithURL:[NSURL URLWithString:url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    [_image setImage:image];
                                    [_loader hideSpinner];
                                }
                            }];
        
    }
}

- (void)buildWithDeck:(Deck *)deck
{
    _card = nil;
    _deck = deck;
    _title.text = deck.name;
    
    if ([deck.favorite_count integerValue] == 0) {
        _favs_count.hidden = YES;
    } else {
        _favs_count.text = [NSString stringWithFormat:@"%@", deck.favorite_count];
        _favs_count.hidden = NO;
    }
    
    if ([deck is_deck_private]) {
        [_locked setHidden:NO];
        
        [_favs updateConstraints:^(MASConstraintMaker *make){
            make.right.equalTo(self.right).with.offset(-8);
            make.top.equalTo(_title.top).with.offset(20);
            make.width.equalTo(@30);
            make.height.equalTo(@16);
        }];
        
    } else {
        [_locked setHidden:YES];
        
        [_favs updateConstraints:^(MASConstraintMaker *make){
            make.right.equalTo(self.right).with.offset(-8);
            make.top.equalTo(_title.top).with.offset(0);
            make.width.equalTo(@30);
            make.height.equalTo(@16);
        }];
    }
    
    if (_deck.image_thumb_url && ![_deck.image_thumb_url isEqualToString:@""]) {
        [self loadImage:_deck.image_thumb_url];
    } else if (_deck.image && [_deck.image isKindOfClass:[UIImage class]]) {
        [self loadLocalImage:_deck.image];
    } else {
        [_image setImage:nil];
    }
}

- (void)setSize:(NSString *)size
{
    if (size && [size isEqualToString:@"small"]) {
        [_title setStyleClass:@"lbl-cell-title-tiny"];
        
        [_title updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@80);
        }];
        
        _favs.hidden = YES;
    }
}

- (void)buildWithCard:(Card *)card
{
    _deck = nil;
    _card = card;
    _title.text = card.name;
    
    _favs_count.hidden = YES;

    if ([card is_card_private]) {
        [_locked setHidden:NO];
        
        [_favs updateConstraints:^(MASConstraintMaker *make){
            make.right.equalTo(self.right).with.offset(-8);
            make.top.equalTo(_title.top).with.offset(20);
            make.width.equalTo(@30);
            make.height.equalTo(@16);
        }];
        
    } else {
        [_locked setHidden:YES];
        
        [_favs updateConstraints:^(MASConstraintMaker *make){
            make.right.equalTo(self.right).with.offset(-8);
            make.top.equalTo(_title.top).with.offset(0);
            make.width.equalTo(@30);
            make.height.equalTo(@16);
        }];
    }
    
    if (_card.image_thumb_url && ![_card.image_thumb_url isEqualToString:@""]) {
        [self loadImage:_card.image_thumb_url];
    } else if (_card.image && [_card.image isKindOfClass:[UIImage class]]) {
        [self loadLocalImage:_card.image];
    } else {
        [_image setImage:nil];
    }
    
}

- (Deck *)cellDeck
{
    if (_deck) {
        return _deck;
    } else {
        return nil;
    }
}

- (Card *)cellCard
{
    if (_card) {
        return _card;
    } else {
        return nil;
    }
}

- (BOOL)isDeck
{
    if (_card) {
        return NO;
    } else if (_deck) {
        return YES;
    } else {
        return NO;
    }
}

@end
