//
//  DeckCVCell.h
//  Inadek
//
//  Created by Josh Lehman on 12/2/14.
//  Copyright (c) 2014 Inadek. All rights reserved.
//
#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "InadekDeckMiniSummary.h"
#import "Deck+Core.h"
#import "Card+Core.h"
#import "InadekLoader.h"

@interface DeckCVCell : UICollectionViewCell

@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) Deck *deck;
@property (nonatomic, retain) Card *card;

@property (nonatomic, retain) UIImageView *image;
@property (nonatomic, retain) UIImageView *favIv;
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UIImageView *locked;
@property (nonatomic, retain) UIView *favs;
@property (nonatomic, retain) UILabel *favs_count;

@property (nonatomic, retain) InadekDeckMiniSummary *summaryView;
@property (nonatomic, retain) InadekLoader *loader;

- (void)loadImage:(NSString *)url;


- (void)buildWithDeck:(Deck *)deck;
- (void)buildWithCard:(Card *)card;
- (Deck *)cellDeck;
- (Card *)cellCard;

- (void)setSize:(NSString *)size;

- (BOOL)isDeck;

@end
