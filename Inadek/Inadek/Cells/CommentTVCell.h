//
//  CommentTVCell.h
//  Inadek
//
//  Created by Josh Lehman on 12/14/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>

@interface CommentTVCell : UITableViewCell

@property (nonatomic, retain) UILabel *name;
@property (nonatomic, retain) UILabel *comment;

- (void)buildCell:(id)comment_object;

@end
