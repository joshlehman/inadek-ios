//
//  Account+Core.m
//  Inadek
//
//  Created by Josh Lehman on 1/20/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "Account+Core.h"
#import "Common.h"

@implementation Account (Core)

- (void)unpackDictionary:(NSDictionary *)dictionary {
    [super unpackDictionary:dictionary];
    self.remoteID = [dictionary objectForKey:@"id"];
    self.username = [dictionary objectForKey:@"username"];
    self.email = [dictionary objectForKey:@"email"];
}







- (void)debug
{
    DLog(@"Debug this object now!");
}

+ (void)logObjectCounts
{
    NSUInteger objects = [Account count];
    DLog(@"Account Count: %lu", (unsigned long)objects);
}

@end
