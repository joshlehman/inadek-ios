//
//  DeckCategory.m
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckCategory.h"
#import "Deck.h"
#import "DeckCategory+Core.h"

@implementation DeckCategory

@dynamic name;
@dynamic cid;
@dynamic summary;
@dynamic remoteID;
@dynamic status_id;
@dynamic sort_order;
@dynamic is_featured;
@dynamic is_default;
@dynamic decks;

@end
