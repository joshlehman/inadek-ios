//
//  Account.m
//  Inadek
//
//  Created by Josh Lehman on 1/21/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "Account.h"
#import "Deck.h"
#import "Account+Core.h"

@implementation Account

@dynamic email;
@dynamic remoteID;
@dynamic username;
@dynamic owned_decks;
@dynamic favorited_decks;

@end
