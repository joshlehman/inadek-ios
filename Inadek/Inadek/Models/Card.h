//
//  Card.h
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SSDataKit/SSDataKit.h>

@class Deck, CardTrigger, CardField, CardComment;

NS_ASSUME_NONNULL_BEGIN

@interface Card : SSRemoteManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Card+CoreDataProperties.h"
