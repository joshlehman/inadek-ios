//
//  Card+CoreDataProperties.h
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Card.h"

NS_ASSUME_NONNULL_BEGIN

@interface Card (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *base_color;
@property (nullable, nonatomic, retain) NSString *cid;
@property (nullable, nonatomic, retain) NSDate *created_at;
@property (nullable, nonatomic, retain) NSNumber *creator_id;
@property (nullable, nonatomic, retain) id image;
@property (nullable, nonatomic, retain) id image_cropped;
@property (nullable, nonatomic, retain) NSString *image_thumb_url;
@property (nullable, nonatomic, retain) NSString *image_url;
@property (nullable, nonatomic, retain) NSNumber *is_private;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *owner_id;
@property (nullable, nonatomic, retain) NSNumber *remoteID;
@property (nullable, nonatomic, retain) NSNumber *type_id;
@property (nullable, nonatomic, retain) NSDate *updated_at;
@property (nullable, nonatomic, retain) NSSet<CardField *> *card_fields;
@property (nullable, nonatomic, retain) NSSet<Deck *> *decks;
@property (nullable, nonatomic, retain) Deck *related_deck;
@property (nullable, nonatomic, retain) NSSet<CardTrigger *> *triggers;
@property (nullable, nonatomic, retain) NSSet<CardComment *> *card_comments;

@end

@interface Card (CoreDataGeneratedAccessors)

- (void)addCard_fieldsObject:(CardField *)value;
- (void)removeCard_fieldsObject:(CardField *)value;
- (void)addCard_fields:(NSSet<CardField *> *)values;
- (void)removeCard_fields:(NSSet<CardField *> *)values;

- (void)addDecksObject:(Deck *)value;
- (void)removeDecksObject:(Deck *)value;
- (void)addDecks:(NSSet<Deck *> *)values;
- (void)removeDecks:(NSSet<Deck *> *)values;

- (void)addTriggersObject:(CardTrigger *)value;
- (void)removeTriggersObject:(CardTrigger *)value;
- (void)addTriggers:(NSSet<CardTrigger *> *)values;
- (void)removeTriggers:(NSSet<CardTrigger *> *)values;

- (void)addCard_commentsObject:(NSManagedObject *)value;
- (void)removeCard_commentsObject:(NSManagedObject *)value;
- (void)addCard_comments:(NSSet<NSManagedObject *> *)values;
- (void)removeCard_comments:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
