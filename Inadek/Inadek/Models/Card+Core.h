//
//  Card+Core.h
//  Inadek
//
//  Created by Josh Lehman on 1/12/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <ObjectiveRecord/ObjectiveRecord.h>
#import "Card.h"
#import "CardTrigger+Core.h"

@interface Card (Core)

+ (Card *)myNew;

- (void)unpackDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)syncParams;
- (BOOL)isValid;
- (BOOL)isMine;
- (BOOL)isContactable;
- (void)deleteLocal;

- (void)setImageObj:(UIImage*)image;
- (UIImage*)imageObj;
- (void)setCroppedImageObj:(UIImage*)image;
- (UIImage*)croppedImageObj;

- (void)createTriggerWithDetails:(NSDictionary *)triggerDefaults andValue:(NSString *)value;

- (void)addFieldWithDetails:(NSDictionary *)fieldDetails isFront:(BOOL)isFront;
- (NSString *)currentDecksAsString;

- (NSArray *)cardBackFields;
- (NSArray *)cardFrontFields;

- (NSNumber *)nextFieldPosition:(BOOL)isFront;
+ (NSNumber *)getTypeIdFor:(NSString *)cardTypeCID;
+ (NSNumber *)getDefaultPrivacyFor:(NSNumber *)cardTypeID;
- (NSNumber *)isPrivateOrDefault;

- (NSMutableArray *)sortedComments;

- (NSDictionary *)prepareForContacts;

- (BOOL)is_card_private;
- (BOOL)hasCoverPhoto;
- (BOOL)hasBackWebContent;

@end
