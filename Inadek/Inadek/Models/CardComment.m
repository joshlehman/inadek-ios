//
//  CardComment.m
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "CardComment.h"
#import "Card.h"
#import "Common.h"

@implementation CardComment

// Insert code here to add functionality to your managed object subclass

- (void)unpackDictionary:(NSDictionary *)dictionary {
    
    [super unpackDictionary:dictionary];
    self.remoteID = [Common sanitizedNumber:[dictionary valueForKey:@"id"]];
    self.comment = [Common sanitizedString:[dictionary valueForKey:@"comment"]];
    self.name = [Common sanitizedString:[dictionary objectForKey:@"name"]];
    self.comment_date = [Common sanitizedDate:[dictionary objectForKey:@"comment_date"]];
    self.is_active = [NSNumber numberWithBool:YES];
    self.is_deleted = [NSNumber numberWithBool:NO];

}

@end
