//
//  DeckDefines.h
//  Inadek
//
//  Created by Jason Preissinger on 11/27/12.
//  Copyright (c) 2012 Jason Preissinger. All rights reserved.
//

#ifndef Inadek_DeckDefines_h
#define Inadek_DeckDefines_h

#define TESTING 1
//#define PAGETESTING 0

#define kDefaultConnectionLimit             25
#define kMaxConcurrentOperations            3

#define kDefaultPopoverWidth                300.0f
#define kDefaultContentWidth                280.0f
#define kDefaultContentHeight               210.0f

#define kLoginTextBlurb @"Experience the people, places and things you love in a whole new way, with Inadek."

#define kUserDefaultHasShownWelcomeTips     @"hasShownWelcomeTips"
#define kUserDefaultLastAppVersion          @"lastAppVersion"

#define kExperimentalLogic                  0

#define kServerEndpoint                   @"https://www.inadekapp.com"

#define kImageResizeEndPoint @"http://img-cdn.inadekapp.com/resize/"


//notifications
#define kUserDidLoginNotification   @"userDidLoginNotification"
#define kUserDidLogoutNotification  @"userDidLogoutNotification"

#define kAuthenticationViewDismissNotification @"authenticationViewDismissNotification"
#define kAuthenticationErrorNotification @"authenticationErrorNotification"

#define kNewUserCreatedOnServerNotification @"newUserCreatedOnServerNotification"
#define kNewUserErrorOnServerNotification   @"newUserErrorOnServerNotification"

#define kUserDeckListNeedsRefreshNotification @"userDeckListNeedsRefreshNotification"
#define kUserJustNeedsReloadDataNotification @"userJustNeedsReloadDataNotification"
#define kCategoryDeckListNeedsRefreshNotification @"categoryDeckListNeedsRefreshNotification"
#define kSearchNeedsReloadDataNotification @"searchNeedsReloadDataNotification"
#define kCategoryJustNeedsReloadDataNotification @"categoryJustNeedsReloadDataNotification"

#define kFavoriteDeckNotification @"favoriteDeckNotification"
#define kCollectCardNotification @"collectCardNotification"

#define kNewDeckCreatedNeedToLoadInMyDecks @"newDeckCreatedNeedToLoadInMyDecks"
#define kNewCardCreatedNeedToLoadInCurrentDeck @"newCardCreatedNeedToLoadInCurrentDeck"

#define kDeleteDeckAndRemoveFromMyDecks @"deleteDeckAndRemoveFromMyDecks"
#define kDeleteCardAndRemoveFromItsDeck @"deleteCardAndRemoveFromItsDeck"

#define kDisplayNetworkSpinnerView @"displayNetworkSpinnerView"
#define kRemoveNetworkSpinnerView @"removeNetworkSpinnerView"

#define kCardViewSpinningWaitingForLoad @"cardViewSpinningWaitingForLoad"

#define kUserCardListNeedsCollectionRefresh @"userCardListNeedsCollectionRefresh"

#define kApnWasRecievedAndNeedToDoSomethingAboutIt @"apnWasRecievedAndNeedToDoSomethingAboutIt"
#define kRemoteCallToTransitionToNewPage @"remoteCallToTransitionToNewPage"

#define kDeckCollectionReadyToCacheNotification @"deckCollectionReadyToCacheNotification"

#define kProfileDecksNeedsRefreshNotification @"profileDecksNeedsRefreshNotification"
#define kProfileNeedsReloadDataNotification @"profileNeedsReloadDataNotification"

#define kWaitingForSubdeckToLoad @"waitingForSubdeckToLoad"

//Sharing Stuff
#define kDefaultShareUrl @"http://inadek.com"
#define kDefaultShareString @"Check out this Inadek deck"

// Inadek API pages
#define kInadekDefault                     @"/api/v1"
#define kInadekUser                        @"/api/v1/users"
#define kInadekMyAccount                   @"/api/v1/users/me"
#define kInadekDecks                       @"/api/v1/decks"
#define kInadekCards                       @"/api/v1/cards"
#define kInadekPushToken                   @"/api/v1/push_devices"
#define kInadekMeta                        @"/api/v1/meta"

#define kPopularDecks @{@"cid": @"popular", @"name":@"Home",@"url":@"/api/v1/decks/popular",@"page":@0,@"cache_file":@"popularDecksCache.plist"}
#define kFavoriteDecks @{@"cid": @"favorite", @"name":@"My Favorite Decks", @"url":@"/api/v1/decks/following",@"page":@1,@"cache_file":@"favoriteDecksCache.plist"}
#define kOwnedDecks @{@"cid": @"owned", @"name":@"My Decks", @"url": @"/api/v1/decks/owned",@"page":@2,@"cache_file":@"ownedDecksCache.plist"}
#define kSearchDecks @{@"cid": @"search", @"name":@"Search", @"url": @"/api/v1/decks",@"page":@3,@"cache_file":@"searchDecksCache.plist"}

#define kUserDecks @{@"name":@"", @"url": @"/api/v1/decks?created_by=",@"page":@0}
#define kCategoryDecks @{@"name":@"", @"url": @"/api/v1/decks?category_id=",@"page":@0}


// pagination
#define kDecksToUsePerPage 26

//for homeViewController -- what are the names of for the pages
#define kPopularDeckTab 0
#define kFavoriteDeckTab 1
#define kOwnedDeckTab 2
#define kSearchDeckTab 3

// deck stacking
#define kPreviousCard       100
#define kPreviousCardBack   1000
#define kCurrentCard        200
#define kCurrentCardBack    2000
#define kNextCard           300
#define kNextCardBack       3000

// for subdeck
#define kChildDeckCountLabel 49
#define kChildDeckTitleLabel 48
#define kChildDeckView       47

#define kSpinnerSubviewTag  127
#define kSpinnerWaitingForDeckToLoadTag 128

#define kImageCellTag 999

//for create card
#define kCreateCardAddressOne   10021
#define kCreateCardCity         10022
#define kCreateCardState        10023
#define kCreateCardZipCode      10024

//error codes
#define kErrorDomain        @"inadek"
#define kCardAlreadyFollowed 5000
#define kDeckAlreadyFollowed 5001

#define ACCOUNTS_SAVED @"ACCOUNTS_SAVED"

#if TARGET_OS_IPHONE
#define				RUNNING_ON_IPAD					(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define				RUNNING_ON_IPHONE				(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define				RUNNING_OS_50					([[UIDevice currentDevice].systemVersion intValue] >= 5)
#define				RUNNING_OS_60					([[UIDevice currentDevice].systemVersion intValue] >= 6)

#define				IS_RETINA_DEVICE				([UIScreen mainScreen].scale > 1.0)
#define				IS_IPHONE5_DEVICE				(CGRectGetHeight([UIScreen mainScreen].bounds) == 568)
#else
#define				RUNNING_ON_40					NO
#define				RUNNING_ON_50					NO
#endif


//card Types


#define kCardDeckCoverType              @"inadek.cover"
#define kCardAdvertisementType          @"inadek.advertisement"
#define kCardBaseballPlayerPitcherType  @"inadek.baseball.players.pitchers"
#define kCardBaseballPlayerBatterType   @"inadek.baseball.players.batters"
#define kCardBaseballCoverType          @"inadek.baseball.cover"
#define kCardBaseballParkType           @"inadek.baseball.park"
#define kCardAdType                     @"inadek.ad"
#define kCardCoverForChildDeckType      @"inadek.system.deck"
#define kCardCoverAdFullImageType       @"inadek.ad.full-image.deck"
#define kCardAdFullImageType            @"inadek.media.full-image"


# pragma mark - Official Card Types Supported

// General
#define kCardPersonalInfoType               @"inadek.personal"
#define kCardPersonalProfileInfoType        @"inadek.profile"
#define kCardBusinessProfileInfoType        @"inadek.business.profile"
#define kCardBusinessInfoType               @"inadek.business"
#define kCardGenericInfoType                @"inadek.generic"

// Sports
#define kCardSportsBaseballSurveyInfoType   @"inadek.sports.baseball.survey"
// -- FUTURE
//#define kCardSportsFootballSurveyInfoType   @"inadek.sports.football.survey"
//#define kCardSportsBasketballSurveyInfoType @"inadek.sports.basketball.survey"
#define kCardSportsVenueInfoType            @"inadek.sports.venue"
#define kCardSportsTeamInfoType             @"inadek.sports.team"
#define kCardSportsPlayerInfoType           @"inadek.sports.player"

// Photo
#define kCardPhotoInfoType                  @"inadek.photo"

// Ad
#define kCardAdFullImageInfoType            @"inadek.ad.fullimage"
#define kCardAdHalfImageInfoType            @"inadek.ad.halfimage"
#define kCardAdLocationInfoType             @"inadek.ad.location"

// Deck in Deck Cards
#define kCardDeckUrlFullImageInfoType       @"inadek.deckurl.fullimage"
#define kCardDeckUrlHalfImageInfoType       @"inadek.deckurl.halfimage"
#define kCardDeckUrlPromotionInfoType       @"inadek.deckurl.promotion"


#define kCardBaseballPlayerType         @"inadek.baseball.player"

// Deck Types
#define kDeckTypePersonal               @"inadek.deck.personal"
#define kDeckTypeBusiness               @"inadek.deck.business"
#define kDeckTypePhotos                 @"inadek.deck.photos"
#define kDeckTypeSports                 @"inadek.deck.sports"
#define kDeckTypeAd                     @"inadek.deck.ad"
#define kDeckTypeHowto                  @"inadek.deck.howto"
#define kDeckTypeProfile                @"inadek.deck.profile"   //for profile deck
#define kDeckTypeSpecialProfiles        @"inadek.deck.special.profiles"
#define kDeckTypeSpecialFavorites       @"inadek.deck.special.favorites"

#pragma mark -


//CARD NIBS and layouts
#define kCardChildDeckCover         0
#define kCardDeckCover              1
#define kCardGenericInfo            2
#define kCardPersonalInfo           3
#define kCardAdvertisement          4
#define kCardBaseballPlayer         5
#define kCardBaseballPlayerBatter   6
#define kCardBaseballCover          7
#define kCardBaseballPark           8
#define kCardAd                     9
#define kCardAdvertisementFullImage 10
#define kDeckAdvertisementFullImage 11
#define kCardBusinessInfo           12
#define kCardDeckFullImage          13
#define kCardDeckHalfImage          14
#define kCardDeckPromotion          15
#define kCardSportsPlayer           16
#define kCardSportsTeam             17
#define kCardPhoto                  18
#define kCardProfile                19
#define kCardBusinessProfile        20

#define kCard


//CARD VIEW SIZES
#define kCardSizeFull               1
#define kCardSizeThumb              2
#define kCardSizeMini               3

//CARD TRANSITION TYPES
#define kCardTransitionNext         1
#define kCardTransitionPrev         2
#define kCardTransitionToSubdeck    3
#define kCardTransitionToBack       4
#define kCardTransitionToFront      5

//PICTURE SELECTION MODES
#define kPickPictureMode            1
#define kTakePictureMode            2

//STYLE DEFINITIONS FOR INTERFACE
#define kStyleCardbackDataTitleLabel            1
#define kStyleCardbackDataValueLabel            2
#define kStyleCardbackDataBioText               3

//TRANSITION CONSTANTS
#define kTransitionDirectionLeft    @"left"
#define kTransitionDirectionUp      @"up"
#define kTransitionDirectionDown    @"down"
#define kTransitionDirectionRight   @"right"

//TAGS FOR TEMPORARY TRANSITION VIEWS
#define kTmpOverlayToHide           6100

//user based stuff:
#define ACCOUNTS_SAVED @"ACCOUNTS_SAVED"

#define kUserDefaultActiveUser              @"UserActive"
#define kUserDefaultAnonymousUser           @"UserAnonymous"
#define kUserDefaultEmail                   @"UserEmail"
#define kUserDefaultAuthorization           @"UserAuth"
#define kUserDefaultDeviceAuth              @"UserDevice"
#define kUserDefaultImageUrl                @"UserImageUrl"
#define kUserDefaultSelectedTheme           @"UserSelectedTheme"
#define kUserDefaultPassword                @"UserDefaultPassword"
#define kUserDefaultUsername                @"UserDefaultUsername"
#define kUserRegistration                   @"kUserRegistration"
#define kUserRegistrationEmailPassword      @"kUserRegistrationEmailPassword"
#define kUserRegistrationDevice             @"kUserRegistrationDevice"
#define kUserDefaultPushToken               @"kUserRegirstrationPushToken"
#define kUserDefaultPushDeviceId            @"kUserDefaultPushDeviceId"
#define kNewsMessageHolder                  @"kNewsMessageHolder"
#define kUserMyFavoriteCardsDeckId          @"kUserMyFavoriteCardsDeckId"
#define kUserMyProfileDeckId                @"kUserMyProfileDeckId"
#define kUserDefaultSearchTerm              @"kUserDefaultSearchTerm"

//MasterViewController Animation stuff:
#define kViewTransitionAnimationDuration        0.25
#define kTransitionAnimationDuration            0.25
#define kTransitionAnimationDurationShorter     0.15
#define kAnimationCornerRadius                  3.0
#define kOverlayTag                             23

// MasterViewController View Transition Stuff
#define kToViewTypeHome                             1
#define kToViewTypeDeck                             2
#define kToViewTypeDeckEdit                         3
#define kToViewTypeCardEdit                         4
#define kToViewTypeAllCardsEdit                     5
#define kToViewTypeAddToExistingDeck                6
#define kToViewTypeDeckCreateCard                   7
#define kToViewTypeSelectDeck                       8
#define kToViewTypeProfile                          9
#define kToViewTypeUpgradeAccount                   10
#define kToViewTypeUsersDecks                       11
#define kToViewTypeDeckStack                        12
#define kToViewTypeDeckGroup                        13

#define kTransitionStyleImmediate                   1
#define kTransitionStyleEaseFromBelow               2
#define kTransitionStyleThreeQuarterFromBelow       3
#define kTransitionStyleOverlayBlur                 4
#define kTransitionStyleFadeAndGrowIn               5
#define kTransitionStyleSlideFromRight              6
#define kTransitionStyleDropAndGrowIn               7

#define kTransitionDirectionFromBottom              1
#define kTransitionDirectionFromTop                 2
#define kTransitionDirectionFromLeft                3
#define kTransitionDirectionFromRight               4
#define kTransitionDirectionNone                    5

//for field properties
#define kPropertyPaddingTop     5
#define kPropertyPaddingBottom  5


#endif
