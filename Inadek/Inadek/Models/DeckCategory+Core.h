//
//  DeckCategory+Core.h
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <ObjectiveRecord/ObjectiveRecord.h>
#import "DeckCategory.h"

@interface DeckCategory (Core)

- (void)unpackDictionary:(NSDictionary *)dictionary;



- (void)debug;
+ (void)logObjectCounts;

@end
