//
//  DeckTrigger+Core.h
//  Inadek
//
//  Created by Josh Lehman on 1/21/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <ObjectiveRecord/ObjectiveRecord.h>
#import "DeckTrigger.h"

@interface DeckTrigger (Core)

- (void)unpackDictionary:(NSDictionary *)dictionary;

- (BOOL)is_url;
- (BOOL)is_phone;
- (BOOL)is_email;
- (BOOL)is_map;

- (void)deleteFromDeck:(Deck *)deck;

@end
