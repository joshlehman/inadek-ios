//
//  DeckTrigger.h
//  Inadek
//
//  Created by Josh Lehman on 1/21/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SSDataKit/SSDataKit.h>

@class Deck;

@interface DeckTrigger : SSRemoteManagedObject

@property (nonatomic, retain) NSNumber * remoteID;
@property (nonatomic, retain) NSString * trigger_type;
@property (nonatomic, retain) NSString * action_verb;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) Deck *deck;

@end
