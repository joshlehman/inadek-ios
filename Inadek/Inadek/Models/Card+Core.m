//
//  Card+Core.m
//  Inadek
//
//  Created by Josh Lehman on 1/12/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "Card+Core.h"
#import "Common.h"
#import "AccountManager.h"
#import "CardTrigger+Core.h"
#import "CardField+Core.h"
#import "CardImage.h"
#import "CardComment.h"


#import "NSArray+ObjectiveSugar.h"

@implementation Card (Core)

// Generates a new deck, and adds the user has owner and generates a short CID
+ (Card *)myNew
{
    Card *newCard = [Card create];
    if ([[AccountManager sharedManager] currentAccountId]) {
        int account_id = [[[AccountManager sharedManager] currentAccountId] integerValue];
        
        [newCard setOwner_id:[NSNumber numberWithInteger:account_id]];
        [newCard setCreator_id:[NSNumber numberWithInteger:account_id]];
        [newCard setCid:@"!NEW!"];
    }
    
    return newCard;
}

+ (NSNumber *)getTypeIdFor:(NSString *)cardTypeCID
{
    NSNumber *cardType = nil;
    NSArray *internalCardTypes = [[DeckManager sharedManager] localCardTypes:nil];

    for (NSDictionary *dt in internalCardTypes) {
        if ([dt objectForKey:@"cid"]) {
            NSString *cid = [dt objectForKey:@"cid"];
            if ([cid isEqualToString:cardTypeCID]) {
                cardType = [dt objectForKey:@"id"];
                break;
            }
        }
    }
    
    return cardType;
}

- (BOOL)is_card_private
{
    if (self.is_private && [self.is_private integerValue] == 1) {
        return YES;
    } else {
        return NO;
    }
}

+ (NSNumber *)getDefaultPrivacyFor:(NSNumber *)cardTypeID
{
    NSNumber *isPrivate = [NSNumber numberWithBool:NO];
    NSArray *internalCardTypes = [[DeckManager sharedManager] localCardTypes:nil];
    
    for (NSDictionary *dt in internalCardTypes) {
        if ([dt objectForKey:@"cid"]) {
            NSNumber *this_id = [dt objectForKey:@"id"];
            if ([this_id integerValue] == [cardTypeID integerValue]) {
                isPrivate = [dt objectForKey:@"is_private"];
                break;
            }
        }
    }
    
    return isPrivate;
}

- (NSNumber *)isPrivateOrDefault
{
    if (self.is_private) {
        return self.is_private;
    } else if ([self.is_private integerValue] == 0) {
        return [NSNumber numberWithBool:NO];
    } else {
        if (self.type_id) {
            NSNumber *is_private = [Card getDefaultPrivacyFor:self.type_id];
            return is_private;
        }
        return [NSNumber numberWithBool:NO];
    }
}

- (BOOL)isValid {
    if (!self.name || [self.name isEqualToString:@""]) {
        if ([self imageObj] || (self.image_url && ![self.image_url isEqualToString:@""])) {
            // We're ok
        } else {
            return NO;
        }
        
    }
    
    if (!self.type_id || [self.type_id integerValue] == 0) {
        return NO;
    }
    
    if ([self.decks allObjects].count < 1) {
        if (![self isProfile]) {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)isProfile
{
    NSNumber *profile_card_type = [Card getTypeIdFor:@"profile"];
    if ([self.type_id integerValue] == [profile_card_type integerValue]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isMine {
    if (self.owner_id && [self.owner_id integerValue] == [[AccountManager sharedManager].currentAccountId integerValue]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isContactable
{
    if (self.type_id) {
        NSArray *internalCardTypes = [[DeckManager sharedManager] localCardTypes:nil];
        
        for (NSDictionary *dt in internalCardTypes) {
            if ([dt objectForKey:@"cid"]) {
                NSNumber *this_id = [dt objectForKey:@"id"];
                if ([this_id integerValue] == [self.type_id integerValue]) {
                    if ([[dt objectForKey:@"cid"] isEqualToString:@"profile"] || [[dt objectForKey:@"cid"] isEqualToString:@"individual"] || [[dt objectForKey:@"cid"] isEqualToString:@"place"]) {
                        return YES;
                    }
                    break;
                }
            }
        }
    }
    
    return NO;
}

- (void)deleteLocal
{
    [self delete];
}

- (void)unpackDictionary:(NSDictionary *)dictionary {
    [super unpackDictionary:dictionary];
    self.remoteID = [Common sanitizedNumber:[dictionary valueForKey:@"id"]];
    self.type_id = [Common sanitizedNumber:[dictionary valueForKey:@"type_id"]];
    self.name = [Common sanitizedString:[dictionary objectForKey:@"name"]];
    self.cid = [Common sanitizedString:[dictionary objectForKey:@"cid"]];
    self.image_url = [Common sanitizedString:[dictionary objectForKey:@"image_url"]];
    self.image_thumb_url = [Common sanitizedString:[dictionary objectForKey:@"thumb_url"]];
    self.base_color = [Common sanitizedString:[dictionary objectForKey:@"base_color"]];
    self.updated_at = [Common sanitizedDate:[dictionary objectForKey:@"updated_at"]];
    self.created_at = [Common sanitizedDate:[dictionary objectForKey:@"created_at"]];
    if ([dictionary objectForKey:@"owner"]) {
        NSDictionary *owner = [dictionary objectForKey:@"owner"];
        self.owner_id = [owner valueForKey:@"id"];
    }
    
    if ([dictionary objectForKey:@"related_deck_id"]) {
        Deck *related = [Deck where:@{@"remoteID":[dictionary objectForKey:@"related_deck_id"]}].firstObject;
        if (related) {
            self.related_deck = related;
        }
    }
    
    [self removeCard_comments:self.card_comments];
    if ([dictionary objectForKey:@"comments"] && [[dictionary objectForKey:@"comments"] isKindOfClass:[NSArray class]]) {
        NSArray *comments = [dictionary objectForKey:@"comments"];
        //NSArray *all_decks = [Deck all];
        for (NSDictionary *comment in comments) {
            NSNumber *comment_id = [comment objectForKey:@"id"];
            CardComment *c = [CardComment where:@{@"remoteID":comment_id}].firstObject;
            if (c) {
                [c unpackDictionary:comment];
                [c setCard:self];
                [c save];
                [self addCard_commentsObject:c];
            } else {
                CardComment *c = [CardComment create];
                [c unpackDictionary:comment];
                [c setCard:self];
                [c save];
                [self addCard_commentsObject:c];
            }
        }
    }
    

    [self removeDecks:self.decks];
    if ([dictionary objectForKey:@"decks"] && [[dictionary objectForKey:@"decks"] isKindOfClass:[NSArray class]]) {
        NSArray *decks = [dictionary objectForKey:@"decks"];
        //NSArray *all_decks = [Deck all];
        for (NSNumber *deck_id in decks) {
            Deck *d = [Deck where:@{@"remoteID":deck_id}].firstObject;
            if (d) {
                [self addDecksObject:d];
            }
        }
    }
    
    NSMutableArray *all_card_fields = NSMutableArray.new;

    if ([dictionary objectForKey:@"card_fields"] && [[dictionary objectForKey:@"card_fields"] isKindOfClass:[NSArray class]]) {
        NSArray *fields = [dictionary objectForKey:@"card_fields"];
        //NSArray *all_decks = [Deck all];
        for (NSDictionary *field in fields) {
                        
            CardField *existing_field = [CardField where:@{@"guid":[field objectForKey:@"guid"]}].firstObject;
            if (existing_field) {
                [existing_field unpackDictionary:field];
                existing_field.card = self;
                [existing_field save];
                [all_card_fields addObject:existing_field];
            } else {
                CardField *existing_field = [CardField myNew];
                [existing_field unpackDictionary:field];
                existing_field.card = self;
                [existing_field save];
                [self addCard_fieldsObject:existing_field];
                [all_card_fields addObject:existing_field];
            }

            
            if ([field objectForKey:@"card_images"]) {
                for (NSDictionary *image in [field objectForKey:@"card_images"]) {
                    CardImage *existing = [CardImage where:@{@"guid":[image objectForKey:@"guid"]}].firstObject;
                    if (existing) {
                        [existing unpackDictionary:image];
                        existing.cardfield = existing_field;
                        [existing save];
                        if (![existing_field.cardimages containsObject:existing]) {
                            [existing_field addCardimagesObject:existing];
                        }
                    } else {
                        CardImage *existing = [CardImage myNew];
                        [existing unpackDictionary:image];
                        existing.cardfield = existing_field;
                        [existing_field addCardimagesObject:existing];
                        [existing save];
                    }
                }
            }
        }
    }
    
    [self pruneCardFields:self.card_fields withExisting:all_card_fields];
    
    
}

- (NSDictionary *)syncParams {
    
    
    NSMutableArray *triggers = NSMutableArray.new;
    for (CardTrigger *t in [[self triggers] allObjects]) {
        NSMutableDictionary *trig = NSMutableDictionary.new;
        if (t.remoteID) {
            [trig setObject:t.remoteID forKey:@"id"];
        }
        
        [trig setObject:t.value forKey:@"value"];
        [trig setObject:t.action_verb forKey:@"verb"];
        [trig setObject:t.trigger_type forKey:@"trigger_type"];
        
        
        [triggers addObject:trig];
    }
    
    NSMutableArray *fields = NSMutableArray.new;
    for (CardField *f in [[self card_fields] allObjects]) {
        NSMutableDictionary *field = NSMutableDictionary.new;
        if (f.remoteID) {
            [field setObject:f.remoteID forKey:@"id"];
        }
        
        [field setObject:f.field_value forKey:@"field_value"];
        [field setObject:f.title forKey:@"title"];
        [field setObject:f.field_cid forKey:@"field_cid"];
        [field setObject:f.position forKey:@"position"];
        [field setObject:f.on_front forKey:@"on_front"];
        [field setObject:f.guid forKey:@"guid"];
        
        if (f.card_type_field_id) {
            [field setObject:f.card_type_field_id forKey:@"card_type_field_id"];
        }
        
        [fields addObject:field];
    }
    
    NSMutableArray *decks = NSMutableArray.new;
    for (Deck *f in [[self decks] allObjects]) {
        if (f.remoteID) {
            [decks addObject:f.remoteID];
        }
    }
    
    NSMutableDictionary *params = [[[NSDictionary alloc] initWithObjectsAndKeys:
                            
                            self.remoteID, @"card[id]",
                            self.cid, @"card[cid]",
                            self.name, @"card[name]",
                            nil] mutableCopy];
    
    [params setObject:[Common nilSafe:self.base_color] forKey:@"card[base_color]"];
    [params setObject:[Common nilSafe:self.owner_id] forKey:@"card[owner_id]"];
    [params setObject:[Common nilSafe:self.creator_id] forKey:@"card[creator_id]"];
    [params setObject:[Common nilSafe:self.type_id] forKey:@"card[type_id]"];
    [params setObject:triggers forKey:@"card[triggers]"];
    [params setObject:fields forKey:@"card[card_fields]"];
    [params setObject:decks forKey:@"card[deck_ids]"];
    
    return params;
}

- (void)pruneCardFields:(NSArray *)allFields withExisting:(NSMutableArray *)existingFields
{
    NSMutableArray *remove_these = NSMutableArray.new;
    
    for (CardField *cf in allFields) {
        if (![existingFields containsObject:cf]) {
            [remove_these addObject:cf];
            [cf delete];
        }
    }
    
    [self removeCard_fields:[NSSet setWithArray:remove_these]];

}

- (NSString *)generateGUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    return (__bridge NSString *)string;
}

- (void)setCroppedImageObj:(UIImage*)image
{
    [self willChangeValueForKey:@"image_cropped"];
    
    NSData *data = UIImagePNGRepresentation(image);
    [self setImage_cropped:data];
    [self setPrimitiveValue:data forKey:@"image_cropped"];
    [self didChangeValueForKey:@"image_cropped"];
}

- (UIImage*)croppedImageObj
{
    [self willAccessValueForKey:@"image_cropped"];
    UIImage *image = [UIImage imageWithData:[self primitiveValueForKey:@"image_cropped"]];
    [self didAccessValueForKey:@"image_cropped"];
    return image;
}

- (void)setImageObj:(UIImage*)image
{
    [self willChangeValueForKey:@"image"];
    
    NSData *data = UIImagePNGRepresentation(image);
    [self setImage:data];
    [self setPrimitiveValue:data forKey:@"image"];
    [self didChangeValueForKey:@"image"];
}

- (void)createTriggerWithDetails:(NSDictionary *)triggerDefaults andValue:(NSString *)value
{
    NSArray *existing_triggers = [self.triggers allObjects];
    if (existing_triggers) {
        BOOL exists = NO;
        for (CardTrigger *trigger in existing_triggers) {
            if (trigger.trigger_type == [triggerDefaults objectForKey:@"trigger_type"]) {
                exists = YES;
                if ([triggerDefaults objectForKey:@"verb"] && ![[triggerDefaults objectForKey:@"verb"] isEqualToString:@""]) {
                    trigger.action_verb = [triggerDefaults objectForKey:@"verb"];
                }
                [trigger setValue:value];
                [trigger save];
            }
        }
        if (!exists) {
            CardTrigger *newTrig = [CardTrigger create];
            newTrig.card = self;
            newTrig.trigger_type = [triggerDefaults objectForKey:@"trigger_type"];
            newTrig.action_verb = [triggerDefaults objectForKey:@"verb"];
            newTrig.name = [triggerDefaults objectForKey:@"name"];
            newTrig.value = value;
            [newTrig save];
        }
    }
}

- (UIImage*)imageObj
{
    [self willAccessValueForKey:@"image"];
    UIImage *image = [UIImage imageWithData:[self primitiveValueForKey:@"image"]];
    [self didAccessValueForKey:@"image"];
    return image;
}

- (void)addFieldWithDetails:(NSDictionary *)fieldDetails isFront:(BOOL)isFront
{
    CardField *newField = nil;
    BOOL isNew = NO;
    if ([fieldDetails objectForKey:@"existing_field"]) {
        newField = [fieldDetails objectForKey:@"existing_field"];
    } else {
        newField = [CardField myNew];
        isNew = YES;
    }
    
    newField.card = self;
    newField.field_cid = [fieldDetails objectForKey:@"cid"];
    newField.field_value = [fieldDetails objectForKey:@"field_value"];
    newField.title = [fieldDetails objectForKey:@"field_title"];
    newField.has_title = [fieldDetails objectForKey:@"has_title"];
    newField.value_type = [fieldDetails objectForKey:@"value_type"];
    if (isFront) {
        newField.on_front = [NSNumber numberWithInt:1];
        newField.position = [self nextFieldPosition:YES];
    } else {
        newField.on_front = [NSNumber numberWithInt:0];
        newField.position = [self nextFieldPosition:NO];
    }
    newField.guid = [self generateGUID];
    [newField save];
    
    if ([fieldDetails objectForKey:@"card_images"]) {
        for (CardImage *ci in [fieldDetails objectForKey:@"card_images"]) {
            ci.cardfield = newField;
            [ci save];
        }
    }
    
    if (isNew) {
        [self addCard_fieldsObject:newField];
    }
}

- (NSString *)currentDecksAsString
{
    if ([self.decks allObjects].count > 0) {
        if ([self.decks allObjects].count > 1) {
            return [NSString stringWithFormat:@"%lu Decks", (unsigned long)[self.decks allObjects].count];
        } else {
            Deck *d = [[self.decks allObjects] objectAtIndex:0];
            return d.name;
        }
    } else {
        return @"";
    }
}

- (NSArray *)cardBackFields
{
    NSMutableArray *fields = NSMutableArray.new;
    
    NSArray *f = [[self.card_fields allObjects] sortBy:@"position"];
    
    for (CardField *cf in f) {
        if ([cf.on_front integerValue] == 0 && [cf.position integerValue] != 0) {
            [fields addObject:cf];
        }
    }
    
    return fields;
}

- (NSArray *)cardFrontFields
{
    NSMutableArray *fields = NSMutableArray.new;
    
    NSArray *f = [[self.card_fields allObjects] sortBy:@"position"];
    
    for (CardField *cf in f) {
        if ([cf.on_front integerValue] == 1) {
            [fields addObject:cf];
        }
    }
    
    return fields;
}

- (NSNumber *)nextFieldPosition:(BOOL)isFront
{
    if (isFront) {
        CardField *lastField = [self cardFrontFields].lastObject;
        if (lastField) {
            int position = [lastField.position integerValue];
            return @(position + 1);
        } else {
            return @1;
        }
    } else {
        CardField *lastField = [self cardBackFields].lastObject;
        if (lastField) {
            int position = [lastField.position integerValue];
            return @(position + 1);
        } else {
            return @1;
        }
    }
    
    return @1;
}

- (BOOL)hasCoverPhoto
{
    if (self.type_id) {
        NSArray *internalCardTypes = [[DeckManager sharedManager] localCardTypes:nil];
        
        for (NSDictionary *dt in internalCardTypes) {
            if ([dt objectForKey:@"cid"]) {
                NSNumber *this_id = [dt objectForKey:@"id"];
                if ([this_id integerValue] == [self.type_id integerValue]) {
                    if ([[dt objectForKey:@"cid"] isEqualToString:@"photo_album"]) {
                        return NO;
                    }
                    break;
                }
            }
        }
    }
    
    return YES;
}

- (NSMutableArray *)sortedComments
{
    NSMutableArray *comments = NSMutableArray.new;
    
    NSArray *f = [[self.card_comments allObjects] sortBy:@"comment_date"];
    
    for (CardComment *cc in f) {
        if ([cc.is_active integerValue] == 1 && [cc.is_deleted integerValue] != 1) {
            [comments addObject:cc];
        }
    }
    
    return comments;
}

- (NSDictionary *)prepareForContacts
{
    NSMutableDictionary *dict = NSMutableDictionary.new;
    
    NSArray *name_parts = [self.name componentsSeparatedByString:@" "];
    
    if (name_parts.count > 1) {
        [dict setObject:[name_parts objectAtIndex:0] forKey:@"first_name"];
        [dict setObject:[name_parts objectAtIndex:1] forKey:@"last_name"];
    } else if (name_parts.count == 1) {
        [dict setObject:[name_parts objectAtIndex:0] forKey:@"first_name"];
    }
    
    for (CardField *c in [self cardFrontFields]) {
        if ([c fieldIsEmail]) {
            [dict setObject:c.field_value forKey:@"email"];
        }
        if ([c fieldIsHomePhone]) {
            [dict setObject:c.field_value forKey:@"home_phone"];
        }
        if ([c fieldIsMobilePhone]) {
            [dict setObject:c.field_value forKey:@"mobile_phone"];
        }
        if ([c fieldIsBusinessPhone]) {
            [dict setObject:c.field_value forKey:@"business_phone"];
        }
    }
    
    for (CardTrigger *t in [[self triggers] allObjects]) {
        if ([t.trigger_type isEqualToString:@"phone"]) {
            [dict setObject:t.value forKey:@"home_phone"];
        }
        
        if ([t.trigger_type isEqualToString:@"email"]) {
            [dict setObject:t.value forKey:@"email"];
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

- (BOOL)hasBackWebContent
{
    for (CardField *c in [self cardBackFields]) {
        if ([c fieldIsWebContent]) {
            return YES;
        }
    }
    
    return NO;
}
        

@end
