//
//  Account.h
//  Inadek
//
//  Created by Josh Lehman on 1/21/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SSDataKit/SSDataKit.h>

@class Deck;

@interface Account : SSRemoteManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * remoteID;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *owned_decks;
@property (nonatomic, retain) NSSet *favorited_decks;
@end

@interface Account (CoreDataGeneratedAccessors)

- (void)addOwned_decksObject:(Deck *)value;
- (void)removeOwned_decksObject:(Deck *)value;
- (void)addOwned_decks:(NSSet *)values;
- (void)removeOwned_decks:(NSSet *)values;

- (void)addFavorited_decksObject:(Deck *)value;
- (void)removeFavorited_decksObject:(Deck *)value;
- (void)addFavorited_decks:(NSSet *)values;
- (void)removeFavorited_decks:(NSSet *)values;

@end
