//
//  AccountNotification+CoreDataProperties.m
//  Inadek
//
//  Created by Josh Lehman on 2/27/16.
//  Copyright © 2016 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AccountNotification+CoreDataProperties.h"

@implementation AccountNotification (CoreDataProperties)

@dynamic remoteID;
@dynamic account_id;
@dynamic message;
@dynamic title;
@dynamic destination;
@dynamic card;
@dynamic deck;
@dynamic is_read;
@dynamic sent_at;
@dynamic card_id;
@dynamic deck_id;
@dynamic card_comment_id;

@end
