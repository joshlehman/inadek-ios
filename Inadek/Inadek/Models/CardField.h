//
//  CardField.h
//  Inadek
//
//  Created by Josh Lehman on 4/4/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SSDataKit/SSDataKit.h>

@class Card, CardImage;

@interface CardField : SSRemoteManagedObject

@property (nonatomic, retain) NSNumber * remoteID;
@property (nonatomic, retain) NSString * field_cid;
@property (nonatomic, retain) NSString * field_value;
@property (nonatomic, retain) NSString * default_title;
@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * on_front;
@property (nonatomic, retain) NSNumber * has_title;
@property (nonatomic, retain) NSNumber * card_type_field_id;
@property (nonatomic, retain) NSString * placeholder;
@property (nonatomic, retain) NSString * value_type;
@property (nonatomic, retain) Card *card;
@property (nonatomic, retain) NSSet *cardimages;

@property (nonatomic, retain) NSString * guid;

- (void)addCardimagesObject:(CardImage *)object;
- (void)removeCardimagesObject:(CardImage *)object;
- (void)addCardimages:(NSSet *)objects;
- (void)removeCardimages:(NSSet *)objects;

@end
