//
//  AccountNotification+CoreDataProperties.h
//  Inadek
//
//  Created by Josh Lehman on 2/27/16.
//  Copyright © 2016 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AccountNotification.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountNotification (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *remoteID;
@property (nullable, nonatomic, retain) NSNumber *account_id;
@property (nullable, nonatomic, retain) NSString *message;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *destination;
@property (nullable, nonatomic, retain) NSNumber *is_read;
@property (nullable, nonatomic, retain) NSDate *sent_at;
@property (nullable, nonatomic, retain) NSNumber *card_id;
@property (nullable, nonatomic, retain) NSNumber *deck_id;
@property (nullable, nonatomic, retain) NSNumber *card_comment_id;
@property (nullable, nonatomic, retain) Card *card;
@property (nullable, nonatomic, retain) Deck *deck;

@end

NS_ASSUME_NONNULL_END
