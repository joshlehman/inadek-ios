//
//  CardField.m
//  Inadek
//
//  Created by Josh Lehman on 4/4/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardField.h"
#import "Card.h"


@implementation CardField

@dynamic remoteID;
@dynamic field_cid;
@dynamic field_value;
@dynamic position;
@dynamic default_title;
@dynamic title;
@dynamic on_front;
@dynamic placeholder;
@dynamic card;
@dynamic has_title;
@dynamic value_type;
@dynamic cardimages;
@dynamic card_type_field_id;

@dynamic guid;

@end
