//
//  Deck+Core.h
//  Inadek
//
//  Created by Josh Lehman on 1/12/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <ObjectiveRecord/ObjectiveRecord.h>
#import "Deck.h"
#import "DeckTrigger+Core.h"

@interface Deck (Core)

+ (Deck *)myNew;

- (void)unpackDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)syncParams;
- (BOOL)isValid;
- (BOOL)isMine;
- (void)deleteLocal;

- (NSString *)nameString;
- (NSURL *)get_shareUrl;
- (NSString *)get_shareText;

// Boolean access methods
- (BOOL)is_deck_private;
- (BOOL)is_deck_popular;
- (BOOL)is_deck_favorite;
- (BOOL)is_deck_followed;

- (void)set_is_favorite:(BOOL)favorite;
- (void)set_is_followed:(BOOL)followed;

- (void)setImageObj:(UIImage*)image;
- (UIImage*)imageObj;
- (void)setCroppedImageObj:(UIImage*)image;
- (UIImage*)croppedImageObj;

- (void)createTriggerWithDetails:(NSDictionary *)triggerDefaults andValue:(NSString *)value;

- (void)debug;
+ (void)logObjectCounts:(NSString *)cid name:(NSString *)name;
+ (void) debugObject:(id)aObject;

- (NSString *) cardCountAsString;
- (NSMutableArray *) orderedCards;

@end
