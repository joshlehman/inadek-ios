//
//  DeckTrigger+Core.m
//  Inadek
//
//  Created by Josh Lehman on 1/21/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardTrigger+Core.h"
#import "Common.h"

@implementation CardTrigger (Core)

- (void)unpackDictionary:(NSDictionary *)dictionary {
    [super unpackDictionary:dictionary];
    self.remoteID = [dictionary objectForKey:@"id"];
    self.trigger_type = [Common sanitizedString:[dictionary objectForKey:@"trigger_type"]];
    self.value = [Common sanitizedString:[dictionary objectForKey:@"value"]];
    self.action_verb = [Common sanitizedString:[dictionary objectForKey:@"verb"]];
    self.name = [Common sanitizedString:[dictionary objectForKey:@"name"]];
    
    if (self.name == nil) {
        self.name = @"";
    }
}

- (BOOL)is_url
{
    if (self.trigger_type && [self.trigger_type isEqualToString:@"web"]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)is_phone
{
    if (self.trigger_type && [self.trigger_type isEqualToString:@"phone"]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)is_email
{
    if (self.trigger_type && [self.trigger_type isEqualToString:@"email"]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)is_map
{
    if (self.trigger_type && [self.trigger_type isEqualToString:@"map"]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)deleteFromCard:(Card *)card
{
    [card removeTriggersObject:self];
}

@end
