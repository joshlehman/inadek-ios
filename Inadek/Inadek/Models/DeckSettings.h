//
//  DeckSettings.h
//  Inadek
//
//  Created by Jason Preissinger on 1/3/13.
//  Copyright (c) 2013 Jason Preissinger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeckSettings : NSObject
{
    
}

+(NSArray *)getHomeViewPages;

+(NSArray *)getGenericCardFields;
+(NSArray *)getPersonalCardFields:(int)side;
+(NSArray *)getEditableCardTypes;
+(NSArray *)getEditableCardTypesByDeckType:(NSString *) deckType;
+(NSArray *)getCardFieldsByCardType:(int) cardType;

+(NSDictionary *)cardTypeDictionary;
+(NSDictionary *)cardImageDictionary;
+(NSDictionary *)cardTitleDictionary;
+(NSDictionary *)cardCaptionDictionary;
+(NSDictionary *)cardCommentDictionary;
+(NSDictionary *)cardPhoneNumberDictionary;
+(NSDictionary *)cardEmailAddressDictionary;
+(NSDictionary *)cardTwitterNameDictionary;

+(NSArray *)getAccountManagementPasswordFields;
+(NSArray *)getAccountManagementNoPasswordFields;
+(NSDictionary *)accountNameDictionary;
+(NSDictionary *)accountEmailDictionary;
+(NSDictionary *)accountPasswordDictionary;
+(NSDictionary *)accountPasswordConfirmationDictionary;
+(NSDictionary *)accountChangePasswordDictionary;
@end
