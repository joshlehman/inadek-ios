//
//  AccountNotification.m
//  Inadek
//
//  Created by Josh Lehman on 2/27/16.
//  Copyright © 2016 Inadek. All rights reserved.
//

#import "AccountNotification.h"
#import "Card.h"
#import "Deck.h"

#import "Common.h"

@implementation AccountNotification

// Insert code here to add functionality to your managed object subclass

+ (AccountNotification *)createWithData:(NSDictionary *)data
{
    AccountNotification *existing = [AccountNotification where:@{@"remoteID":[Common sanitizedNumber:[data valueForKey:@"id"]]}].firstObject;
    
    if (!existing) {
        existing = [AccountNotification create];
    }
    
    [existing unpackDictionary:data];
    [existing save];
    
    [[InadekAPIManager sharedManager] fetchNotificationObjects:existing callback:^(id responseObject) {
        
        if (responseObject && ([responseObject count] > 0) && [[responseObject objectAtIndex:0] isKindOfClass:[Deck class]]) {
            existing.deck = [responseObject objectAtIndex:0];
        }
        
        if (existing.card_id && [existing.card_id integerValue] != 0) {
            Card *thisCard = [Card where:@{@"remoteID":existing.card_id}].firstObject;
            if (thisCard) {
                existing.card = thisCard;
            }
            [existing save];
        } else if (existing.deck) {
            [existing save];
        }
        
        DLog(@"Gathered up some new stuff");
    } failureCallback:^(id responseObject) {
        DLog(@"Failed miserably");
    }];
    
    return existing;
}

+ (NSArray *)allNotifications
{
    NSArray *notifications = [AccountNotification allWithOrder:@"sent_at DESC"];
    return notifications;
}

+ (NSNumber *)unreadNotificationCount
{
    NSArray *notifs = [AccountNotification where:@{@"is_read":@0}];
    
    if (notifs) {
        return [NSNumber numberWithInt:[notifs count]];
    } else {
        return [NSNumber numberWithInt:0];
    }
}

- (void)unpackDictionary:(NSDictionary *)dictionary {
    
    [super unpackDictionary:dictionary];
    self.remoteID = [Common sanitizedNumber:[dictionary valueForKey:@"id"]];
    self.message = [Common sanitizedString:[dictionary valueForKey:@"notice_text"]];
    self.title = [Common sanitizedString:[dictionary objectForKey:@"notice_title"]];
    self.destination = [Common sanitizedString:[dictionary objectForKey:@"destination"]];
    self.is_read = [Common sanitizedBool:[dictionary objectForKey:@"is_read"]];
    self.sent_at = [Common sanitizedDate:[dictionary objectForKey:@"sent_at"]];
    self.card_id = [Common sanitizedNumber:[dictionary valueForKey:@"card_id"]];
    self.deck_id = [Common sanitizedNumber:[dictionary valueForKey:@"deck_id"]];
    self.card_comment_id = [Common sanitizedNumber:[dictionary valueForKey:@"card_comment_id"]];
    
}

@end
