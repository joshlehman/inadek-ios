//
//  DeckCategory.h
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SSDataKit/SSDataKit.h>

@class Deck;

@interface DeckCategory : SSRemoteManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * cid;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSNumber * remoteID;
@property (nonatomic, retain) NSNumber * status_id;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSNumber * is_featured;
@property (nonatomic, retain) NSNumber * is_default;
@property (nonatomic, retain) NSSet *decks;

- (void)addDecksObject:(Deck *)value;
- (void)removeDecksObject:(Deck *)value;
- (void)addDecks:(NSSet *)values;
- (void)removeDecks:(NSSet *)values;

@end
