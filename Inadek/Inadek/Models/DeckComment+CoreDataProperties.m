//
//  DeckComment+CoreDataProperties.m
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DeckComment+CoreDataProperties.h"

@implementation DeckComment (CoreDataProperties)

@dynamic is_active;
@dynamic is_deleted;
@dynamic name;
@dynamic comment;
@dynamic comment_date;
@dynamic remoteID;
@dynamic deck;

@end
