//
//  DeckComment.h
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Deck;

NS_ASSUME_NONNULL_BEGIN

@interface DeckComment : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "DeckComment+CoreDataProperties.h"
