//
//  AccountNotification.h
//  Inadek
//
//  Created by Josh Lehman on 2/27/16.
//  Copyright © 2016 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SSDataKit/SSDataKit.h>
#import <ObjectiveRecord/ObjectiveRecord.h>

@class Card, Deck;

NS_ASSUME_NONNULL_BEGIN

@interface AccountNotification : SSRemoteManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (AccountNotification *)createWithData:(NSDictionary *)data;
+ (NSNumber *)unreadNotificationCount;
+ (NSArray *)allNotifications;

@end

NS_ASSUME_NONNULL_END

#import "AccountNotification+CoreDataProperties.h"
