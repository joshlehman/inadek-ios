//
//  DeckSettings.m
//  Inadek
//
//  Created by Jason Preissinger on 1/3/13.
//  Copyright (c) 2013 Jason Preissinger. All rights reserved.
//

#import "DeckSettings.h"

#define kDefaultEntryFieldHeight    66.0
#define kDefaultAddressFieldHeight  136.0

@implementation DeckSettings

+(NSArray *)getHomeViewPages{
    return [NSArray arrayWithObjects:
            @{@"id":[NSNumber numberWithInt:kPopularDeckTab], @"name":@"Home",@"url":@"/api/v1/decks"},
            @{@"id":[NSNumber numberWithInt:kFavoriteDeckTab], @"name":@"My Favorite Decks", @"url":@"/api/v1/decks/following"},
            @{@"id":[NSNumber numberWithInt:kOwnedDeckTab], @"name":@"My Decks", @"url": @"/api/v1/decks/owned"},
            nil];
}

+(NSArray *)getGenericCardFields{
    return [NSArray arrayWithObjects:
            [self cardImageDictionary],
            [self cardTitleDictionary],
            [self cardCaptionDictionary],
            [self cardCommentDictionary],
            nil];
    // [self cardTypeDictionary],

}

#pragma mark - Fields: Personal Card
+(NSArray *)getPhotoCardFields:(int)side {
    
    switch (side) {
        case 1: // Front
        {
            return [NSArray arrayWithObjects:
                    [self cardImageDictionary],
                    [self getDivider:@"front"],
                    [self cardTitleDictionary:@"PHOTO TITLE"],
                    [self cardCaptionDictionary],
                    nil];
        }
            break;
        case 2: // Back
        {
            return [NSArray arrayWithObjects:
                    [self getDivider:@"back"],
                    [self customFieldDictionary:@"NOTES" withArea:@"textArea" andSaveAs:@"notes"],
                    nil];
        }
            break;
        default:
            return nil;
    }
}

#pragma mark - Fields: Personal Card
+(NSArray *)getPersonalCardFields:(int)side {
    
    switch (side) {
        case 1: // Front
        {
            return [NSArray arrayWithObjects:
                    [self cardImageDictionary],
                    [self getDivider:@"front"],
                    [self cardTitleDictionary],
                    [self cardCaptionDictionary],
                    nil];
        }
            break;
        case 2: // Back
        {
            return [NSArray arrayWithObjects:
                    [self getDivider:@"back"],
                    [self cardPhoneNumberDictionary],
                    [self cardEmailAddressDictionary],
                    [self customFieldDictionary:@"RELATIONSHIP STATUS" withArea:@"picker" withDefaultValue:@"- Select -" withOptions:@[@"Single",@"Married",@"Divorced",@"Widowed"] withKeyboardType:nil andSaveAs:@"relationship_status"],
                    [self cardWebsiteDictionary],
                    [self cardTwitterNameDictionary],
                    [self cardPersonalStreetAddressDictionary],
                    [self cardBioDictionary],
                    nil];
        }
            break;
        default:
            return nil;
    }
}

#pragma mark - Fields: Profile Card
+(NSArray *)getProfileCardFields:(int)side {
    
    switch (side) {
        case 1: // Front
        {
            return [NSArray arrayWithObjects:
                    [self cardImageDictionary],
                    [self getDivider:@"front"],
                    [self cardTitleDictionary:@"YOUR NAME"],
                    [self cardCaptionDictionary],
                    nil];
        }
            break;
        case 2: // Back
        {
            return [NSArray arrayWithObjects:
                    [self getDivider:@"back"],
                    [self cardPhoneNumberDictionary],
                    [self cardEmailAddressDictionary],
                    [self customFieldDictionary:@"RELATIONSHIP STATUS" withArea:@"picker" withDefaultValue:@"- Select -" withOptions:@[@"Single",@"Married",@"Divorced",@"Widowed"] withKeyboardType:nil andSaveAs:@"relationship_status"],
                    [self cardWebsiteDictionary],
                    [self cardTwitterNameDictionary],
                    [self cardPersonalStreetAddressDictionary],
                    [self cardBioDictionary],
                    nil];
        }
            break;
        default:
            return nil;
    }
}

#pragma mark - Fields: Business Profile Card
+(NSArray *)getBusinessProfileCardFields:(int)side {
    
    switch (side) {
        case 1: // Front
        {
            return [NSArray arrayWithObjects:
                    [self cardImageDictionary],
                    [self getDivider:@"front"],
                    [self cardTitleDictionary:@"YOUR NAME"],
                    [self cardCaptionDictionary:@"YOUR TITLE"],
                    nil];
        }
            break;
        case 2: // Back
        {
            return [NSArray arrayWithObjects:
                    [self getDivider:@"back"],
                    [self cardPhoneNumberDictionary],
                    [self cardEmailAddressDictionary],
                    [self cardWebsiteDictionary],
                    [self cardTwitterNameDictionary],
                    [self cardBusinessStreetAddressDictionary],
                    [self cardAboutDictionary],
                    nil];
        }
            break;
        default:
            return nil;
    }
}


#pragma mark - Sports: Player Card
+(NSArray *)getSportsPlayerCardFields:(int)side {
    
    switch (side) {
        case 1: // Front
        {
            return [NSArray arrayWithObjects:
                    [self cardImageDictionary],
                    [self getDivider:@"front"],
                    [self cardTitleDictionary:@"PLAYER NAME"],
                    [self cardCaptionDictionary:@"POSITION"],
                    nil];
        }
            break;
        case 2: // Back
        {
            return [NSArray arrayWithObjects:
                    [self getDivider:@"back"],
                    [self cardPhoneNumberDictionary],
                    [self cardEmailAddressDictionary],
                    [self cardTwitterNameDictionary],
                    [self cardBioDictionary],
                    [self customFieldDictionary:@"MY FAVORITE PRO TEAM" andSaveAs:@"favorite_pro_team"],
                    [self customFieldDictionary:@"FAVORITE COACH" andSaveAs:@"favorite_coach"],
                    [self customFieldDictionary:@"FAVORITE BAND" andSaveAs:@"favorite_band"],
                    [self customFieldDictionary:@"BEST ADVICE" withArea:@"textArea" andSaveAs:@"best_advice"],
                    nil];
        }
            break;
        default:
            return nil;
    }
}

#pragma mark - Sports: Team Card
+(NSArray *)getSportsTeamCardFields:(int)side {
    
    switch (side) {
        case 1: // Front
        {
            return [NSArray arrayWithObjects:
                    [self cardImageDictionary],
                    [self getDivider:@"front"],
                    [self cardTitleDictionary:@"TEAM NAME"],
                    [self cardCaptionDictionary:@"TEAM SLOGAN"],
                    nil];
        }
            break;
        case 2: // Back
        {
            return [NSArray arrayWithObjects:
                    [self getDivider:@"back"],
                    [self cardPhoneNumberDictionary],
                    [self cardEmailAddressDictionary:@"E-MAIL LIST"],
                    [self cardTwitterNameDictionary],
                    [self cardBioDictionary:@"ABOUT THE TEAM"],
                    nil];
        }
            break;
        default:
            return nil;
    }
}

#pragma mark - Fields: Business Card
+(NSArray *)getBusinessCardFields:(int)side {
    
    switch (side) {
        case 1: // Front
        {
            return [NSArray arrayWithObjects:
                    [self cardImageDictionary],
                    [self getDivider:@"front"],
                    [self cardBusinessNameDictionary],
                    [self cardCaptionDictionary:@"TAGLINE"],
                    nil];
        }
            break;
        case 2: // Back
        {
            return [NSArray arrayWithObjects:
                    [self getDivider:@"back"],
                    [self cardWebsiteDictionary],
                    [self cardBusinessStreetAddressDictionary],
                    [self cardPhoneNumberDictionary],
                    [self cardAboutDictionary],
                    nil];
        }
            break;
        default:
            return nil;
    }
}

#pragma mark - Fields: Sports Cards
+(NSArray *)getSportsCardFields:(int)side {
    
    switch (side) {
        case 1: // Front
            {
                return [NSArray arrayWithObjects:
                        [self cardImageDictionary],
                        [self cardTitleDictionary],
                        [self cardCaptionDictionary],
                        nil];
            }
            break;
        case 2: // Back
            {
                return [NSArray arrayWithObjects:
                        [self cardPhoneNumberDictionary],
                        [self cardTwitterNameDictionary],
                        [self cardPersonalStreetAddressDictionary],
                        [self cardBioDictionary],
                        nil];
            }
            break;
        default:
            return nil;
    }
}



+(NSArray *)getDeckinDeckFields:(NSString *)coverSize {
    
    if([coverSize isEqualToString:@"full"]) {
        
        return [NSArray arrayWithObjects:
                [self cardImageDictionary],
                [self deckTitleDictionary],
                nil];
        
        //                [self userAvailableDecksDictionary],
        
    } else if([coverSize isEqualToString:@"promotional"]) {
        
    } else {
        return [NSArray arrayWithObjects:
                [self cardImageDictionary],
                [self deckTitleDictionary],
                nil];
    }
    
    return nil;
}

+(NSDictionary *)getDivider:(NSString *)side
{
    NSString *display = @"FRONT OF CARD";
    if ([side isEqualToString:@"back"]) {
        display = @"BACK OF CARD";
    }
    
    return @{@"displayName":display,
             @"fieldType":@"divider", @"fieldHeight":[NSNumber numberWithFloat:23]};

}

+(NSArray *)getCardFieldsByCardType:(int)cardType
{
    NSMutableArray *fields = [[NSMutableArray alloc] init];
    switch (cardType) {
        case kCardProfile:
            {
                [fields addObjectsFromArray:[self getProfileCardFields:1]];
                [fields addObjectsFromArray:[self getProfileCardFields:2]];
                return fields;
            }
            
            break;
        case kCardBusinessProfile:
        {
            [fields addObjectsFromArray:[self getBusinessProfileCardFields:1]];
            [fields addObjectsFromArray:[self getBusinessProfileCardFields:2]];
            return fields;
        }
            
            break;
        case kCardPersonalInfo:
            {
                [fields addObjectsFromArray:[self getPersonalCardFields:1]];
                [fields addObjectsFromArray:[self getPersonalCardFields:2]];
                return fields;
            }
            
            break;
        case kCardBusinessInfo:
            {
                [fields addObjectsFromArray:[self getBusinessCardFields:1]];
                [fields addObjectsFromArray:[self getBusinessCardFields:2]];
                return fields;
            }
            
            break;
        case kCardAdvertisementFullImage:
            return [self getDeckinDeckFields:@"full"];
            break;
        case kCardDeckHalfImage:
            return [self getDeckinDeckFields:@"half"];
            break;
        case kCardDeckPromotion:
            return [self getDeckinDeckFields:@"promotional"];
            break;
        case kCardSportsPlayer:
            {
                [fields addObjectsFromArray:[self getSportsPlayerCardFields:1]];
                [fields addObjectsFromArray:[self getSportsPlayerCardFields:2]];
                return fields;
            }
            
            break;
        case kCardSportsTeam:
        {
            [fields addObjectsFromArray:[self getSportsTeamCardFields:1]];
            [fields addObjectsFromArray:[self getSportsTeamCardFields:2]];
            return fields;
        }
            
            break;
        case kCardPhoto:
            [fields addObjectsFromArray:[self getPhotoCardFields:1]];
            [fields addObjectsFromArray:[self getPhotoCardFields:2]];
            
            return fields;
            break;
        default:
            return self.getGenericCardFields;
            break;
    }
}


+(NSDictionary *)customFieldDictionary:(NSString *)title withArea:(NSString *)area andSaveAs:(NSString *)saveAs{
    return [self customFieldDictionary:title withArea:area withDefaultValue:nil withOptions:nil withKeyboardType:nil andSaveAs:saveAs];
}

+(NSDictionary *)customFieldDictionary:(NSString *) title withArea:(NSString *)area withDefaultValue:(NSString *)dValue withOptions:(NSArray*)options withKeyboardType:(NSString *)keyboardType andSaveAs:(NSString *)saveAs{
    NSString *fieldType = @"text_field_with_title"; //set as default
    NSArray *pickerOptions = @[];
    if(options){
        pickerOptions = options;
    }
    NSString *defaultValue = dValue;
    if(!defaultValue){
        defaultValue = @"";
    }
    NSNumber *height = [NSNumber numberWithFloat:kDefaultEntryFieldHeight];
    NSString *keyboards = keyboardType;
    if(!keyboards){
        keyboards = @"standard";
    }
    if ([area isEqualToString:@"picker"]) {
        //we need defaults, which are in options
        fieldType = @"select_field_with_title";
        //a picker must have options to select from
        if(!dValue && [options count] > 0){
            defaultValue = (NSString *)[options objectAtIndex:0];
        }
    } else if([area isEqualToString:@"textArea"]){
        fieldType = @"text_area";
        height = [NSNumber numberWithFloat:96.0];
    } else if([area isEqualToString:@"date_picker_with_title"]){
        fieldType = @"date_picker";
        if(!dValue && [options count] > 0){
            defaultValue = (NSString *)[options objectAtIndex:0];
        }
    } else if([area isEqualToString:@"toggle"]){
        fieldType = @"on_off_toggle";
        if ([defaultValue caseInsensitiveCompare:@"on"] == NSOrderedSame || [defaultValue caseInsensitiveCompare:@"true"] == NSOrderedSame || [defaultValue caseInsensitiveCompare:@"yes"] == NSOrderedSame) {
            defaultValue = @"on";
        } else {
            defaultValue = @"off";
        }
    }
    NSDictionary * thisField = @{@"displayName":title,
             @"fieldType":fieldType,
             @"placeholder":@"",
             @"saveAs":saveAs,
             @"is_custom_field":[NSNumber numberWithBool:YES],
             @"custom_field":title,
             @"fieldHeight":height,
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:NO],
             @"capitalize":[NSNumber numberWithBool:YES],
             @"selectOptions":pickerOptions,
             @"placeholder":defaultValue,
             @"keyboard_type":keyboards};
    return thisField;
}

+(NSDictionary *)customFieldDictionary:(NSString *)title andSaveAs:(NSString *)saveAs{
    return [self customFieldDictionary:title withArea:nil withDefaultValue:nil withOptions:nil withKeyboardType:nil andSaveAs:saveAs];
}


+(NSDictionary *)cardTypeDictionary{
    return  @{@"displayName":@"Card Type",
    @"fieldType":@"card_type",
    @"saveAs":@"card_type",
    @"fieldHeight":[NSNumber numberWithFloat:54.0],
    @"editable":[NSNumber numberWithBool:YES],
    @"required":[NSNumber numberWithBool:YES]};
}

+(NSDictionary *)cardBusinessNameDictionary{
    return @{@"displayName":@"BUSINESS NAME",
             @"fieldType":@"text_field_with_title",
             @"saveAs":@"title",
             @"placeholder":@"",
             @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:YES],
             @"capitalize":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardBusinessStreetAddressDictionary{
    return  @{@"displayName":@"BUSINESS ADDRESS",
              @"fieldType":@"address_field_with_title",
              @"saveAs":@"business_address",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultAddressFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"has_subfields":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"capitalize":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardPersonalStreetAddressDictionary{
    return  @{@"displayName":@"PERSONAL ADDRESS",
              @"fieldType":@"address_field_with_title",
              @"saveAs":@"personal_address",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultAddressFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"has_subfields":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"capitalize":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardWebsiteDictionary{
    return  @{@"displayName":@"WEBSITE",
              @"fieldType":@"text_field_with_title",
              @"saveAs":@"website",
              @"placeholder":@"",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"capitalize":[NSNumber numberWithBool:NO],
              @"keyboard_type":@"url"};
}

+(NSDictionary *)cardImageDictionary{
    return @{@"displayName":@"Card Image",
    @"fieldType":@"image_select",
    @"saveAs":@"image",
    @"fieldHeight":[NSNumber numberWithFloat:156.0],
    @"editable":[NSNumber numberWithBool:YES],
    @"required":[NSNumber numberWithBool:YES]};
}

+(NSDictionary *)cardTitleDictionary:(NSString *)title {
    return  @{@"displayName":title,
              @"fieldType":@"text_field_with_title",
              @"placeholder":@"",
              @"saveAs":@"title",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:YES],
              @"capitalize":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardTitleDictionary{
    return  @{@"displayName":@"CARD TITLE",
              @"fieldType":@"text_field_with_title",
              @"placeholder":@"",
              @"saveAs":@"title",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:YES],
              @"capitalize":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardCaptionDictionary:(NSString *)caption{
    return  @{@"displayName":caption,
              @"fieldType":@"text_field_with_title",
              @"placeholder":@"",
              @"saveAs":@"caption",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"capitalize":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardCaptionDictionary{
    return  @{@"displayName":@"SUBTITLE",
              @"fieldType":@"text_field_with_title",
              @"placeholder":@"",
              @"saveAs":@"caption",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"capitalize":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardCommentDictionary{
    return @{@"displayName":@"INFORMATION",
             @"fieldType":@"text_area",
             @"saveAs":@"comment",
             @"placeholder":@"",
             @"fieldHeight":[NSNumber numberWithFloat:96.0],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:NO],
             @"capitalize":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardBioDictionary:(NSString *)title{
    return @{@"displayName":title,
             @"fieldType":@"text_area",
             @"saveAs":@"about",
             @"placeholder":@"",
             @"fieldHeight":[NSNumber numberWithFloat:96.0],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:NO],
             @"capitalize":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardBioDictionary{
    return @{@"displayName":@"BIO INFORMATION",
             @"fieldType":@"text_area",
             @"saveAs":@"about",
             @"placeholder":@"",
             @"fieldHeight":[NSNumber numberWithFloat:96.0],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:NO],
             @"capitalize":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardAboutDictionary{
    return @{@"displayName":@"ABOUT THE COMPANY",
             @"fieldType":@"text_area",
             @"saveAs":@"about",
             @"placeholder":@"",
             @"fieldHeight":[NSNumber numberWithFloat:96.0],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:NO],
             @"capitalize":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"standard"};
}

+(NSDictionary *)cardPhoneNumberDictionary{
    return  @{@"displayName":@"PHONE NUMBER",
              @"fieldType":@"text_field_with_title",
              @"placeholder":@"",
              @"saveAs":@"phone_number",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"numeric":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"numberic"};
}

+(NSDictionary *)cardEmailAddressDictionary:(NSString *)title{
    return  @{@"displayName":title,
              @"fieldType":@"text_field_with_title",
              @"placeholder":@"",
              @"saveAs":@"email_address",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"capitalize":[NSNumber numberWithBool:NO],
              @"keyboard_type":@"email"};
}

+(NSDictionary *)cardEmailAddressDictionary{
    return  @{@"displayName":@"EMAIL ADDRESS",
              @"fieldType":@"text_field_with_title",
              @"placeholder":@"",
              @"saveAs":@"email_address",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"capitalize":[NSNumber numberWithBool:NO],
              @"keyboard_type":@"email"};
}

+(NSDictionary *)cardTwitterNameDictionary{
    return  @{@"displayName":@"TWITTER NAME",
              @"fieldType":@"text_field_with_title",
              @"placeholder":@"@",
              @"saveAs":@"twitter_handle",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO],
              @"capitalize":[NSNumber numberWithBool:NO],
              @"keyboard_type":@"twitter"};
}

+(NSDictionary *)cardRelationshipStatusDictionary{
    return  @{@"displayName":@"RELATIONSHIP STATUS",
              @"fieldType":@"select_field_with_title",
              @"placeholder":@"- Select -",
              @"saveAs":@"relationship_status",
              @"selectOptions":[NSArray arrayWithObjects:@"Single", @"Married", nil],
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:NO]};
}

+(NSArray *)getEditableCardTypes{
    return [NSArray arrayWithObjects:@"Personal Card", @"Picture Card", nil];
}



+(NSDictionary *)deckTitleDictionary{
    return  @{@"displayName":@"DECK TITLE",
              @"fieldType":@"text_field_with_title",
              @"saveAs":@"title",
              @"placeholder":@"",
              @"fieldHeight":[NSNumber numberWithFloat:kDefaultEntryFieldHeight],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"standard"};
}




+(NSArray *)getEditableCardTypesByDeckType:(NSString *) deckType {
    
    if (deckType != nil && NSNull.null != (id)deckType) {
        if ([deckType isEqualToString:kDeckTypeProfile] || [deckType isEqualToString:kDeckTypeSpecialProfiles]) {
            return [NSArray arrayWithObjects:
                    @{@"name":@"My Profile Card", @"card_type":kCardPersonalProfileInfoType, @"card_description":@"Your own personal Inadek card about you"},
                    @{@"name":@"Business Profile Card", @"card_type":kCardBusinessProfileInfoType, @"card_description":@"Inadek profile card for your business"},
                    nil
                    ];
        } else if ([deckType isEqualToString:kDeckTypePersonal]) {
            return [NSArray arrayWithObjects:
                    @{@"name":@"Personal Card", @"card_type":kCardPersonalInfoType, @"card_description":@"Your own personal Inadek card about you"},
                    @{@"name":@"Business Card", @"card_type":kCardBusinessInfoType, @"card_description":@"Inadek card for your business"},
                    @{@"name":@"Generic Card", @"card_type":kCardGenericInfoType, @"card_description":@"A generic photo card with title"},
                    @{@"name":@"Deck in Deck", @"card_type":kCardDeckUrlFullImageInfoType, @"card_description":@"Add a link to another deck"},
                    nil
                    ];
        } else if ([deckType isEqualToString:kDeckTypeBusiness]) {
            return [NSArray arrayWithObjects:
                    @{@"name":@"Business Card", @"card_type":kCardBusinessInfoType, @"card_description":@"Inadek card for a business"},
                    @{@"name":@"Personal Card", @"card_type":kCardPersonalInfoType, @"card_description":@"A personal card related to the business"},
                    @{@"name":@"Generic Card", @"card_type":kCardGenericInfoType, @"card_description":@"A generic photo card with title"},
                    @{@"name":@"Deck in Deck", @"card_type":kCardDeckUrlFullImageInfoType, @"card_description":@"Add a link to another deck"},
                    nil
                    ];
            
            
        } else if ([deckType isEqualToString:kDeckTypePhotos]) {
            return [NSArray arrayWithObjects:
                    @{@"name":@"Photo Card", @"card_type":kCardPhotoInfoType, @"card_description":@"A photo with comments and likes"},
                    @{@"name":@"Deck in Deck", @"card_type":kCardDeckUrlFullImageInfoType, @"card_description":@"Add a link to another deck"},
                    nil
                    ];
        } else if ([deckType isEqualToString:kDeckTypeSports]) {
            return [NSArray arrayWithObjects:
                    @{@"name":@"Sports Player Card", @"card_type":kCardSportsPlayerInfoType, @"card_description":@"Fun facts on a baseball player"},
                    @{@"name":@"Sports Team Card", @"card_type":kCardSportsTeamInfoType, @"card_description":@"A team summary card"},
                    @{@"name":@"Sports Venue Card", @"card_type":kCardSportsVenueInfoType, @"card_description":@"Info on a sports venue"},
                    @{@"name":@"Deck in Deck", @"card_type":kCardDeckUrlFullImageInfoType, @"card_description":@"Add a link to another deck"},
                    nil
                    ];
        } else if ([deckType isEqualToString:kDeckTypeHowto]) {
            return [NSArray arrayWithObjects:
                    @{@"name":@"Simple How-To", @"card_type":@"inadek.personal", @"card_description":@"Simple steps to complete a task"},
                    @{@"name":@"Video How-To", @"card_type":@"inadek.business", @"card_description":@"Include video in your how-to steps"},
                    @{@"name":@"Deck in Deck", @"card_type":kCardDeckUrlFullImageInfoType, @"card_description":@"Add a link to another deck"},
                    nil
                    ];
            
        } else if ([deckType isEqualToString:kDeckTypeAd]) {
            return [NSArray arrayWithObjects:
                    @{@"name":@"Full Image Ad", @"card_type":kCardAdFullImageInfoType, @"card_description":@"Simple ad using a full image only"},
                    @{@"name":@"Half Image Ad", @"card_type":kCardAdHalfImageInfoType, @"card_description":@"Simple ad with half an image and title"},
                    @{@"name":@"Promotional Ad", @"card_type":kCardAdLocationInfoType, @"card_description":@"Add with a promotional call to action"},
                    @{@"name":@"Deck in Deck", @"card_type":kCardDeckUrlFullImageInfoType, @"card_description":@"Add a link to another deck"},
                    nil
                    ];
        } else  {
            return [NSArray arrayWithObjects:
                    @{@"name":@"Personal Card", @"card_type":@"inadek.personal", @"card_description":@"Your own personal Inadek card for yourself or a family member"},
                    @{@"name":@"Business Card", @"card_type":@"inadek.business", @"card_description":@"Inadek card for your business"},
                    @{@"name":@"Deck in Deck", @"card_type":kCardDeckUrlFullImageInfoType, @"card_description":@"Add a link to another deck"},
                    nil
                    ];
        }
    } else {
        return [NSArray arrayWithObjects:
                @{@"name":@"Personal Card", @"card_type":@"inadek.personal", @"card_description":@"Your own personal Inadek card for yourself or a family member"},
                @{@"name":@"Business Card", @"card_type":@"inadek.business", @"card_description":@"Inadek card for your business"},
                @{@"name":@"Deck in Deck", @"card_type":kCardDeckUrlFullImageInfoType, @"card_description":@"Add a link to another deck"},
                nil
                ];
    }
}


+(NSArray *)getAccountManagementPasswordFields{
    return [NSArray arrayWithObjects:
            [self accountNameDictionary],
            [self accountEmailDictionary],
            [self accountPasswordDictionary],
            [self accountPasswordConfirmationDictionary],
            nil];
}
+(NSArray *)getAccountManagementNoPasswordFields{
    return [NSArray arrayWithObjects:
            [self accountNameDictionary],
            [self accountEmailDictionary],
            [self accountChangePasswordDictionary],
            nil];
}

+(NSDictionary *)accountNameDictionary{
    return @{@"displayName":@"Name",
             @"fieldType":@"text_field",
             @"saveAs":@"username",
             @"fieldHeight":[NSNumber numberWithFloat:67.0],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"standard"};
}
+(NSDictionary *)accountEmailDictionary{
    return @{@"displayName":@"Email Address",
             @"fieldType":@"text_field",
             @"saveAs":@"email",
             @"fieldHeight":[NSNumber numberWithFloat:67.0],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"email"};
}
+(NSDictionary *)accountPasswordDictionary{
    return @{@"displayName":@"Password",
             @"fieldType":@"text_field_hidden",
             @"saveAs":@"password",
             @"fieldHeight":[NSNumber numberWithFloat:67.0],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"secure"};
}
+(NSDictionary *)accountPasswordConfirmationDictionary{
    return @{@"displayName":@"Confirm Password",
             @"fieldType":@"text_field_hidden",
             @"saveAs":@"password_confirmation",
             @"fieldHeight":[NSNumber numberWithFloat:67.0],
             @"editable":[NSNumber numberWithBool:YES],
             @"required":[NSNumber numberWithBool:YES],
             @"keyboard_type":@"secure"};
}
+(NSDictionary *)accountChangePasswordDictionary{
    return  @{@"displayName":@"Change Password",
              @"fieldType":@"trigger",
              @"saveAs":@"none",
              @"fieldHeight":[NSNumber numberWithFloat:67.0],
              @"editable":[NSNumber numberWithBool:YES],
              @"required":[NSNumber numberWithBool:YES],
              @"keyboard_type":@"standard"};
}

@end
