//
//  Account+Core.h
//  Inadek
//
//  Created by Josh Lehman on 1/20/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <ObjectiveRecord/ObjectiveRecord.h>
#import "Account.h"

@interface Account (Core)

- (void)unpackDictionary:(NSDictionary *)dictionary;



- (void)debug;
+ (void)logObjectCounts;

@end
