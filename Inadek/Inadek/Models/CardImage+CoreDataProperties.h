//
//  CardImage+CoreDataProperties.h
//  Inadek
//
//  Created by Josh Lehman on 11/15/15.
//  Copyright © 2015 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CardImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface CardImage (CoreDataProperties)

@property (nullable, nonatomic, retain) id image;
@property (nullable, nonatomic, retain) id image_cropped;
@property (nullable, nonatomic, retain) NSString *guid;
@property (nullable, nonatomic, retain) NSString *image_url;
@property (nullable, nonatomic, retain) NSString *image_thumb_url;
@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) NSNumber *remoteID;
@property (nullable, nonatomic, retain) NSNumber *is_synced;
@property (nullable, nonatomic, retain) CardField *cardfield;

@end

NS_ASSUME_NONNULL_END
