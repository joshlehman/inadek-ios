//
//  Card+CoreDataProperties.m
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Card+CoreDataProperties.h"

@implementation Card (CoreDataProperties)

@dynamic base_color;
@dynamic cid;
@dynamic created_at;
@dynamic creator_id;
@dynamic image;
@dynamic image_cropped;
@dynamic image_thumb_url;
@dynamic image_url;
@dynamic is_private;
@dynamic name;
@dynamic owner_id;
@dynamic remoteID;
@dynamic type_id;
@dynamic updated_at;
@dynamic card_fields;
@dynamic decks;
@dynamic related_deck;
@dynamic triggers;
@dynamic card_comments;

@end
