//
//  CardField+Core.m
//  Inadek
//
//  Created by Josh Lehman on 1/21/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardField+Core.h"
#import "Common.h"

@implementation CardField (Core)

+ (CardField *)myNew
{
    CardField *newObject = [CardField create];
    [newObject setGuid:[Common generateGUID]];
    
    return newObject;
}

- (void)unpackDictionary:(NSDictionary *)dictionary {
    [super unpackDictionary:dictionary];
    self.remoteID = [dictionary objectForKey:@"id"];
    self.field_cid = [Common sanitizedString:[dictionary objectForKey:@"cid"]];
    self.field_value = [Common sanitizedString:[dictionary objectForKey:@"field_value"]];
    self.position = [Common sanitizedNumber:[dictionary objectForKey:@"position"]];
    self.title = [Common sanitizedString:[dictionary objectForKey:@"title"]];
    self.on_front = [Common sanitizedBool:[dictionary objectForKey:@"on_front"]];
    self.guid = [Common sanitizedString:[dictionary objectForKey:@"guid"]];
    
    if ([dictionary objectForKey:@"on_front"] && [[dictionary objectForKey:@"on_front"] integerValue] == 1) {
        self.on_front = [NSNumber numberWithInt:1];
    } else {
        self.on_front = [NSNumber numberWithInt:0];
    }
    
    self.placeholder = [Common sanitizedString:[dictionary objectForKey:@"placeholder"]];
    self.has_title = [Common sanitizedBool:[dictionary objectForKey:@"has_title"]];
    self.value_type = [Common sanitizedString:[dictionary objectForKey:@"value_type"]];
    
    if (self.has_title == nil) {
        self.has_title = [NSNumber numberWithBool:NO];
    }
    
    self.card_type_field_id = [Common sanitizedNumber:[dictionary objectForKey:@"card_type_field_id"]];
}

- (BOOL)fieldIsMobilePhone
{
    if ([self.value_type isEqualToString:@"phone"]) {
        if (self.has_title && self.title) {
            NSString *titleDowncase = [self.title lowercaseString];
            if ([titleDowncase isEqualToString:@"mobile"] || [titleDowncase isEqualToString:@"mobile phone"] || [titleDowncase isEqualToString:@"mobile number"] || [titleDowncase isEqualToString:@"cell"] || [titleDowncase isEqualToString:@"cell number"]) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (BOOL)fieldIsBusinessPhone
{
    if ([self.value_type isEqualToString:@"phone"]) {
        if (self.has_title && self.title) {
            NSString *titleDowncase = [self.title lowercaseString];
            if ([titleDowncase isEqualToString:@"work"] || [titleDowncase isEqualToString:@"work phone"] || [titleDowncase isEqualToString:@"business number"] || [titleDowncase isEqualToString:@"business"] || [titleDowncase isEqualToString:@"office"]) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (BOOL)fieldIsHomePhone
{
    if ([self.value_type isEqualToString:@"phone"]) {
        if (self.has_title && self.title) {
            NSString *titleDowncase = [self.title lowercaseString];
            if ([titleDowncase isEqualToString:@"home"] || [titleDowncase isEqualToString:@"home phone"] || [titleDowncase isEqualToString:@"home number"]) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (BOOL)fieldIsEmail
{
    if ([self.value_type isEqualToString:@"email"]) {
        if (self.has_title && self.title) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)fieldIsWebContent
{
    if ([self.value_type isEqualToString:@"web"]) {
        return YES;
    }
    
    return NO;
}

- (BOOL)fieldIsPhotoGallery
{
    if ([Common isFieldTypePhotos:self.field_cid]) {
        return YES;
    } else {
        return NO;
    }
}

- (NSString *)fieldValueAsText
{
    if ([Common isFieldTypePhotos:self.field_cid]) {
        
        if ([self.cardimages allObjects].count == 1) {
            return [NSString stringWithFormat:@"%lu Photo", (unsigned long)[self.cardimages allObjects].count];
        } else {
            return [NSString stringWithFormat:@"%lu Photos", (unsigned long)[self.cardimages allObjects].count];
        }
        
    } else {
        return self.field_value;
    }
}

- (NSString *)fieldShortValueAsText
{
    if ([Common isFieldTypePhotos:self.field_cid]) {
        
        if ([self.cardimages allObjects].count == 1) {
            return [NSString stringWithFormat:@"%lu Photo", (unsigned long)[self.cardimages allObjects].count];
        } else {
            return [NSString stringWithFormat:@"%lu Photos", (unsigned long)[self.cardimages allObjects].count];
        }
        
    } else {
        if ([self.field_value length] > 30) {
            NSString *str = [self.field_value substringToIndex: MIN(30, [self.field_value length])];
            return [NSString stringWithFormat:@"%@...", str];
        } else {
            return self.field_value;
        }
    }
}

- (NSMutableArray *)allCardImages
{
    if (self.cardimages) {
        return [[self.cardimages allObjects] mutableCopy];
    } else {
        return [@[] mutableCopy];
    }
}

- (UIView *)getFormattedField:(int)inContext
{
    if (inContext == kCardFieldContextFullCard) {
        
    } else if (inContext == kCardFieldContextMiniCard) {
        
    }
    
    return nil;
}

+ (NSString *)getDefaultTitleForCid:(NSString *)cid
{
    NSString *defaultTitle = @"";
    
    NSArray *fields = [[NSUserDefaults standardUserDefaults] objectForKey:@"fields"];
    for (NSDictionary *t in fields) {
        if ([[t objectForKey:@"cid"] isEqualToString:cid]) {
            defaultTitle = [t objectForKey:@"name"];
            break;
        }
    }
    
    return defaultTitle;
}

@end
