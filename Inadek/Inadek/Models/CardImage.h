//
//  CardImage.h
//  Inadek
//
//  Created by Josh Lehman on 11/15/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SSDataKit/SSDataKit.h>

@class CardField;

NS_ASSUME_NONNULL_BEGIN

@interface CardImage : SSRemoteManagedObject

+ (CardImage *)myNew;
- (void)unpackDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)syncParams;
- (void)queueImageForSync;

@end

NS_ASSUME_NONNULL_END

#import "CardImage+CoreDataProperties.h"
