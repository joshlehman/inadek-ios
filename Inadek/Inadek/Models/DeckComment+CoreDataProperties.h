//
//  DeckComment+CoreDataProperties.h
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DeckComment.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeckComment (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *is_active;
@property (nullable, nonatomic, retain) NSNumber *is_deleted;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *comment;
@property (nullable, nonatomic, retain) NSDate *comment_date;
@property (nullable, nonatomic, retain) NSNumber *remoteID;
@property (nullable, nonatomic, retain) Deck *deck;

@end

NS_ASSUME_NONNULL_END
