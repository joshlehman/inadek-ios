//
//  Deck.h
//  Inadek
//
//  Created by Josh Lehman on 1/25/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SSDataKit/SSDataKit.h>

@class Card, DeckCategory, DeckTrigger;

@interface Deck : SSRemoteManagedObject

@property (nonatomic, retain) NSString * cid;
@property (nonatomic, retain) NSNumber * creator_id;
@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) id image;
@property (nonatomic, retain) NSString * image_thumb_url;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSString * base_color;
@property (nonatomic, retain) NSString * card_order;
@property (nonatomic, retain) NSNumber * is_popular;
@property (nonatomic, retain) NSNumber * is_private;
@property (nonatomic, retain) NSNumber * is_favorite;
@property (nonatomic, retain) NSNumber * is_followed;
@property (nonatomic, retain) NSNumber * favorite_count;
@property (nonatomic, retain) NSNumber * card_count;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * owner_id;
@property (nonatomic, retain) NSString * owner_name;
@property (nonatomic, retain) NSNumber * remoteID;
@property (nonatomic, retain) NSNumber * type_id;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) id image_cropped;
@property (nonatomic, retain) NSSet * cards;
@property (nonatomic, retain) NSSet * triggers;
@property (nonatomic, retain) NSSet * categories;
@property (nonatomic, retain) NSString * sharetext;
@property (nonatomic, retain) NSString * shareurl;
@end

@interface Deck (CoreDataGeneratedAccessors)

- (void)addCardsObject:(Card *)value;
- (void)removeCardsObject:(Card *)value;
- (void)addCards:(NSSet *)values;
- (void)removeCards:(NSSet *)values;

- (void)addTriggersObject:(DeckTrigger *)value;
- (void)removeTriggersObject:(DeckTrigger *)value;
- (void)addTriggers:(NSSet *)values;
- (void)removeTriggers:(NSSet *)values;

- (void)addCategoriesObject:(DeckCategory *)value;
- (void)removeCategoriesObject:(DeckCategory *)value;
- (void)addCategories:(NSSet *)values;
- (void)removeCategories:(NSSet *)values;

@end
