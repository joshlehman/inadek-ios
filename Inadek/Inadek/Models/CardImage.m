//
//  CardImage.m
//  Inadek
//
//  Created by Josh Lehman on 11/15/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "CardImage.h"
#import "CardField.h"
#import "Common.h"
#import "AccountManager.h"

@implementation CardImage

+ (CardImage *)myNew
{
    CardImage *newObject = [CardImage create];
    [newObject setGuid:[CardImage generateGUID]];
    
    return newObject;
}

+ (NSString *)generateGUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    return (__bridge NSString *)string;
}

- (void)queueImageForSync {
    self.is_synced = [NSNumber numberWithBool:NO];
    [self save];
}

- (void)unpackDictionary:(NSDictionary *)dictionary {
    
    [super unpackDictionary:dictionary];

    self.remoteID = [dictionary objectForKey:@"id"];
    self.image_url = [Common sanitizedString:[dictionary objectForKey:@"image_url"]];
    self.image_thumb_url = [Common sanitizedString:[dictionary objectForKey:@"image_thumb_url"]];
    self.position = [Common sanitizedNumber:[dictionary objectForKey:@"position"]];
    self.is_synced = [NSNumber numberWithBool:YES];
    self.guid = [Common sanitizedString:[dictionary objectForKey:@"guid"]];
    
    self.image = nil;
    self.image_cropped = nil;
    
    if (self.cardfield == nil && [dictionary objectForKey:@"card_field_id"]) {
        CardField *cf = [CardField where:@{@"remoteID":[dictionary objectForKey:@"card_field_id"]}].firstObject;
        if (cf) {
            self.cardfield = cf;
        }
    }
    
}

- (NSDictionary *)syncParams
{
    NSMutableDictionary *params = NSMutableDictionary.new;
    
    if (!self.cardfield) {
        return nil;
    }
    
    [params setObject:self.cardfield.remoteID forKey:@"card_field_id"];
    [params setObject:self.guid forKey:@"guid"];
    [params setObject:self.position forKey:@"position"];
    [params setObject:@"yes" forKey:@"is_gallery"];
    
    return [NSDictionary dictionaryWithDictionary:params];
}

@end
