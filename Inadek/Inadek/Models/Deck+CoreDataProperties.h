//
//  Deck+CoreDataProperties.h
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Deck.h"

NS_ASSUME_NONNULL_BEGIN

@interface Deck (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *base_color;
@property (nullable, nonatomic, retain) NSNumber *card_count;
@property (nullable, nonatomic, retain) NSString *card_order;
@property (nullable, nonatomic, retain) NSString *cid;
@property (nullable, nonatomic, retain) NSDate *created_at;
@property (nullable, nonatomic, retain) NSNumber *creator_id;
@property (nullable, nonatomic, retain) NSNumber *favorite_count;
@property (nullable, nonatomic, retain) id image;
@property (nullable, nonatomic, retain) id image_cropped;
@property (nullable, nonatomic, retain) NSString *image_thumb_url;
@property (nullable, nonatomic, retain) NSString *image_url;
@property (nullable, nonatomic, retain) NSNumber *is_favorite;
@property (nullable, nonatomic, retain) NSNumber *is_followed;
@property (nullable, nonatomic, retain) NSNumber *is_popular;
@property (nullable, nonatomic, retain) NSNumber *is_private;
@property (nullable, nonatomic, retain) NSNumber *lat;
@property (nullable, nonatomic, retain) NSNumber *lon;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *owner_id;
@property (nullable, nonatomic, retain) NSString *owner_name;
@property (nullable, nonatomic, retain) NSNumber *remoteID;
@property (nullable, nonatomic, retain) NSString *sharetext;
@property (nullable, nonatomic, retain) NSString *shareurl;
@property (nullable, nonatomic, retain) NSNumber *type_id;
@property (nullable, nonatomic, retain) NSDate *updated_at;
@property (nullable, nonatomic, retain) NSSet<Card *> *cards;
@property (nullable, nonatomic, retain) NSSet<DeckCategory *> *categories;
@property (nullable, nonatomic, retain) NSSet<DeckTrigger *> *triggers;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *deck_comments;

@end

@interface Deck (CoreDataGeneratedAccessors)

- (void)addCardsObject:(Card *)value;
- (void)removeCardsObject:(Card *)value;
- (void)addCards:(NSSet<Card *> *)values;
- (void)removeCards:(NSSet<Card *> *)values;

- (void)addCategoriesObject:(DeckCategory *)value;
- (void)removeCategoriesObject:(DeckCategory *)value;
- (void)addCategories:(NSSet<DeckCategory *> *)values;
- (void)removeCategories:(NSSet<DeckCategory *> *)values;

- (void)addTriggersObject:(DeckTrigger *)value;
- (void)removeTriggersObject:(DeckTrigger *)value;
- (void)addTriggers:(NSSet<DeckTrigger *> *)values;
- (void)removeTriggers:(NSSet<DeckTrigger *> *)values;

- (void)addDeck_commentsObject:(NSManagedObject *)value;
- (void)removeDeck_commentsObject:(NSManagedObject *)value;
- (void)addDeck_comments:(NSSet<NSManagedObject *> *)values;
- (void)removeDeck_comments:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
