//
//  DeckTrigger.m
//  Inadek
//
//  Created by Josh Lehman on 1/21/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckTrigger.h"
#import "Deck.h"
#import "DeckTrigger+Core.h"

@implementation DeckTrigger

@dynamic remoteID;
@dynamic trigger_type;
@dynamic action_verb;
@dynamic name;
@dynamic value;
@dynamic deck;

@end
