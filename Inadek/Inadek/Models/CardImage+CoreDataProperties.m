//
//  CardImage+CoreDataProperties.m
//  Inadek
//
//  Created by Josh Lehman on 11/15/15.
//  Copyright © 2015 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CardImage+CoreDataProperties.h"

@implementation CardImage (CoreDataProperties)

@dynamic guid;
@dynamic image;
@dynamic image_cropped;
@dynamic image_url;
@dynamic image_thumb_url;
@dynamic position;
@dynamic remoteID;
@dynamic is_synced;
@dynamic cardfield;

@end
