//
//  Card.m
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import "Card.h"
#import "CardField+Core.h"
#import "CardTrigger+Core.h"
#import "Deck+Core.h"
#import "CardComment.h"

@implementation Card

// Insert code here to add functionality to your managed object subclass

@end
