//
//  CardField+Core.h
//  Inadek
//
//  Created by Josh Lehman on 1/21/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <ObjectiveRecord/ObjectiveRecord.h>
#import "CardField.h"

#define kCardFieldContextFullCard   1
#define kCardFieldContextMiniCard   2

@interface CardField (Core)

+ (CardField *)myNew;
- (void)unpackDictionary:(NSDictionary *)dictionary;
- (NSString *)fieldValueAsText;
- (NSString *)fieldShortValueAsText;
- (NSMutableArray *)allCardImages;
- (BOOL)fieldIsPhotoGallery;
- (BOOL)fieldIsMobilePhone;
- (BOOL)fieldIsBusinessPhone;
- (BOOL)fieldIsHomePhone;
- (BOOL)fieldIsEmail;
- (BOOL)fieldIsWebContent;

- (UIView *)getFormattedField:(int)inContext;
+ (NSString *)getDefaultTitleForCid:(NSString *)cid;

@end
