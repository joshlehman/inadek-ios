//
//  DeckCategory+Core.m
//  Inadek
//
//  Created by Josh Lehman on 1/24/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckCategory+Core.h"
#import "Common.h"

@implementation DeckCategory (Core)

- (void)unpackDictionary:(NSDictionary *)dictionary {
    [super unpackDictionary:dictionary];
    self.remoteID = [dictionary objectForKey:@"id"];
    self.name = [dictionary objectForKey:@"name"];
    self.cid = [dictionary objectForKey:@"cid"];
    self.summary = [Common sanitizedString:[dictionary objectForKey:@"summary"]];
    self.sort_order = [Common sanitizedNumber:[dictionary objectForKey:@"sort_order"]];
    self.is_featured = [Common sanitizedBool:[dictionary objectForKey:@"is_featured"]];
    self.status_id = [dictionary objectForKey:@"status_id"];
    self.is_default = [dictionary objectForKey:@"is_default"];
}







- (void)debug
{
    DLog(@"Debug this object now!");
}

+ (void)logObjectCounts
{
    NSUInteger objects = [DeckCategory count];
    DLog(@"DeckCategory Count: %lu", (unsigned long)objects);
}

@end
