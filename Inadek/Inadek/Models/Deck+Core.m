//
//  Deck+Core.m
//  Inadek
//
//  Created by Josh Lehman on 1/12/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "Deck+Core.h"
#import "Common.h"
#import "AccountManager.h"
#import "DeckCategory+Core.h"

#import "NSArray+ObjectiveSugar.h"

@implementation Deck (Core)


// Generates a new deck, and adds the user has owner and generates a short CID
+ (Deck *)myNew
{
    Deck *newDeck = [Deck create];
    if ([[AccountManager sharedManager] currentAccountId]) {
        int account_id = [[[AccountManager sharedManager] currentAccountId] integerValue];
        
        [newDeck setOwner_id:[NSNumber numberWithInteger:account_id]];
        [newDeck setCreator_id:[NSNumber numberWithInteger:account_id]];
        [newDeck setCid:@"!NEW!"];
    }
    
    return newDeck;
}


- (BOOL)isValid {
    if (!self.name || [self.name isEqualToString:@""]) {
        return NO;
    }
    
    if (!self.type_id || [self.type_id integerValue] == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)isMine {
    if (self.owner_id && [self.owner_id integerValue] == [[AccountManager sharedManager].currentAccountId integerValue]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)unpackDictionary:(NSDictionary *)dictionary {
    [super unpackDictionary:dictionary];
    self.remoteID = [Common sanitizedNumber:[dictionary objectForKey:@"id"]];
    self.name = [Common sanitizedString:[dictionary objectForKey:@"name"]];
    self.cid = [Common sanitizedString:[dictionary objectForKey:@"cid"]];
    self.type_id = [Common sanitizedNumber:[dictionary objectForKey:@"type_id"]];
    self.is_private = [Common sanitizedBool:[dictionary objectForKey:@"is_private"]];
    self.is_favorite = [Common sanitizedBool:[dictionary objectForKey:@"is_favorite"]];
    self.is_followed = [Common sanitizedBool:[dictionary objectForKey:@"is_followed"]];
    self.card_count = [Common sanitizedNumber:[dictionary objectForKey:@"card_count"]];
    self.favorite_count = [Common sanitizedNumber:[dictionary objectForKey:@"favorite_count"]];
    self.image_url = [Common sanitizedString:[dictionary objectForKey:@"image_url"]];
    self.image_thumb_url = [Common sanitizedString:[dictionary objectForKey:@"thumb_url"]];
    self.base_color = [Common sanitizedString:[dictionary objectForKey:@"base_color"]];
    self.shareurl = [Common sanitizedString:[dictionary objectForKey:@"share_url"]];
    self.sharetext = [Common sanitizedString:[dictionary objectForKey:@"share_text"]];
    self.updated_at = [Common sanitizedDate:[dictionary objectForKey:@"updated_at"]];
    self.created_at = [Common sanitizedDate:[dictionary objectForKey:@"created_at"]];
    if ([dictionary objectForKey:@"owner"]) {
        NSDictionary *owner = [dictionary objectForKey:@"owner"];
        self.owner_id = [owner objectForKey:@"id"];
        self.owner_name = [owner objectForKey:@"name"];
    }
    if ([dictionary objectForKey:@"categories"] && [[dictionary objectForKey:@"categories"] isKindOfClass:[NSArray class]]) {
        NSArray *categories = [dictionary objectForKey:@"categories"];
        NSArray *all_categories = [DeckCategory all];
        for (NSNumber *cat in categories) {
            DeckCategory *dc = [DeckCategory where:@{@"remoteID":cat}].firstObject;
            if (dc) {
                [self addCategoriesObject:dc];
            }
        }
    }
    
    if ([dictionary objectForKey:@"cards"] && [[dictionary objectForKey:@"cards"] isKindOfClass:[NSArray class]]) {
        NSString *card_order_string = @"";
        NSString *seperator = @"";
        for (NSNumber *card_id in [dictionary objectForKey:@"cards"]) {
            card_order_string = [card_order_string stringByAppendingString:[NSString stringWithFormat:@"%@%@", seperator, card_id]];
            seperator = @"|";
        }
        self.card_order = card_order_string;
    }
}

- (NSDictionary *)syncParams {
    
    NSMutableArray *categories = NSMutableArray.new;
    for (DeckCategory *c in [[self categories] allObjects]) {
        [categories addObject:c.remoteID];
    }
    
    NSMutableArray *triggers = NSMutableArray.new;
    for (DeckTrigger *t in [[self triggers] allObjects]) {
        NSMutableDictionary *trig = NSMutableDictionary.new;
        if (t.remoteID) {
            [trig setObject:t.remoteID forKey:@"id"];
        }
        
        [trig setObject:t.value forKey:@"value"];
        [trig setObject:t.action_verb forKey:@"verb"];
        [trig setObject:t.trigger_type forKey:@"trigger_type"];
        
        
        [triggers addObject:trig];
    }
    
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            
                            self.remoteID, @"deck[id]",
                            self.cid, @"deck[cid]",
                            self.name, @"deck[name]",
                            self.type_id, @"deck[type_id]",
                            self.owner_id, @"deck[owner_id]",
                            self.creator_id, @"deck[creator_id]",
                            self.base_color, @"deck[base_color]",
                            categories, @"deck[categories]",
                            triggers, @"deck[triggers]",
                            nil];
    
    return params;
}


- (BOOL)is_deck_private
{
    if ([self.is_private integerValue] == [[NSNumber numberWithBool:YES] integerValue]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)is_deck_popular
{
    if ([self.is_popular integerValue] == [[NSNumber numberWithBool:YES] integerValue]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)is_deck_favorite
{
    if ([self.is_favorite integerValue] == [[NSNumber numberWithBool:YES] integerValue]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)is_deck_followed
{
    if ([self.is_followed integerValue] == [[NSNumber numberWithBool:YES] integerValue]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)set_is_favorite:(BOOL)favorite
{
    self.is_favorite = [NSNumber numberWithBool:favorite];
}

- (void)set_is_followed:(BOOL)followed
{
    self.is_followed = [NSNumber numberWithBool:followed];
}

- (NSString *)nameString {
    if (self.name) {
        return self.name;
    } else {
        return @"";
    }
}

- (NSURL *)get_shareUrl {
    if (self.shareurl) {
        return [NSURL URLWithString:self.shareurl];
    } else {
        return nil;
    }
}

- (NSString *)get_shareText {
    if (self.sharetext) {
        return self.sharetext;
    } else {
        return @"";
    }
}


- (NSMutableArray *)orderedCards
{
    NSMutableArray *cardOrder;
    NSArray *card_order_array = NSArray.new;
    if (self.card_order) {
        card_order_array = [self.card_order componentsSeparatedByString:@"|"];
    }
    
    NSMutableArray *cards = NSMutableArray.new;
    
    for (NSString *card_id in card_order_array) {
        NSNumber *this_card_id = [NSNumber numberWithInt:[card_id integerValue]];
        Card *thisCard = [Card where:@{@"remoteID":this_card_id}].firstObject;
        if (thisCard && [[self.cards allObjects] containsObject:thisCard]) {
            [cards addObject:thisCard];
        }
    }

    return cards;
}

- (NSString *) cardCountAsString
{
    if ([self.card_count integerValue] > 1) {
        return [NSString stringWithFormat:@"%lu Cards", (unsigned long)[[self.cards allObjects] count]];
    } else if ([self.card_count integerValue] == 1) {
        return [NSString stringWithFormat:@"%lu Card", (unsigned long)[[self.cards allObjects] count]];
    } else {
        return @"No Cards";
    }
}


- (void)setCroppedImageObj:(UIImage*)image
{
    [self willChangeValueForKey:@"image_cropped"];
    
    NSData *data = UIImagePNGRepresentation(image);
    [self setImage_cropped:data];
    [self setPrimitiveValue:data forKey:@"image_cropped"];
    [self didChangeValueForKey:@"image_cropped"];
}

- (UIImage*)croppedImageObj
{
    [self willAccessValueForKey:@"image_cropped"];
    UIImage *image = [UIImage imageWithData:[self primitiveValueForKey:@"image_cropped"]];
    [self didAccessValueForKey:@"image_cropped"];
    return image;
}

- (void)setImageObj:(UIImage*)image
{
    [self willChangeValueForKey:@"image"];
    
    NSData *data = UIImagePNGRepresentation(image);
    [self setImage:data];
    [self setPrimitiveValue:data forKey:@"image"];
    [self didChangeValueForKey:@"image"];
}

- (UIImage*)imageObj
{
    [self willAccessValueForKey:@"image"];
    UIImage *image = [UIImage imageWithData:[self primitiveValueForKey:@"image"]];
    [self didAccessValueForKey:@"image"];
    return image;
}

- (void)createTriggerWithDetails:(NSDictionary *)triggerDefaults andValue:(NSString *)value
{
    NSArray *existing_triggers = [self.triggers allObjects];
    if (existing_triggers) {
        BOOL exists = NO;
        for (DeckTrigger *trigger in existing_triggers) {
            if (trigger.trigger_type == [triggerDefaults objectForKey:@"trigger_type"]) {
                exists = YES;
                if ([triggerDefaults objectForKey:@"verb"] && ![[triggerDefaults objectForKey:@"verb"] isEqualToString:@""]) {
                    trigger.action_verb = [triggerDefaults objectForKey:@"verb"];
                }
                [trigger setValue:value];
                [trigger save];
            }
        }
        if (!exists) {
            DeckTrigger *newTrig = [DeckTrigger create];
            newTrig.deck = self;
            newTrig.trigger_type = [triggerDefaults objectForKey:@"trigger_type"];
            newTrig.action_verb = [triggerDefaults objectForKey:@"verb"];
            newTrig.name = [triggerDefaults objectForKey:@"name"];
            newTrig.value = value;
            [newTrig save];
        }
    }
}

- (void)deleteLocal
{
    [self delete];
}


- (void)debug
{
    DLog(@"Debug this object now!");
}

+ (void)logObjectCounts:(NSString *)cid name:(NSString *)name
{
    if (cid && ![cid isEqualToString:@""]) {
        NSUInteger objects = [Deck countWhere:@{@"cid":cid}];
        DLog(@"%@ (cid) Deck Count: %lu", cid, (unsigned long)objects);
    } else if (name && ![name isEqualToString:@""]) {
        NSUInteger objects = [Deck countWhere:@{@"name":name}];
        DLog(@"%@ (name) Deck Count: %lu", name, (unsigned long)objects);
    } else {
        NSUInteger objects = [Deck count];
        DLog(@"Deck Count: %lu", (unsigned long)objects);
    }
}

+ (void) debugObject:(id)aObject
{
    
    /* - Core Loop, should make this a category on NSManagedObject - */
    NSEntityDescription *entity = [(NSManagedObject *)aObject entity];
    NSDictionary *attributes = [entity attributesByName];
    NSString *output = @"\r- - - - Core Data Object - - - -\r";
    for (NSString *attribute in attributes) {
        id value = [aObject valueForKey: attribute];
        output = [NSString stringWithFormat:@"%@%@: %@%@", output, attribute, value, @"\r"];
    }
    output = [NSString stringWithFormat:@"%@%@", output, @"- - - - - - - - - - - - - -\r\r"];
    DLog(@"%@", output);
    /* - END - */
    
}

@end
