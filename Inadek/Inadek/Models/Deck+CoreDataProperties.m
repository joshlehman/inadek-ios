//
//  Deck+CoreDataProperties.m
//  Inadek
//
//  Created by Josh Lehman on 12/13/15.
//  Copyright © 2015 Inadek. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Deck+CoreDataProperties.h"

@implementation Deck (CoreDataProperties)

@dynamic base_color;
@dynamic card_count;
@dynamic card_order;
@dynamic cid;
@dynamic created_at;
@dynamic creator_id;
@dynamic favorite_count;
@dynamic image;
@dynamic image_cropped;
@dynamic image_thumb_url;
@dynamic image_url;
@dynamic is_favorite;
@dynamic is_followed;
@dynamic is_popular;
@dynamic is_private;
@dynamic lat;
@dynamic lon;
@dynamic name;
@dynamic owner_id;
@dynamic owner_name;
@dynamic remoteID;
@dynamic sharetext;
@dynamic shareurl;
@dynamic type_id;
@dynamic updated_at;
@dynamic cards;
@dynamic categories;
@dynamic triggers;
@dynamic deck_comments;

@end
