//
//  CardTrigger.h
//  Inadek
//
//  Created by Josh Lehman on 3/4/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <SSDataKit/SSDataKit.h>

@class Card;

@interface CardTrigger : SSRemoteManagedObject

@property (nonatomic, retain) NSString * action_verb;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * remoteID;
@property (nonatomic, retain) NSString * trigger_type;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) Card *card;

@end
