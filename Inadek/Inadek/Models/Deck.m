//
//  Deck.m
//  Inadek
//
//  Created by Josh Lehman on 1/25/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "Deck.h"
#import "Card.h"
#import "DeckCategory.h"
#import "DeckTrigger.h"
#import "Deck+Core.h"


@implementation Deck

@dynamic cid;
@dynamic creator_id;
@dynamic created_at;
@dynamic image;
@dynamic image_cropped;
@dynamic image_thumb_url;
@dynamic image_url;
@dynamic base_color;
@dynamic is_popular;
@dynamic is_private;
@dynamic is_favorite;
@dynamic is_followed;
@dynamic card_count;
@dynamic favorite_count;
@dynamic lat;
@dynamic lon;
@dynamic name;
@dynamic owner_id;
@dynamic owner_name;
@dynamic remoteID;
@dynamic type_id;
@dynamic updated_at;
@dynamic cards;
@dynamic triggers;
@dynamic categories;
@dynamic sharetext;
@dynamic shareurl;

@dynamic card_order;

@end
