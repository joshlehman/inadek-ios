//
//  CardTrigger.m
//  Inadek
//
//  Created by Josh Lehman on 3/4/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "CardTrigger.h"
#import "Card.h"


@implementation CardTrigger

@dynamic action_verb;
@dynamic name;
@dynamic remoteID;
@dynamic trigger_type;
@dynamic value;
@dynamic card;

@end
