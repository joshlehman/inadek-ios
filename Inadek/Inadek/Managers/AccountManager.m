//
//  AccountManager.m
//  Inadek
//
//  Created by Josh Lehman on 1/19/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "AccountManager.h"
#import "DeckManager.h"

#import "Card+Core.h"
#import "Deck+Core.h"

@implementation AccountManager


- (Account *)currentAccount
{
    Account *current = [Account all].firstObject;
    if (current) {
        return current;
    } else {
        return nil;
    }
}

- (NSNumber *)currentAccountId
{
    if ([self currentAccount]) {
        Account *me = [self currentAccount];
        return me.remoteID;
    } else {
        return nil;
    }
}

- (NSString *) profileCardCountString
{
    return [NSString stringWithFormat:@"%lu",(unsigned long)[[DeckManager sharedManager] localProfileCards].count];
}

- (NSString *) favDeckCountString
{
    return [NSString stringWithFormat:@"%lu",(unsigned long)[[DeckManager sharedManager] localFavoriteDecks].count];
}

- (NSString *) fanCountString
{
    return @"0";
}

- (NSString *) groupDeckCountString
{
    return @"0";
}

- (void) cleanData
{
    NSArray *removableCards = [Card where:@{@"cid":@"!NEW!"}];
    for (Card *c in removableCards) {
        [c delete];
    }
    
    NSArray *removableDecks = [Deck where:@{@"cid":@"!NEW!"}];
    for (Deck *d in removableDecks) {
        [d delete];
    }
}

- (BOOL) isFavoriteDeck:(Deck *)deck
{
    if (!deck) {
        return NO;
    } else {
        if ([deck is_deck_favorite]) {
            return YES;
        } else {
            return NO;
        }
    }
}


#pragma mark - Singleton Init

+ (AccountManager *)sharedManager
{
    static dispatch_once_t pred;
    static AccountManager *_sharedManager = nil;
    
    dispatch_once(&pred, ^{ _sharedManager = [[self alloc] init]; });
    return _sharedManager;
}

@end
