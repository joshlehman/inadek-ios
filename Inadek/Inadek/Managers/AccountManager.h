//
//  AccountManager.h
//  Inadek
//
//  Created by Josh Lehman on 1/19/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Account+Core.h"
#import "Deck+Core.h"

@interface AccountManager : NSObject

#pragma mark - Singleton Headers

+ (AccountManager *)sharedManager;
- (Account *)currentAccount;
- (NSNumber *)currentAccountId;

- (NSString *) profileCardCountString;
- (NSString *) favDeckCountString;
- (NSString *) fanCountString;
- (NSString *) groupDeckCountString;

- (BOOL)isFavoriteDeck:(Deck *)deck;

- (void) cleanData;

@end
