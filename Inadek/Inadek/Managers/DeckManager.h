//
//  DeckManager.h
//  Inadek
//
//  Created by Josh Lehman on 1/15/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Common.h"
#import "InadekAPIManager.h"

#import "Deck+Core.h"
#import "Card+Core.h"

@interface DeckManager : NSObject

#pragma mark - Singleton Headers

+ (DeckManager *)sharedManager;

- (NSArray *)localDeckTypes:(NSDictionary *)findCriteria;
- (NSDictionary *)deckTypeDetailsByCid:(NSString *)cid;
- (NSDictionary *)deckTypeDetailsById:(NSNumber *)type_id;

- (NSDictionary *)cardTypeDetailsByCid:(NSString *)cid;
- (NSDictionary *)cardTypeDetailsById:(NSNumber *)type_id;

- (void)localDecksWithBlock:(NSString *)filter callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)localCardsWithBlock:(NSString *)filter callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

- (void)searchForDecks:(NSString *)search_term callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)searchForCategoryDecks:(NSNumber *)category_id callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

- (void)addDeckToDeck:(Deck *)insertingDeck intoDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;


- (NSArray *)myLocalDecks;

- (NSArray *)getAllCategories;

- (BOOL)saveDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (BOOL)saveCard:(Card *)card callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)updateDeckCardSort:(NSArray *)decks;
- (void)updateCardSort:(NSArray *)cards forDeck:(Deck *)deck;

- (BOOL)deleteDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (BOOL)deleteCard:(Card *)card callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

- (NSArray *)localCardTypes:(NSDictionary *)findCriteria;
- (NSArray *)allDeckNames;
- (NSArray *)localProfileCards;
- (NSArray *)localFavoriteDecks;

// --- Deck Actions

- (void)favoriteDeck:(Deck *)deck;
- (void)followDeck:(Deck *)deck;
- (void)addCommentToDeck:(Deck *)deck;





@end
