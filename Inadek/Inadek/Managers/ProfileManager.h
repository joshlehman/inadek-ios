//
//  ProfileManager.h
//  Inadek
//
//  Created by Josh Lehman on 4/18/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Common.h"
#import "InadekAPIManager.h"

#import "Deck+Core.h"
#import "Card+Core.h"

@interface ProfileManager : NSObject

+ (ProfileManager *)sharedManager;

- (void)localProfilesWithBlock:(NSString *)filter callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

@end
