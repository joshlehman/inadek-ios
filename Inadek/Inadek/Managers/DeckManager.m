//
//  DeckManager.m
//  Inadek
//
//  Created by Josh Lehman on 1/15/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "DeckManager.h"
#import "AccountManager.h"

#import "CardField+Core.h"
#import "CardImage.h"

@implementation DeckManager


- (NSArray *)localDeckTypes:(NSDictionary *)findCriteria
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"Defaults: %@", defaults);
    
    NSArray *retData = NSArray.new;
    if ([defaults objectForKey:@"deck_types"]) {
        if (findCriteria) {
            NSString *key = [[findCriteria allKeys] objectAtIndex:0];
            NSString *value = [findCriteria objectForKey:key];
            retData = [defaults objectForKey:@"deck_types"];
            NSArray *filtered = NSArray.new;
            if ([key isEqualToString:@"segment"]) {
                filtered = [retData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(segment == %@)", value]];
            }
            
            return filtered;
        }
        
        return [defaults objectForKey:@"deck_types"];
    }
    
    return [[NSArray alloc] init];
    
    
}

- (NSArray *)localCardTypes:(NSDictionary *)findCriteria
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"Defaults: %@", defaults);
    
    NSArray *retData = NSArray.new;
    if ([defaults objectForKey:@"card_types"]) {
        if (findCriteria) {
            NSString *key = [[findCriteria allKeys] objectAtIndex:0];
            id value = [findCriteria objectForKey:key];
            retData = [defaults objectForKey:@"card_types"];
            NSArray *filtered = NSArray.new;
            if ([key isEqualToString:@"segment"]) {
                if ([value isKindOfClass:[NSString class]]) {
                    filtered = [retData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(segment == %@)", value]];
                    return filtered;
                } else if ([value isKindOfClass:[NSArray class]]) {
                    NSMutableArray *filtered_results = NSMutableArray.new;
                    for (NSString *v in value) {
                        filtered = [retData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(segment == %@)", v]];
                        [filtered_results addObjectsFromArray:filtered];
                    }
                    
                    return filtered_results;
                }
            }
        }
        
        return [defaults objectForKey:@"card_types"];
    }
    
    return [[NSArray alloc] init];
    
    
}

- (NSDictionary *)deckTypeDetailsById:(NSNumber *)type_id
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *retData = NSArray.new;
    NSArray *filtered = NSArray.new;
    if ([defaults objectForKey:@"deck_types"]) {
        retData = [defaults objectForKey:@"deck_types"];
        filtered = [retData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(id == %@)", type_id]];
    }
    
    if (filtered && filtered.count > 0) {
        NSDictionary *retValue = [filtered objectAtIndex:0];
        return retValue;
    } else {
        return nil;
    }
}

- (NSDictionary *)deckTypeDetailsByCid:(NSString *)cid
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *retData = NSArray.new;
    NSArray *filtered = NSArray.new;
    if ([defaults objectForKey:@"deck_types"]) {
        retData = [defaults objectForKey:@"deck_types"];
        filtered = [retData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(cid == %@)", cid]];
    }
    
    if (filtered && filtered.count > 0) {
        NSDictionary *retValue = [filtered objectAtIndex:0];
        return retValue;
    } else {
        return nil;
    }
}

- (NSDictionary *)cardTypeDetailsById:(NSNumber *)type_id
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *retData = NSArray.new;
    NSArray *filtered = NSArray.new;
    if ([defaults objectForKey:@"card_types"]) {
        retData = [defaults objectForKey:@"card_types"];
        filtered = [retData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(id == %@)", type_id]];
    }
    
    if (filtered && filtered.count > 0) {
        NSDictionary *retValue = [filtered objectAtIndex:0];
        return retValue;
    } else {
        return nil;
    }
}

- (NSDictionary *)cardTypeDetailsByCid:(NSString *)cid
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *retData = NSArray.new;
    NSArray *filtered = NSArray.new;
    if ([defaults objectForKey:@"card_types"]) {
        retData = [defaults objectForKey:@"card_types"];
        filtered = [retData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(cid == %@)", cid]];
    }
    
    if (filtered && filtered.count > 0) {
        NSDictionary *retValue = [filtered objectAtIndex:0];
        return retValue;
    } else {
        return nil;
    }
}

- (void)localDecksWithBlock:(NSString *)filter callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    if ([filter isEqualToString:@"popular"]) {
        
        NSDate *now = NSDate.date;
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"last_updated"]) {
            NSDate *last_updated = [[NSUserDefaults standardUserDefaults] objectForKey:@"last_updated"];
            
            
            if( [now timeIntervalSinceDate:last_updated] < (60 * 60)) {
                DLog(@"No need to refresh again")
                
                NSMutableArray *nearby = [self localNearbyDecks];
                NSMutableArray *featured_categories = [self localFeaturedCategories];
                
                NSMutableArray *collection_array = NSMutableArray.new;
                if (nearby && [nearby count] != 0) {
                    [collection_array addObject:@{@"title":@"Nearby",@"summary":@"",@"decks":nearby}];
                }
                
                if (featured_categories && [featured_categories count] != 0) {
                    for (DeckCategory *dc in featured_categories) {
                        NSArray *theseDecks = [dc.decks allObjects];
                        if ([theseDecks count] > 0) {
                            [collection_array addObject:@{@"title":dc.name,@"summary":dc.summary,@"decks":[dc.decks allObjects]}];
                        }
                    }
                }
                
                NSArray *myDecks = [self myLocalDecks];
                if ([myDecks count] > 0) {
                    [collection_array addObject:@{@"title":@"Your Decks",@"summary":@"Inadek is powered by you! Quickly access the decks you have created in Inadek and share with others.",@"decks":myDecks}];
                }
                
                [collection_array addObject:@{@"type":@"category_cell",@"title":@"Browse Categories",@"category_id":@0}];
                
                callback(collection_array);
                
                return;
            }
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:now forKey:@"last_updated"];
        [defaults synchronize];
        
        [[InadekAPIManager sharedManager] fetchFeaturedDecks:nil callback:^(id responseObject) {
            
            NSMutableArray *nearby = [self localNearbyDecks];
            NSMutableArray *featured_categories = [self localFeaturedCategories];
            
            NSMutableArray *collection_array = NSMutableArray.new;
            if (nearby && [nearby count] != 0) {
                [collection_array addObject:@{@"title":@"Nearby",@"summary":@"",@"decks":nearby}];
            }
            
            if (featured_categories && [featured_categories count] != 0) {
                for (DeckCategory *dc in featured_categories) {
                    NSArray *theseDecks = [dc.decks allObjects];
                    if ([theseDecks count] > 0) {
                        [collection_array addObject:@{@"title":dc.name,@"summary":dc.summary,@"decks":[dc.decks allObjects]}];
                    }
                }
            }
            
            NSArray *myDecks = [self myLocalDecks];
            if ([myDecks count] > 0) {
                [collection_array addObject:@{@"title":@"Your Decks",@"summary":@"Inadek is powered by you! Quickly access the decks you have created in Inadek and share with others.",@"decks":myDecks}];
            }
            
            [collection_array addObject:@{@"type":@"category_cell",@"title":@"Browse Categories",@"category_id":@0}];
            
            callback(collection_array);
            
        } failureCallback:^(id responseObject) {
            failureCallback(responseObject);
        }];
        
    } else if ([filter isEqualToString:@"mine"]) {
        [[InadekAPIManager sharedManager] fetchMyDecks:nil callback:^(id responseObject) {
            
            NSMutableArray *collection_array = NSMutableArray.new;
            
            NSArray *myDecks = [self myLocalDecks];
            if ([myDecks count] > 0) {
                [collection_array addObject:@{@"title":@"My Decks",@"summary":@"Inadek is powered by you! Quickly access the decks you have created in Inadek and share with others.",@"decks":myDecks}];
            }
            
            NSArray *myFollowedDecks = [self myLocalFollowedDecks];
            if ([myFollowedDecks count] > 0) {
                [collection_array addObject:@{@"title":@"My Followed Decks",@"summary":@"By following a deck you will be alerted when updates or new cards are added. Here are the decks you currently follow.",@"decks":myFollowedDecks}];
            }
            
            callback(collection_array);
            
        } failureCallback:^(id responseObject) {
            failureCallback(responseObject);
        }];
        
    }
    
    if ([filter isEqualToString:@"favorite"]) {
        [[InadekAPIManager sharedManager] fetchFavoriteDecks:nil callback:^(id responseObject) {
            
            NSMutableArray *favorite = [self localFavoriteDecks];
            
            NSMutableArray *collection_array = NSMutableArray.new;
            if (favorite && [favorite count] != 0) {
                [collection_array addObject:@{@"title":@"Your Favorites",@"summary":@"",@"decks":favorite}];
            }
            
            callback(collection_array);
            
        } failureCallback:^(id responseObject) {
            failureCallback(responseObject);
        }];
        
    }

}

- (void)localCardsWithBlock:(NSString *)filter callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    if ([filter isEqualToString:@"profiles"]) {
        [[InadekAPIManager sharedManager] fetchProfileCards:nil callback:^(id responseObject) {
            
            NSMutableArray *my_profile_cards = [[self localProfileCards] mutableCopy];
            
            NSMutableArray *collection_array = NSMutableArray.new;
            
            [collection_array addObject:@{@"title":@"Your Profile Cards",@"summary":@"The profile cards you have created since joining Inadek.",@"cards":my_profile_cards}];
            
            callback(collection_array);
            
        } failureCallback:^(id responseObject) {
            failureCallback(responseObject);
        }];
        
    }
    
}

- (void)searchForDecks:(NSString *)search_term callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    [[InadekAPIManager sharedManager] searchForDecks:@{@"search_term":search_term} callback:^(id responseArray) {
        
        NSMutableArray *collection_array = NSMutableArray.new;
        
        NSArray *myDecks = responseArray;
        if ([myDecks count] > 0) {
            [collection_array addObject:@{@"title":@"Search Results",@"summary":[NSString stringWithFormat:@"Decks that match your search for '%@'", search_term],@"decks":myDecks}];
        }
        
        callback(collection_array);
        
    } failureCallback:^(id responseObject) {
        failureCallback(responseObject);
    }];
}

- (void)searchForCategoryDecks:(NSNumber *)category_id callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    if ([category_id integerValue] == 0) {
        
        NSMutableArray *collection_array = NSMutableArray.new;
        
        [collection_array addObject:@{@"type":@"back",@"title":@"Back",@"category_id":@0}];
        
        NSArray *categories = [self getAllCategories];
        for (DeckCategory *cat in categories) {
            [collection_array addObject:@{@"type":@"category_cell",@"title":cat.name,@"category_id":cat.remoteID}];
        }
        
        callback(collection_array);
        
    } else {
        
        
        
        [[InadekAPIManager sharedManager] searchForCategoryDecks:@{@"category_id":category_id} callback:^(id responseArray) {
            
            NSMutableArray *collection_array = NSMutableArray.new;
            
            [collection_array addObject:@{@"type":@"category_cell",@"title":@"Back",@"category_id":@0,@"direction":@"back"}];
            
            NSArray *myDecks = responseArray;
            
            DeckCategory *dc = [DeckCategory where:@{@"remoteID":category_id}].firstObject;
            
            NSString *cat_name = @"Decks";
            if (dc) {
                cat_name = dc.name;
            }
            
            if ([myDecks count] > 0) {
                [collection_array addObject:@{@"title":cat_name,@"summary":[NSString stringWithFormat:@"%@", @""],@"decks":myDecks}];
            }
            
            callback(collection_array);
            
        } failureCallback:^(id responseObject) {
            failureCallback(responseObject);
        }];
        
    }
    
    
}

- (void)addDeckToDeck:(Deck *)insertingDeck intoDeck:(Deck *)deck callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback

{
    if (!insertingDeck || !deck) {
        return;
    }
    
    [[InadekAPIManager sharedManager] insertDeckInDeck:@{@"deck":insertingDeck,@"in_deck":deck} callback:^(id responseArray) {
        callback(nil);
        
    } failureCallback:^(id responseObject) {
        failureCallback(responseObject);
    }];
}

- (NSArray *)localProfileCards
{
    NSArray *cards = [Card where:@{@"owner_id":[[AccountManager sharedManager] currentAccountId],@"type_id":[Card getTypeIdFor:@"profile"]}];
    
    return cards;
}

- (NSArray *)myLocalDecks
{
    int account_id = [[[AccountManager sharedManager] currentAccountId] integerValue];
    
    NSArray *myDecks = [Deck where:@{@"owner_id":@(account_id)}];
    return myDecks;
}

- (NSArray *)myLocalFollowedDecks
{

    NSArray *myDecks = [Deck where:@{@"is_followed":@(1)}];
    return myDecks;
}

- (NSArray *)localNearbyDecks
{
    return NSArray.new;
}

- (NSArray *)localFavoriteDecks
{
    NSArray *myDecks = [Deck where:@{@"is_favorite":@YES}];
    return myDecks;
}

- (NSArray *)localFeaturedCategories
{
    NSArray *featured_categories = [DeckCategory where:@"is_featured = 1" order:@"sort_order ASC"];
    //NSArray *featured = [Deck where:@"is_popular = 1" order:@"updated_at DESC"];
    return featured_categories;
}

- (NSArray *)getAllCategories
{
    NSArray *categories = [DeckCategory allWithOrder:@"name ASC"];
    return categories;
}

- (NSArray *)allDeckNames
{
    NSArray *all_decks = [Deck all];
    NSMutableArray *deck_names = NSMutableArray.new;
    for (Deck *deck in all_decks) {
        [deck_names addObject:[NSString stringWithFormat:@"Name: %@", deck.name]];
    }
    
    return [NSArray arrayWithArray:deck_names];
}



/* -- SAVE METHODS -- */
- (BOOL)saveDeck:(Deck *)deck callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    if ([deck isValid]) {
        
        [[InadekAPIManager sharedManager] saveDeck:deck callback:^(id responseObject) {
            callback(responseObject);
        } failureCallback:^(id responseObject) {
            failureCallback(responseObject);
        }];
        
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)saveCard:(Card *)card callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    if ([card isValid]) {
        
        for (CardField *cf in [card.card_fields allObjects]) {
            for (CardImage *ci in [cf.cardimages allObjects]) {
                [ci queueImageForSync];
            }
        }
        
        [[InadekAPIManager sharedManager] saveCard:card callback:^(id responseObject) {
            [self saveCardImages:card];
            callback(responseObject);
        } failureCallback:^(id responseObject) {
            failureCallback(responseObject);
        }];
        
        return YES;
    } else {
        return NO;
    }
}

- (void)saveCardImages:(Card *)card
{
    
    for (CardField *cf in [card.card_fields allObjects]) {
        for (CardImage *ci in [cf.cardimages allObjects]) {
            [[InadekAPIManager sharedManager] syncCardImage:ci forCard:card];
        }
    }
    
}

- (void)updateCardSort:(NSArray *)cards forDeck:(Deck *)deck
{
    NSString *card_order_string = @"";
    NSString *seperator = @"";
    for (Card *card in cards) {
        card_order_string = [card_order_string stringByAppendingString:[NSString stringWithFormat:@"%@%@", seperator, card.remoteID]];
        seperator = @"|";
    }
    
    [[InadekAPIManager sharedManager] updateCardSort:@{@"card_sort":card_order_string,@"deck":deck} callback:^(id responseObject) {
        //
    } failureCallback:^(id responseObject) {
        //
    }];
}

- (void)updateDeckCardSort:(NSArray *)decks
{
    for (NSDictionary *d in decks) {
        if ([d objectForKey:@"deck_id"] && [d objectForKey:@"cards"] && [[d objectForKey:@"cards"] isKindOfClass:[NSArray class]]) {
            
            Deck *deck = [Deck where:@{@"remoteID":[d objectForKey:@"deck_id"]}].firstObject;
            
            if (deck) {
                
                NSString *card_order_string = @"";
                NSString *seperator = @"";
                for (NSNumber *card_id in [d objectForKey:@"cards"]) {
                    card_order_string = [card_order_string stringByAppendingString:[NSString stringWithFormat:@"%@%@", seperator, card_id]];
                    seperator = @"|";
                }
                deck.card_order = card_order_string;
                deck.save;
                
            }
        }
    }
}

- (void)favoriteDeck:(Deck *)deck
{
    [[InadekAPIManager sharedManager] favoriteDeck:deck callback:^(id responseObject) {
        //
    } failureCallback:^(id responseObject) {
        //
    }];
}

- (void)followDeck:(Deck *)deck
{
    [[InadekAPIManager sharedManager] followDeck:deck callback:^(id responseObject) {
        //
    } failureCallback:^(id responseObject) {
        //
    }];
}


- (void)addCommentToDeck:(Deck *)deck
{
    
}

- (BOOL)deleteDeck:(Deck *)deck callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    if (deck) {

        [[InadekAPIManager sharedManager] deleteDeck:deck callback:^(id responseObject) {
            
            [deck deleteLocal];
            
            callback(responseObject);
        } failureCallback:^(id responseObject) {
            failureCallback(responseObject);
        }];
        
        return YES;
    } else {
        return NO;
    }
    

}

- (BOOL)deleteCard:(Card *)card callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    [[InadekAPIManager sharedManager] deleteCard:card callback:^(id responseObject) {
        
        [card deleteLocal];
        
        callback(responseObject);
    } failureCallback:^(id responseObject) {
        failureCallback(responseObject);
    }];
    
    return YES;
}


#pragma mark - Singleton Init

+ (DeckManager *)sharedManager
{
    static dispatch_once_t pred;
    static DeckManager *_sharedManager = nil;
    
    dispatch_once(&pred, ^{ _sharedManager = [[self alloc] init]; });
    return _sharedManager;
}

@end
