//
//  InadekLocationManager.h
//  Inadek
//
//  Created by Josh Lehman on 1/20/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "UIAlertView+BCAlertView.h"

@interface InadekLocationManager : NSObject <CLLocationManagerDelegate> {
    BOOL isLocationDisabled;
    BOOL locationPromptInProgress;
    BOOL locationSearchInProgress;
}

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) NSMutableArray *locationResults;

#pragma mark - Singleton Headers

+ (InadekLocationManager *)sharedManager;

- (void)getCurrentLocation;

@end
