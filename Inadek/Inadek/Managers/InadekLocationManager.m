//
//  InadekLocationManager.m
//  Inadek
//
//  Created by Josh Lehman on 1/20/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "InadekLocationManager.h"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@implementation InadekLocationManager {
    NSMutableArray *locationResults;
    CLLocation *thisLocation;
    BOOL queryInProgress;
}

- (void)getCurrentLocation
{
    if(IS_OS_8_OR_LATER) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            locationPromptInProgress = YES;
        }
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
        
    }
    
    thisLocation = [[CLLocation alloc] init];
    self.locationResults = NSMutableArray.new;
    locationSearchInProgress = YES;
    
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
        NSLog(@"Not authorized!");
        isLocationDisabled = YES;
        
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        if ([userDefaults objectForKey:@"LocationServices-DisableAll"] && [userDefaults objectForKey:@"LocationServices-DisableAll"] == [NSNumber numberWithBool:YES]) {
            return;
        }
        
        if (locationPromptInProgress) {
            return;
        }
        
        NSString *otherButton = nil;
        NSString *messageText = @"You have chosen to disabled location for this app. You will not be able to save new decks or cards with location information.";
        
        if (IS_OS_8_OR_LATER) {
            otherButton = @"Open Settings";
            messageText = @"You have chosen to disabled location for this app. You will not be able to save new decks or cards with location information unless you re-enable these services. To do this open the Settings app below and enabled Location Services.";
        }
        
        UIAlertView *errorPrompt = [[UIAlertView alloc] initWithTitle:@"Location Disabled" message:messageText delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Disable Location Permanently", otherButton, nil];
        
        [errorPrompt showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
            NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
            if (buttonIndex == 0) {
                // Cancel
                [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:@"LocationServices-DisableAll"];
                [userDefaults synchronize];
            } else if (buttonIndex == 1) {
                // User never wants us to ask about location again, ever... bummer
                [userDefaults setObject:[NSNumber numberWithBool:YES] forKey:@"LocationServices-DisableAll"];
                [userDefaults synchronize];
                
            } else if (buttonIndex == 2) {
                // Open settings app...
                [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:@"LocationServices-DisableAll"];
                [userDefaults synchronize];
                if (IS_OS_8_OR_LATER) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }
            }
        }];
        
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized) {
        isLocationDisabled = NO;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"didUpdateToLocation: %@", [locations lastObject]);
    thisLocation = [locations lastObject];
    
    if (thisLocation != nil) {
        
    }
    
    [manager stopUpdatingLocation];
    
}

#pragma mark - Singleton Init

+ (InadekLocationManager *)sharedManager
{
    static dispatch_once_t pred;
    static InadekLocationManager *_sharedManager = nil;
    
    dispatch_once(&pred, ^{
        _sharedManager = [[self alloc] init];
        _sharedManager.locationManager = [[CLLocationManager alloc] init];
        _sharedManager.locationManager.delegate = _sharedManager;
        _sharedManager.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        _sharedManager.locationResults = NSMutableArray.new;
        
    });
    return _sharedManager;
}

@end
