//
//  ProfileManager.m
//  Inadek
//
//  Created by Josh Lehman on 4/18/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "ProfileManager.h"
#import "AccountManager.h"

@implementation ProfileManager

#pragma mark - Singleton Init

+ (ProfileManager *)sharedManager
{
    static dispatch_once_t pred;
    static ProfileManager *_sharedManager = nil;
    
    dispatch_once(&pred, ^{ _sharedManager = [[self alloc] init]; });
    return _sharedManager;
}

- (void)localProfilesWithBlock:(NSString *)filter callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSNumber *account_id = [[AccountManager sharedManager] currentAccountId];
    
    if ([filter isEqualToString:@"fans"]) {
        [[InadekAPIManager sharedManager] fetchProfiles:@{@"group":@"fans",@"account":account_id} callback:^(id responseObject) {
            
            NSMutableArray *fans = [self localFans];
            
            NSMutableArray *collection_array = NSMutableArray.new;
            [collection_array addObject:@{@"title":@"Fans",@"profiles":fans}];
            
            callback(collection_array);
            
        } failureCallback:^(id responseObject) {
            failureCallback(nil);
        }];
        
    }
}

- (NSMutableArray *)localFans
{
    NSArray *fans = @[];
    
    return [fans mutableCopy];
}

@end
