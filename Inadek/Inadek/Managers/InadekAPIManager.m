//
//  InadekAPIManager.m
//  Inadek
//
//  Created by Josh Lehman on 1/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "InadekAPIManager.h"
#import "AccountManager.h"

//#define kBaseUrl        @"http://inadek-brightcraft.rhcloud.com"
//#define kBaseUrl        @"http://192.168.1.16:3000"
//#define kBaseUrl        @"http://0.0.0.0:3000"
#define kBaseUrl        @"http://inadek.herokuapp.com"
//#define kBaseUrl        @"http://localhost:3000"

#define kAPISuffix      @".json"

#define kAPIPathDecks       @"api/v1/decks"
#define kAPIPathProfiles    @"api/v1/profiles"
#define kAPIPathMeta        @"api/v1/metadata"
#define kAPIPathAccount     @"api/v1/accounts"
#define kAPIPathAuth        @"api/v1/auth"
#define kAPIPathCards       @"api/v1/cards"
#define kAPIPathNotices     @"api/v1/notifications"

#define kLogResponse        0


@implementation InadekAPIManager

- (void)fetchDecks:(void (^)(id))callback
{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", kAPIPathDecks, kAPISuffix];
    
    DLog(@"\r* * * STARTING call to: %@ \r* * *\r\r", urlPath);
    
    [self GET:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeDecks:[responseObject objectForKey:@"decks"] mapCards:NO setFeatured:NO];
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:YES];
            // First, build the decks with objects in the DECKS variable
                // Update where the remoteID already exists, and where it doesn't add to the DB
            
            // Now, create cards with the cards in the CARDS variable, and map to newly created decks
            
            if (callback) {
                callback(responseObject);
            }
        }
            
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
    }];
}

- (void)fetchFeaturedDecks:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathDecks, @"/featured", kAPISuffix, [self accountSuffix]];

    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, params);
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeDecks:[responseObject objectForKey:@"decks"] mapCards:NO setFeatured:YES];
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:YES];
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}

- (void)fetchMyDecks:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathDecks, @"/mine", kAPISuffix, [self accountSuffix]];
    
    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, params);
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeDecks:[responseObject objectForKey:@"decks"] mapCards:NO setFeatured:NO];
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:YES];
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}

- (void)fetchFavoriteDecks:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathDecks, @"/favorites", kAPISuffix, [self accountSuffix]];
    
    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, params);
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeDecks:[responseObject objectForKey:@"decks"] mapCards:NO setFeatured:NO];
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:YES];
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}

- (void)searchForDecks:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathDecks, @"/search", kAPISuffix, [self accountSuffix]];
    
    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, params);
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            NSMutableArray *decks = [self storeDecks:[responseObject objectForKey:@"decks"] mapCards:NO setFeatured:NO];
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:YES];
            
            if (callback) {
                callback(decks);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}

- (void)searchForCategoryDecks:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@/category/%@%@", kAPIPathDecks, [params objectForKey:@"category_id"], kAPISuffix, [self accountSuffix]];
    
    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, params);
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            NSMutableArray *decks = [self storeDecks:[responseObject objectForKey:@"decks"] mapCards:NO setFeatured:NO];
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:YES];
            
            if (callback) {
                callback(decks);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}

- (void)fetchProfileCards:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathDecks, @"/profile_cards", kAPISuffix, [self accountSuffix]];
    
    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, params);
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:NO];
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}

- (void)fetchProfiles:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathProfiles, @"/list", kAPISuffix, [self accountSuffix]];
    
    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, params);
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            int thistype = kProfileTypeGeneral;
            if (params && [params objectForKey:@"group"]) {
                if ([[params objectForKey:@"group"] isEqualToString:@"fans"]) {
                    thistype = kProfileTypeFan;
                } else if ([[params objectForKey:@"group"] isEqualToString:@"friends"]) {
                    thistype = kProfileTypeFriend;
                }
            }
            
            [self storeProfiles:[responseObject objectForKey:@"profiles"] asType:thistype];
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
    
}

- (void)fetchMetadata:(void (^)(id))callback
{
    //NSArray *decks = NSArray.new;
    //callback(decks);
    
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", kAPIPathMeta, kAPISuffix];
    
    DLog(@"\r* * * STARTING call to: %@ \r* * *\r\r", urlPath);
    
    [self GET:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeMetadata:responseObject];
            // BUILD METADATA STORE PLACED IN USERDEFAULTS DICTIONARY
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:nil];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
    }];
}

- (void)fetchNotificationObjects:(AccountNotification *)notification callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSNumber *deck_id = nil;
    NSNumber *card_id = nil;
    
    NSString *urlPath = @"";
    
    if ((notification && notification.deck_id && [notification.deck_id integerValue] != 0) || (notification && notification.card_id && [notification.card_id integerValue] != 0)) {
        if (notification.deck_id) {
            deck_id = notification.deck_id;
            
            urlPath = [NSString stringWithFormat:@"%@/%@%@%@", kAPIPathDecks, deck_id, kAPISuffix, [self accountSuffix]];
            
        } else if (notification.card_id) {
            card_id = notification.card_id;
            
            urlPath = [NSString stringWithFormat:@"%@/%@%@%@", kAPIPathCards, card_id, kAPISuffix, [self accountSuffix]];
            
        }
    } else {
        callback(nil);
        return;
    }
    
    if ([urlPath isEqualToString:@""]) {
        callback(nil);
        return;
    }
    
    [self GET:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            NSMutableArray *decks = [self storeDecks:[responseObject objectForKey:@"decks"] mapCards:NO setFeatured:NO];
            
            BOOL should_map_decks = YES;
            if ([[responseObject objectForKey:@"decks"] count] == 0) {
                should_map_decks = NO;
            }
            
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:should_map_decks];
            
            if (callback) {
                callback(decks);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}

- (void)createAccount:(NSDictionary *)userParams callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", kAPIPathAccount, @""];
    
    NSMutableDictionary *postParams = NSMutableDictionary.new;
    [postParams setObject:userParams forKey:@"account"];
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    [self POST:urlPath parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeAccount:responseObject];
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
    }];
    
}

- (void)loginAccount:(NSDictionary *)userParams callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", kAPIPathAuth, @"/login"];
    
    NSMutableDictionary *postParams = NSMutableDictionary.new;
    [postParams setObject:userParams forKey:@"auth"];
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    [self POST:urlPath parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeAccount:responseObject];
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
    }];
    
}

- (void)loginFacebookAccount:(NSDictionary *)userParams callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@", kAPIPathAuth, @"/facebook"];
    
    NSMutableDictionary *postParams = NSMutableDictionary.new;
    [postParams setObject:userParams forKey:@"auth"];
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    [self POST:urlPath parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (kLogResponse == 1) {
                NSLog(@"\r  Call: %@ \r  Response: %@", urlPath, responseObject);
            }
            
            [self storeAccount:responseObject];
            
            if (callback) {
                callback(responseObject);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
    }];
    
}

- (void)insertDeckInDeck:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    Deck *initialDeck = [params objectForKey:@"deck"];
    Deck *intoDeck = [params objectForKey:@"in_deck"];
    
    NSString *urlPath = [NSString stringWithFormat:@"%@/insert_deck/%@/%@/%@", kAPIPathDecks, initialDeck.remoteID, intoDeck.remoteID, [self accountSuffix]];
    
    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, nil);
    
    [self POST:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject objectForKey:@"decks"]) {
            [self storeDecks:[responseObject objectForKey:@"decks"] mapCards:NO setFeatured:NO];
        }
        
        if ([responseObject objectForKey:@"cards"]) {
            [self storeCards:[responseObject objectForKey:@"cards"] mapDecks:YES];
        }
        
        if (callback) {
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}

- (void)updateCardSort:(NSDictionary *)params callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    Deck *deck = [params objectForKey:@"deck"];
    
    NSString *urlPath = [NSString stringWithFormat:@"%@/resort/%@/%@", kAPIPathDecks, deck.remoteID, [self accountSuffix]];
    
    DLog(@"\r* * * STARTING call to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, nil);
    
    [self POST:urlPath parameters:@{@"card_sort":[params objectForKey:@"card_sort"]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject objectForKey:@"deck"]) {
            [self storeDecks:@[[responseObject objectForKey:@"deck"]] mapCards:NO setFeatured:NO];
        }
        
        if (callback) {
            callback(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED call to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
        
    }];
}


- (void)saveDeck:(Deck *)deck callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@", kAPIPathDecks];
    
    NSMutableDictionary *postParams = [deck syncParams];
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    __block __weak Deck *weakDeck = deck;
    [self POST:urlPath parameters:postParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if ([weakDeck imageObj] != nil) {
            DLog(@"\r      APPENDING IMAGE");
            NSData *imageToUpload = UIImageJPEGRepresentation([weakDeck imageObj], 0.9);
            [formData appendPartWithFileData:imageToUpload name:@"deck_image[image]" fileName:@"deck.jpeg" mimeType:@"image/jpeg"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject objectForKey:@"deck"]) {
            [self updateDeck:weakDeck withData:[responseObject objectForKey:@"deck"]];
        } else {
            [self updateDeck:weakDeck withData:responseObject];
        }
        callback(nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
    }];
    
}

- (void)updateDeck:(Deck *)deck withData:(NSDictionary *)obj
{
    NSArray *decks = [Deck all];
    if (deck) {
        [deck unpackDictionary:obj];
        [deck save];
    }
    decks = [Deck all];
    
}

- (void)favoriteDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback
{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathDecks, @"/", deck.remoteID, @"/favorite"];
    
    NSDictionary *postParams = nil;
    if ([[AccountManager sharedManager] currentAccountId]) {
        postParams = @{@"a_id":[[AccountManager sharedManager] currentAccountId]};
    }
    
    [self POST:urlPath parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        
        callback(nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:nil];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
    }];
    
}

- (void)postComment:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback
{
    id thisObject = [params objectForKey:@"object"];
    
    NSString *urlPath = @"";
    if (thisObject && [thisObject isKindOfClass:[Card class]]) {
        Card *card = thisObject;
        urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathCards, @"/", card.remoteID, @"/add_comment"];
    } else if (thisObject && [thisObject isKindOfClass:[Deck class]]) {
        Deck *deck = thisObject;
        urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathDecks, @"/", deck.remoteID, @"/add_comment"];
    }
    
  
    NSMutableDictionary *postParams = nil;
    if ([[AccountManager sharedManager] currentAccountId]) {
        postParams = [@{@"a_id":[[AccountManager sharedManager] currentAccountId]} mutableCopy];
    }
    if ([params objectForKey:@"comment"]) {
        [postParams setObject:[params objectForKey:@"comment"] forKey:@"comment"];
    }
    
    [self POST:urlPath parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        
        Card *card = nil;
        if ([responseObject objectForKey:@"card"]) {
            [self storeCards:@[[responseObject objectForKey:@"card"]] mapDecks:YES];
            card = [Card where:@{@"remoteID":[[responseObject objectForKey:@"card"] objectForKey:@"id"]}].firstObject;
        }
        
        callback(card);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:nil];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
    }];
}

- (void)followDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback
{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@%@", kAPIPathDecks, @"/", deck.remoteID, @"/follow"];
    
    NSDictionary *postParams = nil;
    if ([[AccountManager sharedManager] currentAccountId]) {
        postParams = @{@"a_id":[[AccountManager sharedManager] currentAccountId]};
    }
    
    [self POST:urlPath parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        
        callback(nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:nil];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
    }];
    
}


- (void)deleteDeck:(Deck *)deck callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@/%@/delete", kAPIPathDecks, deck.remoteID];
    
    NSDictionary *postParams = nil;
    if ([[AccountManager sharedManager] currentAccountId]) {
        postParams = @{@"a_id":[[AccountManager sharedManager] currentAccountId]};
    }
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    __block __weak Deck *weakDeck = deck;
    [self POST:urlPath parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        
        callback(nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        
        failureCallback(error);
    }];
    
}

- (void)deleteCard:(Card *)card callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@/%@/delete", kAPIPathCards, card.remoteID];
    
    NSDictionary *postParams = nil;
    if ([[AccountManager sharedManager] currentAccountId]) {
        postParams = @{@"a_id":[[AccountManager sharedManager] currentAccountId]};
    }
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    __block __weak Card *weakCard = card;
    [self POST:urlPath parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        
        callback(nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        
        failureCallback(error);
    }];
    
}

- (void)registerDevice:(NSString *)deviceToken
{
    NSString *urlPath = [NSString stringWithFormat:@"%@/register_device", kAPIPathAccount];
    
    NSMutableDictionary *params = NSMutableDictionary.new;
    [params setObject:deviceToken forKey:@"device_token"];
    [params setObject:@"ios" forKey:@"platform"];
    [params setObject:[[AccountManager sharedManager] currentAccount].remoteID forKey:@"a_id"];
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"Device registered");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Failed to register device");
    }];
}

- (void)refreshNotifications:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@/recent", kAPIPathNotices];
    
    NSMutableDictionary *params = NSMutableDictionary.new;
    [params setObject:[[AccountManager sharedManager] currentAccount].remoteID forKey:@"a_id"];
    
    [self GET:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"Device registered");
        
        NSMutableArray *notices = [self storeNotifications:[responseObject objectForKey:@"notifications"]];
        
        callback(notices);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Failed to register device");
        failureCallback(error);
    }];
}

- (void)markNotificationsRead:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@/read", kAPIPathNotices];
    
    NSMutableDictionary *params = NSMutableDictionary.new;
    [params setObject:[[AccountManager sharedManager] currentAccount].remoteID forKey:@"a_id"];
    
    [self POST:urlPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        callback(nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureCallback(error);
    }];
}

- (NSMutableArray *)storeNotifications:(NSArray *)objects
{
    
    NSMutableArray *notifications = NSMutableArray.new;
    for (NSDictionary *obj in objects) {
        AccountNotification *this = [AccountNotification createWithData:obj];
        if (this) {
            [notifications addObject:this];
        }
    }
    
    return notifications;
}

- (void)storeProfiles:(NSArray *)objects asType:(int)profileType
{
    switch (profileType) {
        case kProfileTypeFan:
        {
            // Do something
            break;
        }
        
        case kProfileTypeFriend:
        {
            // Do something
            break;
        }
            
        case kProfileTypeGeneral:
        {
            // Do something
            break;
        }
            
        default:
            break;
    }
}

- (NSMutableArray *)storeDecks:(NSArray *)objects mapCards:(BOOL)should_map setFeatured:(BOOL)set_featured
{
    NSMutableArray *newdecks = NSMutableArray.new;
    for (NSDictionary *obj in objects) {
        
        NSArray *decks = [Deck all];
        for (Deck *d in decks) {
            if (d.remoteID == nil) {
                [d delete];
            }
        }
        
        Deck *existing = [Deck where:@{@"remoteID":[obj objectForKey:@"id"]}].firstObject;
        
        if (existing) {
            if ([obj objectForKey:@"status_id"] && [[obj objectForKey:@"status_id"] integerValue] == 0) {
                // Delete this deck, it has been deleted on the server
                [Common log:@"DELETING A DECK"];
                [existing delete];
            } else {
                [existing unpackDictionary:obj];
                if (set_featured) {
                    existing.is_popular = [NSNumber numberWithBool:YES];
                } else {
                    if (!existing.is_popular) {
                        existing.is_popular = [NSNumber numberWithBool:NO];
                    }
                }
                [existing save];
                
            }
        } else {
            Deck *newDeck = [Deck create];
            [newDeck unpackDictionary:obj];
            if (set_featured) {
                newDeck.is_popular = [NSNumber numberWithBool:YES];
            } else {
                newDeck.is_popular = [NSNumber numberWithBool:NO];
            }
            [newDeck save];
            existing = newDeck;
        }
        
        if (existing && [obj objectForKey:@"triggers"]) {
            NSArray *triggers = [obj objectForKey:@"triggers"];
            
            if ([[existing.triggers allObjects] count] > 0) {
                
                [existing removeTriggers:existing.triggers];
                
                for (NSDictionary *t in triggers) {
                    
                    DeckTrigger *existingTrig = [DeckTrigger where:@{@"remoteID":[t objectForKey:@"id"]}].firstObject;
                    if (existingTrig) {
                        [existingTrig unpackDictionary:t];
                        [existingTrig save];
                        [existing addTriggersObject:existingTrig];
                    } else {
                        DeckTrigger *newTrig = [DeckTrigger create];
                        [newTrig unpackDictionary:t];
                        [newTrig save];
                        [existing addTriggersObject:newTrig];
                    }

                }
            } else {
                for (NSDictionary *t in triggers) {
                    DeckTrigger *newTrig = [DeckTrigger create];
                    [newTrig unpackDictionary:t];
                    [newTrig save];
                    [existing addTriggersObject:newTrig];
                }
            }
        }
        
        if (existing) {
            [newdecks addObject:existing];
        }
        
        if (existing && should_map) {
            // Empty the SET of cards
            // Add all decks by ID to the card decks set
        }
    }
    
    return newdecks;
}

- (void)saveCard:(Card *)card callback:(void (^)(id))callback failureCallback:(void (^)(id))failureCallback
{
    NSString *urlPath = [NSString stringWithFormat:@"%@", kAPIPathCards];
    
    NSDictionary *postParams = [card syncParams];
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    __block __weak Card *weakCard = card;
    [self POST:urlPath parameters:postParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if ([weakCard imageObj] != nil) {
            DLog(@"\r      APPENDING IMAGE, POSTPONED");
//            NSData *imageToUpload = UIImageJPEGRepresentation([weakCard imageObj], 0.9);
//            [formData appendPartWithFileData:imageToUpload name:@"card_image[image]" fileName:@"card.jpeg" mimeType:@"image/jpeg"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject objectForKey:@"card"]) {
            [self updateCard:weakCard withData:[responseObject objectForKey:@"card"]];
        } else {
            [self updateCard:weakCard withData:responseObject];
        }
        
        if ([responseObject objectForKey:@"decks"]) {
            [[DeckManager sharedManager] updateDeckCardSort:[responseObject objectForKey:@"decks"]];
        }
        
        if ([weakCard imageObj] != nil) {
            [self syncCardPrimaryImage:weakCard];
        }
        
        callback(nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        failureCallback(@{@"error":[self getErrorMessage:operation]});
    }];
    
}

- (void)syncCardPrimaryImage:(Card *)card
{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@/%@/add_image", kAPIPathCards, card.remoteID];
    
    NSDictionary *postParams = @{};
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    __block __weak Card *weakCard = card;
    [self POST:urlPath parameters:postParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if ([weakCard imageObj] != nil) {
            DLog(@"\r      APPENDING IMAGE, ASYNC");
            NSData *imageToUpload = UIImageJPEGRepresentation([weakCard imageObj], 0.9);
            [formData appendPartWithFileData:imageToUpload name:@"card_image[image]" fileName:@"card.jpeg" mimeType:@"image/jpeg"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject objectForKey:@"card"]) {
            [self updateCard:weakCard withData:[responseObject objectForKey:@"card"]];
        } else {
            [self updateCard:weakCard withData:responseObject];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
    }];
    
}


- (void)syncCardImage:(CardImage *)card_image forCard:(Card *)card
{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@/%@/add_image", kAPIPathCards, card.remoteID];
    
    if (card_image.remoteID && [card_image.remoteID integerValue] != 0) {
        urlPath = [NSString stringWithFormat:@"%@/%@/update_image/%@", kAPIPathCards, card.remoteID, card_image.remoteID];
    }
    
    NSDictionary *postParams = [card_image syncParams];
    
    DLog(@"\r* * * STARTING POST to: %@ \rPARAMS: %@ \r* * *\r\r", urlPath, postParams);
    
    __block __weak CardImage *weakCardImage = card_image;
    [self POST:urlPath parameters:postParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (weakCardImage.image != nil) {
            DLog(@"\r      APPENDING IMAGE");
            NSData *imageToUpload = UIImageJPEGRepresentation(weakCardImage.image, 0.9);
            [formData appendPartWithFileData:imageToUpload name:@"image" fileName:@"card.jpeg" mimeType:@"image/jpeg"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"\r+ + + COMPLETED call to: %@ \r+ + +\r\r", urlPath);
        if ([responseObject objectForKey:@"card_image"]) {
            [self updateCardImage:weakCardImage withData:[responseObject objectForKey:@"card_image"]];
        } else {
            [weakCardImage setIs_synced:NO];
            [weakCardImage save];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self logError:error withResponseObject:operation.responseObject];
        DLog(@"\r- - - FAILED POST to: %@ \r- - -\r\r", urlPath);
        [weakCardImage setIs_synced:NO];
        [weakCardImage save];
    }];
    
}

- (void)updateCardImage:(CardImage *)card_image withData:(NSDictionary *)obj
{
    [card_image unpackDictionary:obj];
    [card_image save];
}

- (void)updateCard:(Card *)card withData:(NSDictionary *)obj
{
    if (card) {
        [card unpackDictionary:obj];
        [card save];
    }
}

- (void)storeCards:(NSArray *)objects mapDecks:(BOOL)should_map
{
    for (NSDictionary *obj in objects) {
        
        Card *existing = [Card where:@{@"remoteID":[obj objectForKey:@"id"]}].firstObject;
        if (existing) {
            if ([obj objectForKey:@"status_id"] && [[obj objectForKey:@"status_id"] integerValue] == 0) {
                // Delete this deck, it has been deleted on the server
                [existing delete];
            } else {
                [existing unpackDictionary:obj];
                [existing save];
            }
        } else {
            Card *newCard = [Card create];
            [newCard unpackDictionary:obj];
            [newCard save];
            existing = newCard;
        }
        
        if (existing && [obj objectForKey:@"triggers"]) {
            NSArray *triggers = [obj objectForKey:@"triggers"];
            
            if ([[existing.triggers allObjects] count] > 0) {
                
                [existing removeTriggers:existing.triggers];
                
                for (NSDictionary *t in triggers) {
                    
                    CardTrigger *existingTrig = [CardTrigger where:@{@"remoteID":[t objectForKey:@"id"]}].firstObject;
                    if (existingTrig) {
                        [existingTrig unpackDictionary:t];
                        [existingTrig save];
                        [existing addTriggersObject:existingTrig];
                    } else {
                        CardTrigger *newTrig = [CardTrigger create];
                        [newTrig unpackDictionary:t];
                        [newTrig save];
                        [existing addTriggersObject:newTrig];
                    }
                    
                }
            } else {
                for (NSDictionary *t in triggers) {
                    CardTrigger *newTrig = [CardTrigger create];
                    [newTrig unpackDictionary:t];
                    [newTrig save];
                    [existing addTriggersObject:newTrig];
                }
            }
        }
        
        if (existing && should_map) {
            // Empty the SET of decks
            // Add all cards by ID to the deck cards set
        }
        
    }
}

- (void)storeMetadata:(NSDictionary *)metadata
{
    
    if ([metadata objectForKey:@"deck_types"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[metadata objectForKey:@"deck_types"] forKey:@"deck_types"];
    }
    
    if ([metadata objectForKey:@"card_types"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[metadata objectForKey:@"card_types"] forKey:@"card_types"];
    }
    
    if ([metadata objectForKey:@"card_type_fields"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[metadata objectForKey:@"card_type_fields"] forKey:@"card_type_fields"];
    }
    
    if ([metadata objectForKey:@"triggers"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[metadata objectForKey:@"triggers"] forKey:@"triggers"];
    }
    
    if ([metadata objectForKey:@"hooks"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[metadata objectForKey:@"hooks"] forKey:@"hooks"];
    }
    
    if ([metadata objectForKey:@"fields"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[metadata objectForKey:@"fields"] forKey:@"fields"];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    // Process the categories, these go into Core Data
    if ([metadata objectForKey:@"categories"]) {
        for (NSDictionary *obj in [metadata objectForKey:@"categories"]) {
            
            DeckCategory *existing = [DeckCategory where:@{@"remoteID":[obj objectForKey:@"id"]}].firstObject;
            if (existing) {
                [existing unpackDictionary:obj];
                [existing save];
            } else {
                DeckCategory *newCat = [DeckCategory create];
                [newCat unpackDictionary:obj];
                [newCat save];
            }
        }
        
        [DeckCategory logObjectCounts];
    }
}

- (void)storeAccount:(NSDictionary *)obj
{
    if ([obj objectForKey:@"account"]) {
        NSDictionary *account_obj = [obj objectForKey:@"account"];
        Account *existing = [Account where:@{@"remoteID":[account_obj objectForKey:@"id"]}].firstObject;
        if (existing) {
            [existing unpackDictionary:account_obj];
            [existing save];
        } else {
            [Account deleteAll];
            Account *newAccount = [Account create];
            [newAccount unpackDictionary:account_obj];
            [newAccount save];
            existing = newAccount;
        }
    } else {
        // TRying to store an account iwth nothing.
    }
}


- (NSString *)getErrorMessage:(AFHTTPRequestOperation *)operation
{
    NSString *errorString = @"";
    if (operation && operation.responseObject && [operation.responseObject objectForKey:@"errors"]) {
        id errors = [operation.responseObject objectForKey:@"errors"];
        if ([errors isKindOfClass:[NSArray class]]) {
            for (NSString *err in errors) {
                errorString = [errorString stringByAppendingString:[NSString stringWithFormat:@"%@%@%@", @"\r", err, @"\r"]];
            }
        } else if ([errors isKindOfClass:[NSString class]]) {
            errorString = errors;
        }
    }
    
    return errorString;
}


- (void)logError:(NSError *)error withResponseObject:(NSDictionary *)responseObject
{
    NSString *errorString = @"\r\rERROR(S): ";
    if (responseObject && [responseObject objectForKey:@"errors"]) {
        
        if ([[responseObject objectForKey:@"errors"] isKindOfClass:[NSString class]]) {
            errorString = [errorString stringByAppendingString:[NSString stringWithFormat:@"%@%@%@", @"\r", [responseObject objectForKey:@"errors"], @"\r"]];
        } else if ([[responseObject objectForKey:@"errors"] isKindOfClass:[NSArray class]]) {
            NSArray *errors = [responseObject objectForKey:@"errors"];
            for (NSString *err in errors) {
                errorString = [errorString stringByAppendingString:[NSString stringWithFormat:@"%@%@%@", @"\r", err, @"\r"]];
            }
        }
    }
    
    if (error) {
        NSString *getError = error.localizedDescription;
        DLog(@"%@\rRESPONSE CODE:\r%@\r\r", errorString, getError);
    }
}

- (NSString *)accountSuffix
{
    if ([[AccountManager sharedManager] currentAccount]) {
        return [NSString stringWithFormat:@"?a_id=%@", [[AccountManager sharedManager] currentAccount].remoteID];
    } else {
        return @"";
    }
}




#pragma mark - Singleton Init

+ (InadekAPIManager *)sharedManager
{
    static dispatch_once_t pred;
    static InadekAPIManager *_sharedManager = nil;
    
    NSString *baseUrlString = kBaseUrl;
    
    dispatch_once(&pred, ^{ _sharedManager = [[self alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseUrlString, @"/"]]]; });
    return _sharedManager;
}



@end
