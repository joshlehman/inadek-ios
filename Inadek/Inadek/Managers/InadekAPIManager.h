//
//  InadekAPIManager.h
//  Inadek
//
//  Created by Josh Lehman on 1/11/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
#import "Common.h"
#import "DeckManager.h"

#import "CardImage.h"
#import "Deck+Core.h"
#import "Card+Core.h"
#import "Account+Core.h"
#import "DeckCategory+Core.h"
#import "AccountNotification.h"


@interface InadekAPIManager : AFHTTPRequestOperationManager

#pragma mark - Singleton Headers

+ (InadekAPIManager *)sharedManager;

// *** DECKS
- (void)fetchDecks:(void (^)(id responseObject))callback;
- (void)fetchFeaturedDecks:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)fetchMyDecks:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)fetchFavoriteDecks:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)fetchProfileCards:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)fetchNotificationObjects:(AccountNotification *)notification callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;


- (void)searchForDecks:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)searchForCategoryDecks:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)insertDeckInDeck:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)updateCardSort:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;


// *** USER PROFILES
- (void)fetchProfiles:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

// *** METADATA
- (void)fetchMetadata:(void (^)(id responseObject))callback;

// *** USER AUTH
- (void)createAccount:(NSDictionary *)userParams callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)loginAccount:(NSDictionary *)userParams callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)loginFacebookAccount:(NSDictionary *)userParams callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

// ** SAVE
- (void)saveDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)saveCard:(Card *)card callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

- (void)syncCardImage:(CardImage *)card_image forCard:(Card *)card;
- (void)syncCardPrimaryImage:(CardImage *)card_image forCard:(Card *)card;

- (void)favoriteDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

- (void)followDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

- (void)postComment:(NSDictionary *)params callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

- (NSMutableArray *)storeDecks:(NSArray *)objects mapCards:(BOOL)should_map setFeatured:(BOOL)set_featured;

// ** DELETE

- (void)deleteDeck:(Deck *)deck callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)deleteCard:(Card *)card callback:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;

- (void)registerDevice:(NSString *)deviceToken;
- (void)refreshNotifications:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;
- (void)markNotificationsRead:(void (^)(id responseObject))callback failureCallback:(void (^)(id responseObject))failureCallback;



@end
