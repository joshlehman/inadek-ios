//
//  BrightUIViewController.m
//  TheWishlist
//
//  Created by Josh Lehman on 10/28/14.
//  Copyright (c) 2014 joshlehman. All rights reserved.
//

#define MAS_SHORTHAND

#import "AppDelegate.h"
#import "BCViewController.h"
#import "BCTableViewCell.h"
#import <ObjectiveSugar/ObjectiveSugar.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import <pop/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>
#import "BCTransitionViewController.h"
#import "BCNavController.h"
#import "Common.h"


@interface BCViewController ()

@end

@implementation BCViewController


- (UIView *) placeSubviewNib:(NSString *)nibName
{
    UIView *subview = UIView.new;
    @try {
        
        NSArray *allViews = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        
        if ([allViews count] > 0) {
            subview = [allViews objectAtIndex:0];
        }
        
        [self.view addSubview:subview];
        [subview makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    } @catch (NSException *exception) {
        NSLog(@"Exception trying to load a NIB that doesn't exist");
        return subview;
    }
    
    return subview;
    
}

- (void)removeBackgroundView:(BOOL)animated
{
    UIView *bgView = [[self.view subviews] objectAtIndex:0];
    if ([bgView tag] == 99) {
        if (animated) {
            [UIView animateWithDuration:0.3 animations:^{
                [bgView setAlpha:0.0];
            } completion:^(BOOL finished) {
                [bgView removeFromSuperview];
            }];
        } else {
            [bgView removeFromSuperview];
        }
    }
}

- (UIView *)makeBackgroundView:(UIColor *)backgroundColor animate:(NSString *)animateStyle withCloseAction:(BOOL)hasCloseAction
{
    UIView *thisView = UIView.new;
    thisView.tag = 99;
    
    if (backgroundColor) {
       [thisView setBackgroundColor:backgroundColor];
    }
    
    [self.view insertSubview:thisView atIndex:0];
    
    [thisView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    if (animateStyle) {
        if ([animateStyle isEqualToString:@"fadein"]) {
            POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
            anim.fromValue = @(0.0);
            anim.toValue = @(1.0);
            [thisView setAlpha:0.0];
            anim.duration = 0.2;
            
            [thisView pop_addAnimation:anim forKey:@"PopFadeInAnimation"];
        }
    }
    
    if (hasCloseAction) {
        UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeMe:)];
        [tapGR setNumberOfTapsRequired:1];
        [thisView addGestureRecognizer:tapGR];
    }
    
    return thisView;
    
}

- (void)closeMe:(id)sender
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.transitionVC closeMe:self];
}

- (void)fadeMe:(id)sender withStatusBarStyle:(UIStatusBarStyle)style
{
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.transitionVC fadeMe:self withStatusBarStyle:style];
}

/*
 
 +  Pass in the holding view that all layout objects should sit within
 +  Pass in the view objects, at least an empty NEW object
 +  Ensure the objects have their target/actions pre-set and all that jazz, this method is purely for layout
 +  Pass in something that can describe their relative positions
 
 
 Relative layout is built into objects array
 + if an object in the array contains an "objects" entity these will be it's child objects
 + otherwise, the "object" entity will be the item to place
 + positional description is as follows
 example: w:f-h:f-p:10  <-- Creates full width, full height object with 10 pixels of edge inset padding inside parent master view
 + w:f|int|strings(1/2, 1/3, 1/4, 1/5) - defaults to f, the string type allows you to set a percentage value for inline elements
 + h:f|int - defaults to 44
 + p:int|comma seperated set of 4 ints - defaults to 0
 + b:inline|stack - defaults to stack
 stack elements will have the next element placed below them.
 inline elements will be placed NEXT to the previous element, be sure to set your widths accordingly to make this acceptable
 
 */
- (void)buildLayoutIntoView:(id)view withObjects:(NSArray *)objects
{
    
    __block UIView *priorObject = nil;
    [objects each:^(id object) {
        __block UIView *parentObject = view;
        NSLog(@"Object: %@", object);
        if ([object objectForKey:@"object"] != nil) {
            
            UIView *viewObject = [object objectForKey:@"object"];
            [self.view addSubview:viewObject];
            
            if ([object objectForKey:@"layout"] != nil) {
                NSDictionary *layoutAttributes = [self buildLayoutConstraints:[object objectForKey:@"layout"]];
                [viewObject mas_makeConstraints:^(MASConstraintMaker *make){
                    
                    
                    NSMutableDictionary *createdConstraints = NSMutableDictionary.new;
                    MASConstraint *cns_top = nil;
                    MASConstraint *cns_left = nil;
                    MASConstraint *cns_right = nil;
                    MASConstraint *cns_bottom = nil;
                    MASConstraint *cns_width = nil;
                    MASConstraint *cns_height = nil;
                    
                    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
                    if ([layoutAttributes objectForKey:@"padding"]) {
                        padding = [(NSValue *)[layoutAttributes objectForKey:@"padding"] UIEdgeInsetsValue];
                    }
                    
                    if ([layoutAttributes objectForKey:@"y_pos"]) {
                        padding.top += [[layoutAttributes objectForKey:@"y_pos"] floatValue];
                    }
                    
                    if ([layoutAttributes objectForKey:@"x_pos"]) {
                        padding.left += [[layoutAttributes objectForKey:@"x_pos"] floatValue];
                    }
                    
                    if ([layoutAttributes objectForKey:@"over"]) {
                        priorObject = view;
                    }
                    
                    
                    BOOL is_y_centered = NO;
                    BOOL is_x_centered = NO;
                    
                    if ([layoutAttributes objectForKey:@"x_origination"] && [[layoutAttributes objectForKey:@"x_origination"] isEqualToString:@"center"]) {
                        cns_left = make.center.equalTo(parentObject);
                        is_x_centered = YES;
                    }
                    
                    if ([layoutAttributes objectForKey:@"y_origination"] && [[layoutAttributes objectForKey:@"y_origination"] isEqualToString:@"center"]) {
                        cns_top = make.center.equalTo(parentObject);
                        is_y_centered = YES;
                    }
                    
                    
                    if (priorObject) {
                        if ([layoutAttributes objectForKey:@"block"] && [(NSString *)[layoutAttributes objectForKey:@"block"] isEqualToString:@"inline"]) {
                            if (!is_y_centered) cns_top = make.top.equalTo(priorObject).with.offset(padding.top);
                            if (!is_x_centered) cns_left = make.left.equalTo(priorObject).with.offset(padding.left);
                        } else {
                            if (!is_y_centered) cns_top = make.top.equalTo(priorObject).with.offset(padding.top);
                            if (!is_x_centered) cns_left = make.left.equalTo(priorObject).with.offset(padding.left);
                        }
                    } else {
                        if (!is_y_centered) cns_top = make.top.equalTo(parentObject).with.offset(padding.top);
                        if (!is_x_centered) cns_left = make.left.equalTo(parentObject).with.offset(padding.left);
                    }
                    
                    if ([layoutAttributes objectForKey:@"width"]) {
                        if ([[layoutAttributes objectForKey:@"width"] isKindOfClass:[NSString class]]) {
                            cns_left = make.left.equalTo(parentObject).with.insets(padding);
                            cns_right = make.right.equalTo(parentObject).with.insets(padding);
                        } else if ([[layoutAttributes objectForKey:@"width"] isKindOfClass:[NSNumber class]]) {
                            cns_width = make.width.equalTo([layoutAttributes objectForKey:@"width"]);
                        }
                    } else {
                        cns_width = make.width.equalTo(parentObject);
                    }
                    
                    if ([layoutAttributes objectForKey:@"height"]) {
                        if ([[layoutAttributes objectForKey:@"height"] isKindOfClass:[NSString class]]) {
                            cns_bottom = make.bottom.equalTo(parentObject).with.offset(-padding.bottom);
                        } else if ([[layoutAttributes objectForKey:@"height"] isKindOfClass:[NSNumber class]]) {
                            cns_height = make.height.equalTo([layoutAttributes objectForKey:@"height"]);
                        }
                    } else {
                        cns_height = make.height.equalTo(@44);
                    }
                    
                    if ([layoutAttributes objectForKey:@"animate_id"]) {
                        NSLog(@"Animatable found...");
                    }
                    
                    if (cns_top) [createdConstraints setObject:cns_top forKey:@"top"];
                    if (cns_left) [createdConstraints setObject:cns_left forKey:@"left"];
                    if (cns_right) [createdConstraints setObject:cns_right forKey:@"right"];
                    if (cns_bottom) [createdConstraints setObject:cns_bottom forKey:@"bottom"];
                    if (cns_width) [createdConstraints setObject:cns_width forKey:@"width"];
                    if (cns_height) [createdConstraints setObject:cns_height forKey:@"height"];
                    
                    if ([layoutAttributes objectForKey:@"animate_id"]) {
                        [self.animatingConstraints setObject:createdConstraints forKey:[layoutAttributes objectForKey:@"animate_id"]];
                    } else {
                        createdConstraints = nil;
                    }
                    //make.centerX.equalTo(parentObject);
                }];
            }
            
            priorObject = viewObject;
            NSLog(@"Prior Object: %@", priorObject.backgroundColor);
        }
    }];
        /*
        [(NSDictionary *)object each:^(id key, id value){
            if ([key isEqualToString:@"object"]) {
                
                parentObject = value;
                // Primary, parent object
            } else if ([key isEqualToString:@"children"]) {
                // This contains subobjects, we need to build them out
            } else if ([key isEqualToString:@"layout"]) {
                
                
                
            }
            
            NSLog(@"Key: %@, Value: %@", key, value);
        }];
        
        priorObject = parentObject;
    }];
    */
}

/*
 Append a collection of objects to a view
 
 */

- (void)addViewStackIntoView:(id)view withObjects:(NSArray *)objects
{
    __block id priorObject = nil;
}

- (NSDictionary *)buildLayoutConstraints:(NSString *)layoutString
{
    NSArray *layoutStyles = [layoutString componentsSeparatedByString:@"-"];
    
    __block NSNumber *height = nil;
    __block NSNumber *width = nil;
    
    __block NSString *height_st = nil;
    __block NSString *width_st = nil;
    
    __block NSString *anim_id = nil;
    
    __block NSString *block = @"block";
    
    __block NSString *y_origination = nil;
    __block NSNumber *y_pos = nil;
    __block NSString *x_origination = nil;
    __block NSNumber *x_pos = nil;
    
    __block NSString *over = nil;
    
    __block UIEdgeInsets padding = UIEdgeInsetsMake(0,0,0,0);
    
    [layoutStyles each:^(id object) {
        NSArray *layoutComponents = [(NSString *)object componentsSeparatedByString:@":"];
        if ([layoutComponents count] == 2) {
            
            // HEIGHT
            if ([[layoutComponents objectAtIndex:0] isEqualToString:@"h"]) {
                if ([[layoutComponents objectAtIndex:1] isEqualToString:@"f"]) {
                    height_st = @"f";
                } else if ([[layoutComponents objectAtIndex:1] rangeOfString:@"/"].location != NSNotFound)  {
                    
                } else {
                    height = [NSNumber numberWithInt:[(NSString *)[layoutComponents objectAtIndex:1] intValue]];
                }
            }
            
            // WIDTH
            if ([[layoutComponents objectAtIndex:0] isEqualToString:@"w"]) {
                if ([[layoutComponents objectAtIndex:1] isEqualToString:@"f"]) {
                    width_st = @"f";
                } else if ([[layoutComponents objectAtIndex:1] rangeOfString:@"/"].location != NSNotFound)  {
                    
                } else {
                    width = [NSNumber numberWithInt:[(NSString *)[layoutComponents objectAtIndex:1] intValue]];
                }
            }
            
            // PADDING
            if ([[layoutComponents objectAtIndex:0] isEqualToString:@"p"]) {
                if ([[layoutComponents objectAtIndex:1] rangeOfString:@","].location != NSNotFound)  {
                    NSArray *paddingElements = [(NSString *)[layoutComponents objectAtIndex:1] componentsSeparatedByString:@","];
                    if ([paddingElements objectAtIndex:0]) {
                        padding.top = [[paddingElements objectAtIndex:0] floatValue];
                    }
                    if ([paddingElements objectAtIndex:1]) {
                        padding.right = [[paddingElements objectAtIndex:1] floatValue];
                    }
                    if ([paddingElements objectAtIndex:2]) {
                        padding.bottom = [[paddingElements objectAtIndex:2] floatValue];
                    }
                    if ([paddingElements objectAtIndex:3]) {
                        padding.left = [[paddingElements objectAtIndex:3] floatValue];
                    }
                } else {
                    float padvalue = [[layoutComponents objectAtIndex:1] floatValue];
                    padding = UIEdgeInsetsMake(padvalue, padvalue, padvalue, padvalue);
                }
            }
            
            // Y POSITION
            if ([[layoutComponents objectAtIndex:0] isEqualToString:@"y"]) {
                if ([[layoutComponents objectAtIndex:1] rangeOfString:@","].location != NSNotFound)  {
                    NSArray *paddingElements = [(NSString *)[layoutComponents objectAtIndex:1] componentsSeparatedByString:@","];
                    
                    if ([paddingElements objectAtIndex:0]) {
                        if ([[paddingElements objectAtIndex:0] isEqualToString:@"top"]) {
                            y_origination = @"top";
                        } else if ([[paddingElements objectAtIndex:0] isEqualToString:@"bottom"]) {
                            y_origination = @"bottom";
                        }
                        if ([paddingElements objectAtIndex:1]) {
                            y_pos = [NSNumber numberWithFloat:[[paddingElements objectAtIndex:1] floatValue]];
                        }
                        
                    }
                } else {
                    if ([[layoutComponents objectAtIndex:1] isEqualToString:@"c"]) {
                        y_origination = @"center";
                    }
                }
            }
            
            // X POSITION
            if ([[layoutComponents objectAtIndex:0] isEqualToString:@"x"]) {
                if ([[layoutComponents objectAtIndex:1] rangeOfString:@","].location != NSNotFound)  {
                    NSArray *paddingElements = [(NSString *)[layoutComponents objectAtIndex:1] componentsSeparatedByString:@","];
                    
                    if ([paddingElements objectAtIndex:0]) {
                        if ([[paddingElements objectAtIndex:0] isEqualToString:@"right"]) {
                            x_origination = @"right";
                        } else if ([[paddingElements objectAtIndex:0] isEqualToString:@"left"]) {
                            x_origination = @"left";
                        }
                        if ([paddingElements objectAtIndex:1]) {
                            x_pos = [NSNumber numberWithFloat:[[paddingElements objectAtIndex:1] floatValue]];
                        }
                    }
                    
                } else {
                    if ([[layoutComponents objectAtIndex:1] isEqualToString:@"c"]) {
                        x_origination = @"center";
                    }
                }
            }
            
            // BLOCK APPROACH
            if ([[layoutComponents objectAtIndex:0] isEqualToString:@"b"]) {
                if ([[layoutComponents objectAtIndex:1] isEqualToString:@"inline"]) {
                    block = @"inline";
                }
            }
            
            // ANIMATION
            if ([[layoutComponents objectAtIndex:0] isEqualToString:@"anim"]) {
                anim_id = [layoutComponents objectAtIndex:1];
            }
            
            // OVERLAY
            if ([[layoutComponents objectAtIndex:0] isEqualToString:@"over"]) {
                over = @"true";
            }
        }
    }];
    
    NSMutableDictionary *result = NSMutableDictionary.new;
    [result setObject:[NSValue valueWithUIEdgeInsets:padding] forKey:@"padding"];
    [result setObject:block forKey:@"block"];
    
    if (anim_id != nil) {
        [result setObject:anim_id forKey:@"animate_id"];
    }
    
    if (width != nil) {
        [result setObject:width forKey:@"width"];
    } else if (width_st != nil) {
        [result setObject:width_st forKey:@"width"];
    }
    
    if (width != nil) {
        [result setObject:width forKey:@"width"];
    } else if (width_st != nil) {
        [result setObject:width_st forKey:@"width"];
    }
    
    if (height != nil) {
        [result setObject:height forKey:@"height"];
    } else if (height_st != nil) {
        [result setObject:height_st forKey:@"height"];
    }
    
    if (x_origination != nil) {
        [result setObject:x_origination forKey:@"x_origination"];
    }
    
    if (y_origination != nil) {
        [result setObject:y_origination forKey:@"y_origination"];
    }
    
    if (y_pos != nil) {
        [result setObject:y_pos forKey:@"y_pos"];
    }
    
    if (over != nil) {
        [result setObject:@"yes" forKey:@"over"];
    }
    
    return result;
    
}

#pragma mark -- Animation Methods --

- (void)animate:(NSString *)animate_id withType:(NSString *)type andValue:(float)value andDirection:(NSString *)direction
{
    
    if ([self.animatingConstraints objectForKey:animate_id] != nil && [self.animatingConstraints objectForKey:animate_id] != [NSNull null]) {
        
        MASConstraint *c = nil;
        NSMutableArray *cs = [[NSMutableArray alloc] init];
        
        if ([direction isEqualToString:@"left"]) {
            MASConstraint *c = [[self.animatingConstraints objectForKey:animate_id] objectForKey:@"left"];
            [cs addObject:c];
        } else if ([direction isEqualToString:@"top"]) {
            MASConstraint *c = [[self.animatingConstraints objectForKey:animate_id] objectForKey:@"top"];
            [cs addObject:c];
        } else if ([direction isEqualToString:@"zoom"]) {
            MASConstraint *c1 = [[self.animatingConstraints objectForKey:animate_id] objectForKey:@"width"];
            [cs addObject:c1];
            MASConstraint *c2 = [[self.animatingConstraints objectForKey:animate_id] objectForKey:@"height"];
            [cs addObject:c2];
        }
        
        NSArray *constraints = [[NSArray alloc] initWithArray:cs];
        
        [self animateLayout:constraints withType:type andValue:value];
    }
    
}

- (void)animateLayout:(NSArray *)contraints withType:(NSString *)type andValue:(float)value
{
    if ([type isEqualToString:@"bounce"]) {
        
        for (MASConstraint *c in contraints) {
            c.offset(value);
        }
    
        [UIView animateWithDuration:0.35 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.4 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.view layoutIfNeeded];
        } completion:nil];
        
    } else if ([type isEqualToString:@"zoom"]) {
        
        [self.view layoutIfNeeded];
        
        for (MASConstraint *c in contraints) {
            c.offset(value);
        }
        
        [UIView animateWithDuration:0.35 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.4 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.view layoutIfNeeded];
        } completion:nil];
    
    } else if ([type isEqualToString:@"static"]) {
        
        for (MASConstraint *c in contraints) {
            c.offset(value);
        }

        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        }];
    
    } else if ([type isEqualToString:@"immediate"]) {
        
        for (MASConstraint *c in contraints) {
            c.offset(value);
        }
        
        [UIView animateWithDuration:0 animations:^{
            [self.view layoutIfNeeded];
        }];
        
    } else if ([type isEqualToString:@"relative"]) {
        
    }
}

- (void)addNotificationObserver:(NSArray *)eventsToObserve
{
    if ([eventsToObserve containsObject:[NSNumber numberWithInt:kRefreshDecks]]) {
        if ([self respondsToSelector:@selector(didUpdateDecks:)]) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateDecks:) name:@"notify.UpdateDecks" object:nil];
        }
    }
    
    if ([eventsToObserve containsObject:[NSNumber numberWithInt:kRefreshCards]]) {
        
    }
}

#pragma mark -- Transition Methods --
- (void)closeAndTransitionTo:(BCViewController *)destinationVC withType:(int)type buildNavController:(BOOL)buildNav
{
    [self closeAndTransitionTo:destinationVC withType:type buildNavController:buildNav closeMe:YES];
}

- (void)closeAndTransitionTo:(BCViewController *)destinationVC withType:(int)type buildNavController:(BOOL)buildNav closeMe:(BOOL)shouldClose
{
    
    if (buildNav) {
        
        BCNavController *baseNC = [[BCNavController alloc] initWithRootViewController:destinationVC];
    
        if ([destinationVC respondsToSelector:@selector(cleanUpUnsavedObject:)]) {
            destinationVC.navigationItem.leftBarButtonItem = [baseNC getNavBarItem:kBCNavButtonStyleBack withTitle:@"" andPosition:kBCNavPositionLeft target:baseNC action:@selector(cleanAndCloseMe:)];
        } else {
            destinationVC.navigationItem.leftBarButtonItem = [baseNC getNavBarItem:kBCNavButtonStyleBack withTitle:@"" andPosition:kBCNavPositionLeft target:baseNC action:@selector(closeMe:)];
        }

        
        if (!destinationVC.navControllerTitleColor) {
            destinationVC.navControllerTitleColor = [UIColor whiteColor];
        }
        
        if (!destinationVC.navControllerTitleTextColor) {
            destinationVC.navControllerTitleTextColor = [UIColor colorWithHexString:@"848484"];
        }
    
        if ([destinationVC.navigationController.navigationBar respondsToSelector:(@selector(setBarTintColor:))]) {
            destinationVC.navigationController.navigationBar.barTintColor = destinationVC.navControllerTitleColor;
        } else {
            destinationVC.navigationController.navigationBar.tintColor = destinationVC.navControllerTitleColor;
        }
        
        //[destinationVC.view setBackgroundColor:destinationVC.baseBackgroundColor];
        
        NSDictionary *titleTextAttributes = @{
                                              UITextAttributeFont: [UIFont fontWithName:@"OpenSans" size:16],
                                              UITextAttributeTextColor: destinationVC.navControllerTitleTextColor
                                              };
        
        [destinationVC.navigationController.navigationBar setTranslucent:NO];
        [destinationVC.navigationController.navigationBar setTitleTextAttributes:titleTextAttributes];
        
        if (!shouldClose) {
            destinationVC.delegateViewController = self;
        }
        
        [self presentViewController:baseNC animated:YES completion:^{
            if (shouldClose) {
                [self closeMe:nil];
            }
        }];
        //[self transitionToVC:baseNC withType:type];
    
    } else {
        [self transitionToVC:destinationVC withType:type];
    }

}

- (void)transitionTo:(BCViewController *)destinationVC withType:(int)type buildNavController:(BOOL)buildNav
{
    
    if (buildNav) {
        
        BCNavController *baseNC = [[BCNavController alloc] initWithRootViewController:destinationVC];
        
        if ([destinationVC respondsToSelector:@selector(cleanUpUnsavedObject:)]) {
            destinationVC.navigationItem.leftBarButtonItem = [baseNC getNavBarItem:kBCNavButtonStyleBack withTitle:@"" andPosition:kBCNavPositionLeft target:baseNC action:@selector(cleanAndCloseMe:)];
        } else {
            destinationVC.navigationItem.leftBarButtonItem = [baseNC getNavBarItem:kBCNavButtonStyleBack withTitle:@"" andPosition:kBCNavPositionLeft target:baseNC action:@selector(closeMe:)];
        }
        
        
        if (!destinationVC.navControllerTitleColor) {
            destinationVC.navControllerTitleColor = [UIColor whiteColor];
        }
        
        if (!destinationVC.navControllerTitleTextColor) {
            destinationVC.navControllerTitleTextColor = [UIColor colorWithHexString:@"848484"];
        }
        
        if ([destinationVC.navigationController.navigationBar respondsToSelector:(@selector(setBarTintColor:))]) {
            destinationVC.navigationController.navigationBar.barTintColor = destinationVC.navControllerTitleColor;
        } else {
            destinationVC.navigationController.navigationBar.tintColor = destinationVC.navControllerTitleColor;
        }
        
        //[destinationVC.view setBackgroundColor:destinationVC.baseBackgroundColor];
        
        NSDictionary *titleTextAttributes = @{
                                              UITextAttributeFont: [UIFont fontWithName:@"OpenSans" size:16],
                                              UITextAttributeTextColor: destinationVC.navControllerTitleTextColor
                                              };
        
        [destinationVC.navigationController.navigationBar setTranslucent:NO];
        [destinationVC.navigationController.navigationBar setTitleTextAttributes:titleTextAttributes];
        
        [self presentViewController:baseNC animated:YES completion:^{
            // That's all
        }];
        //[self transitionToVC:baseNC withType:type];
        
    } else {
        [self transitionToVC:destinationVC withType:type];
    }
    
}


- (void)transitionToVC:(BCViewController *)toVC withType:(int)type
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.transitionVC setStatusBarStyle:[toVC getStatusBarStyle]];
    [appDelegate.transitionVC transitionToViewController:toVC withOptions:UIViewAnimationOptionCurveEaseOut andTransitionType:type];
}

#pragma mark -- Core Methods --

- (void)viewDidLoad
{
    if (!statusBarStyle) {
        statusBarStyle = UIStatusBarStyleDefault;
    }
    
    self.animatingConstraints = NSMutableDictionary.new;
    
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Do any additional setup after loading the view.
}

- (void)registerBCTableViewCell:(UITableView *)tableview
{
    if ([self respondsToSelector:@selector(numberOfSectionsInTableView:)]) {
        [tableview registerClass:[BCTableViewCell class] forCellReuseIdentifier:@"BCTableViewCell"];
    }
    
    [tableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (BCTransitionViewController *)baseViewController
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    return appDelegate.transitionVC;
}

- (IBAction)popCurrentViewController:(id)sender
{
    if (self.navigationController) {
        [sender setSelected:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)setStatusBarStyle:(UIStatusBarStyle)style {
    statusBarStyle = style;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarStyle)getStatusBarStyle
{
    if (statusBarStyle) {
        return statusBarStyle;
    } else {
        return UIStatusBarStyleDefault;
    }
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return statusBarStyle;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
