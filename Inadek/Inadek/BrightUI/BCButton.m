//
//  BCButton.m
//  Inadek
//
//  Created by Josh Lehman on 1/16/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCButton.h"

@implementation BCButton

- (void)buildType:(NSString *)type
{
    self.btnType = type;
    
    if ([type isEqualToString:@"blue"]) {
        [self setStyleClass:@"btn-blue"];
    } else if ([type isEqualToString:@"orange"]) {
        [self setStyleClass:@"btn-orange"];
    } else if ([type isEqualToString:@"grey"]) {
        [self setStyleClass:@"btn-grey"];
    } else if ([type isEqualToString:@"clear"]) {
        [self setStyleClass:@"btn-clear"];
    } else if ([type isEqualToString:@"cancel"]) {
        [self setStyleClass:@"btn-glass"];
    } else if ([type isEqualToString:@"deck.action"]) {
        [self setStyleClass:@"btn-deck-action"];
    }
    
}

- (void)setColor:(UIColor *)color
{
    baseColor = color;
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        if (baseColor) {
            self.backgroundColor = [baseColor lighterColor];
        } else {
            self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.08];
        }
    } else {
        if (baseColor) {
            self.backgroundColor = baseColor;
        } else {
            self.backgroundColor = [UIColor clearColor];
        }
    }
    
}

-(void) setSelected:(BOOL)selected {
    
    NSLog(@"Set selected");
    if(selected) {
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.1];
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            self.backgroundColor = [UIColor clearColor];
        }];
        
    }
    [super setSelected:selected];
}

- (void)highlightWhenPressed:(BOOL)highlight
{
    if (highlight) {
        highlightOnPress = YES;
    } else {
        highlightOnPress = NO;
    }
}


@end
