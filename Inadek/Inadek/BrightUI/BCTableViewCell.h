//
//  BCTableViewCell.h
//  TheWishlist
//
//  Created by Josh Lehman on 10/28/14.
//  Copyright (c) 2014 joshlehman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>

@interface BCTableViewCell : UITableViewCell {
    UILabel *lblTitle;
    UILabel *lblSubTitle;
    UILabel *lblDescription;
}

@property (nonatomic) float height;
@property (nonatomic) float width;
@property (nonatomic, retain) UIView *bgView;
@property (nonatomic, retain) UIView *bgSelectedView;

- (void) buildView:(NSString *)viewType;
- (void) setTitle:(NSString *)title;
- (void) setSubtitle:(NSString *)subTitle;
- (void) setDescription:(NSString *)description;
- (void) setShowDisclosure:(BOOL)isShown;


@end

