//
//  BCTransitionViewController.m
//  TheWishlist
//
//  Created by Josh Lehman on 10/28/14.
//  Copyright (c) 2014 joshlehman. All rights reserved.
//

#define MAS_SHORTHAND

#import "BCTransitionViewController.h"
#import "BCViewController.h"
#import "BCNavController.h"
#import <Masonry/Masonry.h>
#import <pop/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>

#import "DeckViewController.h"
#import "CreatePopupViewController.h"
#import "Common.h"

@interface BCTransitionViewController () {
    NSMutableArray *popupAnimations;
}

@end

@implementation BCTransitionViewController

- (id)initWithViewController:(BCViewController *)viewController
{
    if (self = [super init]) {
        _viewController = viewController;
    }
    return self;
}

- (id)initWithStackedViewControllers:(NSArray *)viewControllers
{
    
    if ([viewControllers count] > 1) {
        if (self = [super init]) {
            _menuVC = [viewControllers objectAtIndex:0];
            _activeTabVC = [viewControllers objectAtIndex:1];
            _viewController = nil;
        }
        return self;
    } else if ([viewControllers count] == 1) {
       return [self initWithViewController:[viewControllers objectAtIndex:0]];
    }
    
    return self;

}

- (void)transitionAfterLogin
{
    
    // Initialize login VC
    _menuVC = [[MenuViewController alloc] init];
    _activeTabVC = [[PopularViewController alloc] init];
    
    [self addStackedViewControllers:self.menuVC bottomViewController:self.activeTabVC withLayout:@[@100,@0]];
    
}

- (void)setStatusBarStyle:(UIStatusBarStyle)style {
    statusBarStyle = style;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)loadView
{
    self.edgesForExtendedLayout = UIRectEdgeAll;
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.view = view;
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"f0f0eb"]];
    
    //_containerView = [[UIView alloc] initWithFrame:view.bounds];
    //_containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_containerView];
    
    if ([self.viewController respondsToSelector:@selector(getStatusBarStyle)]) {
        [self setStatusBarStyle:[(BCViewController *)self.viewController getStatusBarStyle]];
    }
    
    if (self.viewController) {
        [self.view addSubview:self.viewController.view];
        self.activeVC = self.viewController;
    } else if (self.menuVC && self.activeTabVC) {
        [self addStackedViewControllers:self.menuVC bottomViewController:self.activeTabVC withLayout:@[@100,@0]];
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return [self.viewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.viewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (void)addStackedViewControllers:(BCViewController *)topVC bottomViewController:(BCViewController *)bottomVC withLayout:(NSArray *)layout
{
    // - Top will overlay the bottom
    // - Sizes will be dicatated by the layout dictionary
    
    [self setStatusBarStyle:UIStatusBarStyleDefault];
    
    self.activeTabVC = bottomVC;
    self.menuVC = topVC;
    
    BOOL animateIn = NO;
    
    if (self.activeVC) {
        animateIn = YES;
    } else {
        self.activeVC = nil;
    }
    
    stackedVCLayout = layout;
    
    UIView *superview = self.view;
    
    if (animateIn) {
        [self.activeTabVC.view setAlpha:0.0];
    }
    
    // Add the current base view controller
    [self addChildViewController:self.activeTabVC];
    [self.activeTabVC didMoveToParentViewController:self];
    [self.view addSubview:self.activeTabVC.view];
    
    
    [self.activeTabVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(superview.width);
        make.height.equalTo(superview.height).with.offset((0 - [[stackedVCLayout objectAtIndex:0] integerValue]));
        make.top.equalTo(superview.top).with.offset([[stackedVCLayout objectAtIndex:0] integerValue]);
    }];
    
    // Add the menu at top
    [self addChildViewController:self.menuVC];
    [self.menuVC didMoveToParentViewController:self];
    [self.view addSubview:self.menuVC.view];
    
    if (animateIn) {
        [self.menuVC.view setAlpha:0.0];
    }
    
    [self.menuVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(superview.width);
        make.height.equalTo([NSNumber numberWithInt:[[stackedVCLayout objectAtIndex:0] integerValue]]);
        make.top.equalTo(superview.top);
    }];
    
    if (animateIn) {
        [UIView animateWithDuration:0.8 animations:^{
            [self.menuVC.view setAlpha:1.0];
            [self.activeTabVC.view setAlpha:1.0];
            [self.activeVC.view setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.activeVC willMoveToParentViewController:nil];
            [self.activeVC.view removeFromSuperview];
            [self.activeVC removeFromParentViewController];
        }];
    }
    
}

- (void)transitionStackedViewController:(BCViewController *)newBottomVC
{
    if (!stackedVCLayout) {
        return;
    }
    
    UIView *superview = self.view;
    
    // Add the current base view controller
    [self addChildViewController:newBottomVC];
    [newBottomVC didMoveToParentViewController:self];
    [self.view insertSubview:newBottomVC.view belowSubview:self.menuVC.view];
    
    [newBottomVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(superview.width);
        make.height.equalTo(superview.height).with.offset((0 - [[stackedVCLayout objectAtIndex:0] integerValue]));
        make.top.equalTo(superview.top).with.offset([[stackedVCLayout objectAtIndex:0] integerValue]);
    }];
    
    [self.activeTabVC willMoveToParentViewController:nil];
    [self.activeTabVC.view removeFromSuperview];
    [self.activeTabVC removeFromParentViewController];
    
    self.activeTabVC = newBottomVC;
    
}

- (void)transitionTo:(NSString *)viewName withParams:(NSDictionary *)params
{
    if ([viewName isEqualToString:@"deck"]) {
        
        DeckViewController *newVC = [[DeckViewController alloc] initWithDeckParams:params];
        [self transitionToViewController:newVC withOptions:UIViewAnimationOptionCurveEaseInOut andTransitionType:1];
        [self setStatusBarStyle:UIStatusBarStyleLightContent];
    
    } else if ([viewName isEqualToString:@"card"]) {
        
        DeckViewController *newVC = [[DeckViewController alloc] initWithCard:params];
        [self transitionToViewController:newVC withOptions:UIViewAnimationOptionCurveEaseInOut andTransitionType:1];
        [self setStatusBarStyle:UIStatusBarStyleLightContent];
        
    } else if ([viewName isEqualToString:@"addNew"]) {
        
        BOOL showAddCard = NO;
        if ([[[DeckManager sharedManager] myLocalDecks] count] > 0) {
            showAddCard = YES;
        }
        
        CreatePopupViewController *newVC = [[CreatePopupViewController alloc] init:showAddCard];
        [self transitionToViewController:newVC withOptions:UIViewAnimationOptionCurveEaseInOut andTransitionType:1];
        [self setStatusBarStyle:UIStatusBarStyleDefault];
    }
}

- (void)transitionToViewController:(BCViewController *)aViewController
                       withOptions:(UIViewAnimationOptions)options andTransitionType:(int)transitionType
{
    
    transition_animationBlock = ^{
        
    };
    
    switch (transitionType) {}
    
    if (!transition_animationBlock) {
        
        __block BCTransitionViewController *weakSelf = self;
        transition_animationBlock = ^{
            [weakSelf.viewController.view removeFromSuperview];
            [weakSelf.containerView addSubview:aViewController.view];
        };
    }
    
    UIView *superview = self.view;
    
    // Add the current base view controller
    [self addChildViewController:aViewController];
    [aViewController didMoveToParentViewController:self];
    [self.view addSubview:aViewController.view];
    
    [aViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(superview);
    }];
    
    self.activeVC = aViewController;
}

- (void)closeMe:(BCViewController *)viewController
{
    if (viewController == nil && self.activeVC) {
        [_activeVC willMoveToParentViewController:nil];
        [_activeVC.view removeFromSuperview];
        [_activeVC removeFromParentViewController];
        _activeVC = nil;
    } else {
        [viewController willMoveToParentViewController:nil];
        [viewController.view removeFromSuperview];
        [viewController removeFromParentViewController];
        if (self.activeVC == viewController) {
            _activeVC = nil;
        }
    }
    
}

- (void)fadeMe:(BCViewController *)viewController withStatusBarStyle:(UIStatusBarStyle)style
{
    if (viewController == nil && self.activeVC) {
        [UIView animateWithDuration:0.3 animations:^{
            [_activeVC.view setAlpha:0.0];
        } completion:^(BOOL finished) {
            [_activeVC willMoveToParentViewController:nil];
            [_activeVC.view removeFromSuperview];
            [_activeVC removeFromParentViewController];
            _activeVC = nil;
        }];

    } else {
        [UIView animateWithDuration:0.3 animations:^{
            [viewController.view setAlpha:0.0];
        } completion:^(BOOL finished) {
            [viewController willMoveToParentViewController:nil];
            [viewController.view removeFromSuperview];
            [viewController removeFromParentViewController];
            if (self.activeVC == viewController) {
                _activeVC = nil;
            }
        }];

    }
    
    [self setStatusBarStyle:style];
}

- (void)closeNavController:(BCNavController *)viewController
{
    if (viewController == nil && self.activeVC) {
        [(BCNavController *)_activeVC dismissViewControllerAnimated:YES completion:nil];
        _activeVC = nil;
    } else {
        [(BCNavController *)viewController dismissViewControllerAnimated:YES completion:nil];
        if (self.activeVC == viewController) {
            _activeVC = nil;
        }
    }
    
}

- (void)presentPopupView:(UIView *)view
{
    int popupPadding = 20;
    
    [view.layer setCornerRadius:4];
    
    overlay = UIView.new;
    [overlay setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
    [overlay setAlpha:0.0];
    [self.view addSubview:overlay];
    
    [overlay makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    currentPopupView = view;
    
    [self.view addSubview:currentPopupView];
    
    __block MASConstraint *animated_top = nil;
    __block MASConstraint *animated_bottom = nil;
    
    [currentPopupView mas_makeConstraints:^(MASConstraintMaker *make) {
        animated_top = make.top.equalTo(self.view).with.offset(self.view.bounds.size.height);
        make.right.equalTo(self.view).with.offset(-popupPadding);
        animated_bottom = make.bottom.equalTo(self.view).with.offset(self.view.bounds.size.height);
        make.left.equalTo(self.view).with.offset(popupPadding);
    }];
    
    popupAnimations = NSMutableArray.new;
    
    [popupAnimations addObject:animated_top];
    [popupAnimations addObject:animated_bottom];
    
    
    int newOffset = popupPadding;
    POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(newOffset * 3);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.springBounciness = 6;
    popupAnimation.springSpeed = 30;
    
    POPSpringAnimation *popupBottomAnimation = [POPSpringAnimation new];
    popupBottomAnimation.toValue = @(0 - (newOffset * 3));
    popupBottomAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupBottomAnimation.springBounciness = 6;
    popupBottomAnimation.springSpeed = 30;
    
    [animated_top pop_addAnimation:popupAnimation forKey:@"offset"];
    [animated_bottom pop_addAnimation:popupBottomAnimation forKey:@"offset"];
    
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [overlay setAlpha:1.0];
    } completion:nil];
}

- (void)hideCurrentPopupView:(id)sender
{
    int popupPadding = 20;
    int newOffset = popupPadding;
    
    POPSpringAnimation *popupAnimation = [POPSpringAnimation new];
    popupAnimation.toValue = @(newOffset + self.view.bounds.size.height);
    popupAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupAnimation.velocity = (@1000);
    
    POPSpringAnimation *popupBottomAnimation = [POPSpringAnimation new];
    popupBottomAnimation.toValue = @(0 - (newOffset * 3) + self.view.bounds.size.height);
    popupBottomAnimation.property = [POPAnimatableProperty mas_offsetProperty];
    popupBottomAnimation.velocity = (@1000);
    
    [[popupAnimations objectAtIndex:0] pop_addAnimation:popupAnimation forKey:@"offset"];
    [[popupAnimations objectAtIndex:1] pop_addAnimation:popupBottomAnimation forKey:@"offset"];
    
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [overlay setAlpha:0.0];
    } completion:^(BOOL finished) {
        [overlay removeFromSuperview];
    }];
    
}

- (UIStatusBarStyle)getStatusBarStyle
{
    if (statusBarStyle) {
        return statusBarStyle;
    } else {
        return UIStatusBarStyleDefault;
    }
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return statusBarStyle;
}

- (void) postUpdateNotification:(int)notificationId withObject:(id)object
{
    if (notificationId == kRefreshDecks) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notify.UpdateDecks" object:nil userInfo:object];
    } else if (notificationId == kRefreshCards) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notify.UpdateCards" object:nil userInfo:object];
    }
}



@end