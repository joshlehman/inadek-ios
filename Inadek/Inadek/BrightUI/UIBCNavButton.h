//
//  UIBCNavButton.h
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>

@interface UIBCNavButton : UIButton

- (id) initAsNavBarButton:(int)style withTitle:(NSString *)title;

@end
