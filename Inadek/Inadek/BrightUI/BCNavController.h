//
//  BCNavController.h
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBCNavButton.h"

@class UIBCNavButton;

@interface BCNavController : UINavigationController {
    UIStatusBarStyle statusBarStyle;
}

- (UIBarButtonItem *) getNavBarItem:(int)style withTitle:(NSString *)title andPosition:(int)position target:(id)target action:(SEL)action;

- (UIStatusBarStyle)getStatusBarStyle;
- (void)closeMe:(id)sender;

@end
