//
//  BCGroupedActionSheet.m
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCGroupedActionSheetView.h"

#define kBackgroundAlpha    0.25
#define kOptionHeight       50
#define kSidePadding        10
#define kFloorPadding       10
#define kFadeInDuration     0.3

@implementation BCGroupedActionSheetView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) initWithOptions:(NSArray *)options withTitle:(NSString *)title andFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _tintedBackground = [[UIView alloc] initWithFrame:CGRectZero];
        [_tintedBackground setBackgroundColor:[UIColor blackColor]];
        [_tintedBackground setAlpha:0];
        
        [self addSubview:_tintedBackground];
        [_tintedBackground makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
         
        
        UIButton *backgroundTap = UIButton.new;
        [backgroundTap addTarget:self action:@selector(doDismissBackground:) forControlEvents:UIControlEventTouchUpInside];
        
        [_tintedBackground addSubview:backgroundTap];
        [backgroundTap makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        [_tintedBackground addSubview:backgroundTap];
        
        _cancelView = [[BCActionSheetView alloc] initWithOptions:@[@{@"name":@"Cancel",@"style":@"cancel"}] title:nil andParentView:self];
        [self addSubview:_cancelView];
        
        [_cancelView setStyleClass:@"box-deck-cell"];
        
        [_cancelView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(kSidePadding);
            make.right.equalTo(self).with.offset(-kSidePadding);
            cancelBottom = make.bottom.equalTo(self).with.offset(-kFloorPadding);
            make.height.equalTo([_cancelView sheetHeightAsNumber]);
        }];
        
        
        _selectionView = [[BCActionSheetView alloc] initWithOptions:options title:title andParentView:self];
        [self addSubview:_selectionView];
        
        [_selectionView setStyleClass:@"box-deck-cell"];
        
        [_selectionView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(kSidePadding);
            make.right.equalTo(self).with.offset(-kSidePadding);
            if (_cancelView) {
                sheetBottom = make.bottom.equalTo(_cancelView.top).with.offset(-kFloorPadding);
            } else {
                sheetBottom = make.bottom.equalTo(self).with.offset(-kFloorPadding);
            }
            make.height.equalTo(@300); //[_selectionView sheetHeightAsNumber]
        }];
        
        sheetBottom.offset = [_selectionView sheetHeight];
        cancelBottom.offset = [_selectionView sheetHeight];
        
        
    }
    return self;
}

//- (id) initWithGroupedOptions:(NSArray *)groupedOptions andFrame:(CGRect)frame {
//    
//}


- (void) showInView:(UIView *)view
{
    [view addSubview:self];

    [self makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    
    POPSpringAnimation *slideIn = [POPSpringAnimation new];
    slideIn.toValue = @(-kFloorPadding);
    slideIn.springBounciness = 6;
    slideIn.springSpeed = 25;
    slideIn.property = [POPAnimatableProperty mas_offsetProperty];
    [cancelBottom pop_addAnimation:slideIn forKey:@"PopDeckSlideInAnimation"];
    [sheetBottom pop_addAnimation:slideIn forKey:@"PopDeckSlideInAnimation"];
    
    [UIView animateWithDuration:kFadeInDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [_tintedBackground setAlpha:kBackgroundAlpha];
                         

                     }
                     completion:^(BOOL finished) {
                         //
                     }];
}

- (void) doDismissBackground:(id)sender
{
    [self.delegate groupedActionSheetDidCancel:self];
    [self dismissFromSuperview];
}

- (void) doSelectField:(id)sender
{
    NSString *cid = @"";
    if ([sender isKindOfClass:[BCButton class]]) {
        BCButton *thisButton = sender;
        NSDictionary *userinfo = thisButton.userInfo;
        cid = [userinfo objectForKey:@"cid"];
    }
    
    [self.delegate groupedActionSheet:self selectedItemWithCid:cid andGroup:_sheetCid];
    [self dismissFromSuperview];
}

- (void) dismissFromSuperview {
    
    POPSpringAnimation *slideOut = [POPSpringAnimation new];
    slideOut.toValue = @([_selectionView sheetHeight]);
    slideOut.springBounciness = 6;
    slideOut.springSpeed = 25;
    slideOut.property = [POPAnimatableProperty mas_offsetProperty];
    [cancelBottom pop_addAnimation:slideOut forKey:@"PopDeckSlideInAnimation"];
    [sheetBottom pop_addAnimation:slideOut forKey:@"PopDeckSlideInAnimation"];
    
    [UIView animateWithDuration:kFadeInDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [_tintedBackground setAlpha:0.0];

                     } completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end

