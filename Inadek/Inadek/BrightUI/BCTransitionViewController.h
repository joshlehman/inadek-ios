//
//  BCTransitionViewController.h
//  TheWishlist
//
//  Created by Josh Lehman on 10/28/14.
//  Copyright (c) 2014 joshlehman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import "BCViewController.h"
#import "MenuViewController.h"
#import "PopularViewController.h"
#import "IntroViewController.h"


typedef void (^ AnimationBlock)(void);

@interface BCTransitionViewController : UIViewController {
    UIStatusBarStyle statusBarStyle;
    AnimationBlock transition_animationBlock;
    float transition_duration;
    NSArray *stackedVCLayout;
    UIView *overlay;
    UIView *currentPopupView;
}


@property (nonatomic, strong)   UIView *                containerView;
@property (nonatomic, strong)   BCViewController *      viewController;

@property (nonatomic, strong)   BCViewController *      activeTabVC;
@property (nonatomic, strong)   BCViewController *      menuVC;

@property (nonatomic, strong)   BCViewController *      activeVC;


- (void)transitionTo:(NSString *)viewName withParams:(NSDictionary *)params;

- (void)setStatusBarStyle:(UIStatusBarStyle)style;
- (id)initWithViewController:(UIViewController *)viewController;
- (void)transitionToViewController:(BCViewController *)aViewController
                       withOptions:(UIViewAnimationOptions)options andTransitionType:(int)transitionType;
- (void)transitionStackedViewController:(BCViewController *)newBottomVC;

- (id)initWithStackedViewControllers:(NSArray *)viewControllers;
- (void)addStackedViewControllers:(BCViewController *)topVC bottomViewController:(BCViewController *)bottomVC withLayout:(NSArray *)layout;

- (void)presentPopupView:(UIView *)view;
- (void)hideCurrentPopupView:(id)sender;
- (void)closeMe:(BCViewController *)viewController;
- (void)fadeMe:(BCViewController *)viewController withStatusBarStyle:(UIStatusBarStyle)style;
- (void)closeNavController:(BCNavController *)viewController;

- (void)transitionAfterLogin;

- (void)postUpdateNotification:(int)notificationId withObject:(id)object;

@end

