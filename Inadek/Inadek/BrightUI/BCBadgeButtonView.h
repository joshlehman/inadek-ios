//
//  BCBadgeButtonView.h
//  Inadek
//
//  Created by Josh Lehman on 2/28/16.
//  Copyright © 2016 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>

@interface BCBadgeButtonView : UIButton

@property (nonatomic, retain) UIView *theBadge;
@property (nonatomic, retain) UILabel *badgeNumber;

- (void)buildBadge;
- (void)hideBadge;
- (void)showBadge;
- (void)updateNumber:(NSNumber *)value;

@end
