//
//  BCScrollView.m
//  GasCubby
//
//  Created by Josh Lehman on 5/15/15.
//  Copyright (c) 2015 Fuelly. All rights reserved.
//

#import "BCScrollView.h"

@implementation BCScrollView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    if ( [view isKindOfClass:[UIButton class]] ) {
        return YES;
    }
    
    return [super touchesShouldCancelInContentView:view];
}

@end
