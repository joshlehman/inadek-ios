//
//  BCTableViewCell.m
//  TheWishlist
//
//  Created by Josh Lehman on 10/28/14.
//  Copyright (c) 2014 joshlehman. All rights reserved.
//

#import "BCTableViewCell.h"

@implementation BCTableViewCell

- (void) buildView:(NSString *)viewType
{
    [self clearViews];
    
    if ([[viewType lowercaseString] isEqualToString:@"role"]) {
        self.height = 100;
        self.width = 320;
        
        self.bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, self.width, self.height - 2)];
        self.bgView.styleClass = @"bg-primary";
        
        self.bgSelectedView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, self.width, self.height - 2)];
        self.bgSelectedView.styleClass = @"bg-primary-dk";
        [self.bgSelectedView setAlpha:0.0];
        
        [self.contentView addSubview:self.bgView];
        [self.contentView insertSubview:self.bgSelectedView aboveSubview:self.bgView];
        
    }
}

- (void) setTitle:(NSString *)title
{
    if (lblTitle) {
        [lblTitle setText:title];
    }
}

- (void) setSubtitle:(NSString *)subTitle
{
    if (lblSubTitle) {
        [lblSubTitle setText:subTitle];
    }
}

- (void) setDescription:(NSString *)description
{
    if (lblDescription) {
        [lblDescription setText:description];
    }
}

- (void) setShowDisclosure:(BOOL)isShown
{
    
}

- (void) clearViews
{
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundColor:[UIColor clearColor]];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.bgView removeFromSuperview];
    self.bgView = nil;
    
    if (lblTitle) {
        [lblTitle removeFromSuperview];
        lblTitle = nil;
    }
    
    if (lblSubTitle) {
        [lblSubTitle removeFromSuperview];
        lblSubTitle = nil;
    }
    
    if (lblDescription) {
        [lblDescription removeFromSuperview];
        lblDescription = nil;
    }
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
        [self.bgSelectedView setAlpha:1.0];
    } else {
        [UIView animateWithDuration:0.8 animations:^{
            [self.bgSelectedView setAlpha:0.0];
        }];
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (!(self.selectionStyle == UITableViewCellSelectionStyleNone)) {
        if (selected) {
            [self.bgSelectedView setAlpha:1.0];
            //[self.bgView setAlpha:0.0];
        } else {
            [self.bgSelectedView setAlpha:0.0];
            //[self.bgView setAlpha:1.0];
        }
    }
}

@end