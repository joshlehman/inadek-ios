//
//  BCGroupedActionSheet.h
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import "BCActionSheetView.h"
#import "BCButton.h"
#import <pop/POP.h>
#import <MSSPopMasonry/MSSPopMasonry.h>

@protocol BCGroupedActionSheetDelegate;

@interface BCGroupedActionSheetView : UIView {
    int optionsHeight;
    
    MASConstraint *cancelBottom;
    MASConstraint *sheetBottom;
}

@property (nonatomic,assign) id<BCGroupedActionSheetDelegate> delegate;    // weak reference

@property (strong, nonatomic) UIView *tintedBackground;
@property (strong, nonatomic) BCActionSheetView *selectionView;
@property (strong, nonatomic) BCActionSheetView *cancelView;
@property (strong, nonatomic) NSString *sheetCid;

- (id) initWithOptions:(NSArray *)options withTitle:(NSString *)title andFrame:(CGRect)frame;
- (id) initWithGroupedOptions:(NSArray *)groupedOptions andFrame:(CGRect)frame;
- (void) showInView:(UIView *)view;
- (void) doDismissBackground:(id)sender;
- (void) doSelectField:(id)sender;

@end

@protocol BCGroupedActionSheetDelegate <NSObject>
@optional

- (void)groupedActionSheet:(BCGroupedActionSheetView *)actionSheet selectedItemWithCid:(NSString *)cid andGroup:(NSString *)group;
- (void)groupedActionSheetDidCancel:(BCGroupedActionSheetView *)actionSheet;

@end