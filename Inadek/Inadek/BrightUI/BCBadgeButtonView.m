//
//  BCBadgeButtonView.m
//  Inadek
//
//  Created by Josh Lehman on 2/28/16.
//  Copyright © 2016 Inadek. All rights reserved.
//

#import "BCBadgeButtonView.h"

@implementation BCBadgeButtonView

- (void)buildBadge
{
    [self setBackgroundColor:[UIColor clearColor]];
    
    _theBadge = UIView.new;
    
    UIView *badgeBg = UIView.new;
    [badgeBg setBackgroundColor:[UIColor whiteColor]];
    [badgeBg.layer setCornerRadius:8];
    //[badgeBg setStyleCSS:@"border-radius: 30px;"];
    [_theBadge addSubview:badgeBg];
    [badgeBg setFrame:CGRectMake(0, 0, 18, 18)];
    
    UIView *badge = UIView.new;
    [badge setBackgroundColor:[UIColor blackColor]];
    [badge.layer setCornerRadius:7];
    //[badgeBg setStyleCSS:@"border-radius: 30px;"];
    [_theBadge addSubview:badge];
    [badge setFrame:CGRectMake(2, 2, 14, 14)];
    
    [self addSubview:_theBadge];
    [_theBadge setFrame:CGRectMake(0, 0, 18, 18)];
    
    _badgeNumber = UILabel.new;
    [_badgeNumber setText:@"0"];
    [_badgeNumber setTextAlignment:NSTextAlignmentCenter];
    [_badgeNumber setStyleClass:@"lbl-card-textvalue"];
    [_badgeNumber setStyleCSS:@"font-size: 7px; color: #ffffff; text-align: center; font-weight: 700;"];
    [_theBadge addSubview:_badgeNumber];
    [_badgeNumber setFrame:CGRectMake(0, 0, 18, 18)];
    [_theBadge setAlpha:0];

}

- (void)updateNumber:(NSNumber *)value
{
    if (_badgeNumber) {
        [_badgeNumber setText:[value stringValue]];
    }
}

- (void)hideBadge
{
    [_theBadge setAlpha:0];
}

- (void)showBadge
{
    [_theBadge setAlpha:1];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
