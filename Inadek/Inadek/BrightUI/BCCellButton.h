//
//  BCCellButton.h
//  Inadek
//
//  Created by Josh Lehman on 1/22/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <PixateFreestyle/PixateFreestyle.h>


@interface BCCellButton : UIButton {
    BOOL highlightOnPress;
}

@property (nonatomic, retain) NSDictionary *userInfo;

- (void)highlightWhenPressed:(BOOL)highlight;

@end