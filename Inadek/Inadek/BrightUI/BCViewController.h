//
//  BrightUIViewController.h
//  TheWishlist
//
//  Created by Josh Lehman on 10/28/14.
//  Copyright (c) 2014 joshlehman. All rights reserved.
//

#define MAS_SHORTHAND

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import "InadekAPIManager.h"
#import "DeckManager.h"

@class BCTransitionViewController;
@class BCNavController;

@interface BCViewController : UIViewController <UIViewControllerTransitioningDelegate> {
    UIStatusBarStyle statusBarStyle;
}

@property (nonatomic, retain) UIColor *navControllerTitleColor;
@property (nonatomic, retain) UIColor *navControllerTitleTextColor;
@property (nonatomic, retain) UIColor *baseBackgroundColor;

@property (nonatomic, assign, getter = isPresenting) BOOL presenting;
@property (nonatomic, retain) NSMutableDictionary *animatingConstraints;

@property (nonatomic, retain) BCViewController *delegateViewController;

- (void)setStatusBarStyle:(UIStatusBarStyle)style;
- (UIStatusBarStyle)getStatusBarStyle;
- (void)registerBCTableViewCell:(UITableView *)tableview;
- (void)transitionToVC:(BCViewController *)toVC withType:(int)type;
- (UIView *)placeSubviewNib:(NSString *)nibName;
- (UIView *)test;
- (BCTransitionViewController *)baseViewController;

- (void)addNotificationObserver:(NSArray *)eventsToObserve;
- (void)cleanUpUnsavedObject:(BOOL)forceDelete;
/*

 +  Pass in the holding view that all layout objects should sit within
 +  Pass in the view objects, at least an empty NEW object
 +  Ensure the objects have their target/actions pre-set and all that jazz, this method is purely for layout
 +  Pass in something that can describe their relative positions
    
 
 Relative layout is built into objects array
 + if an object in the array contains an "objects" entity these will be it's child objects
 + otherwise, the "object" entity will be the item to place
 + positional description is as follows
    example: w:f-h:f-p:10  <-- Creates full width, full height object with 10 pixels of edge inset padding inside parent master view
    + w:f|int|strings(1/2, 1/3, 1/4, 1/5) - defaults to f, the string type allows you to set a percentage value for inline elements
    + h:f|int - defaults to 44
    + p:int|comma seperated set of 4 ints - defaults to 0
    + b:inline|stack - defaults to stack
        stack elements will have the next element placed below them.
        inline elements will be placed NEXT to the previous element, be sure to set your widths accordingly to make this acceptable
 
 
 view could be:
    - UIScrollview
    - UIView
 */
- (void)buildLayoutIntoView:(id)view withObjects:(NSArray *)objects;

- (void)animate:(NSString *)animate_id withType:(NSString *)type andValue:(float)value andDirection:(NSString *)direction;
- (void)animateLayout:(NSArray *)contraints withType:(NSString *)type andValue:(float)value;


- (UIView *)makeBackgroundView:(UIColor *)backgroundColor animate:(NSString *)animateStyle withCloseAction:(BOOL)hasCloseAction;
- (void)removeBackgroundView:(BOOL)animated;

- (void)closeMe:(id)sender;
- (void)fadeMe:(id)sender withStatusBarStyle:(UIStatusBarStyle)style;

- (IBAction)popCurrentViewController:(id)sender;

- (void)closeAndTransitionTo:(BCViewController *)destinationVC withType:(int)type buildNavController:(BOOL)buildNav;
- (void)closeAndTransitionTo:(BCViewController *)destinationVC withType:(int)type buildNavController:(BOOL)buildNav closeMe:(BOOL)shouldClose;

- (void)transitionTo:(BCViewController *)destinationVC withType:(int)type buildNavController:(BOOL)buildNav;

@end
