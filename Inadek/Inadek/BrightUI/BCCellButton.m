//
//  BCCellButton.m
//  Inadek
//
//  Created by Josh Lehman on 1/22/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCCellButton.h"

@implementation BCCellButton

-(void) setHighlighted:(BOOL)highlighted {
    
    if(highlighted) {
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.1];
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            self.backgroundColor = [UIColor clearColor];
        }];

    }
    [super setHighlighted:highlighted];
}

-(void) setSelected:(BOOL)selected {
    
    NSLog(@"Set selected");
    if(selected) {
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.1];
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            self.backgroundColor = [UIColor clearColor];
        }];
        
    }
    [super setSelected:selected];
}

- (void)highlightWhenPressed:(BOOL)highlight
{
    if (highlight) {
        highlightOnPress = YES;
    } else {
        highlightOnPress = NO;
    }
}

@end
