//
//  BCActionSheetView.h
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import <Masonry/Masonry.h>
#import <QuartzCore/QuartzCore.h>
#import "BCButton.h"

@class BCGroupedActionSheetView;

@interface BCActionSheetView : UIView {
    float neededHeight;
}

@property (nonatomic, retain) BCGroupedActionSheetView *parentView;

- (float)sheetHeight;
- (NSNumber *)sheetHeightAsNumber;

- (BCActionSheetView *)initWithOptions:(NSArray *)options title:(NSString *)title andParentView:(id)parentView;

@end
