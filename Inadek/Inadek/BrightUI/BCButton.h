//
//  BCButton.h
//  Inadek
//
//  Created by Josh Lehman on 1/16/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#define MAS_SHORTHAND

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <PixateFreestyle/PixateFreestyle.h>
#import "UIColor+BCColor.h"

@interface BCButton : UIButton {
    BOOL highlightOnPress;
    UIColor *baseColor;
}

@property (nonatomic, retain) NSDictionary *userInfo;
@property (nonatomic, retain) NSString *btnType;

- (void)buildType:(NSString *)type;
- (void)highlightWhenPressed:(BOOL)highlight;
- (void)setColor:(UIColor *)color;

@end
