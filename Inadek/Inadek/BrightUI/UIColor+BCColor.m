//
//  UIColor+BCColor.m
//  Inadek
//
//  Created by Josh Lehman on 1/29/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "UIColor+BCColor.h"

@implementation UIColor (BCColor)


- (UIColor *)lighterColor
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:MIN(b * 1.2, 1.0)
                               alpha:a];
    return nil;
}

- (UIColor *)darkerColor
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:b * 0.75
                               alpha:a];
    return nil;
}

@end
