//
//  BCNavController.m
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCNavController.h"
#import "UIBCNavButton.h"
#import "BCViewController.h"
#import "Common.h"
#import "AppDelegate.h"

@interface BCNavController ()

@end

@implementation BCNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIBarButtonItem *) getNavBarItem:(int)style withTitle:(NSString *)title andPosition:(int)position target:(id)target action:(SEL)action;
{
    UIBCNavButton *barButton = [[UIBCNavButton alloc] initAsNavBarButton:style withTitle:title];
    [barButton setAdjustsImageWhenHighlighted:NO];
    
    if (target != nil) {
        [barButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
        
        switch (position) {
            case kBCNavPositionLeft:
                if (style == kBCNavButtonStyleText || style == kBCNavButtonStyleTextSmall) {
                    barButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    [barButton setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
                }
                //[barButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
                break;
                
            case kBCNavPositionRight:
                if (style == kBCNavButtonStyleText || style == kBCNavButtonStyleTextSmall) {
                    barButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                    [barButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -10)];
                }
                //[barButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -25)];
                break;
                
            default:
                break;
        }
    }
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:barButton];
    
    return barButtonItem;
    
}

- (void) pushViewController:(BCViewController *)viewController animated:(BOOL)animated
{
    if (self.topViewController != nil) {
        viewController.navigationItem.leftBarButtonItem = [self getNavBarItem:kBCNavButtonStyleBack withTitle:@"" andPosition:kBCNavPositionLeft target:viewController action:@selector(popCurrentViewController:)];
    }
    
    [super pushViewController:viewController animated:animated];
}

- (UIStatusBarStyle)getStatusBarStyle
{
    if (statusBarStyle) {
        return statusBarStyle;
    } else {
        return UIStatusBarStyleDefault;
    }
}

- (void)cleanAndCloseMe:(id)sender {
    if ([(BCViewController *)[self topViewController] respondsToSelector:@selector(cleanUpUnsavedObject:)]) {
        [(BCViewController *)[self topViewController] cleanUpUnsavedObject:YES];
    }
    [self closeMe:sender];
}

- (void)closeMe:(id)sender
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.transitionVC closeNavController:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
