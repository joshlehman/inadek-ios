//
//  BCScrollView.h
//  GasCubby
//
//  Created by Josh Lehman on 5/15/15.
//  Copyright (c) 2015 Fuelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCScrollView : UIScrollView

@end
