//
//  BCLabel.m
//  Inadek
//
//  Created by Josh Lehman on 1/30/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCLabel.h"

@implementation BCLabel

- (void) layoutSubviews {
    [super layoutSubviews];
    self.preferredMaxLayoutWidth = self.bounds.size.width;
}

@end
