//
//  UIColor+BCColor.h
//  Inadek
//
//  Created by Josh Lehman on 1/29/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BCColor)

- (UIColor *)darkerColor;
- (UIColor *)lighterColor;

@end
