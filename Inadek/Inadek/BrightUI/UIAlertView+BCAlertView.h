//
//  UIAlertView+BCAlertView.h
//  Inadek
//
//  Created by Josh Lehman on 1/20/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (BCAlertView)

- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion;

@end
