//
//  UIProfileButton.h
//  Inadek
//
//  Created by Josh Lehman on 3/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIProfileButton : UIButton

@property (nonatomic, retain) NSDictionary *userInfo;

- (void)setActive;
- (void)setInactive;

@end
