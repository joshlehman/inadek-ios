//
//  UIBCNavButton.m
//  Inadek
//
//  Created by Josh Lehman on 1/14/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "UIBCNavButton.h"
#import "Common.h"

@implementation UIBCNavButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initAsNavBarButton:(int)style withTitle:(NSString *)title
{
    self = [super initWithFrame:CGRectMake(0, 0, 44, 52)];
    if (self) {
        switch (style) {
            case kBCNavButtonStyleBack:
                [self setImage:[UIImage imageNamed:@"Btn-Back"] forState:UIControlStateNormal];
                [self setTitleColor:[UIColor colorWithHexString:@"E85800"] forState:UIControlStateNormal];
                [self setTitle:@"" forState:UIControlStateNormal];
                
                [self.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:14.0f]];
                [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
                [self setContentEdgeInsets:UIEdgeInsetsMake(2, -15, 0, 0)];
                [self setImageEdgeInsets:UIEdgeInsetsMake(-2, 0, 0, 0)];
                [self setFrame:CGRectMake(0, 0, 20, 33)];
                break;
                
            case kBCNavButtonStyleText:
                [self setTitleColor:[UIColor colorWithHexString:@"E85800"] forState:UIControlStateNormal];
                [self setTitle:title forState:UIControlStateNormal];
                [self.titleLabel setFont:[UIFont fontWithName:@"OpenSans-ExtraBold" size:14.0f]];
                [self.titleLabel setTextAlignment:NSTextAlignmentRight];
                [self setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
                [self setFrame:CGRectMake(15, 40, 60, 44)];
                break;
                
            case kBCNavButtonStyleTextSmall:
                [self setTitleColor:[UIColor colorWithHexString:@"E85800"] forState:UIControlStateNormal];
                [self setTitle:title forState:UIControlStateNormal];
                [self.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:14.0f]];
                [self.titleLabel setTextAlignment:NSTextAlignmentRight];
                [self setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -15)];
                [self setFrame:CGRectMake(15, 40, 60, 44)];
                
            default:
                break;
        }
        
    }
    
    return self;
}


@end
