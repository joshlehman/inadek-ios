//
//  BCActionSheetView.m
//  Inadek
//
//  Created by Josh Lehman on 1/26/15.
//  Copyright (c) 2015 Inadek. All rights reserved.
//

#import "BCActionSheetView.h"
#import "BCScrollView.h"

#define kOptionTitleHeight  33
#define kOptionHeight       50
#define kSidePadding        10
#define kFloorPadding       10
#define kFadeInDuration     0.3

@implementation BCActionSheetView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (BCActionSheetView *)initWithOptions:(NSArray *)options title:(NSString *)title andParentView:(id)parentView
{
    self = [self init];
    if (self) {
        
        if (parentView) {
            self.parentView = parentView;
        }
        [self setBackgroundColor:[UIColor whiteColor]];
        
        neededHeight = 0;
        
        BCScrollView *sv = BCScrollView.new;
        [sv setBackgroundColor:[UIColor whiteColor]];
        sv.canCancelContentTouches = YES;
        
        [self addSubview:sv];
        [sv makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        
        UIView *holderView = UIView.new;
        [holderView setBackgroundColor:[UIColor clearColor]];
        [sv addSubview:holderView];
        
        [holderView makeConstraints:^(MASConstraintMaker *make) {
            make.edges.width.equalTo(sv);
        }];
        
        if (title) {
        
            UIView *oView = UIView.new;
            [oView setBackgroundColor:[UIColor clearColor]];
            
            UILabel *oViewText = UILabel.new;
            [oViewText setStyleClass:@"lbl-actionsheet-title"];
            [oViewText setTextAlignment:NSTextAlignmentCenter];
            [oViewText setText:title];
            
            [oView addSubview:oViewText];
            [oViewText makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(oView);
            }];
            
            UIView *bottomBorder = [[UIView alloc] init];
            [bottomBorder setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.12]];
            [oView addSubview:bottomBorder];
            [bottomBorder makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@1);
                make.bottom.equalTo(oView);
                make.width.equalTo(oView);
                make.left.equalTo(oView);
            }];
            
            [holderView addSubview:oView];
            [oView makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self);
                make.right.equalTo(self);
                make.height.equalTo([NSNumber numberWithFloat:kOptionTitleHeight]);
                make.top.equalTo(@0);
            }];
            
            neededHeight += kOptionTitleHeight;
        }
        
        UIView *priorView = nil;
        int count = 0;
        for (NSDictionary *option in options) {
            count++;
            
            UIView *oView = UIView.new;
            [oView setBackgroundColor:[UIColor clearColor]];
            
            BCButton *oViewText = BCButton.new;
            if ([option valueForKey:@"style"] && [[option valueForKey:@"style"] isEqualToString:@"cancel"]) {
                [oViewText setStyleClass:@"field-normal-bold-red"];
                [oViewText addTarget:self.parentView action:@selector(doDismissBackground:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                [oViewText setStyleClass:@"field-normal-bold"];
                [oViewText addTarget:self.parentView action:@selector(doSelectField:) forControlEvents:UIControlEventTouchUpInside];
            }
            [oViewText setTitle:[option valueForKey:@"name"] forState:UIControlStateNormal];
            if ([option valueForKey:@"id"]) {
                [oViewText setUserInfo:@{@"cid":[option valueForKey:@"id"]}];
            }
            [oView addSubview:oViewText];
            [oViewText makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(oView);
            }];
            
            if (count < [options count]) {
                UIView *bottomBorder = [[UIView alloc] init];
                [bottomBorder setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.12]];
                [oView addSubview:bottomBorder];
                [bottomBorder makeConstraints:^(MASConstraintMaker *make) {
                    make.height.equalTo(@1);
                    make.bottom.equalTo(oView);
                    make.width.equalTo(oView);
                    make.left.equalTo(oView);
                }];
            }
            
            [holderView addSubview:oView];
            [oView makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self);
                make.right.equalTo(self);
                make.height.equalTo([NSNumber numberWithFloat:kOptionHeight]);
                if (priorView) {
                    make.top.equalTo(priorView.bottom);
                } else {
                    if (title) {
                        make.top.equalTo([NSNumber numberWithInt:kOptionTitleHeight]);
                    } else {
                        make.top.equalTo(@0);
                    }
                }
                
            }];
            priorView = oView;
            neededHeight += kOptionHeight;
        }
        
        [holderView updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(priorView.bottom);
        }];
    }
    
    return self;
    // Look through array, adding rows and increasing neededHeight and adding target callbacks until we have a full view, then return it
}

- (float)sheetHeight
{
    return neededHeight;
}

- (NSNumber *)sheetHeightAsNumber
{
    return [NSNumber numberWithFloat:neededHeight];
}

@end
