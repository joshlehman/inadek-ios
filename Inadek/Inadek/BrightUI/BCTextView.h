//
//  BCTextView.h
//  Inadek
//
//  Created by Josh Lehman on 11/11/15.
//  Copyright © 2015 Inadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCTextView : UITextView


@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) NSAttributedString *attributedPlaceholder;

- (CGRect)placeholderRectForBounds:(CGRect)bounds;

@end
